% !TEX root = handbook-physics.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode






\chapter{Electric Field Strength And Displacement}
\label{ch-15}

\section{Electric Field. Field Strength}
\label{sec-15.1}

\subsection{}\label{15.1.1} A \emph{force field} is a form of matter that is studied in physics along with its study of other forms of matter known commonly as substances. The most basic feature of force fields is that various kinds of interaction are accomplished by means of them. A gravitational field \ssect{6.2.1}, for instance, accomplishes gravitational interaction between bodies of various masses located in the field. All force fields have vital properties that characterize their material nature. Primarily this concerns their energy. There are no impassible barriers between a field and material substances; one can be converted into the other \ssect{43.5.5}.

A physical field cannot be defined as space in which certain forces act. Space, like time, is a form of the existence of matter. The fact that a field exists in space by no means infers that the field can be identified with space, because the form of existence of matter cannot be confused with the matter itself.

\subsection{}\label{15.1.2} Coulomb interaction \ssect{14.2.2} between fixed electrically charged particles or bodies is accomplished through the \emph{electrostatic field} set up by the charges. An electrostatic field is a \emph{steady-state electric field}, i.e. one that does not change in the course of time, and is set up by fixed electric charges. This is one of the forms of an \emph{electromagnetic field} by means of which interaction is accomplished between electrically charged particles (or bodies) that travel, in the general case, in some arbitrary way with respect to the chosen frame of reference.

A typical property of an arbitrary electric field, distinguishing it from all other physical fields, is the action it exerts on both travelling and fixed electric charges (charged particles and bodies).

\subsection{}\label{15.1.3} The quantitative force characteristic of the action exert­ ed by an electric field on charged particles and bodies is the vector $\vb{E}$ of the \emph{electric field strength} (formerly called the electric intensity). The electric field strength at a given point is the ratio of the force $\vb{F}$, exerted by the field on a fixed point \ssect{14.2.3} \emph{electric test charge} placed at the point of the field being considered, to the magnitude $q_{0}$ of the charge:
\begin{equation*}%
\vb{E} = \frac{\vb{F}}{q_{0}}
\end{equation*}
It is assumed that the test charge $q_{0}$ is so small that its presence does not lead to a redistribution in space of the charges that set up the field being investigated. In other words, the lest charge does not distort the field.

An electric field is said to be \emph{uniform} (a \emph{uniform electric field}) if the electric field, vector $\vb{E}$ has a constant magnitude and direction at any point of the field.

\subsection{}\label{15.1.4} The strength of the electrostatic field of point charge $q$ at a point located at the distance $r$ from the charge is
\begin{align*}%
\vb{E} & = \frac{1}{\fpe} \frac{q}{\epsilon_{r} r^{3}} \,\vb{r} & \text{(in SI units)}\\
\vb{E} & = \frac{q}{\epsilon_{r} r^{3}}\, \vb{r} &\text{(in cgse units)}
\end{align*}
where $\vb{r}$ is the radius vector joining charge $q$ and the point at which the field strength is to be calculated. Vectors $\vb{E}$ are directed at all points of the field radially away from charge $q$ when $q > 0$, and directed radially toward the charge when $q < 0$.

The projection $E_{r}$ of the field strength on the direction of radius vector $\vb{r}$ is
\begin{align*}%
E_{r} & = \ofpe \frac{q}{\epsilon_{r} r^{2}} & \text{(in SI units)}\\
E_{r} & = \frac{q}{\epsilon_{r} r^{2}} &\text{(in cgse units)}
\end{align*}
These formulas are also used to calculate the strength of the field set up by a hollow sphere, having the charge $q$ on its surface, at the distance $r$ from the centre of the sphere whose radius equals $R$. Inside the hollow sphere $E_{r} = 0$.

\subsection{}\label{15.1.5} The force $\vb{F}$ exerted by an electric field on an arbitrary charge $q$ located at a given point in the field is
\begin{equation*}%
\vb{F} = q\,\vb{E}
\end{equation*}
where $\vb{E}$ is the field strength at the point where charge $q$ is located and is distorting the field, i.e. making it different from the field set up before introducing charge $q$.

\subsection{}\label{15.1.6} Electrostatic fields are represented graphically by employing \emph{lines of force}.

\emph{Lines of force} are imaginary curves drawn in such a way that their direction at any point (i.e. the direction of the tangent to the line at the point) coincides with that of the electric field vector. Lines of force are assumed to have the same direction as the corresponding electric field vectors. Lines of force do not intersect because at each point of the field vector $\vb{E}$  has only a single direction.

Lines of force are not identical with the paths of motion of light charged particles in an electrostatic field. Tangent at each point of the path of a particle is its velocity. Tangent to a line of force is the force acting on a charged particle. This, consequently, corresponds to the acceleration of the particle.

\section{Principle of Superposition of Electric Fields}
\label{sec-15.2}


\subsection{}\label{15.2.1} The basic problem of electrostatics can be formulated as follows: find the magnitude and direction of the electric field vector $\vb{E}$ for each point of a field from the given space distribution and magnitudes of the electric charges that are the source of the field.


\subsection{}\label{15.2.2} When the field is set up by a system of fixed charges $q_{1},\,q_{2},\, \ldots{} q_{n}$ the resultant force $\vb{F}$ exerted on a test charge $q_{0}$ \ssect{15.1.3} at any point of the field being considered is equal to the vector sum of the forces exerted on charge $q_{0}$ by the fields set up by each of the charges $q_{i}$:
\begin{equation*}%
\vb{F} = \sum_{i=1}^{n} \vb{F}_{i}
\end{equation*}
According to Sect. \ssect{15.1.3}, $\vb{F} = q_{0} \vb{E}$ and $\vb{F}_{i} = q_{0} \vb{E}_{i}$ where $\vb{E}$ is the strength of the resultant field, and $\vb{E}_{i}$ is the strength of the field set up by charge $q_{i}$. Following from the preceding formulas is the \redem{principle of independent action of electric fields}, or the \redem{principle of superposition of electric fields}:
\begin{equation*}%
\vb{E} = \sum_{i=1}^{n} \vb{E}_{i}
\end{equation*}
The strength of an electric field of a system of charges is equal to the vector sum of the strengths of the fields set up by each charge separately. The strength of the resultant field is found by superposition of the strengths of the fields of the separate charges.

For charges distributed continuously in space \ssect{15.2.3}, the principle of superposition of fields is of the form
\begin{equation*}%
\vb{E} = \int \dd \vb{E}
\end{equation*}
where integration extends over all the continuously distributed sources of the fields, setting up electric fields with the strength

\hlblue{Example.} A system of fixed point charges $q_{1},\,q_{2},\, \ldots{} q_{n}$ sets up an electrostatic field whose strength $\vb{E}$ equals
\begin{align*}%
\vb{E} & = \ofpe \sum_{i=1}^{n} \frac{q_{i}}{\epsilon_{r} r_{i}^{3}}\, \vb{r}_{i} & \text{(in SI units)}\\
\vb{E} & = \sum_{i=1}^{n}  \frac{q_{i}}{\epsilon_{r} r_{i}^{3}} &\text{(in cgse units)}
\end{align*}
where $\vb{r}_{i}$ is the radius vector from point charge $q_{i}$ to the point being investigated in the field. Any charged body on which the charge $Q$ is discretely distributed \ssect{15.2.3} can be divided into extremely small parts, each having a point charge. Therefore, the preceding formula is of general significance for calculating electrostatic fields in a homogeneous, isotropic medium that fills the whole field.

\subsection{}\label{15.2.3} Electric charges, the sources of electrostatic fields, can be distributed in space either \emph{discretely} (\emph{discrete charge distribution}) at various points in space, or \emph{continuously} (\emph{continuous charge distribution}). In the latter case, the charges are distributed either along a certain line, on the surface of some body or within a certain volume. The concept of charge density is introduced for continuous charge distribution. When the electric charge is distributed continuously along a line we have \emph{linear charge density} $\tau$:
\begin{equation*}%
\tau = \dv{q}{l}
\end{equation*}
where $\dd q$ is the charge of an infinitesimal clement of length $\dd l$. When the electric charge is distributed continuously over a certain surface, we speak of the \emph{surface charge density} $\sigma$:
\begin{equation*}%
\sigma = \dv{q}{S}
\end{equation*}
where $\dd q$ is the charge on an infinitesimal element of area $\dd S$. When the electric charge is distributed continuously throughout some volume, we speak of the volume charge density $\rho$:
\begin{equation*}%
\rho = \dv{q}{V}
\end{equation*}
where $\dd q$ is the charge in an infinitesimal element of volume $\dd V$.

\hlblue{Example.} The electrostatic field of an electric dipole. An \emph{electric dipole} is a system of two charges of equal magnitude, but of opposite sign. These charges, $q$ and $-q$ $(q > 0)$ are separated by the distance $l$ which is small in comparison with the distance to the points being considered in the field. The \emph{arm of the dipole} is the vector $\vb{l}$, directed along the dipole axis from the negative toward the positive charge and equal to the distance between them (\figr{fig-15-01}). The product of the charge $q$ of the dipole $(q > 0)$ hy the arm $\vb{l}$ is called the \emph{electric dipole moment} $\vb{p}_{e}$:
\begin{equation*}%
\vb{p}_{e} = q \, \vb{l}
\end{equation*}
The strength E of the field of an electric dipole at an arbitrary point is:
\begin{equation*}%
\vb{E} = \vb{E}_{+} + \vb{E}_{-}
\end{equation*}
where $\vb{E}_{+}$ and $\vb{E}_{-}$ are the strengths of the fields set up by the charges $q$ and $-q$ (\figr{fig-15-01}).
\begin{marginfigure}
\centering
\includegraphics[width=\textwidth]{figs/ch-15/fig-15-01.pdf}
\caption{Electric dipole moment.\label{fig-15-01}}
\end{marginfigure}
At point $A$, located on the dipole axis at the distance $r$ from its middle (\figr{fig-15-01}) $(r \gg l)$, the strength of the field of a dipole equals:
\begin{align*}%
\vb{E} & = \ofpe \frac{2 \vb{p}_{e}}{\epsilon_{r} r^{3}} & \text{(in SI units)}\\
\vb{E} & =   \frac{2 \vb{p}_{e}}{\epsilon_{r} r^{3}} &\text{(in cgse units)}
\end{align*}
At point $B$, located on a perpendicular erected at the middle of the dipole axis and at the distance $r$ from the middle of the axis $(r \gg l)$
\begin{align*}%
\vb{E} & = -\, \ofpe \frac{\vb{p}_{e}}{\epsilon_{r} r^{3}} & \text{(in SI units)}\\
\vb{E} & =  - \frac{\vb{p}_{e}}{\epsilon_{r} r^{3}} &\text{(in cgse units)}
\end{align*}
At the arbitrary point $O$ sufficiently distant from the dipole $(r \gg l)$ (\figr{fig-15-02}), the magnitude of its field strength is:
\begin{align*}%
\vb{E} & =  \ofpe \frac{{p}_{e}}{\epsilon_{r} r^{3}} \sqrt{3 \cos^{2} \theta + 1} & \text{(in SI units)}\\
\vb{E} & =  - \frac{{p}_{e}}{\epsilon_{r} r^{3}} \sqrt{3 \cos^{2} \theta + 1} & \text{(in cgse units)}
\end{align*}
\begin{marginfigure}
\centering
\includegraphics[width=\textwidth]{figs/ch-15/fig-15-02.pdf}
\caption{Electric field strength due to a dipole at an arbitrary point $O$.\label{fig-15-02}}
\end{marginfigure}
 
\section{Electric Displacement. Ostrogradsky-Gauss Electric Flux Theorem}
\label{sec-15.3}

\subsection{}\label{15.3.1} The electric field strength \ssect{15.1.3} depends upon the properties of the medium. In a homogeneous isotropic medium, the strength $\vb{E}$ is inversely proportional to $\epsilon_{r}$ (\ssect{4.2.5}. Another characteristic of an electric field, along with its strength $\vb{E}$, is the vector $\vb{D}$ of \redem{electric displacement}, or \redem{electric induction} (other names are dielectric displacement, dielectric flux density, displacement, electric displacement density and electric flux density). For a field in an electrically isotropic medium, the relation between $\vb{D}$ and $\vb{E}$ is of the form
\begin{align*}%
\vb{D} & =  \epsilon_{0}\epsilon_{r} \vb{E} & \text{(in SI units)}\\
\vb{D} & =  \epsilon_{r} \vb{E} & \text{(in cgse units)}
\end{align*}
The general relation for $\vb{D}$, valid for anisotropic media is given
in \ssect{18.3.4}.

\hlblue{Example.} For the field of point electric charge $q$ \ssect{14.2.3}
\begin{align*}%
\vb{D} & =  \frac{1}{4 \pi} \, \frac{q}{r^{3}} \, \vb{r} & \text{(in SI units)}\\
\vb{D} & =  \frac{q}{r^{3}} \, \vb{r} & \text{(in cgse units)}
\end{align*}
The projection of $\vb{D}$ on the direction of radius vector $\vb{r}$, from the point charge to the given point in the field, is
\begin{align*}%
\vb{D} & =  \frac{1}{4 \pi} \, \frac{q}{r^{2}} & \text{(in SI units)}\\
\vb{D} & =   \frac{q}{r^{2}} & \text{(in cgse units)}
\end{align*}

\subsection{}\label{15.3.2} An \redem{element} $\dd \Phi_{e}$ of \redem{electric displacement flux} through an element of surface with the area $\dd S$ is the scalar physical quantity defined by the equation
\begin{equation*}%
\dd \Phi_{e} = \vb{D}\, \vdot \dd \vb{S} = D \, \dd S \cos \alpha = D_{n}\, \dd S = D \, \dd S_{\perp}
\end{equation*}
where $\vb{D}$ is the displacement vector \ssect{15.3.1}, $\vu{n}$ is a unit vector normal to area $\dd S$, $\dd \vb{S} = \dd S \vu{n}$ is the vector of the element of surface $\dd S$, $D_{n} = D \cos \alpha$ is the projection of vector $\vb{D}$ on the direction of vector $\vu{n}$, $\dd S_{\perp} = \dd S \cos \alpha$ is the area of the projection of the element of surface $\dd S$ on a plane perpendicular to vector $\vb{D}$, and $\alpha$ is the angle between vectors $\vb{D}$ and $\vu{n}$ (\figr{fig-15-03})

\begin{marginfigure}[-4cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-15/fig-15-03.pdf}
\caption{Electric displacement flux.\label{fig-15-03}}
\end{marginfigure}

When the electric field is set up by point charge $q$, the element of displacement flux $\dd \Phi_{e}$ through the element of surface $\dd S$ of the closed surface $S$, within which the point charge is located, is
\begin{equation*}%
\dd \Phi_{e} = \frac{q}{4 \pi} \, \dd \omega
\end{equation*}
where $\dd \omega$ is the solid angle subtended at point charge $q$ by the element $\dd S$ of surface $S$ (\figr{fig-15-03}).

The total \redem{displacement flux} $\Phi_{e}$ through the surface $S$ is the summation or integration of all the elements of flux:
\begin{align*}%
\Phi_{e} & = \int_{S} \vb{D} \, \dd \vb{S} = \int_{S} {D} \, \dd {S} \cos \alpha \\
& = \int_{S} {D}_{n} \, \dd {S} = \int_{S} {D} \, \dd {S}_{\perp}
\end{align*}
where $\alpha $ is the angle between vectors $\vb{D}$ and $\vu{n}$. Here all the vec­tors and normals to the elements $\dd S$ of surface are directed toward the same side of surface $S$. In the case of a closed surface $S$ (\figr{fig-15-03}), all the vectors n of the normals must be either outward or inward ones.\sidenote{Only outward normals arc used in the following.}

\subsection{}\label{15.3.3} The \redem{Ostrogradsky-Gauss theorem} states that the displacement flux passing through an arbitrary closed surface is equal to or proportional to the algebraic sum of the electric charges $q_{1},\,q_{2},\, \ldots{}, q_{k}$ enclosed within the surface:
\begin{align*}%
\oint_{S} \vb{D} \, \dd \vb{S} & = \oint_{S} {D}_{n} \, \dd {S}  = \sum_{i=1}^{k} q_{i} & \text{(in SI units)}\\
\oint_{S} \vb{D} \, \dd \vb{S} & = \oint_{S} {D}_{n} \, \dd {S}  = 4 \pi \sum_{i=1}^{k} q_{i} & \text{(in cgse units)}
\end{align*}
The displacement flux passing through an arbitrary closed sur­face that does not enclose any electric charges equals zero. The differential form of the Ostrogradsky-Gauss theorem is one of Maxwell’s equations for an electromagnetic field \ssect{27.4.2.}

\subsection{}\label{15.3.4} The Ostrogradsky-Gauss theorem for the electrostatic field in vacuum states that vector flux of electrostatic field strength in vacuum passing through an arbitrary closed surface is proportional to the algebraic sum of the electric charges enclosed within the surface:
\begin{align*}%
\oint_{S} \vb{E} \, \dd \vb{S} & = \frac{1}{\epsilon_{0}} \sum_{i=1}^{k} q_{i} & \text{(in SI units)}\\
\oint_{S} \vb{E} \, \dd \vb{S} & = 4 \pi \sum_{i=1}^{k} q_{i}& \text{(in cgse units)}
\end{align*}
where $\epsilon_{0}$ is the electric constant \ssect{14.2.7}.

The Ostrogradsky-Gauss theorem for the field in a dielectric is discussed in \ssect{18.3.3}.

\subsection{}\label{5.3.5} Together with the principle of superposition of fields \ssect{15.2.2}, the Ostrogradsky-Gauss theorem is applied to calculate electric field vectors $\vb{D}$. This is done by choosing a closed surface in such a way that in the expression for the displacement flux, the quantity $\vb{D}$ can be placed outside the sign of the surface integral. This can be done for fields set up by the simplest symmetrically arranged charges (charged lines, plane, sphere, etc.).