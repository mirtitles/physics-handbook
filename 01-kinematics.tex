% !TEX root = handbook-physics.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter{Kinematics}
\label{ch-01}

\section{Mechanical Motion. The Subject Matter of Mechanics}
\label{sec-1.1}
\subsection{}\label{1.1.1} The simplest form of motion in nature is \redem{mechanical motion}. It consists of changes in the relative positions of bodies or their parts in space in the course of time. The branch of phys­ics dealing with the laws of mechanical motion is called \redem{me­chanics}. In a more narrow sense of the word, this is often under­ stood to mean \redem{classical mechanics}, which deals with the motion of macroscopic bodies travelling at velocities a great many times less than that of light in free space. Classical mechanics is based on Newton’s laws and is therefore frequently referred to as \redem{Newtonian mechanics}. The laws of motion of bodies at velocities close to that of light in free space are taken up in \redem{relativistic mechanics} \ssect{5.1.1}, and the laws of motion of micro­ particles (for example, the electrons in atoms, molecules, crystals, etc.) are the subject matter of \redem{quantum mechanics} \ssect{38.1.1}. 

\subsection{}\label{1.1.2}Classical mechanics comprises three basic branches: sta­tics, kinematics and dynamics. Statics deals with the laws for the composition of forces and the conditions for the equilibrium of bodies. \redem{Kinematics} provides a mathematical description of all possible kinds of mechanical motion, regardless of what causes each particular kind of motion. \redem{Dynamics} investigates the effect of the interaction between bodies on their mechanical motion. 

\subsection{}\label{1.1.3}The mechanical properties of bodies are determined by their chemical nature, internal structure and state. These are beyond the scope of mechanics and are studied in other branches of physics. Hence, in accordance with the conditions of each specific problem on hand, various simplified models are employed to describe real bodies in motion. These include the particle, the perfectly rigid body, the perfectly elastic body, the perfectly inelastic body and others.

A \redem{material point}, or \redem{particle}, is a body whose shape and size are of no consequence under the conditions of the problem being considered. For example, in a first approximation, the motion of a ship from one point to another can be dealt with as the motion of a particle. If, however, it becomes necessary to take into account such details of this motion as the pitching and rolling of the ship by swells or heavy seas, the ship should be dealt with as an extended body of definite shape. In the liter­ature the word particle is more frequently used than mate­rial point.

Any extended body or system of such bodies, making up the \redem{mechanical system} being investigated, can be regarded as a \redem{system of particles}. For this purpose, ail the bodies of the system can be conceived of as being broken down into such a large number of parts that the size of each part is negligibly small compared to the sizes of the bodies themselves.

\subsection{}\label{1.1.4} A \redem{perfectly rigid body}, or simply \redem{rigid body}, is one whose deformation can be neglected under the conditions of the given problem. The distance between any two points of a rigid body remains constant no matter what action the body is subjected to. A perfectly rigid body can be regarded as a system of par­ticles rigidly connected to one another.

A \redem{perfectly elastic body} is one whose deformation obeys Hooke’s law \ssect{40.3.4}. After the external forces applied to such a body have been removed, its initial size and shape are completely restored.

A \redem{perfectly inelastic body} is one that completely retains its deformed state, caused by external forces applied to the body, when these forces have been removed.

\section[Frames of Reference. Path, Path Length]{Frames of Reference. Path, Path Length and Displacement Vector of a Particle}
\label{sec-1.2}

\subsection{}\label{1.2.1} The position of a body in space can be specified only in reference to other bodies. It makes sense, for instance, to speak of the location of the planets with respect to the sun, that of an air liner or steamship with respect to the earth, but we cannot indicate their position in space ``in general'' without ref­erence to some specific body. A perfectly rigid body in which a coordinate system is rigidly fixed, furnished with a ``clock'' and employed to determine the position of bodies and particles being investigated in space at various instants of time, is called a frame of reference. Sometimes the frame of reference is the coordinate system itself if it is provided with a ``clock'' and the solid in which the system is rigidly fixed is called the body of reference. In each specific problem the frame of refer­ence is selected so as to simplify the problem to the maximum extent. Inertial frames of reference \ssect{2.1.2} are the most com­monly used in physics.

\subsection{}\label{1.2.2} Most extensively employed is the Cartesian rectangular system of coordinates (\figr{fig-01-01}) whose orthonormal (normal orthogonal) basis is formed by the three vectors $\vb{i}$, $\vb{j}$ and $\vb{k}$, which  are unit vectors with respect to their magnitudes, mutually orthogonal and pass through the origin $O$ of coordinates. The position of an arbitrary point $M$ is specified by the radius vector $\vb{r}$, which connects the ori­gin $O$ of coordinates to point $M$. Vector $\vb{r}$ can be resolved along the axes $\vb{i}$, $\vb{j}$ and $\vb{k}$ of the basis as follows:
\begin{equation*}%
\vb{r}= x \vb{i} + y \vb{j} + z \vb{k}
\end{equation*}
where $x\vb{i}$, $y\vb{j}$ and $z\vb{k}$ are the com­ponents of vector $\vb{r}$ along the coordinate axes. The coefficients of resolution, $x$, $y$ and $z$, are the Cartesian coordinates of point $M$ and, due to the orthogo­nality of the vectors of the basis, the projections of radius vector $\vb{r}$ on the corresponding coordinate axes.

\begin{marginfigure}[-1cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-01/fig-01-01.pdf}
\caption{Cartesian system of coordinates.
\label{fig-01-01}}
\end{marginfigure}

The motion of a particle is completely specified if three continuous and single-valued functions of the time $t$,
\begin{equation*}%
x= x(t), \,\,y= y(t)\,\,\textrm{and}\,\, z= z(t),
\end{equation*}
are given to describe the changes in the coordinates of the particle with time.


These equations are said to be the \redem{kinematic equations of motion of a particle}. They are equivalent to the single vector equation
$\vb{r} = \vb{r} (t)$, for a moving particle.

\subsection{}\label{1.2.3} The line described by a moving particle in space is called its \redem{path}. The kinematic equations of motion of a particle spec­ify the equation of its path in parametric form (the parameter being the time $t$). Depending upon the shape of the path, distinction is made between \redem{rectilinear} and \redem{curvilinear} motion. The motion of a particle is said to be \redem{plane} if its path lies com­pletely in a single plane.

The mechanical motion of a body is relative, i.e. the nature of the motion and, in particular, the kind of path travelled by the points of the body depend upon the chosen frame of re­ference.

\subsection{}\label{1.2.4} In the general case, the path of a moving particle is a spatial, rather than plane, curve. Introduced for such curves is the concept of an \redem{osculating plane}. The osculating plane at the arbitrary point $M$ of a curve is in the limiting position of a plane passing through any three points of the curve when these points approach point $M$ without limit.

An \redem{osculating circle} at point $M$ of a curve is the limit of a circle passing through three points of the curve being considered when these points approach po­int $M$ without limit. The oscu­lating circle lies in the oscula­ting plane. The centre of the osculating circle and its radius are called the \redem{centre of curvature} and \redem{radius of curvature}, at point $M$, of the curve being considered. The straight line connecting point $M$ to the centre of curvature is called the \redem{principal normal} to the curve at point $M$. The tan­gent to the curve at point $M$ is perpendicular to the principal normal and also lies in the osculating plane.

\subsection{}\label{1.2.5} The \redem{path length} is the sum of the lengths of all the por­tions of the path over which the particle passes m the given interval of time. The instant of time $t = t_{0}$, from which we begin to consider the motion of a particle, is called the initial instant of time, and the position of the particle at this instant (point $A$ in \figr{fig-01-02}) is called its initial position. Owing to the arbitrary nature of the time reference point it is usually as­sumed that $t_{0}= 0$. 
\begin{figure}[!ht]
%\begin{wrapfigure}{l}[10pt]{0.55\linewidth}
\centering
%\includegraphics{figs/ch-01/fig-01-01.pdf}
\input{figs/ch-01/fig-01-02}
%  \vspace{-13pt}
\caption{A particle moving on a curve.}
\label{fig-01-02}
%\end{wrapfigure}
\end{figure}

The path length $s$, travelled by the particle from its initial position, is a scalar function of time: $s = s (t)$. As is obvious from its definition, the path length of a particle cannot be a negative quantity. If the particle travels along an arc of the path $AB$ (\figr{fig-01-02}), always in the same direction and is at point $M$ at the instant of time $t$,then $s(t)= \wideparen{AM}$. If the particle travels along the path in a more complicated way, for example if it travels from $A$ to $B$, reaching $B$ at the instant of time $t_{1}< t$, and then reverses, returning to point $M$ at instant $t$, then $s (t) = \wideparen{AB} + \wideparen{BM}$.


\subsection{}\label{1.2.6} The displacement vector of a moving particle during the time interval from $t= t_{1}$ to $t= t_{2}$ is the vector from the, position of the particle at the instant of time $t_{1}$ to its position at instant $t_{2}$. It is equal to the increment in the radius vector of the particle during the time interval being considered:
\begin{equation*}%
\vb{r_{2}} - \vb{r_{1}} = \vb{r}(t_{2}) - \vb{r}t_{1}
\end{equation*}
The displacement vector is always directed along the chord subtending the corresponding portion of the path.

Shown in \figr{fig-01-02} is the displacement vector of the particle for the time interval from $t_{0}$ to $t$. It is equal to $\vb{r}- \vb{r_{0}} = \vb{r} (t)- \vb{r}(t_{0})$.

The displacement vector of a moving particle for the time inter­val from $t$ to $t + \Delta t$ equals
\begin{equation*}%
\Delta r = r (t + \Delta t) - r (t) = \Delta x \times \vb{i} + \Delta y \times \vb{j} + \Delta z \times \vb{k}
\end{equation*}
where $\Delta x$, $\Delta y$ and $\Delta z$ are the increments (changes) in the coordi­nates of the particle during the time interval being considered. 

\subsection{}\label{1.2.7} A particle travelling freely in space can accomplish only three \redem{independent motions}, i.e. motions that cannot be repre­sented as combinations of the other two. As a matter of fact, the motion of a particle along each of the axes of a Cartesian rectangular coordinate system cannot be obtained by its motion along the other two axes. The number of independent motions that a mechanical system can accomplish is called the \redem{number of degrees of freedom} of the system. Hence, a free particle has three degrees of freedom.

\section{Velocity}
\label{sec-1.3}

\subsection{}\label{1.3.1} The concept of velocity is introduced in mechanics to characterize the rate of motion of bodies. It is the time rate of change in the position of moving bodies. The \redem{average velocity} of a moving particle in the time interval from $t$ to $t + \Delta t$ is the vector $\vb{v_{av}}$, equal to the ratio of the increment $\Delta \vb{r}$ in the radius vector of the particle during this time interval to the length of the time interval $\Delta t$:
\begin{equation*}%
\vb{v_{av}} = \frac{\Delta \vb{r}}{\Delta t}
\end{equation*}
Vector $\vb{v_{av}}$ has the same direction as $\Delta \vb{r}$, i.e. along the chord subtending the corresponding portion of the particle’s path. 

\subsection{}\label{1.3.2} The \redem{velocity} (or \redem{instantaneous velocity}) of a moving particle is the vector quantity $\vb{v}$, equal to the first time derivative of the radius vector $\vb{r}$ of the particle being considered:
\begin{equation*}%
\vb{v} = \dv{\vb{r}}{t}
\end{equation*}
The velocity of the particle at the instant of time $t$ is the lim­iting value that the average velocity $\vb{v_{av}}$ approaches for an infinitely small value of the time interval $\Delta t$. Thus
\begin{equation*}%
\vb{v} = \lim_{\Delta t \to 0}\frac{\Delta \vb{r}}{\Delta t} = \lim_{\Delta t \to 0}\vb{v_{av}}
\end{equation*}
The velocity vector $\vb{v}$ of the particle has the direction along a tangent to the path toward the motion in the same way as the vector $d\vb{r} = \vb{v} dt$ of infinitely small displacement of the particle during an infinitely short time interval $dt$.

The path length $ds$ travelled by the particle during the length of time $dt$ is equal to the magnitude of the displacement vector: $ds = \abs{d\vb{r}}$. Therefore the magnitude of the velocity vector of a moving particle is equal to the first time derivative of the path length:
\begin{equation*}%
v = \abs{\,\vb{v}\,} = \dv{s}{t}
\end{equation*}

\subsection{}\label{1.5.3} The resolution of vector $\vb{v}$ along the basis of a Cartesian rectangular coordinate system is of the following form:
\begin{equation*}%
\vb{v} = v_{x} \times \vb{i} + v_{y} \times \vb{j} +v_{z} \times \vb{k}
\end{equation*}
The projections of the velocity of a moving particle on the coor­dinate axes are equal to the first time derivative of the corres­ponding coordinates of the particle:
\begin{equation*}%
v_{x}= \dv{x}{t}, \,\, v_{y}= \dv{y}{t}, \,\, v_{z}= \dv{z}{t}
\end{equation*}
and the magnitude of the velocity vector, sometimes called the \redem{speed}, is
\begin{equation*}%
v = \abs{\,\vb{v}\,} = \sqrt{\left( \dv{x}{t} \right)^{2} + \left(\dv{y}{t}\right)^{2} + \left(\dv{x}{t}\right)^{2}}
\end{equation*}

\subsection{}\label{1.3.4} In rectilinear motion of a particle, the direction of its velocity vector remains constant. The motion of a particle is said to be \redem{uniform} if the magnitude of its velocity does not change with time: $v = ds/dt = \textrm{const}$. The path length $s$ tra­velled by a uniformly moving particle is a linear function of time: $s = vt$ (provided that $t_{0}= 0$, see \ssect{1.2.5}).

If the magnitude of the particle’s velocity, i.e. its speed, increa­ses with time $(dv/dt > 0)$, the motion is said to be \redem{accelerated}; if the speed decreases with time $(dv/dt < 0)$, the motion is said to be \redem{decelerated}.

\subsection{}\label{1.3.5} The \redem{average velocity of nonuniform motion} of a particle over a given portion of its path is the scalar $v_{\textrm{av}}$, called the \redem{travelling speed}, which is equal to the ratio of the length $\Delta s$ of this portion to the length $\Delta t$ of the time interval required to travel over this portion:
\begin{equation*}%
v_{\textrm{av}} = \frac{\Delta s}{\Delta t}.
\end{equation*}
It is equal to the speed of uniform motion in which the time required to travel the same path length As is the same as in the nonuniform motion being considered.

In curvilinear motion of a particle $\abs{\, \Delta \vb{r} \, } < \Delta s$. Therefore, in the general case, the average velocity (travelling speed) $v_{\textrm{av}}$ of the particle is not equal to the magnitude of its average velocity $\vb{v_{av}}$ over the same portion of its path \ssect{1.3.1}: $v_{\textrm{av}} \geqslant \abs{\,\vb{v}\,}$ where the equality sign corresponds to a rectilinear portion of the path.

\subsection{}\label{1.3.6} In the case of plane motion of a particle $M$ \ssect{1.2.3}, it often proves convenient to use the polar coordinates $r$ and $\varphi$, where $r$ is the distance from pole $O$ to particle $M$ and $\varphi$ is the vec­torial angle measured from polar axis $OA$ (\figr{fig-01-03}).
%\begin{wrapfigure}{l}[0pt]{0.5\linewidth}
\begin{figure}[!ht]
\centering
%\includegraphics[width=0.4\textwidth]{figs/ch-01/fig-01-03.pdf}
\input{figs/ch-01/fig-01-03}
%  \vspace{-13pt}
\caption{Polar coordinates of a moving particle.}
\label{fig-01-03}
\end{figure}
%\end{wrapfigure}
The velocity $\vb{v}$ of particle $M$ can be resolved into two mutually perpendicular components: the \redem{radial velocity} $\vb{v}_{r}$ and the \redem{transverse velocity} $\vb{v}_{\varphi}$:
\begin{equation*}%
\vb{v} = \vb{v}_{r} + \vb{v}_{\varphi},
\end{equation*}
in which
\begin{equation*}%
\vb{v}_{r} = \frac{1}{r} \dv{r}{t} \vb{r},\,\,\textrm{and}\,\,\vb{v}_{\varphi} =  \dv{\varphi}{t} [\vb{kr}],
\end{equation*}
where $\vb{r}$ is the polar radius vector of particle $M$, and $\vb{k}$ is the unit vector directed perpendicular to the plane of motion of the particle so that the rotation of radius vector $\vb{r}$ upon an increase in angle $\varphi$ is seen, from the head of unit vector $\vb{k}$, to be counterclockwise.

The magnitude $v$ of the velocity vector of particle $M$, in plane motion, is
\begin{equation*}%
v = \sqrt{\left(\dv{r}{t} \right)^{2} + \left( r \dv{\varphi}{t} \right)^{2} }.
\end{equation*}
During the infinitely short length of time $dt$, polar radius vector $\vb{r}$ of the particle in plane motion sweeps over a sector of a circle, the area of the sector being $dS = r^{2} \,d\varphi/2$. Hence, the quantity
\begin{equation*}%
\sigma = \dv{S}{t} = \frac{1}{2}r^{2}\dv{\varphi}{t}=\frac{1}{2} r v_{\varphi},
\end{equation*}
is called the \redem{areal} velocity.

\section{Acceleration}
\label{sec-1.4}


\subsection{}\label{1.4.1} The concept of acceleration is introduced in mechanics to characterize the rate of change of the velocity vector of a mov­ ing particle. The \redem{average acceleration} in the time interval from $t$ to $t + \Delta t$ is 	the vector $\vb{a_{\textrm{av}}}$, which is equal to the ratio of the increment $\Delta \vb{v}$ of the velocity vector of the particle, during the lime interval, to the length of the time interval $\Delta t$:
\begin{equation*}%
\vb{v_{\textrm{av}}} = \frac{\Delta \vb{v}}{\Delta t}
\end{equation*}

\subsection{}\label{1.4.2} \redem{Acceleration} (or \redem{instantaneous acceleration}) of a moving particle is the vector quantity $\vb{a}$, equal to the first time deriv­ative of the velocity $\vb{v}$ of the particle being considered or, other­ wise, the second time derivative of the particle’s radius vector $\vb{r}$:
\begin{equation*}%
\vb{a} = \dv{\vb{v}}{t} = \dv[2]{\vb{r}}{t}.
\end{equation*}
The acceleration of a moving particle at the instant of time $t$ is the limiting value that the average acceleration $\vb{a_{\textrm{av}}}$ approaches for an infinitely small time interval $\Delta t$. Thus
\begin{equation*}%
\vb{a} = \lim_{\Delta t \to 0}\frac{\Delta \vb{v}}{\Delta t} = \lim_{\Delta t \to 0} \vb{a_{\textrm{av}}}.
\end{equation*}

\subsection{}\label{1.4.3} The resolution of vector $\vb{a}$ along the basis of a rectangular Cartesian coordinate system is of the form:
\begin{equation*}%
\vb{a} = a_{x} \vb{i} + a_{y} \vb{j} + a_{z} \vb{k}.
\end{equation*}
The projections of the acceleration vector of a moving particle on the coordinate axes are equal to the first time derivatives of the corresponding velocity projections or, otherwise, the second time derivatives of the corresponding coordinates of the par­ticle:
\begin{equation*}%
a_{x} = \dv{v_{x}}{t} = \dv[2]{x}{t}, \,\,a_{y} = \dv{v_{y}}{t} = \dv[2]{y}{t}, \,\,a_{z} = \dv{v_{z}}{t} = \dv[2]{z}{t}.
\end{equation*}
The magnitude of the acceleration vector is
\begin{align*}%
a = \abs{\,a\,} & = \sqrt{\left( \dv{v_{x}}{t}\right)^{2}+\left( \dv{v_{y}}{t}\right)^{2}
+\left( \dv{v_{z}}{t}\right)^{2}} \\
& = \sqrt{\left( \dv[2]{x}{t}\right)^{2}+\left( \dv[2]{y}{t}\right)^{2}
+\left( \dv[2]{z}{t}\right)^{2}} .
\end{align*}


\subsection{}\label{1.4.4} The acceleration vector of a moving particle lies in the osculating plane \ssect{1.2.4} passing through point $M$ of the path being considered and is direct­ ed toward the concavity of the path $BC$ (\figr{fig-01-04}). In this plane the acceleration vector $\vb{a}$ can be resolved into two mutually perpendicular components $\vb{a}_{\tau}$ and $\vb{a}_{n}$:
\begin{equation*}%
\vb{a} = \vb{a}_{\tau} + \vb{a}_{n}.
\end{equation*}

%\begin{wrapfigure}{l}[0pt]{0.5\linewidth}
\begin{figure}[!ht]
\centering
%\includegraphics[width=0.4\textwidth]{figs/ch-01/fig-01-04.pdf}
\input{figs/ch-01/fig-01-04}
%  \vspace{-13pt}
\caption{Tangential and normal components of the acceleration of a moving particle.}
\label{fig-01-04}
\end{figure}
%\end{wrapfigure}

\subsection{}\label{1.4.5} Component $\vb{a}_{\tau}$ is called the \redem{tangential acceleration} of the moving particle. It is directed along the tangent to the path and equals 
\begin{equation*}%
\vb{a}_{\tau} = \dv{v}{t} \vb{\tau}  \qand  \vb{a}_{t} = \dv{v}{t},
\end{equation*}
where $\vb{\tau} = \vb{v}/v$ is the unit vector of the tangent, from point $M$ of the path in the direction of velocity $\vb{v}$ of the particle, and $a_{\tau}$ is the projection of the tangential acceleration on the direction of vector $\vb{v}$. The tangential acceleration characterizes the rate of change of the magnitude of the particle’s velocity vector. Vectors $\vb{a}_{\tau}$ and $\vb{v}$ coincide in direction, i.e. $a_{\tau} > 0$, in accelerat­ed motion \ssect{1.3.4}; vectors $\vb{a}_{\tau}$ and $\vb{v}$ are opposite in direction $a_{\tau} < 0$, in decelerated motion of the particle; and $a_{\tau} = 0$ in its uniform motion. If $a_{\tau} = \textrm{const} \neq 0$, the motion is said to be uniformly variable. In uniformly variable motion the magni­tude of the particle’s velocity is linearly time-dependent:
\begin{equation*}%
v = v_{0} + a_{\tau}t,
\end{equation*}
where $v_{0} = v (0)$ is the magnitude of the initial velocity, i.e. the velocity at the initial instant of time $t = 0$. If $a_{\tau} = \textrm{const} > 0$, the motion of the particle is said to be \redem{uniformly accelerated}, whereas if $a_{\tau} = \textrm{const} < 0$, it is said to be \redem{uniformly de­celerated}.

\subsection{}\label{1.4.6} Component $\vb{a}_{n}$ of acceleration $\vb{a}$ of the moving particle is called its \redem{normal acceleration}. It is directed along the principal normal to the path at point $M$ being considered, toward the centre of curvature \ssect{1.2.4} of the path. Therefore, an is often called the \redem{centripetal acceleration} of the particle. The normal acceleration equals
\begin{equation*}%
\vb{a}_{n} = \frac{v^{2}}{R} \vb{n},
\end{equation*}
where $\vb{n}$ is the unit vector of the principal normal, and $R$ is the radius of curvature of the path. The normal acceleration characterizes the rate of change in direction of the particle’s velocity vector.

If the particle travels in a straight line the normal acceleration $\vb{a}_{n} = 0$ and the acceleration is equal to the tangential accele­ration of the particle: $\vb{a} = \vb{a}_{\tau}$.


\section[Translational and Rotary Motion of a Rigid Body]{Translational and Rotary Motion of a Rigid Body\protect\footnote{Only perfectly rigid bodies are dealt with in this section; for the sake of brevity, they have been called simply ``rigid bodies''.}}
\label{sec-01.05}

\subsection{}\label{1.5.1} \redem{Translation} is the kind of motion of a rigid body in which any straight line rigidly fixed in the body (for instance, line $AB$ in \figr{fig-01-05}), remains paral­lel to its initial position ($A_{0}B_{0}$). With respect to the earth, a lift cabin (elevator car), lathe tool, and many other items have translational motion.
%\begin{wrapfigure}{l}[0pt]{0.5\linewidth}
\begin{figure}[!ht]
\centering
%\includegraphics[width=0.6\textwidth]{figs/ch-01/fig-01-05.pdf}
\input{figs/ch-01/fig-01-05}
%  \vspace{-13pt}
\caption{Translational motion of a rigid body.}
\label{fig-01-05}
\end{figure}
%\end{wrapfigure}
In the translational motion of a rigid rigid body all of its points are displaced in an identical way.


During the infinitely short time $\dd t$ the radius vectors of these
points are changed by the same amount $\dd r$. Hence the velocity
of all points of the body is the same in each instant of time and is equal to $\dd\vb{r}/\dd t$ and consequently, their accelerating is also the same. For this reason, a kinematic discussion of the translational motion of a rigid body reduces to the investigation of the motion of any one of its points, or particles. Dynamics usually deals with the motion of the centre of mass of the body \ssect{2.3.3}. A rigid body travel­ ling freely in space has three translational degrees of freedom \ssect{1.2.7}, corresponding to its translational motions along the three coordinate axes.

\subsection{}\label{1.5.2} The motion of a rigid body in which two of its points, $A$ and $B$, remain fixed is called \redem{rotation (or rotary motion) of the body about a fixed axis}. The fixed straight line $AB$ is the axis of rotation. In the rotation of a rigid body about a fixed axis all the points of the body describe circles whose centres lie on the axis of rotation and whose planes are perpendicular to this axis. The rotors of turbines, motors and generators, for exam­ple, have rotary motion with respect to the earth if these ma­chines are rigidly mounted.

A rigid body rotating about a fixed axis has only one degree of freedom \ssect{1.2.7}. Its position in space can be completely defined by specifying the angle of rotation (position angle) $\varphi$ from some definite (initial) position.


\subsection{}\label{1.5.3} The angular velocity serves to characterize the rate and direction of rotation of a body about an axis. The \redem{angular veloc­ity} is the vector $\pmb{\omega}$, which is equal in magnitude to the first derivative of the angle of rotation $\varphi$ with respect to time $t$ and is directed along the fixed axis of rotation in such a way that from its head the rotation of the body is counterclockwise (Fig. 1.6)\footnote{The direction of vector $\pmb{\omega}$ can also be determined by the \redem{right-hand screw rule}. It coincides with the direction of transla­ tional motion of a right-hand screw rotating together with the body. Vectors like $\pmb{\omega}$, whose direction is related to the direction of rotation and is reversed in transforming from a right-handed to a left-handed system of coordinates, are called \redem{pseudovectors}, or \redem{axial vectors} (in distinction to ordinary, \redem{polar vectors}, which are not reversed in the above-mentioned transformation of coor­ dinates). For example, the vector product of two polar vectors is a pseudovector, and that of a pseudovector and a polar vector is a polar vector.}:
\begin{equation*}%
\pmb{\omega} = \dv{\pmb{\varphi}}{t}\,\,\textrm{and}\,\, \omega = \dv{\varphi}{t}.
\end{equation*}
Here $\dd\pmb{\varphi}$ is the vector of an element of rotation of the body dur­ing the time $dt$. This vector is directed along the axis of rota­tion, also according to the right-hand screw rule (\figr{fig-01-06}). Axial vectors $\dd\pmb{\varphi}$ and $\pmb{\omega}$ have no definite points of application: they can be laid off from any point on the axis of rotation. In \figr{fig-01-06} they are laid off from a certain point $O$ of the fixed axis of rotation. This point is also taken as the origin of the reference frame.
%\begin{wrapfigure}{l}[0pt]{0.5\linewidth}
\begin{figure}[!ht]
\centering
%\includegraphics[width=0.3\textwidth]{figs/ch-01/fig-01-06.pdf}
\input{figs/ch-01/fig-01-06}
%  \vspace{-13pt}
\caption{Angular velocity and right-hand rule.}
\label{fig-01-06}
\end{figure}
%\end{wrapfigure}


Rotation of a body is said to be \redem{uniform} if the magnitude of its angular velocity remains constant with time $\omega = \textrm{const}$. In this case the angle of rotation of the body is linearly dependent on time: $\varphi = \omega t$.

\subsection{}\label{1.5.4} An arbitrary point $M$ of a rigid body rotating about fixed axis $OZ$ with the angular velocity $\pmb{\omega}$ describes a circle of radius $\rho$ with its centre at point $O'$ (\figr{fig-01-07}). As distinct from the angular velocity of the body, the velocity $\vb{v}$ of point M is often called the \redem{linear velocity}. It is perpendicular both to the axis of rotation (i.e. to vector $\pmb{\omega}$) and to radius vector $\pmb{\rho}$, from the centre $O'$ of the circle to point $M$, and is equal to their vector product:
\begin{equation*}%
\vb{v} = [\pmb{\omega \rho}]=[\pmb{\omega}\vb{r}]\,\,\textrm{and}\,\, v = \omega  \rho.
\end{equation*}
Here $\vb{r} = \vb{OO'} + \pmb{\rho}$ and is the radius vector of point $M$, from point $O$ of the axis of rotation. Point $O$ is taken as the origin of coordinates.

\subsection{}\label{1.5.5} The \redem{period of revolution} is the time interval $T$ required for a body, rotating uniformly at the angular velocity $\omega$, to com­plete one revolution about its axis of rotation (i.e. to rotate through the angle $\varphi = 2\pi$): 
\begin{equation*}%
T = \frac{2\pi}{\omega}.
\end{equation*}
The \redem{rotational speed} $n = 1/T = \omega/2\pi$ is the number of revolutions made by a body in unit time in uniform rotation at the angular velocity $\omega$.

\subsection{}\label{1.5.6} The rotation of a rigid body in which one of its points remains fixed is said to be \redem{rotation about a fixed point}. This point is usually taken as the origin of the fixed reference frame. In such rotation, all the points of the body travel along the surfaces of concentric spheres whose centre is at the fixed point. 

At each instant of time, this motion of the rigid body can be regarded as rotation about a certain axis that passes through the fixed point and is called the instantaneous axis of rotation. In the general case, the position of the \redem{instantaneous axis of rotation} varies with respect to both a fixed reference frame and one rigidly fixed in the rotating body.

The velocity $\vb{v}$ of arbitrary point $M$ of such a body is 
\begin{equation*}%
\vb{v} = [\pmb{\omega}\vb{r}]\,\, \textrm{and}\,\, v = \omega\rho.
\end{equation*}
Here $\pmb{\omega} = \dd \pmb{\varphi}/\dd t$ is the angular velocity of the body and is directed along the instantaneous axis of rotation as is the vector­ $\dd \pmb{\varphi}$ of an element of rotation of the body during the infinitely short time $\dd t$, $\vb{r}$ is the radius vector from fixed point $O$, about which the body rotates, to point $M$, and $\rho$ is the distance from point $M$ to the instantaneous axis of rotation. Such a body can have three independent motions: it can rotate about each of three mutually perpendicular axes passing through fixed point $O$. It consequently has three degrees of freedom \ssect{1.2.7}.

\subsection{}\label{1.5.7} The \redem{angular acceleration} vector $\pmb{\epsilon}$ is introduced to indicate the rate of change of the angular velocity vector with time in nonuniform rotation of a body about a fixed axis or its rotation about a fixed point. Vector $\pmb{\epsilon}$ is equal to the first derivative of the angular velocity $\pmb{\omega}$ of the body with respect to the time $t$:
\begin{equation*}%
\pmb{\epsilon} = \dv{\pmb{\omega}}{t}.
\end{equation*}
If the body rotates about a fixed axis, vector $\pmb{\epsilon}$ is directed along this axis. It coincides in direction with $\pmb{\omega}$ in accelerated rota­tion ($\dd\omega /\dd t > 0$) and is opposite to $\pmb{\omega}$ in direction in decelerated rotation ($\dd\omega /\dd t < 0$). The projection of the angular acceleration on the fixed axis $OZ$ of rotation is equal to 
\begin{equation*}%
\epsilon_{z} = \dv{\omega_{z}}{t},
\end{equation*}
where $\omega_{z}$ is the projection of vector $\pmb{\omega}$ on the same axis.

\subsection{}\label{1.5.8} As distinct from the angular acceleration, the accelera­tion a of arbitrary point $M$ of a rigid body rotating about fixed point $O$, or about a fixed axis passing through point $O$, is often called the \redem{linear acceleration}. It equals
\begin{equation*}%
\vb{a} = \dv{\vb{v}}{t} = \dv{t}[\pmb{\omega} \vb{r}] = \vb{a}_{r} + \vb{a}_{\textrm{ax}},
\end{equation*}
where $\vb{a}_{r} = [\pmb{\epsilon}\vb{r}]$ is the \redem{rotary acceleration} of the point, and $\vb{a}_{\textrm{ax}} = [\pmb{\omega}[\pmb{\omega}\vb{r}]]$ is its \redem{axipetal acceleration}, which is directed toward the instantaneous axis of rotation.
%\begin{wrapfigure}{l}[0pt]{0.5\linewidth}
\begin{figure}[!ht]
\centering
%\includegraphics[width=0.4\textwidth]{figs/ch-01/fig-01-07.pdf}
\input{figs/ch-01/fig-01-07}
%  \vspace{-13pt}
\caption{Relation between linear and rotational acceleration.}
\label{fig-01-07}
\end{figure}
%\end{wrapfigure}
If the body rotates about the fixed axis $OZ$ (\figr{fig-01-07}), the rotary acceleration of point $M$ coincides with its tangential accelera­tion $\vb{a}_{\tau}$ \ssect{1.4.5}, and its axipetal acceleration coincides with its normal acceleration \ssect{1.4.6}:
\begin{equation*}%
\vb{a}_{\tau} = [\pmb{\epsilon}\vb{r}] = [\pmb{\epsilon \rho}]\,\, \textrm{and}\,\, \vb{a}_{n} = - \,\pmb{\omega^{2}\rho}.
\end{equation*}
\subsection{}\label{1.5.9} Any complex motion of a rigid body can be broken down into two simple motions: translational motion at the velocity $\vb{v}_{A}$ of a certain arbitrarily chosen point $A$ of the body and rota­tion about an instantaneous axis passing through this point. The angular velocity $\pmb{\omega}$ of rotation is independent of the choice of point $A$. The velocity of arbitrary point $M$ of the body is
\begin{equation*}%
\vb{v}= \vb{v}_{A}+ [ \pmb{\omega}(\vb{r} - \vb{r}_{A})],
\end{equation*}
where $\vb{r}$ and $\vb{r}_{A}$ are the radius vectors of points $M$ and $A$. 

In the dynamics of a rigid body it usually proves convenient to deal with its complex motion as the combination of two simul­taneous motions: translational motion at the velocity of the body’s centre of mass \ssect{2.3.3} and rotation about the centre of mass.

The simplest case of complex motion of a body is \redem{plane}, or \redem{plane-parallel}, \redem{motion}, in which all the points of the body trav­el in parallel planes. This is the motion, for instance, of a homogeneous circular cylinder rolling down an inclined plane. In plane motion the direction of the instantaneous axis of ro­tation of the body about point $A$ remains constant and vectors $\pmb{\omega}$ and $\vb{v}_{A}$ are perpendicular to each other.























