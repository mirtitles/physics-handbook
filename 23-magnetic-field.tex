% !TEX root = handbook-physics.tex
% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode






\chapter{Magnetic Field Of Direct Current}
\label{ch-23}

\section{Magnetic Field. Ampere's Law}
\label{sec-23.1}

\subsection{}\label{23.1.1} 

A \redem{magnetic field} is one of the forms of an electromagnetic field \ssect{15.1.2}. A magnetic field is set up by moving charged particles, as well as moving bodies carrying electric charges. Magnetic fields act only on moving electric charges and moving charged bodies.

Variable electric fields (displacement currents) \ssect{27.3.1} are
also sources of magnetic fields.

\subsection{}\label{23.1.2} The main force characteristic of a magnetic field is
\redem{vector of magnetic induction} $\vb{B}$ (also called \redem{magnetic flux density}, \redem{magnetic displacement}, or, simply, \redem{magnetic vector}). Vector B is determined by one of three methods:
\begin{enumerate}[label= (\alph*)]
\item from Ampere’s law \ssect{23.1.4};
\item from the effect of the magnetic field on a current-carrying loop \ssect{23.4.2};
\item from the expression for the Lorentz force \ssect{24.1.3}.
\end{enumerate}

\subsection{}\label{23.1.3} Magnetic fields are represented graphically by lines of magnetic induction. \redem{Lines of magnetic induction (lines of magnetic force)} are curves whose tangent at each point coincides with vector $\vb{B}$ at this point of the field. The directions of the magnetic vector and the lines of magnetic induction are determined by the \redem{corkscrew (Maxwell) rule} (also called the \redem{right-hand screw rule}): if a corkscrew with right-hand thread is screwed in the direction of the current density vector in the conductor \ssect{20.2.3}, the direction that the corkscrew handle turns indicates the direction of the lines of magnetic induction and the magnetic vector.

Lines of magnetic induction are not interrupted at any point of the magnetic field, i.e. they neither begin nor end. These lines are either closed curves or they extend from infinity to infinity or they are wound infinitely on some surface, covering it densely all over, but never returning again to any point of the surface. A similar situation is observed, for example, in a field set up by a system of a circular current and an infinite rectilinear current passing through the centre of the circular current and perpendicular to its plane. 

A magnetic field is said to be \redem{uniform}, or \redem{homogeneous}, if vector $\vb{B}$ is constant at any point in the field. Otherwise, it is said to be \redem{nonuniform (nonhomogeneous}).

\subsection{}\label{23.1.4} The force exerted by a magnetic field on a current-carrying conductor placed in the field is called \redem{Ampere's force}. 

\redem{Ampere's law} states that an element of force $\dd \vb{F}$ exerted on a small element of length $\dd l$ of a conductor, carrying a current and located in a magnetic field, is proportional to the vector product of the element $\dd \vb{l}$ of conductor length by the magnetic induction $\vb{B}$:
\begin{align*}%
\dd F & = I \, [\dd \vb{l} \cross \vb{B}] && \text{(in SI units),}\\
\dd F & = \frac{I}{c} \, [\dd \vb{l} \cross \vb{B}]&& \text{(in Gaussian units, see Appendix~I),}
\end{align*}
Here $\dd \vb{l}$ is a vector with the magnitude $\dd l$ and has the same direction as the density vector $\vb{j}$ of the current in the conductor \ssect{20.2.3}.

Ampere’s force $\vb{F}$, acting in a magnetic field on a current-carrying conductor of finite length, is
\begin{equation*}%
\vb{F} = \int I \,  [\dd \vb{l} \cross \vb{B}],
\end{equation*}
in which integration is carried out over the whole length of the conductor. In the case of a uniform magnetic field \ssect{23.1.3}.
\begin{align*}%
\dd F & = I \, Bl \sin \alpha && \text{(in SI units),}\\
\dd F & = \frac{I}{c} \, Bl \sin \alpha && \text{(in Gaussian units),}
\end{align*}
where $\alpha$ is the angle between the density vector of the current in the conductor and vector $\vb{B}$. The relative positions of vectors $\dd \vb{F}$, $\vb{B}$ and $\dd \vb{l}$ are shown in \figr{fig-23-01}. If $\dd \vb{l}$ is perpendicular to $\vb{B}$, the direction of force $\dd \vb{F}$ is found by the \redem{left-hand rule}: if the left hand, opened flat with the fingers parallel to one another and the thumb perpendicular to the fingers, is held so that the magnetic induction vector enters the palm and the fingers indicate the direction of the electric current, the thumb indicates the direction of the force exerted by the magnetic field on the current-carrying conductor. 

It is evident from \figr{fig-23-01} that vector $\dd \vb{F}$ is perpendicular to the plane passing through vectors $\dd \vb{l}$ and $\vb{B}$, and that its direction is such that from the head of vector $\dd \vb{F}$ the direction of shortest rotation from vector d to vector $\vb{B}$ is counter-clockwise. In other words, vector $\dd \vb{F}$ coincides in direction with the vector product $[\dd \vb{l} \cross \vb{B}]$.

\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-23/fig-23-01.pdf}
\sidecaption{Directions of vectors $\dd \vb{F}$, $\dd \vb{l}$ and $\vb{B}$.\label{fig-23-01}}
\end{marginfigure}

\subsection{}\label{23.1.5} It follows that the magnetic induction vector in SI units is numerically equal to the limit of the following ratio: the force exerted by a magnetic field on an element of a current-carrying conductor to the product of the current by the length of an element of the conductor, the limit being taken as the length of the element approaches zero and is positioned in the field so that the limit reaches its maximum value. Thus
\begin{align*}%
B & = \frac{1}{I} \, \left( \dv{F}{l}\right)_{\text{max}} && \text{(in SI units),}\\
B & = \frac{c}{I} \, \left( \dv{F}{l}\right)_{\text{max}} && \text{(in Gaussian units),}
\end{align*}
where $c$ is the electrodynamic constant (Appendix~I).

\subsection{}\label{23.1.6} In contrast to electrostatic forces \ssect{14.2.2}, Ampere’s force is not a central one \ssect{3.3.4}. Ampere’s force is perpendicular to the lines of magnetic induction and to the current-carrying conductors.
\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-23/fig-23-02.pdf}
\sidecaption{Biot-Savart Laplace law to find magnetic induction at any point in a magnetic field set up by a direct electric current flowing along a conductor of any shape.\label{fig-23-02}}
\end{marginfigure}

\section{The Biot-Savart-Laplace Law}
\label{sec-23.2}
 
\subsection{}\label{23.2.1} The \redem{Biot-Savart-Laplace law} determines the magnetic induction at any point in a magnetic field set up by a direct electric current flowing along a conductor of any shape. The vector $\dd \vb{B}$ of magnetic induction at any point $C$ of a magnetic field, set up by an element of a conductor carrying the current $I$ and of the length $\dd l$ is determined by the formula
\begin{equation*}%
\dd \vb{B}= k \mu_{r} \, [\dd \vb{l} \cross \vb{r}],
\end{equation*}
where $\dd \vb{l}$ is the vector of an element of length of the conductor \ssect{23.1.4}, $\vb{r}$ is the radius vector from the element $\dd \vb{l}$ of the conductor to point $C$ (\figr{fig-23-02}), $r$ is the magnitude of the radius vector $\vb{r}$, $k$ is a factor depending only upon the chosen system of units, and $\mu_{r}$ is a dimension­ less quantity that characterizes the magnetic properties of the medium and called the \redem{relative magnetic permeability} (or simply  \redem{relative permeability}) of the medium. The relative permeability does not depend upon the choice of units and equals unity for vacuum. For all substances, except ferromagnetic ones, p,r differs only slightly from unity \ssect{26.5.1}. See \ssect{26.4.5} for a general definition of $\mu_{r}$.



\subsection{}\label{23.2.2} In SI units $k = \mu_{0}/4\pi$, where $\mu_{0} = 4\pi \times \SI{d-7}{\henry\per\meter}$ is the \redem{permeability} of free space, or the \redem{magnetic constant} (Appen­dix~II). In Gaussian units, $k = 1/c$, where $c = \SI{3d10}{\centi\meter\per\second}$ is the electrodynamic constant (Appendix~I).

The Biot-Savart-Laplace law is of the form
\begin{equation*}%
\dd \vb{B} = \frac{\mu_{0} \mu_{r}}{4 \pi} \, \frac{I}{r^{3}} \, [\dd \vb{l} \cross \vb{r}]
\end{equation*}
This form of the Biot-Savart-Laplace law and of all the electromagnetic field equations is said to be \redem{rationalized}.

The product $\mu_{0} \mu_{r}$ is sometimes called the \redem{absolute magnetic permeability} (or simply \redem{permeability}) of the medium.

In Gaussian units 
\begin{equation*}%
\dd \vb{B} = \frac{1}{c} \, \frac{I}{r^{3}} \, [\dd \vb{l} \cross \vb{r}]
\end{equation*}
The magnitude of vector $\dd \vb{B}$ is equal to
\begin{align*}%
\dd \vb{B} & = \frac{\mu_{0} \mu_{r}}{4 \pi} \frac{I \dd l \sin \alpha}{r^{2}} && \text{(in SI units),} \\
\dd \vb{B} & = \frac{\mu_{r}}{c} \frac{I \dd l \sin \alpha}{r^{2}}  && \text{(in Gaussian units.)}
\end{align*}
where $\alpha$ is the angle between vectors $\dd \vb{l}$ and $\vb{r}$.

\subsection{}\label{23.2.3} The \redem{magnetic field strength} $\vb{H}$ is the vector characteristic of a magnetic field that, for a homogeneous isotropic medium, is related to B as follows:
\begin{align*}%
\vb{H} & = \frac{\vb{B}}{\mu_{0} \mu_{r}} && \text{(in SI units),} \\
\vb{H} & = \frac{\vb{B}}{\mu_{r}}  && \text{(in Gaussian units.)}
\end{align*}
The universal relation between vectors $\vb{B}$ and $\vb{H}$ for a magnetic field in an arbitrary medium and the more general definition of the magnetic field strength vector $\vb{H}$ are discussed in \ssect{26.4.4}.

The strength of a magnetic field set up by a direct current in a homogeneous isotropic medium is independent of the magnetic properties of the medium. Thus
\begin{alignat*}{3}%
\dd \vb{H} & = \frac{I}{4 \pi r^{3}}\, [\dd \vb{l} \cross \vb{r}], \quad  && \dd H  = \frac{I \dd l \sin \alpha}{4 \pi r^{2}} \,\, && \text{(in SI units),} \\
\dd \vb{H} & =  \frac{I}{c r^{3}}\, [\dd \vb{l} \cross \vb{r}], \quad && \dd H  = \frac{I \dd l \sin \alpha}{c r^{2}} \,\,&& \text{(in Gaussian units.)}
\end{alignat*}
where $\alpha$ is the angle between vectors $\dd \vb{l}$ and $\vb{r}$.

\subsection{}\label{23.2.4} It follows from a comparison of the characteristics of electric ($\vb{E}$ and $\vb{D}$) and magnetic ($\vb{B}$ and $\vb{H}$) fields that the electric field strength vector $\vb{E}$ is analogous to the magnetic vector $\vb{B}$. Both vectors specify the force effects of the fields and depend upon the properties of the medium in which the field is set up.

The analogue of vector $\vb{D}$ of electric displacement \ssect{15.3.1} is the magnetic field strength vector $\vb{H}$.
\begin{marginfigure}%[-2cm]
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-23/fig-23-03.pdf}
\sidecaption{Magnetic field by a travelling electric charge in an unbounded homogeneous and isotropic medium at the constant velocity.\label{fig-23-03}}
\end{marginfigure}
\subsection{}\label{23.2.5} An electric charge $q$, travelling in an unbounded homogeneous and isotropic medium at the constant velocity $\vb{v}$, sets up a magnetic field whose magnetic induction $\vb{B}_{q}$ can be calculated by the formula
\begin{alignat*}{3}%
\vb{B}_{q} & = \frac{\mu_{0} \mu_{r}}{4 \pi }\, \frac{q}{r^{3}}
[\vb{v} \cross \vb{r}],\,\,  && {B}_{q}  = \frac{\mu_{0} \mu_{r}}{4 \pi }\, \frac{qv \sin \beta}{r^{2}} \,\, && \text{(in SI units),} \\
\vb{B}_{q} & =  \frac{\mu_{r}}{c}\, \frac{q}{r^{3}}
[\vb{v} \cross \vb{r}],\,\, && {B}_{q} = \frac{ \mu_{r}}{c}\, \frac{qv \sin \beta}{r^{2}}\,\, && \text{(in Gaussian units.)}
\end{alignat*}
where $\beta$ is the angle between vectors $\vb{v}$ and $\vb{r}$, and $\vb{r}$ is the radius vector from the travelling charge to the point $A$ being considered in the field. Vectors $\vb{B}_{q}$ and $\vb{H}_{q}$ are perpendicular to the plane passing through vectors $\vb{v}$ and $\vb{r}$.

When $q > 0$, the shortest rotation from vector $\vb{v}$ to vector $\vb{r}$ is seen from the head of vector $\vb{B}_{q}$ (and $\vb{H}_{q}$) to be counter­
\figr{fig-23-03} clockwise (\figr{fig-23-03}~\drkgry{(a)}). When $q < 0$, then $\vb{B}_{q}$ (and $\vb{H}_{q}$) have the opposite direction (\figr{fig-23-03}~\drkgry{(b)}). The magnetic field of a moving charge is variable, because the radius vector $\vb{r}$ varies in magnitude and direction as charge $q$ travels even if $\vb{v} = \text{const}$. The magnetic field of a moving charge, depending upon angle $\beta$ between vectors $\vb{v}$ and $\vb{r}$, is not spherically symmetrical, as in the case of the electrostatic field of a point charge \ssect{14.2.3}. The magnetic field being considered has mirror symmetry with respect to the direction of $\vb{v}$. The field strength is maximum at points in a plane passed through the charge perpendicularly to vector $\vb{v}$ (under the condition that $v \ll c$). At all points on a straight line coinciding with vector $\vb{v}$ there is no magnetic field induction,

\section[Simplest Cases of Magnetic Fields]{Simplest Cases of Magnetic Fields Set Up by Direct Currents}
\label{sec-23.3}

\subsection{}\label{23.3.1} Making use of the Biot-Savart-Laplace law we can determine the characteristics ($\vb{B}$ and $\vb{H}$) of a magnetic field set up by an electric current flowing in a conductor of finite dimensions and arbitrary shape. According to the principle of superposition of fields \ssect{15.2.2} the magnetic induction $\vb{B}$ at an arbitrary point of a magnetic field of a conductor carrying the current $I$ is equal to
\begin{equation*}%
\vb{B} = \int_{L} \dd \vb{B},
\end{equation*}
where $\dd \vb{B}$ is the magnetic induction of the field set up by an element
of the conductor with the length $\dd l$. Integration is carried out over the
whole length $L$ of the conductor.

\subsection{}\label{23.3.2} A \redem{straight conductor} $MN$ carrying the current I sets up a magnetic field with induction B and strength H at an arbitrary point A equal to\sidenote{It is assumed in all the examples of \ssect{23.3} that the medium is homogeneous, isotropic and completely fills all the space in which the magnetic field exists.}
\begin{alignat*}{3}%
B & = \frac{\mu_{0} \mu_{r}}{4 \pi }\, \frac{I}{r_{0}} \,(\cos \varphi_{1} - \cos \varphi_{2}),\,\,  && H  = \frac{B}{\mu_{0} \mu_{r}} \,\,&& \text{(in SI units),} \\
B & =  \frac{1}{c}\, \frac{I}{r_{0}}, (\cos \varphi_{1} - \cos \varphi_{2}),\,\,  && H = \frac{B}{\mu_{r}} \,\,&& \text{(in Gaussian units.)}
\end{alignat*}
where $r_{0}$ is the distance from point $A$ to the conductor, $\varphi_{1}$ and $\varphi_{2}$ are the angles formed by the radius vectors from point $A$ to the beginning and end of the conductor (\figr{fig-23-04}), $\mu_{r}$ is the relative magnetic permeability of the medium, and $\mu_{0}$ is the magnetic constant in SI units (Appendix~II).
\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-23/fig-23-04.pdf}
\sidecaption{Magnetic field by a current carrying conductor.\label{fig-23-04}}
\end{marginfigure}
For an infinitely long conductor $\varphi_{1} = 0$ and ($\varphi_{2} = \pi$)
\begin{alignat*}{3}%
B & = \frac{\mu_{0} \mu_{r}}{4 \pi }\, \frac{I}{r_{0}} \,(\cos \varphi_{1} - \cos \varphi_{2}),  \,\, && H  = \frac{B}{\mu_{0} \mu_{r}} \,\, &&  \text{(in SI units),} \\
B & =  \frac{1}{c}\, \frac{I}{r_{0}}, (\cos \varphi_{1} - \cos \varphi_{2}), \,\, &&  H = \frac{B}{\mu_{r}} \,\, &&  \text{(in Gaussian units.)}
\end{alignat*}

\subsection{}\label{23.3.3} For the magnetic field at the centre of a \redem{rectangular turn (loop) carrying the current} $I$
\begin{alignat*}{3}%
B & = \frac{\mu_{0} \mu_{r}}{4 \pi }\, \frac{8I\sqrt{a^{2}+b^{2}}}{ab},  \,\, && H  = \frac{B}{\mu_{0} \mu_{r}} \,\, &&  \text{(in SI units),} \\
B & =  \frac{1}{c}\, \mu_{r} \, \frac{8I\sqrt{a^{2}+b^{2}}}{ab}, \,\, &&  H = \frac{B}{\mu_{r}} \,\, &&  \text{(in Gaussian units.)}
\end{alignat*}
where $a$ and $b$ are the sides of the rectangle.

\subsection{}\label{23.3.4} The \redem{magnetic moment} $\vb{p}_{m}$ of a closed loop of arbitrary shape, carrying the current $I$, is equal to
\begin{align*}%
\vb{p}_{m} & = I \int_{S} \vb{n}\, \dd S && \text{(in SI units),} \\
\vb{p}_{m} & = \frac{I}{c}\int_{S} \vb{n}\, \dd S  && \text{(in Gaussian units.)}
\end{align*}
where $\vb{n}$ is a unit vector of the outward normal to element $\dd S$ of the surface $S$, bounded by the current-carrying loop. In the case of a flat loop, surface $S$ is plane and all the normals have the same direction. Hence
\begin{alignat*}{3}%
\vb{p}_{m} & = I S \vb{n},  \,\, && p_{m}  = IS \,\, &&  \text{(in SI units),} \\
\vb{p}_{m} & =  \frac{1}{c} I S \vb{n}, \,\, &&  p_{m} = \frac{1}{c}IS \,\, &&  \text{(in Gaussian units.)}
\end{alignat*}
Vector $\vb{p}_{m}$ is directed so that from its head the current in the circuit is seen to be flowing counterclockwise (\figr{fig-23-05}).
\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-23/fig-23-05.pdf}
\sidecaption{Orientation of magnetic moment vector.\label{fig-23-05}}
\end{marginfigure}

\subsection{}\label{23.3.5} The magnetic field set up by a circular turn, or loop, carrying the current $I$, at an arbitrary point $A$ on the axis of the turn (loop) (\figr{fig-23-06}), is specified by
\begin{alignat*}{3}%
\vb{B} & = \frac{\mu_{0} \mu_{r}}{4 \pi }\, \frac{2\vb{p}_{m}}{(R^{2}+h^{2})^{3/2}},  \,\, && \vb{H}  = \frac{\vb{B}}{\mu_{0} \mu_{r}} \,\, &&  \text{(in SI units),} \\
\vb{B} & =  \mu_{r}\, \frac{2\vb{p}_{m}}{(R^{2}+h^{2})^{3/2}},  \,\, && \vb{H}  = \frac{\vb{B}}{\mu_{r}}\,\, &&  \text{(in Gaussian units.)}
\end{alignat*}
Here $\vb{p}_{m}$ is the magnetic moment of a current-carrying turn, or loop, \ssect{23.3.4}.

The magnitudes of vectors $\vb{B}$ and $\vb{H}$ equal
\begin{alignat*}{3}%
B & = \frac{\mu_{0} \mu_{r}}{2}\, \frac{IR^{2}}{(R^{2}+h^{2})^{3/2}} = \frac{\mu_{0} \mu_{r}IS}{2 \pi (R^{2}+h^{2})^{3/2}},  \,\, && H  = \frac{B}{\mu_{0} \mu_{r}} \\ &  \text{(in SI units),} \\
B & =  \frac{1}{c} \mu_{r}\, \frac{2 \pi IR^{2}}{(R^{2}+h^{2})^{3/2}} = \frac{1}{c} \mu_{r} \frac{2IS}{(R^{2}+h^{2})^{3/2}},  \,\, && H  = \frac{B}{\mu_{r}}\\ &  \text{(in Gaussian units.)}
\end{alignat*}
where $h$ is the distance from point $A$ to the centre of the turn (loop), $R$ is the radius and $S$ is the area of the turn.

\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-23/fig-23-06.pdf}
\sidecaption{The magnetic field at the centre of a circular turn, or loop.\label{fig-23-06}}
\end{marginfigure}

\subsection{}\label{23.3.6} The magnetic field at the centre of a circular turn, or loop, has the characteristics \ssect{23.3.5}:
\begin{alignat*}{3}%
\vb{B} & = \frac{\mu_{0} \mu_{r}}{4 \pi}\, \frac{2 \vb{p}_{m}}{R^{3}},  \,\, && \vb{H}  = \frac{\vb{B}}{\mu_{0} \mu_{r}} &&  \text{(in SI units),} \\
\vb{B} & =  \mu_{r}\, \frac{2 \vb{p}_{m}}{(R^{3}},\,\, && \vb{H}  = \frac{\vb{B}}{\mu_{r}} &&  \text{(in Gaussian units.)}
\end{alignat*}
The magnitudes of vectors $\vb{B}$ and $\vb{H}$ equal
\begin{alignat*}{3}%
B & = \mu_{0} \mu_{r}\, \frac{I}{2R}, \,\, && H  = \frac{I}{2R}\,\, &&  \text{(in SI units),} \\
B & =  \frac{2 \pi}{c} \mu_{r}\, \frac{I}{R}, \,\, && H  = \frac{1}{c} \, \frac{2 \pi I}{R}\,\, &&  \text{(in Gaussian units.)}
\end{alignat*}
The direction of the magnetic field is along the axis of the loop and perpendicular to its plane (\figr{fig-23-06}).

\begin{marginfigure}%[-2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-23/fig-23-07.pdf}
\sidecaption{The magnetic field at the centre of a toroid.\label{fig-23-07}}
\end{marginfigure}

\subsection{}\label{23.3.7} A \redem{toroid} is an annular coil wound on a core having the shape of a torus (\figr{fig-23-07}). The magnetic field of a toroid is completely confined within its volume.

The characteristics of this magnetic field are calculated by the formulas
\begin{alignat*}{3}%
B & = \mu_{0} \mu_{r}\, \frac{NI}{2\pi r}, \,\, && H  = \frac{NI}{2 \pi r} &&  \text{(in SI units),} \\
B & =  \frac{1}{c} \mu_{r}\, \frac{2NI}{r}, \,\, && H  = \frac{1}{c} \, \frac{2 N I}{r}\,\, &&  \text{(in Gaussian units.)}
\end{alignat*}
The magnetic induction ${B}$ and the strength ${H}$ of the magnetic field on the circular centre line of the toroid are equal to
\begin{equation*}%
B_{\text{cl}} = \mu_{0} \mu_{r}\, \frac{NI}{2\pi R_{\text{cl}}} =  \mu_{0} \mu_{r}nI, \quad H = nI \quad   \text{(in SI units).}
\end{equation*}
Here $N$ is the number of turns of the toroid, $I$ is the current in the turns, $r$ is the radius of a circle in the toroid, $ R_{\text{cl}} = (R_{1} + R_{2})/2$, $R_{1}$ and $R_{2}$ are the external and internal radii of the torus, and $n$ is the number of turns per unit length of the toroid’s centre line.

\subsection{}\label{23.3.8} A \redem{solenoid} is a cylindrical coil consisting of a large number of turns of wire in the form of a helix. If the turns are wound tight up against or sufficiently close to one another, the solenoid is dealt with as a system of circular closed current-carrying loops connected in series and having the same radius and a common axis.

The magnetic moment $\vb{p}_{m}$ \ssect{23.3.4} of the solenoid is equal to the vector sum of the magnetic moments of all $N$ of its turns. Thus
\begin{equation*}%
\vb{p}_{m}= NIS\vb{n},
\end{equation*}
where $I$ is the current in the solenoid’s turns, $S$ is the area of a turn, and $\vb{n}$ is a unit vector of the normal to the plane of a turn. Vector $\vb{p}_{m}$ is along the axis of the solenoid and coincides in direction with the magnetic field as determined by the cork­ screw rule \ssect{23.1.3}.
\begin{figure}%[-2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-23/fig-23-08.pdf}
\sidecaption{The magnetic field of a solenoid.\label{fig-23-08}}
\end{figure}

The magnetic induction ${B}$ and strength ${H}$ of the solenoid’s magnetic field at an arbitrary point $A$, lying on its axis, are numerically equal to
\begin{alignat*}{3}%
B & = \frac{\mu_{0} \mu_{r}}{2}\, NI \, (\cos \alpha_{2} - \cos \alpha_{1}), && H  = \frac{B}{\mu_{0} \mu_{r}} &&  \text{(in SI units),} \\
B & =  \frac{1}{c} \, {2 \pi \mu_{r} n I}\mu_{r}\,(\cos \alpha_{2} - \cos \alpha_{1}),   && H  =\frac{B}{\mu_{0} \mu_{r}}\,\, &&  \text{(in Gaussian units.)}
\end{alignat*}
where $n = N/L$ is the number of turns per unit length of the solenoid, $\alpha_{2}$ and $\alpha_{1}$ are angles at which the ends of the solenoid are seen from point $A$ $(\alpha_{2}< \alpha_{1})$,
\begin{equation*}%
\cos \alpha_{1} = - \frac{l_{1}}{\sqrt{R^{2}+l_{1}^{2}}}, \quad \cos \alpha_{2} =  \frac{L - l_{1}}{\sqrt{R^{2}+(L - l_{1})^{2}}},].
\end{equation*}
$L$ is the length of the solenoid (\figr{fig-23-08}), and $R$ is the radius of the cylindrical coil.


\subsection{}\label{23.3.9} Under the condition that $L \gg R$, the magnetic field inside the solenoid at points on its axis sufficiently distant from its ends is characterized by the values
\begin{alignat*}{3}%
B & = \mu_{0} \mu_{r}nI, && H  = nI &&  \text{(in SI units),} \\
B & =  \frac{1}{c} \, 4 \pi \mu_{r} n I,   && H  = \frac{1}{c} \,4 \pi nI\,\, &&  \text{(in Gaussian units.)}
\end{alignat*}
The magnetic induction $B$ and strength $H$ of the magnetic field of a solenoid of sufficient length, at points of its axis coinciding with its ends, are equal to
\begin{alignat*}{3}%
B & = \frac{\mu_{0} \mu_{r}}{2}\, nI, && H  = \frac{1}{2}nI &&  \text{(in SI units),} \\
B & =  \frac{1}{c} \, {2 \pi \mu_{r} n I},   && H  =\frac{1}{c} \,2 \pi nI\,\, &&  \text{(in Gaussian units.)}
\end{alignat*}

\section[Interaction of Conductors]{Interaction of Conductors. Effect of a Magnetic Field on Current-Carrying Conductors}\label{sec-23.4}

\subsection{}\label{23.4.1} Ampere’s force \ssect{23.1.4}, which acts on an element $\dd l$ of length of a straight conductor carrying the current $I_{1}$ and is exerted by a long straight conductor carrying the current $I_{2}$, located parallel to the first conductor at a distance of $a$, is numerically equal to
\begin{align*}%
\dd F & = \frac{\mu_{0} \mu_{r}}{4 \pi}\, \frac{2I_{1}I_{2}}{a} \, \dd l, &&  \text{(in SI units),} \\
\dd F & =  \frac{1}{c} \, \mu_{r} \frac{2I_{1}I_{2}}{a} \, \dd l, &&  \text{(in Gaussian units.)}
\end{align*}
where $\mu_{0}$ is the magnetic constant in SI units \ssect{23.2.2} and $c$ is the electrodynamic constant (Appendix~I). The force $\dd F$ is a force of magnetic interaction \ssect{24.1.4}.

It is assumed that the lengths of the conductors are many times greater than the distance a between them, and that element $\dd l$ is located far from the ends of the first conductor. It is moreover assumed that the conductors are in a homogeneous isotropic medium having the relative magnetic permeability $\mu_{r}$.

The force ${F}$ exerted on a current-carrying conductor of length $l$ is equal to
\begin{align*}%
F & = \frac{\mu_{0} \mu_{r}}{4 \pi}\, \frac{2I_{1}I_{2}}{a} \, l, &&  \text{(in SI units),} \\
F & =  \frac{1}{c} \, \mu_{r} \frac{2I_{1}I_{2}}{a} \, l, &&  \text{(in Gaussian units.)}
\end{align*}
Conductors carrying currents $I_{1}$ and $I_{2}$ that flow in the same direction attract each other. If the currents flow in opposite directions, the conductors repulse each other (see also Sect. \ssect{24.1.4}).

\subsection{}\label{23.4.2} A closed flat current-carrying loop, of arbitrary geometric shape, placed in a uniform magnetic held \ssect{23.1.3}, is subject to a moment of force (torque) $\vb{M}$ equal to
\begin{equation*}%
\vb{M} = [\vb{p}_{m} \cross \vb{B}],
\end{equation*}
where $\vb{p}_{m}$ is the magnetic moment vector of a current-carrying loop \ssect{23.3.4}, and $\vb{B}$ is the vector of magnetic induction \ssect{23.1.2} of the held. Torque $\vb{M}$ is perpendicular to the plane passing through vectors $\vb{p}_{m}$ and $\vb{B}$ in such a manner that the shortest rotation from $\vb{p}_{m}$ to $\vb{B}$ is seen from the head of vector $\vb{M}$ to be counterclockwise. The torque tends to turn the loop to a position of stable equilibrium, in which vectors $\vb{p}_{m}$ and $\vb{M}$ are parallel to each other.

The preceding formula provides for a definition of the magnetic induction $\vb{B}$ \ssect{23.1.2}: the magnitude of the magnetic induction vector at a given point of a uniform magnetic field is equal to the maximum value of the torque $M_{\text{max}}$ acting in the vicinity of the point on a small flat closed current-carrying loop, having a magnetic moment ${p}_{m}$ of unit magnitude:
\begin{align*}%
B & = \frac{M_{\text{max}}}{{p}_{m}} = \frac{M_{\text{max}}}{IS} &&  \text{(in SI units),} \\
B & =  \frac{M_{\text{max}}}{{p}_{m}} = \frac{M_{\text{max}}}{\dfrac{1}{c}IS} &&  \text{(in Gaussian units).}
\end{align*}

\subsection{}\label{23.4.3} When a closed current-carrying loop is placed into a nonuniform magnetic field \ssect{23.1.3} in which the magnetic induction $\vb{B}$ of the field varies over distances comparable to the linear dimensions of the loop, the loop is subject to the force $\vb{F}$ equal to
\begin{equation*}%
\vb{F} = p_{mx} \pdv{\vb{B}}{x} + p_{my} \pdv{\vb{B}}{y} + p_{mz} \pdv{\vb{B}}{z},
\end{equation*}
where $p_{mx}$, $p_{my}$ and $p_{mz}$ are the projections of vector $\vb{p}_{m}$ on the axes of Cartesian coordinates. In particular, when vector $\vb{B}$ is along the $OX$ axis and depends only on coordinate $x$ [i.e. $B_{x} = B(x)$, $B_{y}= B_{z} = 0$ and $\vb{B} = \vb{B}_{x}\vu{i}$ then
\begin{equation*}%
\vb{F} = p_{mx} \dv{{B}}{x} \vu{i}.
\end{equation*}
By the action of force $\vb{F}$ an unfastened current-carrying loop is attracted into a region with a stronger magnetic field.

\section{Total Current Law. Magnetic Circuits}
\label{sec-23.5}

\subsection{}\label{23.5.1} The \redem{circulation} of the magnetic induction vector $\vb{B}$ along a closed circuit $L$ is an integral of the form:
\begin{equation*}%
\oint_{L} \vb{B}\, \dd \vb{l}  = \oint_{L} {B}\, \dd \vb{l} \cos \alpha,
\end{equation*}
where $L$ is the closed loop of arbitrary shape, $\alpha$ is the angle between vectors $\vb{B}$ and $\dd \vb{l}$, and $\dd \vb{l}$ is the vector of an element of length of the loop in the direction it is circuited.

\subsection{}\label{23.5.2} The \redem{total current law for a magnetic field in vacuum states}: 
\begin{quote}
the circulation of the magnetic induction vector of a magnetic field along a closed loop in vacuum is proportional to the algebraic sum of the currents bounded by the loop. 
\end{quote}
Thus	
\begin{align*}%
\oint_{L} \vb{B}\, \dd \vb{l}  & = \mu_{0} \sum_{k =1}^{n}I_{k} &&  \text{(in SI units),} \\
\oint_{L} \vb{B}\, \dd \vb{l}  & =  \frac{4 \pi}{c} \sum_{k =1}^{n}I_{k} &&  \text{(in Gaussian units).}
\end{align*}
where $\mu_{0}$ is the magnetic constant in SI units \ssect{23.2.2}, $c$ is the electrodynamic constant \ssect{23.2.2} and $n$ is the number of current-carrying conductors bounded by loop $L$ of arbitrary shape. This law is valid for current-carrying conductors of any shape or size.

In calculating the algebraic sum of the currents, a current is considered positive if from the head of the current density vector \ssect{20.2.3} the circuiting of loop $L$ is seen to be counter­ clockwise. Otherwise, the current is considered to be negative. For a generalization of the total current law to cover magnetic fields in an arbitrary media, see \ssect{26.4.2}.

\subsection{}\label{23.5.3} In contrast to an electrostatic conservative field, in which the circulation of the strength $\vb{E}$ along any closed path equals zero \ssect{16.1.4}, a magnetic field has a curl (it is called a \redem{rotational}, \redem{vortical} or \redem{circuital field}). In such a field the circulation of the magnetic induction vector $\vb{B}$ of the field along a closed path is not equal to zero. If no currents are bound by loop $L$, the circulation of vector $\vb{B}$ along this loop equals zero. But this does not alter the rotational nature of the magnetic field.

\subsection{}\label{23.5.4} The total current law can be written for either vacuum or for an arbitrary medium in the form of the circulation of field strength vector $\vb{H}$ \ssect{23.2.3} along an arbitrary closed loop $L$ that bounds the currents:
\begin{align*}%
\oint_{L} \vb{H}\, \dd \vb{l}  & = \mu_{0} \sum_{k =1}^{n}I_{k} &&  \text{(in SI units),} \\
\oint_{L} \vb{H}\, \dd \vb{l}  & =  \frac{4 \pi}{c} \sum_{k =1}^{n}I_{k} &&  \text{(in Gaussian units).}
\end{align*}
If no currents are bounded by loop $L$, the circulation of vector $\vb{H}$ along this loop equals zero.

\subsection{}\label{23.5.5} The \redem{flux of the magnetic induction vector} $\vb{B}$ (\redem{magnetic flux}) through a surface element of area $\dd S$ is the scalar physical quantity equal to
\begin{equation*}%
\dd \Phi_{m} = \vb{B} \, \dd S = B_{n} \dd S = B \, \dd S \cos \gamma,
\end{equation*}
where $\dd S = \vu{n} \dd S$, $\vu{n}$ is the unit vector of the normal to the surface of area $\dd S$, $B_{n}$ is the projection of vector $\vb{B}$ on the direction of the normal (\figr{fig-23-09}), and $\gamma$ is the angle between vectors $\vb{B}$ and $\vu{n}$. The magnetic flux $\Phi_{m}$ through an arbitrary surface $S$ is
\begin{equation*}%
\Phi_{m} = \int_{S} \vb{B}\, \dd S = \int_{S} B_{n} \, \dd S.
\end{equation*}
In calculating this integral, the vectors $\vu{n}$ of the normals to the elements of surface $\dd S$ should be all in the same direction with respect to surface $S$. Thus, if $S$ is a closed surface, vectors $\vu{n}$ should be directed either all outward or all inward. 

When the field is uniform and $S$ is a plane surface perpendicular to vector $\vb{B}$, then $B_{n} = B = \text{const.}$ and $\Phi_{m}= BS$.
\begin{marginfigure}%[-2cm]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-23/fig-23-09.pdf}
\sidecaption{The projection of vector $\vb{B}$ on the direction of the normal.\label{fig-23-09}}
\end{marginfigure}


\subsection{}\label{23.5.6} The \redem{Ostrogradsky-Gauss for a magnetic field} states that the magnetic flux through an arbitrary closed surface equals zero.
Thus
\begin{equation*}%
 \oint_{S} \vb{B}\, \dd \vb{S} = \int_{S} B_{n} \, \dd S = 0.
\end{equation*}
This theorem is a mathematical expression of the fact that in nature there are no magnetic charges at which lines of magnetic induction begin or end \ssect{23.1.3}.

The differential form of the Ostrogradsky-Gauss theorem for a magnetic field is one of Maxwell's equations for an electromagnetic field \ssect{27.4.2}.

\subsection{}\label{23.5.7} A \redem{magnetic circuit} is a set of bodies or regions of space to which a magnetic field is confined. For example, the internal region of a toroid \ssect{23.3.7} and that of an infinitely long solenoid \ssect{23.3.9} are magnetic circuits. To strengthen their magnetic fields, magnetic circuits are made of materials having a high relative magnetic permeability $\mu_{r}$ \ssect{23.2.1}, such as iron. The design of magnetic circuits, which are the essential components of electrical machinery and such electric devices as transformers and electromagnets, is based on the laws of magnetic circuits. 

\subsection{}\label{23.5.8} \redem{Ohm's law for a closed magnetic circuit} (the \redem{Hopkinson formula}) is 
\begin{equation*}%
\Phi_{m} = \frac{F_{m}}{R_{m}},
\end{equation*}
where $\Phi_{m}$ is the magnetic flux, which is constant along each portion of the magnetic circuit, $F_{m} = IN$ is the \redem{magnetomotive} force (\redem{mmf}) or \redem{magnetizing force} (in SI units), $N$ is the number of turns carrying the electric current $I$ for magnetizing the circuit, and $R_{m}$ is the \redem{total magnetic resistance} (\redem{reluctance}) of the circuit. The magnetic resistance of a magnetic circuit of length $l_{i}$ is
\begin{equation*}%
R_{mi} = \int^{l_{i}} \frac{\dd l}{\mu_{0}\mu_{r}S}\,\, \text{(in SI units),}
\end{equation*}
where $\mu_{r}$ is the relative magnetic permeability of the given circuit, $\mu_{0}$ is the magnetic constant in SI units \ssect{23.2.2}, and $S$ is the cross-sectional area of the circuit. When $S = \text{const.}$,
$R_{mi} = l_{i}/\mu_{0}\mu_{r}S$ (in SI units).

\subsection{}\label{23.5.9} The total magnetic resistance $R_{m}$ of the parts of a magnetic circuit connected in series is
\begin{equation*}%
R_{m} = \sum_{i =1}^{n} R_{mi},
\end{equation*}
where $R_{mi}$ is the magnetic resistance of the $i$-th part of the magnetic circuit, and $n$ is the number of parts that make up the circuit.

If $n$ magnetic resistances are connected in parallel, the total magnetic resistance $R_{m}$ of the circuit is
\begin{equation*}%
R_{m} = \frac{1}{\displaystyle\sum_{i =1}^{n} \dfrac{1}{R_{mi}}}.
\end{equation*}

\subsection{}\label{23.5.10} A \redem{branch point} of a magnetic circuit is a region of space or bodies where there are more than two possible directions of the lines of magnetic induction \ssect{23.1.3}.

\redem{Kirchhoff's first law for branched magnetic circuits} states that the algebraic sum of the magnetic fluxes in the branches that join in a branch point is equal to zero. Thus
\begin{equation*}%
\sum_{i =1}^{n} \Phi_{mi} = 0,
\end{equation*}
where $n$ is the number of branches joined at the branch point cf. \ssect{23.3.1}.

A magnetic flux is considered positive if the lines of induction enter the branch point, and negative if the lines emerge from it. 

\subsection{}\label{23.5.11} \redem{Kirchhoff's second law for magnetic circuits} states that in any closed loop (mesh of a network), arbitrarily chosen in a branched magnetic circuit (network), the algebraic sum of the products of the magnetic fluxes by the magnetic resistances of the corresponding portions of the circuit \ssect{23.5.8} equals the algebraic sum of the magnetomotive forces in this loop \ssect{23.5.8}:
\begin{equation*}%
\sum_{i =1}^{k} \Phi_{mi} R_{mi}= \sum_{i =1}^{k} F_{mi},
\end{equation*}
where $k$ is the number of portions that make up the closed loop (cf. \ssect{21.3.2}). The fluxes and mmf’s $F_{mi}$ are considered positive if the directions of the corresponding lines of magnetic induction \ssect{23.1.3} coincide with the arbitrarily chosen direction for circuiting the loop.

\section[Work Done in a Magnetic Field]{Work Done in Moving a Current-Carrying Conductor in a Magnetic Field}
\label{sec-23.6}

\subsection{}\label{23.6.1} An unfixed current-carrying conductor is moved by Ampere’s force \ssect{23.1.4} when it is placed in a magnetic field. The element of work done by the Ampere force in moving an element, of length $\dd l$, of a conductor carrying the current $I$ is
\begin{align*}%
\delta W^{*} & = I \, \dd \Phi_{m}^{*} &&  \text{(in SI units),} \\
\delta W^{*} & = \frac{1}{c}\,I \, \dd \Phi_{m}^{*}&&  \text{(in Gaussian units),}
\end{align*}
where $\dd \Phi_{m}^{*}$ is the element of magnetic flux \ssect{23.5.5} through the element of surface $\dd S$ generated by the element of the conductor, of length $\dd l$, in its small displacement, and $c$ is the electrodynamic constant \ssect{23.2.2}.

\subsection{}\label{23.6.2} In a small displacement of a current-carrying conductor of finite length, the work $\delta W$ done by the Ampere forces is the sum of the elements of work for all the elements of length of the
conductor. It is equal to the integral of $\delta W^{*}$ calculated along the whole length $I$ of the conductor. Thus
\begin{align*}%
\delta W & = \int_{l} I \, \dd \Phi_{m}^{*} = I \, \dd \Phi_{m}&&  \text{(in SI units),} \\
\delta W & = \frac{1}{c}\,I \, \dd \Phi_{m}&&  \text{(in Gaussian units),}
\end{align*}
where $\dd \Phi_{m}$ is the magnetic flux through the surface generated by the conductor of length $l$ in a small displacement.

\subsection{}\label{23.6.3} If a conductor of finite length $l$ carries a constant current $I$ and is moved a finite distance, the required work of the Ampere forces for this motion is
\begin{equation*}%
W = I \Phi_{m},
\end{equation*}
where $\Phi_{m}$ is the magnetic flux through the surface generated by the conductor in its motion.

\subsection{}\label{23.6.4} The work done by Ampere forces in the displacement of a closed flat loop carrying the constant current $I$ in a magnetic field from initial position $\mathit{1}$ to final position $\mathit{2}$ is equal to
\begin{equation*}%
W_{12} = I (\Phi_{m2} - \Phi_{m1}),
\end{equation*}
where $\Phi_{m1}$ and $\Phi_{m2}$ are magnetic fluxes ``linked to the loop'' in positions $\mathit{1}$ and $\mathit{2}$, i.e. the magnetic fluxes through a surface bounded by the loop. In calculating these magnetic fluxes, the direction of normal $\vu{n}$ \ssect{23.5.5} is coordinated with the direction of the current in the loop by the corkscrew rule: from the head of the vector of the normal, the current is seen flowing counter­ clockwise in the loop.

\subsection{}\label{23.6.5} The formulas given above are valid for a current-carrying coil of $N$ turns moving in a magnetic field by Ampere forces, or, in general, for a loop of arbitrary shape. For example, the work done in a small displacement of a coil carrying the current $I$ is equal to
\begin{equation*}%
\delta W = I \sum_{i = 1}^{N} \dd \Phi_{mi} = Id \left(\sum_{i = 1}^{N} \dd \Phi_{mi} \right) = I\, \dd \Psi,
\end{equation*}
where $\Psi = \sum_{i = 1}^{N}\Phi_{mi}$ is the total magnetic flux through all $N$
turns of the coil; it is called the \redem{flux linkage of the circuit}. 

\subsection{}\label{23.6.6} The work required to move a conductor or closed loop carrying a constant current in a magnetic field is done at the expense of the energy consumed in the current source \ssect{21.1.3}.










