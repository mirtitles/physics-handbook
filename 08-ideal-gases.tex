% !TEX root = handbook-physics.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode

\chapter{Ideal Gases}
\label{ch-08}

\section{Subject Matter of Molecular Physics. Thermal Motion}
\label{sec-8.1}
\subsection{}\label{8.1.1} The branch of physics called \emph{molecular physics} deals with the dependence of the structure and physical properties of bodies on the nature of the motion of and the interaction be­ tween the particles making up these bodies.

Molecular physics is based on the \emph{molecular-kinetic theory} of the structure of matter. According to this theory all bodies consist of submicroscopic particles -- atoms, molecules or ions -- in continuous chaotic motion, which is called \emph{thermal motion}. Experimental confirmations of the molecular-kinetic theory are: the Brownian movement \ssect{11.7.1}, the transport phenomenon in various aggregate states of matter and other phenomena. 

\subsection{}\label{8.1.2} The molecular-kinetic theory of the structure of matter is used to advantage in various branches of physics. A great variety of physical phenomena that proceed due to the interac­tion and motion of particles of matter are dealt with in this theory from a unified point of view. This theory, for example, enables the mechanism of elasticity in solids \ssect{40.3.1} to be understood, reveals the cause of internal friction in gases \ssect{10.8.4} and liquids, explains the difference between real gases \ssect{12.1.2} and ideal gases \ssect{8.4.1}, and elucidates the mechanism of elec­trical conduction in conductors of electric current of various kinds, and the electrical and magnetic properties of matter.

\subsection{}\label{8.1.3} The thermal motion of particles of matter in various states of aggregation is not the same. It depends upon the forces of attraction or repulsion between the atoms, molecules and ions.

There are practically no attractive forces between the atoms and molecules in sufficiently dilute gases. This is due to the fact that the particles of such gases are located at distances from one another exceeding the range of molecular action \ssect{12.2.3}. The particles of such gases travel at uniform velocity and in a straight line until they collide with one another or with the walls of the container. These collisions are of a random nature. Each molecule in the volume of the gas may collide with any of the nearby particles, changing the direction of its motion in any arbitrary manner. The collision of a molecule
(or atom) of the gas with a wall may occur at any angle $\theta$ where
$-\pi/2 \leqslant \theta \leqslant \pi/2$. As a result, the thermal motion of molecules of gas is a purely random one and, on the average, the same number of molecules are travelling at any instant of time in any arbitrary direction within the gas.

\subsection{}\label{8.1.4} Solid crystalline bodies are characterized by considerable forces of interaction between their particles (atoms, molecules and ions). The joint action of the forces of attraction and repul­sion between the particles \ssect{12.1.3} leads to the vibration of the particles of solids about their equilibrium positions, which are called the \emph{sites}, or \emph{points, of the crystal lattice} \ssect{40.1.1}. Intermolecular interaction and violations of periodicity in crystals (40.1.1) make these vibrations anharmonic \ssect{28.1.3}. 

\subsection{}\label{8.1.5} The thermal motion of molecules of a liquid is of a kind intermediate between the two preceding types of motion \ssect{8.1.3} and \ssect{8.1.4}. For a definite length of time the molecules of a liquid vibrate about a.certain equilibrium position and are in a tem­porarily settled state. At the end of this time the equilibrium position of the molecules is displaced and a new settled arrange­ ment is formed. Simultaneously, a slow displacement of the molecules and their vibrations occur within small volumes
of the liquid (see also \ssect{13.2.3}).

\section[Statistical and Thermodynamic Methods]{Statistical and Thermodynamic\\ Methods of Investigation}
\label{sec-8.2} 

\subsection{}\label{8.2.1} There is a huge number of atoms (or molecules) in any body. For instance, in \SI{1}{\centi\meter\cubed} of a gas whose properties are close to those of an ideal gas there are \num{2.7e19} molecules at stand­ard conditions. In the condensed states, liquid and solid, the amount is of the order of \num{d22} particles per \si{\centi\meter\cubed}. If we assume that each atom (or molecule) obeys Newton’s second law \ssect{2.4.1} it is obviously quite impossible, not only to solve the dif­ferential equations of motion of the separate particles, but even to write these equations. Therefore, the behaviour of a separate molecule (or atom) of a body, for example, its path and the sequence of changes in its state, cannot be investigated by the methods used in classical mechanics.

\subsection{}\label{8.2.2} The macroscopic properties of systems consisting of a very great number of particles are studied by the \emph{statistical method}. It is based on the application of probability theory to definite models of the structure of the systems being investigated. The branch of theoretical physics in which the physical prop­erties of systems are studied by the statistical method is called \emph{statistical physics (statistical mechanics)}. Special laws, known as statistical laws are manifested in the joint behaviour of large numbers of particles. Certain \emph{average values of the physical quantities} can be found for a system consisting of a great many particles. These values characterize the set of particles as a whole. In gases, for instance, there are average values of the velocity of thermal motion of the molecules \ssect{10.3.6} and of their energy \ssect{10.2.4}. In a solid there is an average energy per degree of freedom of the vibrational motion of its particles \ssect{41.7.2}, etc. All properties of a system of particles are due, not only to the individual properties of the particles themselves, but to the features of their joint motion and average values of their dynamic characteristics (average velocities, average energies, etc.).

In addition to statistical laws, there are also dynamical laws, describing the motion of separate particles. The relationship between dynamical and statistical laws is manifested in the fact that the laws of motion of individual particles have an effect of the description of a system of particles, which is being investigated by the statistical method.

\subsection{}\label{8.2.3} Another procedure for studying physical phenomena is the \emph{thermodynamic method}, which is not concerned with either the internal structure of the substances making up the bodies (or systems) being investigated or with the nature of the motion of the separate particles. The thermodynamic method is based on the study of the various conversions of energy that occur in a system. The conditions of these conver­sions and the relations between the various kinds of energy enable the physical properties of systems to be studied in a great variety of processes in which the systems participate. The branch of physics in which research in the physical properties of systems is conducted by means of the thermodynamic method is called \emph{thermodynamics (phenomenological thermody­namics)}. Thermodynamics is based on two experimentally established principles, or laws, of thermodynamics \ssect{9.3.1} and \ssect{11.3.2}, as well as the Nernst. heat theorem, or third law of thermodynamics \ssect{11.8.2}.

\section[Thermodynamic Variables]{Thermodynamic Variables. Equations of State. Thermodynamic Processes}
\label{sec-8.3}

\subsection{}\label{8.3.1} Thermodynamics deals with \emph{thermodynamic systems}. There are macroscopic items (bodies and fields) that can interchange energy with one another as well as with the external medium, i.e. bodies and fields that are external with respect to the given system.

\subsection{}\label{8.3.2} The physical quantities introduced to describe the state of a thermodynamic system are called \emph{thermodynamic variables}, or \emph{parameters (state variables)}, of the system. Usually the pres­sure, specific volume and temperature are taken as the thermo­ dynamic variables.

The \emph{pressure} $p$ is a physical quantity equal to the force acting on unit area of a body’s surface in the direction normal to the surface:
\begin{equation*}%
p = \dv{F_{n}}{A}
\end{equation*}
where $\dd F_{n}$ is the numerical value of the normal force acting on an infinitely small part of the surface of the body, the area of this part being $\dd A$.

The \emph{specific volume} $v$ is the reciprocal of the density $\rho$ of a body: $v = 1/\rho$. The specific volume of a homogeneous body is equal to the volume of unit mass of the body.

\subsection{}\label{8.3.3} The concept of temperature is of significance for equilibrium states of a thermodynamic system \ssect{8.3.4}. An \emph{equilib­rium state (state of thermodynamic equilibrium)} is a state of a system which does not change with time (\emph{steady state}), the steady nature of the state not being due to processes occurring in the external medium. An equilibrium state is set up in
a system subject to constant external conditions and is main­tained in the system for an arbitrary length of time. The tem­perature is the same in all parts of a thermodynamic system that is in a state of thermodynamic equilibrium. If two bodies with different temperatures come into contact, internal ener­gy \ssect{9.1.2} is transferred from the body at the higher temperature to that at the lower temperature. The process ceases when the temperatures of the two bodies become equal.

\subsection{}\label{8.3.4} The temperature of an equilibrium system is a measure of the intensity of thermal motion of its molecules (or atoms or ions). In an equilibrium system of particles that obeys the laws of classical statistical mechanics \ssect{8.2.2}, the average kinetic energy of the thermal motion of the particles is pro­portional to the absolute temperature \ssect{10.2.5}.

Temperature can only be measured indirectly, making use of the fact that many physical properties of bodies, lending themselves to direct measurement, depend on the temperature. A change in the temperature of a body leads to changes in its length and volume, density, electric resistance, elastic prop­erties, etc. A change in any of these properties can be used to measure temperature. For this purpose, it is necessary for a certain body, said to be \emph{temperature-indicating}, that the functional dependence of the given property on the temperature be known. Temperature scales based on temperature-indicating bodies are said to be \emph{empirical}.

In the \emph{international centigrade (Celsius)} scale, temperature is measured in  \si{\degreeCelsius} and is denoted by $t$ (degree centigrade or degree Celsius). It is assumed that at a standard pressure of \SI{1.01325d5}{\pascal} (\ref{app-01}) the melting point of ice and the boiling point of water are equal to \SI{0}{\degreeCelsius} and \SI{100}{\degreeCelsius}.

In the thermodynamic (absolute) temperature scale, the tem­perature is measured in kelvins (\si{\kelvin}) and is denoted by $T$.

The relation between the absolute temperature and the tem­perature measured by the Celsius scale is: 
\begin{equation*}%
T = 273.15 + t
\end{equation*}
The temperature $T = \SI{0}{\kelvin}$ (or $t = \SI{-273.15}{\degreeCelsius}$) is called absolute zero temperature (the impossibility of reaching absolute zero is discussed in \ssect{11.8.4}).

\subsection{}\label{8.3.5} The state variables of a system are classified as being either external or internal. \emph{External state variables} are physical quantities depending upon the position in space and various properties of bodies that are external with respect to the given system. For a gas, for instance, the volume $V$ of the vessel in which the gas is contained is an external variable because the volume depends upon the position of external bodies: the walls of the vessel. For a dielectric in an electric field, the intensity of the field, set up by certain external sources, is an external variable. If a liquid is held in an open vessel, the atmospheric pressure, for example, is an external variable. 

The \emph{internal variables of a system} are physical quantities that depend both on the position of bodies external with respect to the system, and on the coordinates and velocities of the particles that make up the system. Internal variables of a gas, for instance, are its pressure and energy because they depend upon the coordinates and velocities of the moving molecules and on the density of the gas.

\subsection{}\label{8.3.6} The state variables of a system that is in equilibrium \ssect{8.3.3} are not independent. The internal variables of such a system depend only on its external variables and on the temperature. The equilibrium state of a simple system\footnote{An example of a simple system is a gas in the absence of external fields. Mixtures of chemically homogeneous gases of constant composition, chemically pure liquids, etc. arc also simple systems.} of given chemical composition and mass $M$ is determined by specifying two variables: the volume $V$ and the temperature $T$. The equation of state (thermal equation of state) of a simple system is the functional dependence of the equilibrium pressure $p$ in the system on the volume and temperature
\begin{equation*}%
p= f(V, T)
\end{equation*}
The equation of state is obtained experimentally in thermo­ dynamics. In statistical physics \ssect{8.2.2}, it is derived theoret­ically. This constitutes the interrelation between the statis­tical \ssect{8.2.2} and thermodynamical \ssect{8.2.3} methods of investi­gation.

\subsection{}\label{8.3.7} If any of the external variables of the system is changed, the state of a thermodynamic system is changed. This is called a \emph{thermodynamic process}. A thermodynamic process is said lo be equilibrium (an \emph{equilibrium, quasi-static process}) if the system passes at an infinitely slow rate through a continuous succession of infinitely close thermodynamic equilibrium states \ssect{8.3.3}. All processes that do not comply with these conditions are said to be \emph{non-equilibrium} ones. Real processes are non-equilibrium ones because they proceed at a finite rate. But the slower they proceed, the closer they are to equilibrium processes

\emph{Isoprocesses} are thermodynamic processes that proceed in a system of constant mass with a constant value of one of the state variables.

An \emph{isothermal process} proceeds at constant temperature $(T = \textrm{const})$.

An \emph{isochoric (isovolumic) process} proceeds at constant volume $(V = \textrm{const})$.

An \emph{isobaric process} proceeds at constant pressure $(p = \textrm{const})$. 

An \emph{adiabatic process} is a thermodynamic process that occurs in a system without heat exchange \ssect{9.2.4} with external bodies (see also \ssect{9.5.10}).

\subsection{}\label{8.3.8} \emph{State functions} are physical quantities characterizing the state of a system. Variation in the state functions during ther­modynamic processes is independent of the kind of process that occurs. State functions are uniquely determined by the values of the variables of the initial and final states of the system. The simplest state functions of a system are its inter­nal energy $U$ \ssect{9.1.2} and its entropy $S$ \ssect{11.4.2}.

\section{Equation of State of an Ideal Gas}
\label{sec-8.4}

\subsection{}\label{8.4.1} An \emph{ideal}, or \emph{perfect, gas} is one whose molecules do not interact with one another at a distance and are of vanishingly small size. In real gases \ssect{12.1.2} the molecules are subject to the forces of intermolecular interaction \ssect{12.1.3}.

In collisions with one another and in striking the walls of the vessel the molecules of an ideal gas behave as perfectly elastic spheres \ssect{1.1.4} of diameter $d$ (\emph{elective diameter of the molecule}), which depends upon the chemical nature of the gas. The existence of an effective diameter $d$ (where $d \approx \SI{d11}{\meter}$) signifies that forces of mutual repulsion \ssect{12.1.3} act between the molecules. Intermolecular forces of attraction \ssect{12.1.3} decrease drastically with an increase in the distance $r$ between molecules and are practically negligible at $ R_{m} > \SI{d9}{\meter}$. In real gases at low densities the average distance $\expval{r}$) between molecules exceeds $R_{m}$ and, to a good approximation, they can be regarded as ideal. Hydrogen, helium, oxygen and nitrogen are assumed to be ideal gases at densities corresponding to standard conditions.

\subsection{}\label{8.4.2} For a given mass of an ideal gas, the ratio of the product of the numerical values of pressure and volume to the absolute temperature is a constant value (\emph{Clapeyron equation}):
\begin{equation*}%
\frac{pV}{T} = C = \textrm{const}
\end{equation*}
The numerical value of the \emph{gas constant} $C$ depends upon the choice of the units of measurement of $p$, $V$ and $T$.

If $v$ is the specific volume of the gas \ssect{8.3.2} and $M$ is its mass, then $V = Mv$ and the Clapeyron equation assumes the form
\begin{equation*}%
pv = \frac{C}{M} T = BT
\end{equation*}
where $B = C/M$ is the \emph{specific gas constant} referred to unit mass.

\subsection{}\label{8.4.3} From the definition of the mole (\ref{app-01}), it is evident that moles of different gases contain the same number of mole­cules. This is called \emph{Avogadro's number} $N_{A}$ (\ref{app-02}).

The \emph{molar mass} $\mu$ of a gas or any body is the physical quantity equal to the ratio of the mass of the gas (or any body) to the number of moles $N$ it contains: $\mu = M/N$.

The molar mass is proportional to the relative mass of the molecules of a gas: $\mu = \num{d-3} m/m_{0}$, where $m$ is the mass of the molecules of the given gas and $m_{0}$ is the atomic mass unit based on the carbon scale (\ref{app-02}). The coefficient \num{d-3} is required because molar mass is measured in \si{\kilo\gram\per\mole} in the SI (International System of Units).

The \emph{molar volume} $V_{\mu}$ is the physical quantity equal to the ratio of the volume $V$ of a gas to the number of moles $N$ it contains: $V_{\mu} = V/N$. The mass of a mole is numerically equal to $\mu$ and therefore $V_{\mu} = \mu v$, where $v$ is the specific volume \ssect{8.3.2}.

\subsection{}\label{8.4.4} The equation of state for a mole of ideal gas is
\begin{equation*}%
\frac{pV_{\mu}}{T} = \mu B = R \qor pV_{\mu} = RT
\end{equation*}
Here $R$ is the \emph{universal gas constant}, being the gas constant per mole of gas. The universal nature of $R$ follows from \emph{Avagadro's law}, which states that moles of all ideal gases occupy the same volume at the same pressure and the same tempera­ture. Under standard conditions ($T = \SI{273.15}{\kelvin} and $p =\SI{1.0132d5}{\pascal} = \SI{1}{\atmos} = \SI{760}{\milli\meter\mercury}) a mole of any gas has a volume of $V_{\mu} = \SI{22.415d-3}{\meter\cubed}$. From this we can calculate the numerical values of $R$ for various systems of units (\ref{app-02}).

If a volume $V$ of gas has the mass $M$ \si{\kilo\gram}, i.e. if it contains $M/\mu$ moles, then $V = M /\mu \times V_{\mu}$ and the equation of state of the gas assumes the form called the \emph{Mendeleev-Clapeyron equation}:
\begin{equation*}%
pV = \frac{M}{\mu} RT
\end{equation*}

\subsection{}\label{8.4.5} \emph{Boltzmann's constant} k is a physical quantity equal to the ratio of the universal gas constant $R$ to Avogadro’s number $N_{A}$, i.e. $k = R/N_{a}$. The values of $k$ in various systems of units are listed in \ref{app-02}.

The equation of state of an ideal gas, expressed by means of Boltzmann’s constant, is of the form
\begin{equation*}%
p = \frac{kN_{A}}{V_{\mu}} = kn_{0}T
\end{equation*}
where $n_{0}= N_{A}/V_{\mu}$ is the number of gas molecules in unit volume (molecule concentration).

At constant temperature, the pressure of a gas is proportional to the concentration of its molecules (or to the density of the gas).

