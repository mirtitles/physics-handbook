% !TEX root = handbook-physics.tex
% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode






\chapter{Electric Current In Liquids And Gases}
\label{ch-22}

\section{Faraday's Laws of Electrolysis. Electrolytic Dissociation}
\label{sec-22.1}

\subsection{}\label{22.1.1} 
Liquids are conductors of electric current (\redem{electrolytes}, \redem{ion conductors}) provided that ordered motion of ions can be accomplished in them by the action of an external electric fields.

The ordered motion of ions in conducting liquids takes place in an electric field set up by \redem{electrodes}, which are conductors connected to the terminals of a source of electrical energy. The positive electrode is called the \redem{anode}, the negative electrode, the \redem{cathode}. Positive ions, called \redem{cations}, are the ions of metals and hydrogen, and move toward the cathode. Negative ions, called \redem{anions}, are the ions of acid radicals and of the hydroxyl group, and move toward the anode. Electric current in electrolytes is accompanied by the phenomenon of \redem{electrolysis}. This is the deposition on the electrodes, or the liberation at them, of the components of the substances contained in the electrolyte or of other substances produced by secondary reactions.

\subsection{}\label{22.1.2} \redem{Faraday's first law of electrolysis} states that the mass $M$ of a substance deposited on or liberated at an electrode is directly proportional to the electric charge $Q$ passing through the electrolyte. Thus
\begin{equation*}%
M = kQ = kIt
\end{equation*}
if direct current $I$ is passed during the time $t$ through the electrolyte.

The proportionality factor $k$ is called the \redem{electrochemical equivalent} of the substance. It is numerically equal to the mass of the substance liberated when a unit electric charge passes through the electrolyte and depends upon the chemical nature of the substance.

\subsection{}\label{22.1.3} \redem{Faraday's second law of electrolysis} states that the electrochemical equivalents of the elements are directly proportional to their chemical equivalents:
\begin{equation*}%
k = C \, k_{ch}
\end{equation*}
where $C$ is a certain constant universal for all the elements, and $k_{ch}$ is the chemical equivalent and is equal to
\begin{equation*}%
k_{ch} = \si{d3} \frac{m_{a}}{z}.
\end{equation*}
Mere $m_{a}$ is the atomic mass of the element (in kg/mol), and $z$ is its valency.

Hence, $k = \dfrac{1}{F}\, \frac{m_{a}}{z}$, where $F = \si{d-3}/C$ is \redem{Faraday's constant} (Appendix~II).

\subsection{}\label{22.1.4} \redem{Faraday's united law of electrolysis},
enables the physical meaning of $F$ to be cleared up: at $M = m_{a}/z,$ Faraday’s constant $F = Q$. The amount of substance equal to $1/z$ moles is called its \redem{gram equivalent}. At $z = 1$ the gram equivalent of substance is equal to one mole. Faraday’s constant is numerically equal to the electric charge that must he passed through an electrolyte to deposit one gram equivalent of any substance on the electrode.

\subsection{}\label{22.1.5} The splitting of neutral molecules into oppositely charged ions as a result of interaction between the dissolved substance (solute) and the solvent is called \redem{electrolytic dissociation}. It is caused by the thermal motion of the polar molecules \ssect{18.1.4} of the solute, consisting of interrelated, oppositely charged ions \ssect{39.4.3}, and by the interaction of these molecules with the polar molecules of the solvent. Both causes lead to weakening of the heteropolar bonds in ionic molecules \ssect{39.4.3} and to the conversion of such molecules into two oppositely charged ions. 

The \redem{degree of dissociation} $\alpha$ is the ratio of the number n’of molecules dissociated into ions in a certain volume to the total number n0of molecules of the solute in the same volume: 
\begin{equation*}%
\alpha = \frac{n'}{n_{0}}.
\end{equation*}

\subsection{}\label{22.1.6} The process opposite to electrolytic dissociation \ssect{22.1.5} is called \redem{deionization}. It consists in the recombination of ions of opposite signs into neutral molecules. Under conditions of dynamic mobile equilibrium between the dissociation and deionization processes, $\alpha$ is determined by the equation
\begin{equation*}%
\frac{1 -\alpha}{\alpha^{2}} = \text{const} \times n_{0}.
\end{equation*}
As $n_{0} \to 0$, we have $\alpha \to 1$, i.e. in weak solutions almost all the molecules dissociate. The degree of dissociation $\alpha$ decreases with an increase in the concentration of the solution. In highly concentrated solutions
\begin{equation*}%
\alpha \approx \frac{\text{const}}{\sqrt{n_{0}}}.
\end{equation*}


\section{Atomicity of Electric Charges}
\label{sec-22.2}


\subsection{}\label{22.2.1} It follows from Faraday's laws of electrolysis that all electric charges consist of a whole number of elementary indivisible charges.

\subsection{}\label{22.2.2} The charge $Q$ of any ion is equal to 
\begin{equation*}%
Q = \pm \frac{zF}{N_{A}},
\end{equation*}
where $z$ is the valency of the ion, $F$ is Faraday’s constant \ssect{22.1.4}, and $N_{A}$ is Avogadro’s number (Appendix~II). The charge of a monovalent ion is equal to the charge $e$ of the electron or proton:
\begin{equation*}%
Q_{1} = e = \SI{1.602d-19}{\coulomb} = \num{4.803d-10} \text{electrostatic\,\, units}.
\end{equation*}
Any electric charge consists of a whole number of elementary charges $e$ \ssect{14.1.2}.

\section{Electrolytic Conduction of Liquids}
\label{sec-22.3}

\begin{marginfigure}[-3cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-22/fig-22-01.pdf}
\sidecaption{Current density in a conducting liquid.\label{fig-22-01}}
\end{marginfigure}

\subsection{}\label{22.3.1} The current density $\vb{j}$ \ssect{20.2.3} in an arbitrary section $SS$, perpendicular to the direction of ion motion (\figr{fig-22-01}), is equal to the sum of the current densities of the positive and negative ions:
\begin{equation*}%
\vb{j} = \vb{j}_{+} + \vb{j}_{-}.
\end{equation*}
Here $\vb{j}_{+} = q_{+}n_{0+} \expval{\vb{v}_{+}}$ and $\vb{j}_{-} = q_{-}n_{0-} \expval{\vb{v}_{-}}$, where $q_{+}$ and $q_{-}$, $n_{0+}$ and $n_{0-}$, and $\expval{\vb{v}_{+}}$ and $\expval{\vb{v}_{-}}$ are the charges, concentrations and average velocities of ordered motion (\redem{drift} due in the action of the electric held) of the positive and negative ions. 

\subsection{}\label{22.3.2} The average velocities of ion drift are proportional to the electric field strength $\vb{E}$:
\begin{align*}%
\expval{\vb{v}_{\perp}} & = u_{+}\vb{E}\,\, \text{and},\\
\expval{\vb{v}_{\parallel}} & = - u_{-}\vb{E},
\end{align*}
where $u_{+}$ and $u_{-}$ are the \redem{mobilities} of the positive and negative \redem{ions}. The mobility of an ion is equal to the ratio of the magnitude of the
average drift velocity vector to that of the field strength vector. The mobility is independent of the field strength vector $\vb{E}$. Since there are no bulk charges in electrolytes, $q_{+}n_{0+} + q_{-}n_{0-} = 0$. Moreover, $q_{+} = ez_{+} = \dfrac{F}{N_{A}}z_{+}$ \ssect{22.2.2}.

\subsection{}\label{22.3.3} \redem{Ohm's law of current density in electrolytes} (cf. \ssect{20.3.4}) is:
\begin{equation*}%
\vb{j} = \frac{F}{N_{A}}z_{+}n_{0+} (u_{+} + u_{-})\vb{E}.
\end{equation*}
The resistivity $\rho$ of an electrolyte \ssect{20.3.4} is equal to
\begin{equation*}%
\rho = \frac{N_{A}}{Fz_{+}n_{0+} (u_{+} + u_{-})}.
\end{equation*}
If a molecule of the solute dissociates into $k_{+}$ positive and $k_{-}$ negative ions, then
\begin{equation*}%
k_{+}z_{+} = k_{-}z_{-}; \quad n_{0+} = k_{+}\alpha n_{0} \qand n_{0-} = k_{-}\alpha n_{0},
\end{equation*}
where $\alpha$ is the degree of dissociation, $n_{0}$ is the concentration of the solute \ssect{22.1.5} and
\begin{equation*}%
\rho = \frac{N_{A}}{Fz_{+}k_{+}\alpha n_{0} (u_{+} + u_{-})}.
\end{equation*}
The ratio $N_{A}/z_{+}$ is the number of positive ions in one gram equivalent \ssect{22.1.4}. We introduce the quantity
\begin{equation*}%
C = \frac{k_{+}n_{0}z_{+}}{N_{A}} = \frac{k_{-}n_{0}z_{-}}{N_{A}},
\end{equation*}
which is called the \redem{equivalent concentration of the solution}. It is the number of gram equivalents of the ions of one sign contained in unit volume of the electrolyte (either in the free state or bound in the molecules). Then
\begin{equation*}%
\rho = \frac{1}{F C \alpha(u_{+} + u_{-})}.
\end{equation*}


\section{Electrical Conduction in Gases}
\label{sec-22.4}


\subsection{}\label{22.4.1} Gases, consisting of neutral atoms and molecules, are insulators and do not conduct an electric current. Gases arc capable of conduction after they are ionized.

The \redem{ionization of a molecule} (or \redem{atom}) consists in the detachment of one or several electrons, after which the molecule (or atom) is converted into a positive ion. If a molecule (or atom) gains electrons, it becomes a negative ion.
The reverse process, in which electrons join a positive ion to form a neutral molecule (or atom), is called \redem{recombination}. 

\subsection{}\label{22.4.2} To ionize a molecule (or atom) it is necessary to perform the \redem{work of ionization} $W_{i}$ to overcome the forces of attraction between the electron being detached and the other particles of the molecule (or atom). The work $W_{i}$ depends upon the energy state of the electron being detached \ssect{30.1.9} in the atom or molecule of the given gas. The ionization energy increases with the electron detachment factor, i.e. with the number of electrons detached from each atom.

\subsection{}\label{22.4.3} The \redem{ionization potential} $V_{i}$ is the potential difference that the charged particle must traverse in an accelerating electric held in order to accumulate energy equal to the work of ionization $W_{i}$. Thus $V_{i} = W_{i}/e$, where $e$ is the magnitude of the charge of the particle.

\subsection{}\label{22.4.4} A gas is ionized by external effects: raising the temperature sufficiently, the action of various kinds of radiation, cosmic radiation, bombardment of the molecules (or atoms) of the gas by high-speed electrons or ions, etc. The \redem{intensity}, or \redem{rate, of ionization} is measured by the number of pairs of particles of opposite sign formed in unit time in unit volume of the gas. 

\subsection{}\label{22.4.5} \redem{Collision ionization} is the process in which a gas is ionized by the action of fast-moving electrons or by ions. The minimum kinetic energy \ssect{3.2.1} that an ionizing particle must have is assessed by applying the laws of conservation of momentum and energy. Thus
\begin{equation*}%
\frac{mv^{2}}{2} = W_{i} \, \left( 1 + \frac{m}{M}\right)
\end{equation*}
where $W_{i}$ is the work of ionization, $m$ is the mass of the electron and $M$ is the mass of the atom.

The smaller the ratio $m/M$, the closer this energy is to $W_{i}$. An electron and a single-charge ion accumulate the same energy $E =e \, \Delta V$ if they traverse the same potential difference $\Delta V$.

It follows from the preceding formula that for collision ionization by electrons and ions, the latter of a mass \num{d4} times that of the electron, ions must travel across a greater potential difference in an accelerating field than electrons.

\section{Various Types of Gas Discharges}
\label{sec-22.5}

\subsection{}\label{22.5.1} A \redem{gas discharge} is the process of current conduction through a gas.


\begin{marginfigure}[-3cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-22/fig-22-02.pdf}
\sidecaption{Current-voltage relationship in a semi-self-maintained discharge gas.\label{fig-22-02}}
\end{marginfigure}


A \redem{semi-self-maintained gas discharge} is one caused by external ionizing agents \ssect{22.4.4} and ceasing when the ionizing action stops. The curve in \figr{fig-22-02} shows the dependence of the current $I$ in a semi-self-maintained discharge on the voltage $U$ across the electrodes, in the 1st region of the curve, at low voltages, Ohm’s law, similar to the one for electrolytes \ssect{22.3.3}, is valid. If electrons and monovalent ions are formed in the gas, then
\begin{equation*}%
\vb{j} = en_{0}(u_{+} + u_{-} ) \vb{E},
\end{equation*}
where $n_{0}$ is the number of pairs of oppositely charged particles in unit volume, $u_{+}$ and $u_{-}$ are the mobilities of the positive and negative ions, and $e$ is the magnitude of the charge of the electron. In a wide pressure range, from 10 to \SI{d7}{\pascal}, the mo­bility of gaseous ions is inversely proportional to the pressure.

\subsection{}\label{22.5.2} The relation between the current $I$ and the voltage $U$ is no longer linear in the 2nd region of the curve in \figr{fig-22-02} because the ion concentration decreases in the gas. In this region $U$ the current increases slower and slower with an increase in $U$.

Beginning with a certain voltage $U_{s}$, the current re­ mains constant in the 3rd
region of the curve in \figr{fig-22-02} when the voltage is in­ creased. This occurs because, at a constant rate of ionization \ssect{22.4.4} in strong electric fields, all the ions formed in unit time in the gas reach the electrodes. No further increase in the current takes place at a fixed rate of ionization. Saturation results because all the charged particles produced in the gas travel in a strong electric field and reach the electrodes before an appreciable part of them have enough time to recombine with particles of opposite charge.

The saturation current $U_{s}$ is the maximum current that can be obtained at a given rate (intensity) of ionization:
\begin{equation*}%
I_{s} = eN_{0}
\end{equation*}
where $N_{0}$ is the maximum number of pairs of monovalent ions formed in the gas in unit time at a given rate of ionization. The linear relation between $I_{s}$ and $N_{0}$ confirms the ionic nature of electrical conduction in gases.

\subsection{}\label{22.5.3} A \redem{self-maintained gas discharge} is one that continues after the action of the external ionizing agent ceases. To obtain such a discharge it is necessary for new pairs of oppositely charged particles to be formed continuously in the gas. The main source of these particles is collision ionization \ssect{22.4.5}. At a certain sufficiently high voltage across the electrodes, the electrons are so highly accelerated by the electric held that their energy proves adequate to ionize the gas molecules \ssect{22.4.5}. This is called \redem{volume ionization}. Secondary electrons, accelerated in the electric held, also ionize the gas molecules. As a result, the number of current carriers in the gas and its electrical conductance are greatly increased (4th region in \figr{fig-22-02}. But, by itself, ionization by the action of electrons is insufficient to produce a self-maintained discharge. 

Electrons travelling in the direction from the cathode toward the anode ionize the molecules of gas located closer to the anode than the place where the electrons are unitied. If the energy of the positive ions is insufficient for collision ionization of the gas molecules or for knocking electrons out of the metallic cathode (\redem{surface ionization}), electrons can be produced near the cathode of a gas-discharge tube only by external ionizing agents. When the action of these agents is- stopped, the region of collision ionization by electrons contracts toward the anode as the electrons move in this direction. Under these conditions the gas discharge is extinguished as soon as collision ionization ceases. 

\subsection{}\label{22.5.4} Surface ionization \ssect{22.5.3}, initiated at high voltage $U$, produces secondary electrons and sets up a two-way avalanche of electrons and positive ions. After this the action of the external ionizing agent is no longer essential for continuing the gas discharge. When the voltage $U$ across the electrodes of a gas-discharge tube is raised sufficiently, an \redem{electric breakdown of the gas} occurs. This is the transition from a semi-self-maintained to a self-maintained gas discharge. The voltage $U = U_{bd}$, corresponding to electric breakdown, is called the \redem{ignition potential}, or \redem{breakdown voltage}. The breakdown voltage for a gas discharge in a tube having flat parallel electrodes at the distance $d$ from each other depends upon the product $pd$, where $p$ is the gas pressure \figr{fig-22-03}. In addition, depends on the chemical nature of the gas and the material of the cathode.
\begin{marginfigure}[-3cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-22/fig-22-03.pdf}
\sidecaption{The breakdown voltage for gas discharge.\label{fig-22-03}}
\end{marginfigure}

Self-maintained gas discharges are classified into glow, corona, spark and arc discharges. They are not discussed in the present handbook.

\section{Certain Information on Plasma}
\label{sec-22.6}

\subsection{}\label{22.6.1} \redem{Plasma} is a special state of matter, whose most important property is a predominant, or even complete, ionization of the particles of matter. The \redem{degree of ionization} $\alpha$ is the ratio of the number of ionized particles to their initial number. Plasma is classified as \redem{weakly ionized} (when $\alpha$ is a fraction of one percent), \redem{moderately ionized} (when $\alpha$ equals several percent) and \redem{fully ionized} (when $\alpha$ is close to 100 per cent).

Weakly ionized plasma is found in the \redem{ionosphere}, the conducting layer of the atmosphere. The ionosphere extends from 60 to \SI{2d4}{\kilo\meter} above the earth’s surface.

Fully ionized plasma, formed at ultra-high temperatures (high-temperature plasma), exists on the sun and hot stars. Under laboratory conditions, plasma is produced in gas discharges \ssect{22.5.1} and gaseous-discharge lamps. Accelerated plasma is employed as the working medium \ssect{11.1.1} in jet engines. Plasma can also be used for the direct conversion of internal energy into electrical energy (magnetohydrodynamic generators and plasma sources of electrical energy).

The great number of charged particles in a plasma provides for its high electrical conductance; in this aspect its properties approach those of conductors of electric current.

\subsection{}\label{22.6.2} A condition for the existence of a plasma is a certain minimum density $\rho_{\text{min}}$ of charged particles. Beginning with this density, we can speak of a plasma rather than a simple accumulation of separate charged particles. The density $\rho_{\text{min}}$ is determined by the inequality $L \gg D$, where $L$ is a linear dimension of the system of charged particles and $D$ is a typical ``plasma'' parameter, the length called the \redem{Debye-H\"uckel screening} radius (it is also called the \redem{Debye shielding length} or \redem{Debye length}):
\begin{align*}%
D & = \left( \sum_{i} \frac{4 \pi q_{i}^{2}n_{i}}{k T_{i}} \right)^{-1/2} && \text{(in cgse units),}\\
D & = \left( \sum_{i} \frac{ q_{i}^{2}n_{i}}{\varepsilon_{0} k T_{i}} \right)^{-1/2} && \text{(in SI units),}
\end{align*}
where $q_{i}$ is the charge of the $i$-th species of particles, $n_{i}$ is their concentration, $T_{i}$ is their temperature, $k$ is Boltzmann’s constant \ssect{8.4.5)} and $\varepsilon_{0}$ is the electric constant. Summation is to be carried out over all the species of particles. The quantity $D$ is the distance over which the Coulomb field of any charge of the plasma is shielded. The shielding occurs because any charge is predominantly surrounded by oppositely charged particles. 

According to a more exact definition, a plasma is a quasi­ neutral assembly of a large number of charged particles that occupy a region of space with the linear dimensions $L \gg D$. The violation of quasi-neutrality in the plasma is eliminated by the strong electric fields set up in it. 

The Debye-H\"uckel screening radius characterizes the interaction of the particles in the plasma. It was found that $E_{p}/E_{k} \propto N^{2/3}$, where $N = 4\pi n D^{3}/3$. Here $E_{p}$ is the potential energy of interaction between two particles located at an average distance of $n^{-1/3}$ from each other ($n$ being the concentration of particles), and $E_{k}$ is the kinetic energy of these particles. The quantity $N$ is the total number of charged particles inside a sphere of radius $D$, and is called the \redem{Debye number}. If $N$ is large, the plasma is said to be \redem{gaseous} and can be dealt with thermodynamically as an ideal gas with the state equation $p = nkT$ \ssect{8.4.5}.

\subsection{}\label{22.6.3} Coulomb long-range interaction of charged particles in a plasma leads to its qualitative uniqueness that enables it to be regarded as a special, \redem{fourth state of aggregation of matter. The most important properties of plasma} are:
\begin{enumerate}[label = (\alph*)]
\item its strong interaction, due to its high electrical conduction, with external magnetic and electric fields;
\item specific collective interaction of particles in the plasma (by means of a special held whose nature and origin is beyond the scope of this handbook);
\item the fact that due to long-range interaction it is a special kind of elastic medium in which various types of vibrations and waves can be readily excited and propagated.
\end{enumerate}
\subsection{}\label{22.6.4} The motion of plasma in a magnetic held is employed to directly convert the internal energy of an ionized gas into electrical energy. This method is made use of in a \redem{magneto-hydrodynamic (MHD) generator}. This generator operates as follows: gas produced in the combustion of fuel and constituting a plasma passes through a strong traverse magnetic held. In a conducting plasma, as in any conductor moving in such a held, electromagnetic induction \ssect{24.1.1} occurs. The induced emf can be taken off by means of electrodes in the external circuit. A schematic diagram of an MHD generator is shown in \figr{fig-22-04}.


\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-22/fig-22-04.pdf}
\sidecaption{ A schematic diagram of an MHD generator.\label{fig-22-04}}
\end{marginfigure}



The ionized gas produced in fuel combustion passes through the nozzle and its internal energy is converted into kinetic energy. As this gas passes through the transverse magnetic field $\vb{B}$, the positive ions move toward the upper electrode by the action of the induced electric held \ssect{24.1.1} and the free electrons move toward the lower electrode. When we connect the electrodes to an external load, a current is obtained in the circuit. 

\subsection{}\label{22.6.5} A state of thermodynamic equilibrium is possible at a definite temperature in the plasma when the loss of charged particles due to recombination \ssect{22.4.1} is recompensed by new acts of ionization. The average kinetic energies of the various particles making up such a plasma are the same. The processes of energy exchange between the particles of this plasma, as well as the energy exchange of the plasma with blackbody radiation \ssect{36.1.8} are equilibrium processes \ssect{8.3.7}. A \redem{plasma} with the aforesaid properties is said to be \redem{isothermal}. It exists in the atmospheres of high-temperature stars.

A condition required for a high degree of ionization of a thermodynamically equilibrium plasma, consisting of two kinds of charged particles, equal in magnitude and opposite in sign, is the maximum reduction in the recombination \ssect{22.4.1} of the particles. A completely ionized plasma can be obtained at $kT \gg eV_{i}$ where $V_{i}$ is the ionization potential \ssect{22.4.3} of the gas atoms and $kT$ is the average energy of thermal motion of the plasma particles. For hydrogen and deuterium this corresponds to $T \approx  \SI{160000}{\kelvin}$. Essential under these conditions are the radiation of the plasma and the difficulty of isolating the plasma from the walls of its container \ssect{22.6.7}.

\subsection{}\label{22.6.6} In the plasma of a gas discharge \ssect{22.5.1} (\redem{gas-discharge plasma}) thermodynamical equilibrium \ssect{22.6.5} is absent. The charged particles of such a plasma are in an accelerating electric field.

The average kinetic energy of the electrons in a gas-discharge plasma is characterized by a certain \redem{electron temperature} $T_{e}$ corresponding to the Maxwellian energy distribution of the electrons \ssect{10.3.7}. Owing to the lack of thermodynamical equilibrium in such a plasma, $T_{e}$ is of a conventional nature. The average kinetic energy of the neutral particles is substantially less than that of the electrons. Besides the electron temperature $T_{e}$, the parameters of a gas-discharge plasma are: the electron concentration $n_{e}$, the number of ionizations per electron per second, the density of the ion or electron current, and the longitudinal electric field strength $E_{z}$ established along the symmetry axis of the plasma.

\subsection{}\label{22.6.7} The existence of a thermodynamically nonequilibrium gas-discharge plasma is made possible by the energy of the discharge current passing through it. In the absence of an external electric field, the gas-discharge plasma disappears.

The disappearance of an unmaintained gas-discharge plasma that has been left to Itself is called \redem{deionization of the gas}.

In addition to the ionization and recombination processes \ssect{22.4.1}, a large share of the energy balance of a plasma that exists in a limited volume consists in the interaction between the plasma and the walls confining its volume, as well as the radiation of the plasma and the transport of radiation in it. The diffusion of the charged particles to the walls and their recombination on the walls, and the transfer of energy to the walls by heat conduction \ssect{10.8.2} contaminate the plasma with impurities and reduce its energy. To avoid these phenomena, measures are taken to confine the plasma by a magnetic field to prevent contact with the walls.

Radiation of the plasma in the optical range and in the far ultraviolet consists of: braking radiation (bremsstrahlung) of the electrons that occurs when they are retarded by the ions, ordinary radiation produced by excited particles, and recombination radiation that occurs in the recombination process
\ssect{22.4.1}. In a magnetic field, a plasma also has a special beta­ tron (synchrotron) radiation, which is beyond the scope of this handbook.