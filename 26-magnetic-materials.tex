% !TEX root = handbook-physics.tex
% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode






\chapter{Magnetic Materials In A Magnetic Field}
\label{ch-26}

\section{Magnetic Moments of Electrons and Atoms}
\label{sec-26.1}


\subsection{}\label{26.1.1} \redem{Magnetic materials} are all substances capable of acquiring magnetic properties, i.e. of being magnetized, in an external magnetic field. This means that they can set up their own magnetic field. The magnetic properties of substances are determined by the magnetic properties of their electrons and atoms.\sidenote{The magnetic properties of atomic nuclei are dealt with in \ssect{42.1.6}.} Magnetic materials are divided according to their magnetic properties into three main groups: diamagnetic \ssect{26.3.2}, para­ magnetic \ssect{26.3.5} and ferromagnetic \ssect{26.5.1} materials. 

\subsection{}\label{26.1.2} The motion of an electron along its orbit in an atom is equivalent to a certain closed loop carrying a current (\redem{orbital current}). According to \ssect{23.3.4}, the \redem{orbital magnetic moment} $\vb{p}_{m}$ of an electron equals 
\begin{align*}%
\vb{p}_{m} & = IS \vu{n} && \text{(in SI units),} \\
\vb{p}_{m} & =  \frac{1}{c} IS \vu{n} && \text{(in Gaussian units)},
\end{align*}
where $I = e\nu$ is the current, $e$ is the magnitude of the charge of the electron, $nu$ is the number of revolutions of the electron along its orbit in unit time, $S$ is the area of the electron orbit, $\vu{n}$ is a unit vector of the normal to area $S$, and $c$ is the electrodynamic constant \ssect{23.2.2}.

\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-26/fig-26-01.pdf}
\sidecaption{Orientation of vectors $\vb{p}_{m}$ and $\vb{L}_{e}$.\label{fig-26-01}}
\end{marginfigure}

An electron moving along its orbit has an orbital angular momentum $\vb{L}_{e}$ \ssect{4.1.4}. The orbital magnetic moment is proportion­al to the orbital angular momentum:
\begin{equation*}%
\vb{p}_{m} = \gamma \vb{L}_{e},
\end{equation*}
where
\begin{align*}%
\gamma & = - \frac{e}{2m} && \text{(in SI units),} \\
\gamma & =  - \frac{e}{2mc} && \text{(in Gaussian units)},
\end{align*}
The quantity $\gamma$ is called the \redem{gyromagnetic ratio of orbital mo­ments}.

In the last formulas $m$ is the mass of the electron, $e$ is the magnitude of its charge, and $c$ is the electrodynamic constant \ssect{23.2.2}.




Vectors $\vb{p}_{m}$ and $\vb{L}_{e}$ are opposite in direction and are perpendicular to the orbital plane of the electron (\figr{fig-26-01}). 

\subsection{}\label{26.1.3} An electron has an \redem{intrinsic angular momentum} $s$, which is called the spin of the electron. The magnitude of the electron spin is
\begin{equation*}%
s = \frac{\sqrt{3}}{2} \, \frac{h}{2\pi} = \frac{\sqrt{3}}{2}  \hbar,
\end{equation*}
where $\hbar$ is Planck’s constant (Appendix~II), and $\hbar = h/2\pi$. The most important feature of electron spin is that it has only two projections on the direction of vector $\vb{B}$ of magnetic induction.\sidenote{Regardless of whether it is an external magnetic field set up, for instance, by current-carrying conductors, or the internal magnetic field of the substance itself \ssect{26.4.2}.} Thus
\begin{equation*}%
s_{B} = \pm \frac{\hbar}{2}.
\end{equation*}

\subsection{}\label{26.1.4} Electron spin $s$ corresponds to the spin magnetic moment $\vb{p}_{ms}$ which is proportional to the spin and opposite in direction. Thus
\begin{equation*}%
\vb{p}_{ms} = \gamma_{s}\,s.
\end{equation*}
The quantity $\gamma_{s}$ is called the \redem{gyromagnetic ratio of spin moments}:
\begin{align*}%
\gamma & = - \frac{e}{m} && \text{(in SI units),} \\
\gamma & =  - \frac{e}{mc} && \text{(in Gaussian units)},
\end{align*}
where the notation is the same as in \ssect{26.1.2}.


The projection of the electron spin magnetic moment on the direction of the field \ssect{26.1.3)} equals
\begin{align*}%
p_{msB} & = \pm \frac{e \hbar}{2m} && \text{(in SI units),} \\
p_{msB} & =  \pm \frac{e \hbar}{2mc} && \text{(in Gaussian units)},
\end{align*}
where $\mu_{B}$ is called the \redem{Bohr magneton}, and is a unit for measuring magnetic moments (Appendix~II).

\subsection{}\label{26.1.5} The information presented in \ssect{26.1.1} through \ssect{26.1.4} is valid for each of the $Z$ electrons in an atom. The number $Z$ coincides with the atomic number in Mendeleev’s periodic table \ssect{39.3.5}.

The \redem{orbital magnetic moment} $\vb{P}_{m}$ of an atom is the vector sum of the orbital magnetic moments $\vb{p}_{m}$ of all its electrons. Thus
\begin{equation*}%
\vb{P}_{m} = \sum_{i = 1}^{Z} \vb{p}_{mi}.
\end{equation*}
The \emph{orbital angular momentum} $L$ of the atom is the vector sum of the orbital angular momenta $\vb{L}_{e}$ of all $Z$ electrons. Thus
\begin{equation*}%
\vb{L} = \sum_{i = 1}^{Z} \vb{L}_{ei}.
\end{equation*}
The atomic orbital magnetic moment $\vb{P}_{m}$ and angular momentum $\vb{L}$ have the following relationship:
\begin{equation*}%
\vb{P} = \gamma \vb{L},
\end{equation*}
where $\gamma$ is the gyromagnetic ratio \ssect{26.1.2}.

\section{An Atom in a Magnetic Field}
\label{sec-26.2}

\subsection{}\label{26.2.1} If a substance is in an external magnetic field, this field can be assumed uniform \ssect{23.1.3} within the limits of an atom. This follows from the smallness of atomic dimensions. Let us assume that an electron in the atom travels along a circular orbit whose plane is perpendicular to the induction vector $\vb{B}$ of the magnetic field. The action of the Lorentz force ${F}_{L}$ \ssect{24.1.1} reduces the force of attraction of the electron to the nucleus. The centripetal force \ssect{2.4.3} is found, to equal the difference ${F}_{e} - F_{L}$, where $F_{e}$ is the Coulomb force \ssect{14.2.2} of attraction
of an electron to the nucleus (\figr{fig-26-02}). This results in a change in the angular velocity $\pmb{\omega}$ \ssect{1.5.3} of the electron along its circular orbit.

\begin{marginfigure}%[-2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-26/fig-26-02.pdf}
\sidecaption{Forces on the electron.\label{fig-26-02}}
\end{marginfigure}

\subsection{}\label{26.2.2} The change in the angular velocity of the electron occurs in the process of growth of the magnetic field into which the atom is placed. This growth takes place during a finite length of time. At this an induced rotational electric field \ssect{25.1.1} is developed that acts on the electron in the atom. The strength $\vb{E}$ of this field is directed along a tangent to the electron’s orbit and the force acting on the electron is $\vb{F} = e \vb{E}$ \ssect{15.1.2}. 

\subsection{}\label{26.2.3} Upon an arbitrary position of the electron’s orbit with respect to vector $\vb{B}$, the orbital magnetic moment pm of the electron \ssect{26.1.2}) makes an angle $\alpha$ with the direction of the magnetic field (Fig. 26.3). In this case there will be a precessional motion of the orbit about the direction of vector $\vb{B}$ (\ssect{4.3.2}). This means that vector $\vb{p}_{m}$, perpendicular to the orbit and maintaining its angle $\alpha$ with the field constant, rotates about the direction, of vector $\vb{B}$ with the angular velocity $\omega_{L}$. Thus
\begin{align*}%
\omega_{L} & =  \frac{e B}{2m} && \text{(in SI units),} \\
\omega_{L} & =  \frac{e B}{2mc} && \text{(in Gaussian units)},
\end{align*}
Here $e$ is the magnitude of the charge of the electron, $m$ is its mass, $H$ is the magnetic field strength, $c$ is the electrodynamic constant \ssect{23.2.2}, and $\omega_{L}$ is called the \redem{angular velocity of Larmor precession}.

Larmor's theorem states that the only result of the effect of a magnetic field on an electron’s orbit in an atom is the precession
(in SI units),  of the orbit and vector $\vb{p}_{m}$ with the angular velocity $\omega_{L}$, about an axis through the nucleus of the atom and parallel to the induction vector $\vb{B}$ of the magnetic field.

\subsection{}\label{26.2.4} The precessional motion of the electron’s orbit results in an additional orbital current $\Delta \, I_{orb}$ (\figr{fig-26-03}) and the corresponding induced orbital magnetic moment $\Delta \, \vb{p}_{m}$ \ssect{26.1.2} with the magnitude
\begin{align*}%
\Delta \, p_{m} & =  \Delta \, I_{orb} S_{\perp} = \frac{e^{2} S_{\perp}}{4 \pi m} && \text{(in SI units),} \\
\Delta \, p_{m} & =  \frac{1}{c} \Delta \, I_{orb} S_{\perp} = \frac{e^{2} S_{\perp}}{4 \pi m c^{2}} && \text{(in Gaussian units)},
\end{align*}
where $S_{\perp}$ is the area of the projection of the orbit on a plane perpendicular to the direction of vector $\vb{B}$. 

\begin{figure}[h]%[-2cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-26/fig-26-03.pdf}
\sidecaption{Precession of the electron.\label{fig-26-03}}
\end{figure}


Vector $\Delta \vb{p}_{m}$ has the direction opposite to the magnetic induction vector $\vb{B}$. Thus
\begin{align*}%
\Delta \, \vb{p}_{m} & =  -\frac{e^{2} S_{\perp}}{4 \pi m} \vb{B}&& \text{(in SI units),} \\
\Delta \, \vb{p}_{m} & =  -\frac{e^{2} S_{\perp}}{4 \pi m c^{2}} \vb{B} && \text{(in Gaussian units)},
\end{align*}
The notation is the same as in \ssect{26.2.3}.

The total induced orbital moment $\Delta \vb{P}_{m}$ of the atom \ssect{26.1.5}
\begin{align*}%
\Delta \, \vb{P}_{m} & =  -\frac{e^{2}Z \expval{S_{\perp}}}{4 \pi m} \vb{B}&& \text{(in SI units),} \\
\Delta \, \vb{P}_{m} & =  -\frac{e^{2}Z \expval{S_{\perp}}}{4 \pi m c^{2}} \vb{B} && \text{(in Gaussian units)},
\end{align*}
where $Z$ is the number of electrons in the atom, and $\expval{S_{\perp}} = \sum_{i=1}^{z} S_{\perp \, i} /Z$ is the average area of the projections of the electron orbits on a plane perpendicular to the direction of vector $\vb{B}$.

\section[Magnetic Materials in a Magnetic Field]{Diamagnetic and Paramagnetic Materials in a Uniform Magnetic Field
}
\label{sec-26.3}

\subsection{}\label{26.3.1} The magnetization of matter is characterized by the \redem{magnetization vector} (\redem{magnetization intensity}) $\vb{M}$, which is the ratio of the magnetic moment of a small volume $\Delta V$ of the substance to the magnitude of this volume. Thus
\begin{equation*}%
\vb{M} = \frac{1}{\Delta V} \sum_{i =1}^{N} \vb{P}_{m\,i}
\end{equation*}
where $\vb{P}_{m\,i}$ is the magnetic moment of the $i$-th atom (or molecule), and $N$ is the total number of atoms (or molecules) in the small volume $\Delta V$. Within the limits of the volume $\Delta V$ the magnetic field is assumed uniform. This volume should simultaneously contain a sufficiently large number of particles $N$ (i.e. $N \gg 1$) to enable the physical quantities characterizing the system of particles to be expediently averaged.

\subsection{}\label{26.3.2} \redem{Diamagnetic materials} are substances in which the magnetic moments of the atoms (or molecules) equal zero in the absence of an external magnetic field. This means that in diamagnetic materials the vector sum of the orbital magnetic moments of all the electrons in an atom equals zero \ssect{26.1.5} and that induced magnetic moments \ssect{26.2.4} exist only in a magnetic field.

\subsection{}\label{26.3.3} In the volume $\Delta V$ of an isotropic diamagnetic material, the induced magnetic moments $\Delta \vb{}_{m}$ of all the atoms (or molecules) are the same and are opposite in direction to vector $\vb{B}$ \ssect{26.2.4}.

The magnetization vector $\vb{M}$ is equal to 
\begin{equation*}%
\vb{M} = \frac{N \Delta \vb{P}_{m}}{\Delta V}= n_{0} \vb{P}_{m},
\end{equation*}
and, taking \ssect{26.2.4} into account:
\begin{align*}%
\vb{M} & =  -\frac{n_{0} e^{2} Z \expval{S_{\perp}}}{4 \pi m} \, \vb{B}&& \text{(in SI units),} \\
\vb{M} & =  -\frac{n_{0} e^{2} Z \expval{S_{\perp}}}{4 \pi mc^{2}} \,  \vb{B} && \text{(in Gaussian units)},
\end{align*}
where $n_{0}$ is the number of atoms (or molecules) in unit volume. The other notation is the same as in \ssect{26.2.4}. Introducing the notation:
\begin{align*}%
\chi_{m}' & =  -\frac{n_{0} e^{2} Z \expval{S_{\perp}}\mu_{0}}{4 \pi m} && \text{(in SI units),} \\
\chi_{m}' & =  -\frac{n_{0} e^{2} Z \expval{S_{\perp}}}{4 \pi mc^{2}} && \text{(in Gaussian units)},
\end{align*}
we have g
\begin{align*}%
\vb{M} & =  \chi_{m}' \frac{\vb{B}}{\mu_{0}} && \text{(in SI units),} \\
\vb{M} & =  \chi_{m}' {\vb{B}}  && \text{(in Gaussian units)},
\end{align*}
where $\chi_{m}'$ is a dimensionless quantity that characterizes the magnetic properties of magnetic materials. For all diamagnetic
materials $\chi_{m}' < 0$.

\subsection{}\label{26.3.4} The \redem{magnetic susceptibility} $\chi_{m}$ is a quantity that is re­lated to $\chi_{m}'$ as follows:
\begin{align*}%
1 + \chi_{m} & =  \frac{1}{1 - \chi_{m}'} && \text{(in SI units),} \\
1 + 4\pi \, \chi_{m} & =  \frac{1}{1 - 4 \pi \,\chi_{m}'}  && \text{(in Gaussian units)},
\end{align*}
from which
\begin{align*}%
\chi_{m} & =  \frac{\chi_{m}'}{1 - \chi_{m}'} && \text{(in SI units),} \\
\chi_{m} & =  \frac{\chi_{m}'}{1 - 4\pi \, \chi_{m}'} && \text{(in Gaussian units)},
\end{align*}
Practically, $\chi_{m} = \chi_{m}'$ for diamagnetic materials, because in absolute value $\chi_{m}'$ is very small: $\abs{\chi_{m}'} \approx  \num{d-6}$.

\subsection{}\label{26.3.5} \redem{Paramagnetic materials} are substances in which the atoms (or molecules) have a certain constant magnetic moment $\vb{P}_{m}$ in the absence of an external magnetic field. This means that the vector sum of the orbital magnetic moments of all the electrons. of an atom (or. molecule) is not equal to zero \ssect{26.1.5}.

\subsection{}\label{26.3.6} When a paramagnetic material is placed into a uniform magnetic field \ssect{23.1.3}, the constant magnetic moments of the atoms (or molecules) precess about the direction of the induction vector $\vb{B}$ of the magnetic field at Larmor's angular velocity $\omega_{L}$ \ssect{26.2.3}.

Thermal motion and collisions of the1atoms (or molecules) of a paramagnetic material lead to a gradual damping of the precession of the magnetic moments and to a reduction in the angles between the directions of the magnetic moment vectors and vector $\vb{B}$. The combined actions of the interatomic collisions and the magnetic field lead to a predominant orientation of the magnetic moments of the atoms in alignment with the external field. 

Though the constant magnetic moment $\vb{P}_{m}$ of an atom (or molecule) is of the order of \SI{d-23}{\joule\per\tesla} (\SI{d-20}{\erg\per\gauss}, the magnetic moments of all the particles in unit volume produce a magnetization that considerably exceeds diamagnetic phenomena \ssect{26.3.3}. A paramagnetic material in an external magnetic field has an intrinsic magnetic field aligned with the external magnetic field.

\subsection{}\label{26.3.7} In the classical theory of paramagnetism the magnitude of the magnetization vector \ssect{26.3.1} is expressed by the formula
\begin{equation*}%
M= n_{0} P_{m} L(a),
\end{equation*}
where $n_{0}$ is the number of atoms (or molecules) in unit volume, and $L (a)$ is the classical Langevin function:
\begin{equation*}%
L(a) = \frac{e^{a} + e^{-a}}{e^{a}-e^{-a}} - \frac{1}{a}.
\end{equation*}
The parameter $a$ is of the form $a = P_{m}B/kT$, where $B$ is the magnetic induction of the field, $k$ is Boltzmann’s constant \ssect{8.4.5}, and $T$ is the absolute temperature. At room temperature and not especially strong external fields, $a \ll 1$ and the function $L (a)$ is, simplified, after expansion into a a series, and we obtain $L (a) \approx a/3$. Hence, the magnetization vector, is equal to
\begin{align*}%
\vb{M} & = \chi_{m}' \frac{\vb{B}}{\mu_{0}} && \text{(in SI units),} \\
\vb{M} & = \chi_{m}' \vb{B} && \text{(in Gaussian units)},
\end{align*}
where $\chi_{m}'$ is determined by the formula
\begin{align*}%
\chi_{m}' & =  \frac{n_{0}P_{m}^{2}\mu_{0}}{3kT} && \text{(in SI units),} \\
\chi_{m}' & =  \frac{n_{0}P_{m}^{2}}{3kT} && \text{(in Gaussian units)}.
\end{align*}
The quantity $\chi_{m}'$ is related to the magnetic susceptibility $\chi_{m}$ of paramagnetic materials by the formulas given in \ssect{26.3.4}.

The value of the quantity $\chi_{m}$ is positive for a paramagnetic
material and ranges from \num{d-5} to \num{d-3}; therefore, $\chi_{m}' = \chi_{m}$ with a high degree of accuracy.

The \redem{Curie law} states that the paramagnetic susceptibility of a substance is inversely proportional to the absolute tempera­ ture.

\subsection{}\label{26.3.8} In very strong external magnetic fields, saturation magnetization is reached; here $a \gg 1$ and the Langevin function $L (a) \to  1$. This means that the magnetic moments of all the atoms (or molecules) are aligned with the external magnetic field and $M = n_{0}P_{m}$.

\section{Magnetic Field in Magnetic Materials}
\label{sec-26.4} 

\subsection{}\label{26.4.1} Two types of currents that set up magnetic fields, macrocurrents and microcurrents, can be distinguished in matter. What we have called \redem{macrocurrents} here are conduction currents \ssect{20.1.2} and convection currents \ssect{20.1.2}. \redem{Microcurrents} (molecular currents) are those due to the motion of electrons in atoms, molecules and ions.

The magnetic field in a substance is the vector sum of two fields \ssect{15.2.2}: the \redem{external} magnetic field, set up by the macrocurrents, and the \redem{internal}, or \redem{intrinsic magnetic field}, set up by microcurrents. The magnetic induction vector $\vb{B}$ \ssect{23.1.2} in the magnetic field in a substance characterizes the resultant magnetic field and is equal to the vector sum of the magnetic inductions of the external $\vb{B}_{0}$ and intrinsic $\vb{B}_{intr}$ magnetic fields. Thus
\begin{equation*}%
\vb{B} = \vb{B}_{0} + \vb{B}_{intr}
\end{equation*}
The primary source of magnetic fields in magnetic materials are the macrocurrents. Their magnetic fields are what leads to the magnetization of substances placed in an external magnetic field.

\subsection{}\label{26.4.2} The \redem{total current law for a magnetic field in a substance} is a generalization of the law formulated in \ssect{23.5.2}:
\begin{align*}%
\oint_{L} \vb{B} \, \dd \vb{l} & =  \mu_{0} (I_{mic} + I_{mac}) && \text{(in SI units),} \\
\oint_{L} \vb{B} \, \dd \vb{l} & = \frac{4\pi}{c} \mu_{0} (I_{mic} + I_{mac}) && \text{(in Gaussian units)}.
\end{align*}
where $I_{mac}$ and $I_{mic}$ are the algebraic sums of the macro- and microcurrents passing through a surface bounded by the loop $L$. 

\subsection{}\label{26.4.3} The algebraic sum of the microcurrents is related to the magnetization vector $\vb{M}$ as follows:
\begin{align*}%
I_{mic} & =  \oint_{L} \vb{M} \, \dd \vb{l} && \text{(in SI units),} \\
I_{mic} & =  c \oint_{L} \vb{M} \, \dd \vb{l} && \text{(in Gaussian units)},
\end{align*}
where $\oint \vb{M} \, \dd \vb{l}$ is the circulation of the magnetization vector $\vb{M}$ \ssect{26.3.1} along the closed loop $L$ through which the microcurrents are threaded.

The final form of the total current law \ssect{26.4.2} is
\begin{align*}%
\oint_{L} \left(\frac{\vb{B}}{\mu_{0}} - \vb{M} \right) \, \dd \vb{l} & =  I_{mac} && \text{(in SI units),} \\
\oint_{L} \left(\vb{B} - 4\pi \vb{M} \right) \, \dd \vb{l} & =  \frac{4\pi}{c} I_{mac} && \text{(in Gaussian units)}.
\end{align*}

\subsection{}\label{26.4.4}

The vector
\begin{align*}%
\vb{H} & = \frac{\vb{B}}{\mu_{0}} - \vb{M} && \text{(in SI units),} \\
\vb{H} & = \vb{B} - 4 \pi \vb{M} && \text{(in Gaussian units)}.
\end{align*}
is called the \redem{strength of the magnetic field} existing in an arbitrary medium (cf. \ssect{23.2.3}). The total current, law for a magnetic field in an arbitrary medium is written in a form identical to that in \ssect{23.5.4}:
\begin{align*}%
\oint_{L} \vb{H} \, \dd \vb{l} & =  I_{mac} && \text{(in SI units),} \\
\oint_{L} \vb{H} \, \dd \vb{l} & =  \frac{4\pi}{c} I_{mac} && \text{(in Gaussian units)}.
\end{align*}
The circulation of the magnetic field strength vector along an arbitrary closed loop is equal (or proportional) to the algebraic sum of the macrocurrents through the surface hounded by this loop.

\subsection{}\label{26.4.5} For an isotropic medium, the relation between the magnetic induction vector $\vb{B}$ and the magnetization vector $\vb{M}$ \ssect{26.3.1} leads to the following result for vector $\vb{H}$ \ssect{26.4.4}:
\begin{align*}%
\vb{H} & =  (1 - \chi_{m}') \frac{\vb{B}}{\mu_{0}} && \text{(in SI units),} \\
\vb{H} & =  (1 - 4 \pi \chi_{m}') \vb{B} && \text{(in Gaussian units)}.
\end{align*}
Substituting for $(1 - \chi_{m}')$ or $(1 - 4\pi \chi_{m}')$ on the basis of Sect. 26.3.4, we have
\begin{align*}%
\vb{H} & =   \frac{\vb{B}}{\mu_{0}\mu_{r}} && \text{(in SI units),} \\
\vb{H} & =  \frac{\vb{B}}{\mu_{r}} && \text{(in Gaussian units)}.
\end{align*}
where
\begin{align*}%
\mu_{r} & =  1 + \chi_{m} && \text{(in SI units),} \\
\mu_{r} & =  1 + 4\pi \chi_{m}  && \text{(in Gaussian units)}.
\end{align*}
The quantity $\mu_{r}$ thus introduced is called the \redem{relative magnetic permeability} of the substance (see also \ssect{23.2.1}). In these formulas $\chi_{m}$ is the magnetic susceptibility \ssect{26.3.4}.


\section{Ferromagnetic Materials}
\label{sec-26.5} 

\subsection{}\label{26.5.1} \redem{Ferromagnetic materials} are substances in which the intrinsic (internal) magnetic field \ssect{26.4.1} is hundreds or even thousands of times stronger than the external magnetic field applied to the substance.

Ferromagnetism is observed in the crystals of the transition metals \ssect{39.3.8}, iron, cobalt and nickel and of certain alloys, provided that $d/a  \gg 1.5$, where $d$ is the diameter of the atom and $a$ is the diameter of the unfilled electronic shell of the atom \ssect{39.3.6}.


\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-26/fig-26-04.pdf}
\sidecaption{The dependence of the magnetization \ssect{26.3.1} on the strength $H$ of the external magnetic field and magnetic saturation $M_{s}$.\label{fig-26-04}}
\end{marginfigure}


\subsection{}\label{26.5.2} The principal properties of ferromagnetic materials, distinguishing them from other magnetic materials are:
\begin{enumerate}[label = (\alph*)]
\item The dependence of the magnetization \ssect{26.3.1} on the strength $H$ of the external magnetic field is characterized by \redem{magnetic saturation} $M_{s}$, which begins at $H \geqslant H_{s}$ (\figr{fig-26-04}).
\item The dependence of the magnetic induction $\vb{B}$ on $\vb{H}$ is characterized by the increase according to a linear law at $H \geqslant H_{s}$ (\figr{fig-26-05}).
\begin{marginfigure}%[2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-26/fig-26-05.pdf}
\sidecaption{The dependence of the magnetic induction $\vb{B}$ on $\vb{H}$.\label{fig-26-05}}
\end{marginfigure}

\item The dependence of the relative magnetic permeability $\mu_{r}$ on the field strength $H$ is of a complex nature (\figr{fig-26-06}). 
\item \redem{Magnetic hysteresis} exists in ferromagnetic materials. This refers to the lag in the variation of magnetization from the variation in the strength of the external magnetizing field, which is variable in magnitude and direction. This lag is due to the dependence of $M$ on its previous values (past history) in magnetizing the substance.
\begin{marginfigure}%[-2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-26/fig-26-06.pdf}
\sidecaption{The dependence of the relative magnetic permeability $\mu_{r}$ on the field strength $H$.\label{fig-26-06}}
\end{marginfigure}
\item The properties of ferromagnetic materials enumerated above\sidenote[][4cm]{As well as many others that are beyond the scope of this handbook.} are observed at temperatures below the \redem{Curie point} $\theta_{C}$ \ssect{18.4.4}. At temperatures $T \geqslant \theta_{C}$, thermal motion breaks up the regions of spontaneous magnetization \ssect{26.5.4} and the ferromagnetic material, losing its special features, is converted into a paramagnetic substance \ssect{26.3.5}. The Curie points for iron, nickel, cobalt and Permalloy are 1043, 631, 1403 and 823 K.
\end{enumerate}



\subsection{}\label{26.5.3} The \redem{hysteresis loop} is a curve showing the change in the magnetization of a ferromagnetic material located in an external magnetic field as the strength of this field is varied from $+H_{s}$ to $-H_{s}$ and back again, where $H_{s}$ is the field strength corresponding to magnetic saturation (\figr{fig-26-07}). The magnetization $\pm M_{s}$ at $H = \pm H_{s}$ is called \redem{saturation magnetization}. The magnetization $\pm M_{r}$ that remains in a ferromagnetic material in the absence of an external field $(H = 0)$ is called the \redem{residual}, or \redem{remanent, magnetization}. This factor is the basis for making permanent magnets. 

\begin{marginfigure}%[-2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-26/fig-26-07.pdf}
\sidecaption{The hysteresis loop of a ferromagnetic material.\label{fig-26-07}}
\end{marginfigure}


The strength $\pm H_{c}$ of the external field that completely demagnetizes a substance is called the \redem{coercive force}. The coercive force specifies the property of a ferromagnetic material to retain its residual magnetization. \redem{Hard}, or \redem{retentive, magnetic materials} have a high coercive force. They have a wide hysteresis loop and are used to make permanent magnets. \redem{Soft}, or \redem{non-retentive, magnetic materials} have a low coercive force. They have a narrow hysteresis loop and are used to make trans­ former cores.

The reversal of magnetization is associated with a change in the orientation of the regions of spontaneous magnetization \ssect{26.5.4} and requires that work be done at the expense of the energy of the external magnetic field. The amount of heat evolved in reversing magnetization is proportional to the area of the hysteresis loop.

\subsection{}\label{26.5.4} At temperatures below the Curie point \ssect{26.5.2} a ferromagnetic material is made up of \redem{domains}, which are small \redem{regions of uniform spontaneous magnetization}. The linear dimensions of these domains range from \num{d-5} to \SI{d-4}{\meter}. Within each domain the substance is magnetized to saturation \ssect{26.5.2}. In the absence of an external magnetic field, the magnetic moments of the various domains are oriented in space so that the resultant magnetic moment of the whole ferromagnetic body equals zero.

Owing to the effect of the external magnetic field, the magnetic moments of whole domains are oriented in a ferromagnetic material, rather than those of the separate particles as in the case of paramagnetic materials \ssect{26.3.6}. As a result, the sub­ stance is magnetized.

Ferromagnetic properties can only exist in the crystalline state of substances in which the interaction between the neighbouring atoms of the crystal lattice leads to the total energy of the system of electrons that provides for compliance with the conditions for the existence of ferromagnetism \ssect{26.5.1}.

\subsection{}\label{26.5.5} Measurement of the gyromagnetic ratio \ssect{26.1.4} for ferromagnetic bodies indicates that the elementary carriers of magnetism in ferromagnetic materials are the spin magnetic moments of the electrons \ssect{26.1.4}. The modern quantum mechanical theory of ferromagnetism explains the nature of the spontaneous magnetization of ferromagnetic materials \ssect{26.5.4} and the reasons for the development of a strong intrinsic magnetic field \ssect{26.4.1}.

Ferromagnetic properties are possessed by crystals of substances whose atoms have inner shells and subshells \ssect{39.3.6} unfilled by electrons and having a nonzero value of the projections of the resultant spin moment on the direction of the magnetic field \ssect{26.1.3}. Developed between the spins of such electrons is a specific kind of quantum mechanical interaction that is not of a magnetic origin. It is called exchange interaction \ssect{39.4.5}. As a result, the state of the system of electrons in fer­ romagnetic materials is found to be stable upon parallel align­ ment of the spins, and spontaneous magnetization \ssect{26.5.4} is developed along with a strong intrinsic magnetic field.
