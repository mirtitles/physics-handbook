% !TEX root = handbook-physics.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode

\chapter{Motion in non-inertial Frames of Reference}
\label{ch-07}

\section{Kinematics of Relative Motion}
\label{sec-7.1}
\subsection{}\label{7.1.1} It is assumed in classical (Newtonian) mechanics that distances and time intervals remain unchanged in going over from one frame of reference to any other frame that is in motion in even the most arbitrary manner with re­spect to the first frame. For example, assume that $K$ is 
an inertial frame with origin at point $O^{*}$ and $S$ is a \emph{non-inertial} frame with its origin at point $O$ (\figr{fig-07-01}).
\begin{figure}[!ht]
%\begin{wrapfigure}{r}{0.5\linewidth}
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-07/fig-07-01.pdf}
%\input{figs/ch-03/fig-03-01}
%  \vspace{-13pt}
\caption{Inertial and non-inertial frames of reference.}
\label{fig-07-01}
\end{figure}

In the general case, the mo­tion of frame $S$ with respect to frame $K$ can be regarded as the sum of two motions: translational motion of point $O$ at the velocity $\vb{v}_{0}$ and rotation about this point at the angular velocity $\pmb{\Omega}$. Radius vectors $\vb{r}^{*}$ and $\vb{r}$ of arbitrary particle $M$, measured in frames $K$ and $S$, are related by the equation
\begin{equation*}%
\vb{r}^{*} = \vb{r}^{*}_{0} + \vb{r}
\end{equation*}
where $\vb{r}^{*}_{0}$ is the radius vector of point $O$ measured in frame of reference $K$.

\subsection{}\label{7.1.2} The motion of particle $M$ with respect to any inertial frame of reference $K$, conditionally assumed to be fixed, is called the \emph{absolute motion} of particle $M$. The motion of the same particle with respect to the non-inertial frame of refer­ence $S$ is said to be \emph{relative motion}.

The \emph{relative velocity} $\vb{v}_{r}$ of particle $M$, i.e. its velocity with respect to frame $S$, equals
\begin{equation*}%
\vb{v}_{r} = \dv{x}{t} \vb{i} +\dv{y}{t} \vb{j} + \dv{z}{t} \vb{k} 
\end{equation*}
where $x$, $y$ and $z$ are the Cartesian coordinates of particle $M$, and $\vb{i}$, $\vb{k}$ and $\vb{k}$ are the unit vectors of the coordinate axes in frame $S$.

The \emph{absolute velocity} of particle $M$, i.e. its velocity $\vb{v}$ in the frame of reference $K$, equals
\begin{equation*}%
\vb{v} = \dv{\vb{r}^{*}}{t}= \vb{v}_{0} + x\dv{\vb{i}}{t}  + y\dv{\vb{j}}{t}  + z \dv{ \vb{k}}{t} + \vb{v}_{r}
\end{equation*}
where $\vb{v}_{0}= \dv*{\vb{r}^{*}_{0}}{t}$ is the absolute velocity of point $O$. Since the unit vectors of moving frame $S$ can change in frame of reference $K$ only due to the rotation of frame $S$ about point $O$ at the angular velocity $\pmb{\Omega}$,
\begin{align*}%
\dv{\vb{i}}{t} & = \qty[\pmb{\Omega} \vb{i}], \quad \dv{\vb{j}}{t} = \qty[\pmb{\Omega} \vb{j}], \quad \dv{\vb{k}}{t} = \qty[\pmb{\Omega} \vb{k}] \\
\intertext{and}
\vb{v} & =  \vb{v}_{t} + \vb{v}_{r}
\end{align*}
where $\vb{v}_{t} = \vb{v}_{0}+ \qty[\pmb{\Omega} \vb{k}]$ is the \emph{velocity of transportation} of par­ticle $M$. It is also called the \emph{bulk}, or \emph{drift}, \emph{velocity} and is equal to the absolute velocity of the point of moving frame of ref­erence $S$ (i.e. rigidly fixed in this frame) in which particle $M$ is located at the given instant of time.

\subsection{}\label{7.1.3} The \emph{relative acceleration} $\vb{a}_{r}$ of particle $M$ (its acceleration with respect to frame $S$) equals
\begin{equation*}%
\vb{a}_{r} = \dv[2]{x}{t} \vb{i} +\dv[2]{y}{t} \vb{j} + \dv[2]{z}{t} \vb{k} 
\end{equation*}
The \emph{absolute acceleration} of particle $M$, i.e. its acceleration with respect to frame $K$, equals
\begin{equation*}%
\vb{a} = \dv{\vb{v}}{t} = \vb{a}_{t} + \vb{a}_{C}  + \vb{a}_{r}
\end{equation*}
Here
\begin{equation*}%
\vb{a}_{t} = \dv{\vb{v}_{0}}{t} + \left[ \dv{\pmb{\Omega}}{t} \vb{r} \right] + \qty[\pmb{\Omega}[\pmb{\Omega}\vb{r}]]
\end{equation*}
is the \emph{acceleration of transportation} of particle $M$. It is equal to the absolute acceleration of the point in moving frame $S$ in which particle $M$ is located at the given instant of time, and
\begin{equation*}%
\vb{a}_{C} = 2\qty[\pmb{\Omega}\vb{v}_{r}]
\end{equation*}
is the \emph{Coriolis acceleration} of particle $M$. The Coriolis accelera­tion has its maximum value if the relative velocity $\vb{v}_{r}$ of the particle is perpendicular to the vector $\pmb{\Omega}$ of angular velocity of the moving frame of reference. It is equal to zero if the angle between vectors $\vb{v}_{r}$ and $\pmb{\Omega}$ equals zero or $\pi$, or if even one of these vectors equals zero.

\section{Inertial Forces}
\label{sec-7.2}

\subsection{}\label{7.2.1} Newton’s laws of motion are not valid in non-inertial frames of reference. In particular, a particle can change the slate of its motion with respect to non-inertial frame S without any action being exerted on the particle by other bodies. A ball, for instance, suspended by a string from the ceiling of a railway coach of a train travelling at uniform velocity in a straight line, deviates backward when the motion of the train is accel­erated and frontward when the motion is decelerated, i.e. it begins to move with respect to a non-inertial frame of reference attached to the coach. But no horizontal forces act on the ball when it deviates.

\subsection{}\label{7.2.2} The fundamental law of dynamics for a particle in non-inertial frames of reference can be derived from Newton’s second law and the relation between the absolute and relative accel­erations of a particle. It follows from \ssect{7.1.3} that the product of the mass $m$ of a particle by its relative acceleration equals
\begin{equation*}%
m \vb{a}_{r} = m \vb{a} - m \vb{a}_{t} - m \vb{a}_{C}
\end{equation*}
According to Newton’s second law, formulated to apply to the absolute motion of a particle, i.e. its motion with respect to inertial frame $K$,
\begin{equation*}%
m \vb{a} = \vb{F}
\end{equation*}
where $\vb{F}$ is the vector sum of all forces acting on the particle. Hence, the \emph{fundamental equation of dynamics of a particle's relative motion} is of the form:
\begin{equation*}%
m \vb{a}_{r} = \vb{F} -  m \vb{a}_{t} - m \vb{a}_{C}
\end{equation*}
This equation can be reduced to a form similar to the funda­mental law of dynamics for the absolute motion of a particle:
\begin{equation*}%
m \vb{a}_{r} = \vb{F} + \vb{I}_{t} + \vb{I}_{C}
\end{equation*}
The vector quantities $\vb{I}_{t} = - m\vb{a}_{t}$ and $\vb{I}_{C} = - m\vb{a}_{C}$ have the dimensionality of force and are called the \emph{inertial force of trans­portation} and the \emph{Coriolis inertial force}, respectively.

\subsection{}\label{7.2.3} It follows from \ssect{7.1.3} that in the general case the inertial force of transportation is the sum of three terms:
\begin{equation*}%
\vb{I}_{t} = -m \dv{\vb{v}_{0}}{t} - m \left[ \dv{\pmb{\Omega}}{t} \vb{r} \right] - m\qty[\pmb{\Omega}[\pmb{\Omega}\vb{r}]]
\end{equation*}
The last term on the right-hand side of this equation
\begin{equation*}%
\vb{I}_{c\!f} = - m\qty[\pmb{\Omega}[\pmb{\Omega}\vb{r}]]
\end{equation*}
is called the \emph{centrifugal force of inertia}, or simply the \emph{centrifugal force}, because this vector is perpendicular to the instantaneous axis of rotation (to vector $\pmb{\Omega}$) of the non-inertial frame $S$ and its direction is away from this axis. Numerically the centri­fugal force equals
\begin{equation*}%
I_{c\!f} = - m\Omega^{2} \rho
\end{equation*}
where $\rho$ is the distance from a particle of mass $m$ to the instan­taneous axis of rotation of the frame of reference.

The inertial force of transportation coincides with the centri­fugal force when the non-inertial frame of reference travels with translational motion at constant velocity $(\vb{v}_{0} = \textrm{const})$ and rotates at constant angular velocity $(\pmb{\Omega} = \textrm{const})$.

\subsection{}\label{7.2.4} The Coriolis inertial force is 
\begin{equation*}%
I_{C} = 2 m \qty[\vb{v}_{r}\pmb{\Omega}]
\end{equation*}
This force acts on the particle only when the non-inertial frame of reference rotates and the particle moves with respect to this frame. Thus, for example, particles of water in rivers of the Northern Hemisphere that flow meridionally are subject to the action of a Coriolis inertial force. This force is perpen­dicular to the velocity of flow of the river and leads to under­ mining the right bank in the direction of flow.

The Coriolis inertial force does no work in the relative motion of a particle because the force is perpendicular to the velocity of relative motion of the particle. Hence, the Coriolis inertial force is an example of the gyroscopic forces \ssect{3.1.7}.

\subsection{}\label{7.2.5} The action of inertial forces on a particle in a non-inertial frame of reference are real and can be measured in this frame by, for example, a spring dynamometer. But, in contrast to ordinary forces of interaction between bodies, it is impossible lo indicate for inertial forces the action of what specific bodies on the particle being considered they express. This feature of inertial forces is due to the fact that the very occurrence of the vector quantities $\vb{I}_{t}$ and $\vb{I}_{C}$ in the fundamental equation of the dynamics of relative motion results only from the non-inertial nature of the frame of reference used to describe the relative motion of the particle. The addition of the inertial forces $\vb{I}_{t}$ and $\vb{I}_{C}$ to the force $\vb{F}$, which characterizes the action on the particle of all the other bodies, enables the fundamental equa­tion of the dynamics of relative motion to be written in a form that resembles the expression for Newton’s second law in an inertial frame of reference.

There can be no closed systems of bodies in non-inertial frames of reference because the inertial forces are always external forces for any of the bodies in the system. Therefore, the laws of conservation of momentum \ssect{2.7.1}, of angular momentum \ssect{4.4.1} and of energy \ssect{3.4.2} are not valid in non-inertial frames of reference.

\section[Frame of Reference Fixed to the Earth]{Relative Motion in a Frame of Reference Fixed to the Earth. Gravity Force and Weight of a Body}


\subsection{}\label{7.3.1} A frame of reference fixed in the earth is non-inertial lor two reasons: firstly, due to the diurnal, or daily, rotation of the earth at constant angular velocity $\pmb{\Omega}$ (where $\Omega = 2 \pi \,\, \textrm{rad per 24 hours}\,\, = \SI{7.3e-5}{\radian\per\second}$) and, in the second place, due to the effects of the gravitational field of the sun, moon, planets and other astronomical bodies on the earth. This gravi­tational field is practically uniform within the limits of the earth and imparts the same acceleration of translational motion to the terrestrial frame of reference and all bodies moving with respect to this frame. This acceleration is $\vb{a}_{0}= \dv*{\vb{v}_{0}}{t}= \vb{g}_{f}$, where $\vb{g}_{f}$ is the intensity of the field \ssect{6.2.1}.

It follows from the equations in \ssect{7.2.2} and \ssect{7.2.3} that the equation of relative motion of a particle of mass $m$ in a frame of reference fixed to the earth is of the form
\begin{equation*}%
m \vb{a}_{r} = \vb{F} + \vb{F}_{g\!r} + \vb{I}_{c\!f} + \vb{I}_{C}
\end{equation*}
where $\vb{I}_{c\!f}$ and $ \vb{I}_{C}$ are the cen­trifugal and Coriolis inertial forces. $\vb{F}_{g\!r}$ is the gravitation­ al attraction of the earth for the particle \ssect{6.1.3}, and $\vb{F}$ is the sum of all the other forces acting on the particle, with the exception of the gravitational forces. 
\begin{figure}[!ht]
%\begin{wrapfigure}{r}{0.5\linewidth}
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-07/fig-07-02.pdf}
%\input{figs/ch-03/fig-03-01}
%  \vspace{-13pt}
\caption{The Coriolis force in a non-inertial frame of reference.}
\label{fig-07-02}
\end{figure}

\subsection{}\label{7.3.2} The \emph{gravity force of a body} is the force P, applied to the body and equal to the vector sum of the force $\vb{F}_{g\!r}$ of gravitational attraction of the earth for the body \ssect{6.1.3} and the centrifugal inertial force $\vb{I}_{c\!f}$ due to the diurnal rotation of the earth (\figr{fig-07-02}):
\begin{equation*}%
\vb{P} = \vb{F}_{g\!r} + \vb{I}_{c\!f}
\end{equation*}
i.e.
\begin{equation*}%
\vb{P} = - G \frac{mM_{\Earth}}{r^{3}}\vb{r} - m \qty[\pmb{\Omega}[\pmb{\Omega}\vb{r}]]
\end{equation*}
Here $m$ and $M_{\Earth}$ are the masses of the body and the earth, $\vb{r}$ is the radius vector drawn from the centre of the earth to the place where the body is located, $\pmb{\Omega}$ is the angular velocity of diurnal rotation of the earth, and $G$ is the gravitational constant \ssect{6.1.1}.

The gravity force causes an unsupported or unfastened body to fall to the earth. It equals the force with which the body presses against a horizontal support (or pulls on a vertical suspension) due to the gravitation attraction of the earth. It can be measured in the terrestrial frame of reference by means of a spring dynamometer. The point of application of the gravity force acting on a body, i.e. the point of appli­cation of the resultant of all the gravity forces acting on all the particles of the body is called the \emph{centre of gravity of the body}. It coincides with its centre of mass \ssect{2.3.3}.

\subsection{}\label{7.3.3} The gravity force of a body is independent of its velocity of relative motion. It is proportional to the mass $m$ of the body and can be presented in the form
\begin{equation*}%
\vb{P} = m\vb{g},
\end{equation*}
where $\vb{g}$ is the \emph{acceleration due to gravity}, or \emph{free fall acceleration} (see \ssect{7.3.5}). At a given place on earth, vector $\vb{g}$ is the same for all bodies and changes for other places.

The gravity force of a body coincides with the gravitational attraction of the earth for the body only at the earth’s North and South poles because there the centrifugal inertial force $\vb{I}_{c\!f} = 0$. The greatest difference between the gravity force and the gravitational attraction is observed at the equator, where the force $\vb{I}_{c\!f}$ reaches its maximum value and is opposite in direction to $\vb{F}_{g\!r}$. Even at the equator, however, the gravity force and the force of gravitational attraction differ by only 0.35 per cent. At all points of the earth’s surface, except the equator and the poles, forces $\vb{P}$ and $\vb{F}_{g\!r}$ fail to coincide in direc­tion as well (\figr{fig-07-02}), but the maximum angle between them does not exceed 6 minutes of arc. The gravity force decreases with the altitude. Near the earth’s surface this decrease is approximately 0.034 per cent per kilometre above the surface. 

\subsection{}\label{7.3.4} The acceleration $\vb{g}$ near the earth’s surface varies from \SI{9.78}{\meter\per\second\squared} at the equator to \SI{9.83}{\meter\per\second\squared} at the North and South poles. This is due, in the first place, to the dependence of the centrifugal inertial force on the geographical latitude of the location and, in the second, to the non-spherical shape of the earth, which is slightly flattened at the poles of its axis of rotation, having the shape of an ellipsoid of rotation, or sphe­roid (the polar and equatorial radii of the earth are $R_{p} = \SI{6357}{\kilo\meter}$ and $R_{e} = \SI{6378}{\kilo\meter}$). The \emph{standard value of free fall acceleration}, accepted in establishing a system of units and for barometric computation, is \SI{9.80665}{\meter\per\second\squared}.

\subsection{}\label{7.3.5} The \emph{free fall of a body} is its motion due to the action of the gravitational held alone.

The free fall acceleration of a body falling to the earth, mea­ sured in a non-inertial frame of reference rotating together with the earth, can he found by the motion equation \ssect{7.3.1} by putting $\vb{F} = 0, \,\, \vb{F}_{g\!r} + \vb{I}_{c\!f} = m\vb{g}$ and $I_{C} = 2m \qty[\vb{v}_{r}\pmb{\Omega}]$. Then
\begin{equation*}%
\vb{a}_{r}= \vb{g} + 2\qty[\vb{v}_{r}\pmb{\Omega}]
\end{equation*}
If $\vb{v}_{r} = 0, \,\, \vb{a}_{r} = \vb{g}$.

Consequently, vector $\vb{g}$ is equal to the free fall acceleration of the body, measured with respect to the terrestrial frame of reference at the instant when the relative velocity of the body equals zero. For this reason, vector $\vb{g}$ is called the \emph{free fall acceleration}.

If the relative velocity of a freely falling body $\vb{v}_{r}=0$, its acceleration with respect to the earth is not $\vb{g}: \vb{g} = \vb{a}_{r}+ 2\qty[\vb{v}_{r}\pmb{\Omega}]$. At velocities $v_{r} < \SI{680}{\meter\per\second}$, however, the values of $\vb{g}$ and $\vb{a}_{r}$ differ by less than one per cent. It can therefore be assumed in many cases that with respect to an observer on the earth free fall of a body is caused only by the gravity force acting on and imparting the acceleration g to the body. In a similar manner the action of the Coriolis inertial force on a freely falling body can be treated as a comparatively small perturbation. Thus, for instance, a freely falling body is deviated by the Coriolis force to the east of the direction assumed by a plumb line, i.e. from the direction of the vector $\vb{P} = m\vb{g}$. This devia­tion for a body falling freely without any initial velocity from the height $h$ is equal, at the latitude $\varphi$, to
\begin{equation*}%
s = \frac{2}{3} \Omega h \sqrt{\dfrac{2h}{g}} \cos \varphi
\end{equation*}
If, for example, $h = \SI{160}{\meter}$ and $\varphi = \ang{45},\,\, s = \SI{1.55}{\centi\meter}$. 

\subsection{}\label{7.3.6} The \emph{weight of a body} is the force $\vb{Q}$ with which it acts, due to the earth’s gravity, on a support or suspension that prevents it from falling freely. It is assumed that the body and its support (or suspension) are fixed with respect to the frame of reference in which the weight is determined. The force $-\vb{Q}$ is exerted by the support or suspension on the body. It follows from the fundamental equation of dynamics of rela­tive motion \ssect{7.2.2}, where $\vb{a}_{r} =\vb{a}_{C} = 0$ and $\vb{F} = \vb{F}_{g\!r} - \vb{Q}$, that
\begin{equation*}%
\vb{Q} = \vb{F}_{g\!r} + \vb{I}_{t}
\end{equation*}
where $\vb{F}_{g\!r}$ is the force of gravitational attraction of the earth for the body, and $\vb{I}_{t}$ is the inertial force of transportation due to the non-inertial nature of the frame of reference. 

\noindent\hlblue{Example 1.} The weight of a body in a frame of reference fixed to the earth is equal to the gravity force of the body \ssect{7.3.2}:
\begin{equation*}%
\vb{Q} = \vb{F}_{g\!r} + \vb{I}_{c\!f} = \vb{P}
\end{equation*}
\hlblue{Example 2.} The weight of a body in a frame of reference at­tached to a lift (elevator) which travels with translational motion with respect to the earth with the acceleration $\vb{a}_{0}$:
\begin{equation*}%
\vb{Q} = \vb{P} - m \vb{a}_{0}
\end{equation*}
If the lift falls freely, $\vb{a}_{0}= \vb{g}$, the free fall acceleration, and the weight of the body in the lift is $\vb{Q} = 0$, i.e. the body is in the state of weightlessness.

\subsection{}\label{7.3.7} \emph{Weightlessness}, or zero gravity, is the state of a mechan­ical system in which the external gravitational field acting on the system does not cause any pressure of the parts against one another or their deformation. Such a state occurs in a me­chanical system complying with the following three conditions: 
\begin{enumerate}[label=(\alph*)]
\item no other external forces, except those of the gravitational field, act on the system; 
\item the size of the system is such that within its limits the external gravitational field can be regarded
as being uniform; and 
\item the system is in translational motion. 
\end{enumerate}
The state of weightlessness is characteristic, for instance, of bodies in a spaceship because, during the main part of its path in a gravitational field, it is in free flight, i.e. coasting with the engines turned off.

\section{Principle of Equivalence}
\label{sec-7.4}

\subsection{}\label{7.4.1} The inertial forces acting on bodies in a non-inertial frame of reference are proportional to the masses of the bodies and, other conditions being equal, impart the same relative accelerations \ssect{7.1.3} to them. In other words, all bodies free of external action travel in an ``inertial force field'', i.e. with respect to a non-inertial frame of reference, in absolutely the same way, provided that the initial conditions of their motion are also the same. A similar behaviour is observed for bodies travelling with respect to an inertial frame of reference and

subject to the action of the forces of a gravitational field. At each point in the field these forces, like the inertial forces, are proportional to the masses of the bodies and impart the same free fall acceleration to them. This acceleration is equal to the field intensity \ssect{6.2.1} at the point being considered.
In a non-inertial frame of reference, for example, fixed to a lift (elevator) travelling with uniform acceleration vertically upward at the acceleration of transportation $\vb{a}_{0}= \textrm{const}$, all free bodies fall in the absence of a gravitational field with the same relative acceleration $\vb{a}_{r} = -\vb{a}_{0}$. Free bodies behave in exactly the same way in the same lift when it travels at uni­form velocity in a uniform gravitational field of the intensity $\vb{g}_{f} = -\vb{a}_{0}$. Thus, on the basis of experiments conducted on the free fall of bodies within a tightly closed lift, it is impossible to establish whether the lift is travelling at uniform velocity in a gravitational field of the intensity $\vb{g}_{f} = \vb{a}_{r}$ (in partic­ular, the lift may also be at rest in this held) or whether it is travelling with constant acceleration of transportation $\vb{a}_{t} = -\vb{a}_{r}$ in the absence of a gravitational field.

\subsection{}\label{7.4.2} The \emph{local equivalence principle} states that the gravitational field in a limited region of space is equivalent to an ``inertial force held'' in a properly chosen non-inertial frame of reference. The region of space should be small enough so that the gravitational held in it can be assumed to be uniform.

The equivalence principle is not to be understood as a state­ment of the identity of inertial forces and Newton’s gravita­tional forces that act between bodies. As a matter of fact, the intensity of a true gravitational held, set up by bodies, decreases as the bodies recede farther and farther away, and approaches zero at infinity. Gravitational fields ``equivalent'' to iner­tial forces do not comply with this condition. For example, the intensity of a gravitational held, ``equivalent'' to the centri­fugal inertial forces in a rotating frame of reference increases without limit at points farther and farther away from the axis of rotation. The intensity of a held ``equivalent'' to the inertial forces of transportation in a frame of reference having transla­tional motion is the same at all points of the held.

\subsection{}\label{7.4.3} A true gravitational held, in contrast to one ``equivalent'' to inertial forces, exists in both non-inertial and inertial frames of reference. No possible choice of a non-inertial frame of ref­erence can completely eliminate a true gravitational held, i.e. compensate for it throughout space by means of a ``field of inertial forces''. This follows in any case, even if only from the different behaviour of ``fields of inertial forces'' and true gravitational fields at infinity. A true gravitational field can be eliminated only locally, i.e. for a small region of space within whose limits the field can he assumed uniform, and for a short time interval during which the field can be assumed constant. The non-inertial frame of reference corresponding to this operation should travel with an acceleration of transpor­tation equal to the free fall acceleration of bodies in the region of the true gravitational field being considered. Thus, in a space ship in free flight in a gravitational field, the gravitational forces of attraction are compensated for by the inertial forces of transportation and do not lead to the relative motion of bodies in the ship.