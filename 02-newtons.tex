% !TEX root = handbook-physics.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter{Newton's Laws}
\label{ch-02}

\section{Newton's First Law. Inertial Frames of Reference}
\label{sec-2.1}
\subsection{}\label{2.1.1} As the \redem{first law of dynamics} Newton took a law that had already been established by Galileo: a particle persistsing its state of rest or uniform motion in a straight line until the action of other bodies compel it to change that state.

Newton’s first law indicates that no external action is required to maintain a state of rest or of uniform motion in a straight line. This manifests a special dynamic property of bodies, called their \redem{inertia}. Newton’s first law is consequently called the \redem{law of inertia}, and the motion of a body not subject to the action of other bodies is said to be \redem{inertial motion}. 

\subsection{}\label{2.1.2}Mechanical motion is relative: its nature may differ for different frames of reference \ssect{1.2.1} that are moving in relation to each other. An astronaut, for instance, on board a man-made satellite orbiting the earth, may be at rest in a reference frame fixed in the satellite. At the same time, he is travelling together with the satellite with respect to the earth along an elliptic or­bit, i.e. not with uniform motion and not in a straight line. It is thus obvious that Newton’s first law does not hold for any frame of reference. A ball, for example, lying on the smooth floor of a stateroom in a ship travelling with uniform motion at sea in a straight line, may suddenly start moving along the floor without being subject to the action of any other bodies. This will occur when the velocity of the ship, i.e. its speed and/or direction, begins to change.

A frame of reference with respect to which a particle, not sub­ject to external action, remains at rest or moves uniformly in a straight line is called an \redem{inertial frame of reference}. The content of Newton’s first law reduces, in essence, to two statements: in the first place, that all material bodies possess the property of inertia, and secondly, that inertial reference frames exist.


\subsection{}\label{2.1.3}Any two inertial reference frames can move only with translational motion with respect to each other, and only uniformly in a straight line. It has been experimentally established that the \redem{heliocentric frame of reference} is practically inertial. The origin of its coordinate system is located at the centre of mass \ssect{2.3.3} of the solar system (approximately at the centre of the sun) and the three axes extend in the directions of three dis­tant stars selected, for example, so that these coordinate axes are mutually perpendicular.

A \redem{laboratory frame of reference}, whose coordinate axes are rigidly fixed in the earth, is noninertial chiefly owing to the earth’s diurnal (daily) rotation. But the earth rotates so slowly that the maximum normal acceleration \ssect{1.4.6} of points on its sur­face due to diurnal rotation does not exceed \SI{0.034}{\meter\per\second\squared}. There­fore, in the great majority of practical problems, the laboratory reference frame can be regarded as being approximately iner­tial.

\subsection{}\label{2.1.4} Inertial reference frames are of prime importance in all branches of physics, not only mechanics. This is due to the fact that according to Einstein’s principle of relativity \ssect{5.1.2} the mathematical expression of any physical law should have the same form in ail inertial reference frames. Hence, in the following we shall make use of only inertial frames of reference, without stipulation each time. The laws of motion of a particle with respect to a noninertial reference frame is dealt with in \textcolor{IndianRed}{Chapter~\ref{ch-07}}.

\section{Force}
\label{sec-2.2}

\subsection{}\label{2.2.1}Force is a vector quantity which is a measure of the mechan­ical action exerted on the body being considered by other bodies. Mechanical interaction can be accomplished either be­ tween bodies in direct contact (for instance, in friction, and in the pressure of bodies against one another), or between remote bodies. A special form of matter, connecting particles of matter into unified systems and transmitting the action of certain par­ticles on others at finite velocities, is called a \redem{physical field}, or simply \redem{field}. Interaction between remote bodies is accomplished by the gravitational or electromagnetic fields they set up (for example, the attraction of the planets to the sun, and the inter­ action of charged bodies or current-carrying conductors). Me­chanical action exerted on a given body by other bodies can be manifested in two ways. In the first place, it can lead to a change in the state of mechanical motion of the body being consid­ered, and secondly, to its deformation. Both of these elects can serve as the basis for measuring forces. For example, the mea­surement of forces by means of a spring dynamometer is based on Hooke’s law \ssect{40.3.4} for axial tension.

Using the concept of force, we usually speak in mechanics of the motion and deformation of a body by the action of the forces applied to it. Here, of course, each force always corresponds to a certain body exerting this force on the body being consid­ered.

Force $\vb{F}$ is completely specified by giving its magnitude, direction in space and point of application. The straight line along which a force is exerted is called its \redem{line of action}.

A field acting on a particle with the force $\vb{F}$ is said to be \redem{steady}, or \redem{stationary}, if it does not change in the course of time $t$, i.e. if force $\vb{F}$ is explicitly independent of time at any point in the held: $\pdv*{\vb{F}}{t} \equiv 0$. For a held to be steady it is necessary that the bodies which set it up are at rest with respect to the ref­erence frame used in considering the held.

\subsection{}\label{2.2.2} The simultaneous action of several forces, $\vb{F_{1}}, \,\vb{F_{2}},\,\vb{F_{3}},\ldots{} \,\vb{F_{n}}$ (\figr{fig-02-01}~\textcolor{IndianRed}{(a)}) on a particle $M$ is equivalent to the action of a single force, called the \redem{resultant force} and equal to the vector sum of the component forces. Thus
\begin{equation*}%
\vb{F}= \sum_{i=1}^{n} \vb{F_{i}}
\end{equation*}
It is the closing line of the polygon of forces $\vb{F_{1}}, \,\vb{F_{2}},\,\vb{F_{3}},\ldots{} \,\vb{F_{n}}$ (\figr{fig-02-01}~\textcolor{IndianRed}{(b)}).

\begin{figure}[!ht]
%\begin{wrapfigure}{r}{0.5\linewidth}
\centering
%\includegraphics{figs/ch-02/fig-02-01.pdf}
\input{figs/ch-02/fig-02-01}
%  \vspace{-13pt}
\caption{Resultant force is equal to vector sum of the component forces.}
\label{fig-02-01}
\end{figure}
%\end{wrapfigure}


If a body is perfectly rigid, the action of a force on it does not
change when the point of application is displaced along the line of action, but within the limits of the body. In other words, forces applied to perfectly rigid bodies can be regarded as non-localized vectors.

\subsection{}\label{2.2.3} A body is said to be \redem{free} if no limitations are imposed on its position and motion in space. An airplane, for example, Hying in the air is a free body, as is a submarine travelling in the depths of the sea. Most bodies that we deal with are not free; certain limitations are imposed on their possible positions and motion. These limitations are called \redem{constraints} in mechan­ics. A ball, for example, hung on a nonextensible thread, can­ not move away from its point of suspension by more than the length of the thread; a streetcar (tram) must travel only along its tracks. Constraints are realized due to the action on the body being considered exerted by other bodies attached to or contacting this body (for instance, the thread on the ball tied to it, the track on the streetcar, etc.).

In investigating the behaviour of constrained bodies or systems of bodies in mechanics, the \redem{principle of releasability} is applied. It states that a restrained body (or system of bodies) can be treated as a free body (or system) if the bodies imposing the constraints are replaced by the corresponding forces. These forces are called \redem{reaction forces} of the constraints, or \redem{constraint reactions}. All other forces exerted on the body are called \redem{active forces}. Thus, the motion of the ball hung by the thread can be treated as the motion of a free ball subject, in addition to all the active forces applied to it (for instance, gravity), to the reaction of the thread as well.

In contrast to the active forces, which should he given in each specific problem, the constraint reactions are not known before­hand. They are to be determined in solving the problem. Their values should be such that under the combined effects of the active forces and constraint reactions, the motion of the ``re­leased'' body completely agrees with the limitations imposed by the constraints on the constrained body being considered. There are no other distinctions between constraint reactions and active forces.

\subsection{}\label{2.2.4} Bodies not included in the mechanical system being in­ vestigated are called \redem{external bodies}. Forces exerted on the system by external bodies are called \redem{external forces}. Correspondingly, forces of interaction between the parts or particles of the system being considered are called \redem{internal forces}.

A mechanical system is said to be \redem{closed}, or \redem{isolated}, if it does not interact with external bodies. No external forces act on any of the bodies of a closed system.

\section{Mass. Momentum}
\label{sec-2.3}

\subsection{}\label{2.3.1} In classical (Newtonian) mechanics, the \redem{mass of a particle} (material point) is a positive scalar quantity which is a measure of the inertia of this particle. Under the action of a force the velocity of a particle changes \redem{gradually}, not instantaneously, i.e. it acquires an acceleration \redem{finite} in magnitude and inversely proportional to the mass of the particle. To compare the masses, $m_{1}$ and $m_{2}$, of two particles, it is sufficient to measure the magnitudes $a_{1}$ and $a_{2}$ of the accelerations acquired by these particles when the same force is applied to them: $m_{2}/m_{1}= a_{1}/a_{2}$. The mass of a body is ordinarily determined by weigh­ing by beam scales.

Classical (Newtonian) mechanics contends that:
\begin{enumerate}[label=(\alph*)]
\item the mass of a particle is independent of its state of motion, being its unalterable characteristic;
\item mass is an additive quantity, i.e. the mass of a system (for instance, a body) is equal to the sum of the masses of all the particles making up the system;
\item the mass of an isolated (closed) system \ssect{2.2.4} does not change with any processes occurring within the system (\redem{law of conservation of mass}).
\end{enumerate}

These propositions of Newtonian mechanics were reconsidered and revised in relativistic mechanics (\ref{5.6.1}, \ref{5.6.2}, \ref{5.7.3} and \ref{5.7.6}).

\subsection{}\label{2.3.2} The density $\rho$ of a body at a given point $M$ is the ratio of the mass dm of an infinitely small element of the body, which includes point $M$, to the quantity $dV$, which is the volume of this element. Thus
\begin{equation*}%
\rho = \frac{dm}{dV}
\end{equation*}
The size of the element being considered should be so small that any variation of density within its limits can be neglected. On the other hand, this size should he a great many times larger than intermolecular distances.

A body is said to be \redem{homogeneous} if the density is constant at all of its points. The mass of a homogeneous body is the product of its density by its volume: $m = \rho V$.

The mass of a nonhomogeneous body is
\begin{equation*}%
m = \int_{(V)} \rho dV
\end{equation*}
where $\rho$ is a function of the coordinates, and integrating is carried out over the whole volume of the body. The average den­sity $\expval{\rho}$ of a nonhomogeneous body is the ratio of its mass to its volume: $\expval{\rho} = m/V$.

\subsection{}\label{2.3.3} The centre of inertia, or centre of mass, of a system of par­ticles is point $C$, whose radius vector $\vb{r}_{c}$ equals
\begin{equation*}%
\vb{r}_{C} = \frac{1}{m} \sum_{i=1}^{n} m_{i} \vb{r}_{i}
\end{equation*}
where $m_{i}$ and $\vb{r}_{i}$ are the mass and radius vector of the $i$th par­ticle, $n$ is the total number of particles in the system, and $m = \sum_{i=1}^{n} m_{i}$ is the mass of the whole system.

The velocity of the centre of mass is
\begin{equation*}%
\vb{v}_{C} = \dv{\vb{r}_{C}}{t} =  \frac{1}{m} \sum_{i=1}^{n} m_{i} \vb{v}_{i}
\end{equation*}

\subsection{}\label{2.3.4} The vector quantity $\vb{p}_{i}$, equal to the product of the mass $m_{i}$ of a particle by its velocity $\vb{v}_{i}$, is called the \redem{momentum of this particle}. The momentum of a system of particles is the vector $\vb{p}$, which is the geometric sum of the momenta of all the particles making up the system:
\begin{equation*}%
\vb{p} =   \sum_{i=1}^{n} \vb{p}_{i}
\end{equation*}
The momentum of a system is equal to the product of the mass of the whole system by the velocity of its centre of mass: $\vb{p}= m\vb{v}_{C}$.

\section{Newton's Second Law}
\label{sec-2.4}

\subsection{}\label{2.4.1} The basic law of the dynamics of a particle is Newton’s second law, which discusses how the mechanical motion of a particle is changed by the effect of forces applied to it. \redem{New­ton's second law} states: the rate of change in the momentum p of a particle is equal to the force $\vb{F}$ acting on it. Thus
\begin{equation*}%
\dv{\vb{p}}{t} = \vb{F} \,\,\textrm{or}\,\,\dv{t} (m \vb{v}) = \vb{F}
\end{equation*}
where $m$ and $\vb{v}$ are the mass and velocity of the particle.

If several forces act simultaneously on a particle, the force $\vb{F}$ in Newton’s second law is understood to he the geometric sum of all acting forces, both active forces and constraint reactions \ssect{2.2.3}, i.e. the resultant force \ssect{2.2.2}.

\subsection{}\label{2.4.2} The vector quantity $\vb{F} \dd t$  is called an element of \redem{impulse of force} $\vb{F}$ during the infinitely short time $\dd t$ of its action. The impulse of force $\vb{F}$ acting in the finite time interval from $t = t_{1}$ to $t = t_{2}$ is equal to the definite integral $\int_{t_{1}}^{t_{2}} \vb{F} \dd t$, where $\vb{F}$, in
the general case, depends upon the time $t$.

According to Newton’s second law, the change in the momentum of a particle is equal to the impulse of the force acting on it:
\begin{equation*}%
\dd \vb{p} =  \vb{F} \dd t = \Delta \vb{p} = \vb{p}_{2} - \vb{p}_{1}  =\int_{t_{1}}^{t_{2}} \vb{F} \dd t
\end{equation*}
where $\vb{p}_{2}= \vb{p} (t_{2})$ and $\vb{p}_{1}= \vb{p} (t_{1})$ are the values of the momen­tum of a particle at the end  $t = t_{2}$ and beginning $t = t_{1}$ of Ihe time interval being considered.

\subsection{}\label{2.4.3} Since in Newtonian mechanics the mass $m$ of a particle is independent of its state of motion, $\dv*{m}{t} = 0$. Hence, the mathematical expression of Newton’s second law can also be presented in the form
\begin{equation*}%
\vb{a} = \frac{\vb{F}}{m}
\end{equation*}
where $\vb{a} = \dv*{\vb{v}}{t} = \dv*[2]{\vb{r}}{t}$ is the acceleration of the particle and $\vb{r}$ is its radius vector. The corresponding statement of \redem{Newton's second law reads}: the acceleration of a particle coin­cides in direction with the force acting on it and is equal to the ratio of this force to the mass of the particle. The tangential and normal accelerations of a particle (\ref{1.4.4}, \ref{1.4.5} and \ref{1.4.6}) are determined by the corresponding compo­nents of force $\vb{F}$:
\begin{align*}%
\vb{a}_{\tau} & = \frac{\vb{F}_{\tau}}{m}, \,\, a_{\tau} = \dv{v}{t} = \frac{F_{\tau}}{m}\\
and& \\
\vb{a}_{n} & = \frac{\vb{F}_{n}}{m}, \,\, a_{n} = \frac{v^{2}}{R} = \frac{F_{n}}{m}
\end{align*}
where $v$ is Ihe magnitude of the particle’s velocity vector, and $R$ is the radius of curvature of its path. The force $F_{n}$, imparting normal acceleration to a particle, is directed toward the centre
of curvature \ssect{1.2.4} of the particle’s path and is therefore called the \redem{centripetal force}.

\subsection{}\label{2.4.4} If a particle is subject to the simultaneous action of sev­ eral forces, $\vb{F_{1}}, \,\vb{F_{2}},\,\vb{F_{3}},\ldots{} \,\vb{F_{n}}$, its acceleration is
\begin{equation*}%
\vb{a} = \frac{1}{m} \sum_{l=1}^{n}\vb{F}_{l} = \sum_{l=1}^{n}\vb{a}_{l} 
\end{equation*}
where $\vb{a}_{l} = \vb{F}_{l}/m$. Consequently, each of the forces acting si­multaneously on a particle imparts to it the acceleration it would have if there were no other forces (\redem{principle of superposi­tion, or independence, of forces}).

The equation
\begin{equation*}%
m \dv[2]{\vb{r}}{t}  = \vb{F} = \sum_{l=1}^{n} \vb{F}_{l}
\end{equation*}
is called the \redem{differential equation of motion of a particle}.

In projections on the axes of a rectangular Cartesian coordinate system, this equation is of the form
\begin{equation*}%
m \dv[2]{x}{t}  = F_{x},\,\,m \dv[2]{y}{t}  = F_{y},\,\,m \dv[2]{z}{t}  = F_{z}
\end{equation*}
where $x$, $y$ and $z$ are the coordinates of the moving particle.



\section{Newton's Third Law. Motion of the Centre of Mass}
\label{sec-2.5}

\subsection{}\label{2.5.1} The mechanical action exerted by bodies on one another is of the nature of their \redem{interaction}. This is what \redem{Newton's third law} deals with: two particles exert forces on each other which are equal in magnitude and opposite in direction along a line connecting these particles.

If $\vb{F}_{ik}$ is a force exerted on the $i$th particle by the $k$th one, and $\vb{F}_{ki}$ is, the force exerted on the $k$th particle by the $i$th one, then, according to Newton’s third law,
\begin{equation*}%
\vb{F}_{ki} = - \vb{F}_{ik}
\end{equation*}
Forces $\vb{F}_{ik}$ and $\vb{F}_{ki}$ are applied to different particles and can be mutually counterbalanced only when particles $i$ and $k$ belong to the same perfectly rigid body.

\subsection{}\label{2.5.2} Newton’s third law is an essential supplement to the first and second laws. It permits us to go over from the dynam­ics of a separate particle to that of an arbitrary mechanical system (system of particles). It follows from Newton’s third law that in any mechanical system the geometric sum of all the internal forces \ssect{2.2.4} equals zero:
\begin{equation*}%
\sum_{i=1}^{n}\sum_{k=1}^{n} \vb{F}_{ik} = 0
\end{equation*}
where $n$ is the number of particles in the system, and $\vb{F}_{ii} = 0$. The vector $\vb{F}^{\textrm{ext}}$, equal to the vector sum of all the external forces \ssect{2.2.4} acting on the system, is called the \redem{principal vector of external forces}:
\begin{equation*}%
\vb{F}^{\textrm{ext}} = \sum_{i=1}^{n} \vb{F}^{\textrm{ext}}_{i}
\end{equation*}
where $\vb{F}^{\textrm{ext}}_{i}$ is the resultant external force applied to the $i$th particle.

\subsection{}\label{2.5.3} It follows from Newton’s first and second laws that the first derivative with respect to the time $t$ of the momentum $\vb{p}$ of a mechanical system \ssect{2.3.4} is equal to the principal vector of all external forces applied to the system. Thus
\begin{equation*}%
\dv{\vb{p}}{t} = \vb{F}^{\textrm{ext}}
\end{equation*}
This equation expresses the \redem{law of the change in the momentum of a system}. 

Since $\vb{p}=m\vb{v}_{c}$, where $m$ is the mass of a system and $\vb{v}_{c}$ is the velocity of its centre of inertia, or mass, the \redem{law of motion of the centre of mass} of a mechanical system is of the form
\begin{equation*}%
\dv{t} (m\vb{v}_{c}) = \vb{F}^{\textrm{ext}}\,\,\textrm{or}\,\, m\vb{a}_{c} = \vb{F}^{\textrm{ext}}
\end{equation*}
where $\vb{a}_{c} = \dv*{\vb{v}_{c}}{t}$ is the acceleration of the centre of mass. Thus, the centre of mass (centre of inertia) of a mechanical system travels like a particle whose mass is equal to the mass of the whole system and which is subject to a force equal to the principal vector of the external forces applied to the system. If the system being considered is a rigid body that travels with translational motion \ssect{1.5.1}, the velocities $\vb{v}_{i}$of all the points of the body and $\vb{v}_{c}$ of its centre of mass are the same and are equal to velocity $\vb{v}$ of the body. Correspondingly, the accelera­tion of the body $\vb{a} = \vb{a}_{c}$ and the \redem{basic equation in the dynamics of translational motion of a rigid body} is of the form
$m \vb{a} = \vb{F}^{\textrm{ext}}$.

\section{Motion of a Body of Variable Mass}
\label{sec-2.6}

\subsection{}\label{2.6.1} In Newtonian mechanics the mass of a body can be changed only if particles of matter are detached from or added to it. An example of such a body is a rocket. In its flight the mass of the rocket is gradually reduced because the gaseous products of combustion in the rocket engine are ejected through the noz­zles.

\redem{The equation of translational motion of a body of variable mass (Meshchersky equation)} is
\begin{equation*}%
m \dv{\vb{v}}{t} = \vb{F}^{\textrm{ext}} + (\vb{v}_{1} - \vb{v} ) \dv{m}{t}
\end{equation*}
where $m$ and $\vb{v}$ are the mass and velocity of the body at the instant of time being considered, $\vb{F}^{\textrm{ext}}$ is the principal vector of the external forces \ssect{2.5.2} acting on the body, and $\vb{v}_{1}$ is the velocity of the detracted particles \redem{after separation} from the rocket (if $\dv*{m}{t} < 0$) or the added particles \redem{before being joined} (if $\dv*{m}{t} > 0$).

\subsection{}\label{2.6.2} The second member on the right-hand side of the Me­ shchersky equation represents the additional force acting on a body of variable mass. It is called the \redem{reactive force}:
\begin{equation*}%
\vb{F}_{\textrm{r}}= (\vb{v}_{1} - \vb{v} ) \dv{m}{t}
= \vb{u}  \dv{m}{t}
\end{equation*}
where $\vb{u} = \vb{v}_{1} - \vb{v} $ is the relative velocity of the particles being separated from or joined to the body, i.e. their velocity with respect to a reference frame in translational motion together with the body.

The reactive force characterizes the mechanical action exerted on the body by the particles separated from or joined to it (for instance, the effect of the ejected exhaust gases on the rocket). 

\subsection{}\label{2.6.3} The equation of motion of a rocket not subject to the action of external forces is
\begin{equation*}%
m \dv{\vb{v}}{t} = \vb{u} \dv{m}{t}
\end{equation*}
If the initial velocity of the rocket is zero the rocket flies in a straight line in the direction opposite to that of the relative velocity $\vb{u}$ of the exhaust gases ejected from the engine nozzle. Here
\begin{equation*}%
m \dv{v}{t} = -u \dv{m}{t}
\end{equation*}
and at $\vb{u} = \textrm{const}$ the relation between the velocity of the rocket and its mass is expressed by \redem{Tsiolkovsky's formula}
\begin{equation*}%
v = u \ln \frac{m_{0}}{m}
\end{equation*}
where $m_{0}$ is the initial, or launching, mass of the rocket. 

\subsection{}\label{2.6.4} The maximum velocity that a rocket can reach in the absence of external forces is called its \redem{characteristic velocity}. This velocity is acquired at the moment the rocket engine stops because it has used up all the fuel and oxidizer that were on board,
\begin{equation*}%
v_{\textrm{max}}=  u \ln  \frac{m_{0}}{m_{0}-m_{\textrm{f}}}
\end{equation*}
where $m_{\textrm{f}}$ is the initial mass of the fuel and oxidizer.
The effects of the earth’s gravity and air resistance lead to an appreciable reduction in the maximum velocity actually ac­quired by the rocket, during the operation of its engine, as com­pared to the characteristic velocity.

\subsection{}\label{2.6.5} The \redem{characteristic velocity of a multistage} (combination) \redem{rocket} is
\begin{equation*}%
v_{\textrm{max}}= \sum_{i=1}^{n} u_{i} \ln  \frac{m_{0i}}{m_{0i}-m_{\textrm{f}i}}
\end{equation*}
where $n$ is the total number of stages of the rocket, $m_{\textrm{f}i}$ is the mass of the fuel and oxidizer intended for the operation of the $i$th stage engine, $ u_{i}$ is the exhaust gas velocity (nozzle velocity) of the $i$th stage engine, and $m_{0i}$ is the launching mass of the multistage rocket, including all the stages from the $i$th to the $n$th. The velocity increase of a multistage rocket, compared to that of a single-stage one having the same launching mass and the same fuel and oxidizer capacity, is due to the reduction in the mass of the rocket as the first, second and subsequent stages are consecutively jettisoned when their fuel has been ex­hausted.
	
\section{Law of Conservation of Momentum}
\label{sec-2.7}

\subsection{}\label{2.7.1} The \redem{law of conservation of momentum} is: the momentum $\vb{p}$ of a closed, or isolated, system does not change in the course of time, i.e.
\begin{equation*}%
\dv{\vb{p}}{t} \equiv 0\,\,\textrm{and}\,\,\vb{p}= \textrm{const}
\end{equation*}
In contrast to Newton’s laws, the law, or principle, of conser­vation of momentum is valid not only within the framework of classical mechanics. It belongs to the most fundamental of physical laws because it is concerned with a definite property of the symmetry of space: its homogeneity. The \redem{homogeneity of space} is manifested in the fact that the physical properties of a closed system and the laws of its motion are independent of the choice of position of the origin of the inertial reference frame, i.e. these properties and laws are unchanged by a parallel translation of the closed system as a whole in space. According to modern concepts, fields can also have momentum, not only particles and bodies. Light, for example, exerts a pressure on the surface of a body that reflects or absorbs it because the electromagnetic field of the light wave has momentum.

\subsection{}\label{2.7.2} In application to systems described by classical (Newto­nian) mechanics, the principle of conservation of momentum can be regarded as a consequence of Newton’s laws. For a closed mechanical system the principal vector of external forces
 
$\vb{F}^{\textrm{ext}}\equiv 0$ and the principle of conservation of momentum follows from \ssect{2.5.3}:
\begin{equation*}%
\vb{p} = \sum{i=1}{n} m_{i} \vb{v}_{i} = \textrm{const}
\end{equation*}
where $m_{i}$ and $\vb{v}_{i}$ are the mass and velocity of the $i$th particle of the system, which consists of $n$ particles.

Consequently, there is also no change in the projections of the momentum of a closed system onto the axes of Cartesian coordi­nates of an inertial reference frame:
\begin{align*}%
p_{x} & = \sum_{i=1}^{n} m_{i} v_{ix} = \textrm{const}\\
p_{y} & = \sum_{i=1}^{n} m_{i} v_{iy} = \textrm{const}\\
p_{z} & = \sum_{i=1}^{n} m_{i} v_{iz} = \textrm{const}
\end{align*}
The momentum of the system is $\vb{p} = m\vb{v}_{c}$, where $m$ is the mass of the system and $\vb{v}_{c}$ is the velocity of its centre of mass \ssect{2.3.4}. It follows, therefore, from the principle of conservation of mo­mentum that in any processes occurring within a closed system the velocity of its centre of mass does not change: $\vb{v}_{c} = \textrm{const}$. 

\subsection{}\label{2.7.3} If a system is not closed, hut the external forces acting on it are such that their principal vector is identically zero ($\vb{F}^{\textrm{ext}}\equiv 0$), then, according to Newton’s laws \ssect{2.5.3}, the mo­mentum of the system does not change with time: $\vb{p} = \textrm{const}$.

As a rule, $\vb{F}^{\textrm{ext}}\not\equiv 0$ and $\vb{p} \neq \textrm{const}$. But if the projection of the principal vector of external forces onto any fixed axis is iden­tically zero, the projection onto the same axis of the momentum vector of the system does not change with time. Thus, $p_{x} = \textrm{const}$
under the condition that $F^{\textrm{ext}}_{x} \equiv 0$. If, for instance, no other external forces, except gravity, are acting on a system, the horizontal component of the momentum of the system, perpendicular to the direction of the gravitational force, remains con­stant.

\subsection{}\label{2.7.4} In some processes (e.g. collisions or in firing shells, etc.), the momenta of the parts of the system undergo great changes in relatively short time intervals. This is due to internal forces of interaction, of short duration, but of quite considerable magni­tude, developed within the system. In comparison with these forces, all external forces acting continuously on the system (for instance, gravity) are small. In such processes, the action of the system of external forces can usually be ignored, i.e. it can be assumed that the momentum of the system as a whole is not changed in the process being considered.

\section[Galilean Transformations]{Galilean Transformations. Mechanical Principle of Relativity}
\label{sec-2.8} 

\subsection{}\label{2.8.1} A \redem{Galilean transformation} is a transformation of coordi­nates and time that is applied in Newtonian mechanics in going over from one inertial frame of reference $K (x, y, z, t)$ to another $K' (x , y', z', t')$ which travels with translational motion with respect to $K$ at the constant velocity $\vb{V}$. The Galilean transformation is based on axioms concerning the absolute nature of time intervals and of lengths. The first axiom con­tends that the passage of time (time interval between any two events) is the same in all inertial frames of reference. According to the second axiom, the dimensions of a body are independent of its velocity of motion with respect to the frame of ref­erence.
\begin{figure}[!ht]
%\begin{wrapfigure}{r}{0.5\linewidth}
\centering
%\includegraphics[width=0.5\textwidth]{figs/ch-02/fig-02-02.pdf}
\input{figs/ch-02/fig-02-02}
%  \vspace{-13pt}
\caption{A general Galilean transformation between two frames of reference $K$ and $K'$ moving with a constant velocity $\vb{V}$.}
\label{fig-02-02}
\end{figure}
%\end{wrapfigure}
If the like axes of Cartesian coordinates of inertial reference frames $K$ and $K'$ are drawn parallel to one another pairwise and if at the initial instant of time $(t = t' = 0)$ the origins $O$ and $O'$ of coordinates coincide with each other (\figr{fig-02-02}), the Galilean transformations are of the form:
\begin{align*}%
x' & = x - V_{x}t,\,\, y'= y - V_{y}t,\,\, z'= z - V_{z}t\,\,\textrm{and}\,\, t'= t\,\,\textrm{or}\\
\vb{r}' & = \vb{r} - \vb{V}t\,\,\textrm{and}\,\,t' = t,
\end{align*}
where $x, y, z$ and $x', y', z'$ are the coordinates of point M in the frames of reference $K$ (at the instant of time $t$) and $K'$ (at the instant of time $t' = t$), $\vb{r}$ and $\vb{r}'$ are the radius vectors of point $M$ in the same frames of reference, and $V_{x},\, V_{y}$ and $V_{z}$ are the projections of the velocity $\vb{V}$ of reference frame $K'$ onto the coordinate axes of reference frame $K$.

The coordinate axes are usually arranged so that frame of ref­erence $K'$ travels along the positive direction of axis $OX$ (\figr{fig-02-03}). Under this condition the Galilean transformations are of their simplest form:
\begin{equation*}%
x = x - Vt, \,\,y' = y, \,\,z’= z \,\,\textrm{and}\,\, t' = t
\end{equation*}

\subsection{}\label{2.8.2} A consequence of the Galilean transformations is the following law for transforming the velocity of arbitrary point $M$ (\figr{fig-2.2}) in going over from one inertial frame of reference $K$ (the velocity of the point is $\vb{v} = \dv*{\vb{r}}{t}$) to another $K'$ (in which the velocity of the same point is $\vb{v}' = \dv*{\vb{r}'}{t}$):
\begin{equation*}%
\vb{v}' = \vb{v} - \vb{V}
\end{equation*}
The projections of the velocity on like coordinate axes are transformed accordingly:
\begin{equation*}%
v_{x'}' = v_{x} - V_{x}, \,\,v_{y'}' = v_{y} - V_{y} \,\,\textrm{and}\,\,v_{z'}' = v_{z} - V_{z} 
\end{equation*}
In particular, when reference frame $K'$ travels along the positive direction of axis $OX$ (\figr{fig-02-03})
\begin{equation*}%
v_{x'}' = v_{x} - V, \,\,v_{y'}' = v_{y} \,\,\textrm{and}\,\,v_{z'}' = v_{z}
\end{equation*}
The accelerations of point $M$ in frames of reference $K' (\vb{a} =\dv*{\vb{v}}{t})$ and $K' (\vb{a}' =\dv*{\vb{v}'}{t})$ are the same: $\vb{a}' = \vb{a}$.

\begin{figure}[!ht]
%\begin{wrapfigure}{r}{0.5\linewidth}
\centering
%\includegraphics[width=0.5\textwidth]{figs/ch-02/fig-02-03.pdf}
\input{figs/ch-02/fig-02-03}
%  \vspace{-13pt}
\caption{Galilean transformation between two frames of reference $K$ and $K'$ when $K'$ travels along direction of axis $OX$.}
\label{fig-02-03}
\end{figure}
%\end{wrapfigure}
Hence, the acceleration of a particle is independent of the choice of the inertial frame of reference or, in other words, it is inva­riant with respect to Galilean transformations.

\subsection{}\label{2.8.3} The forces of interaction between particles depend only upon their relative position and their velocity with respect to one another. The relative position of any two particles, 2 and 1, is represented by a vector equal to the difference between the radius vectors of the two particles, i.e. to the vector $\vb{r}_{21} = \vb{r}_{2} - \vb{r}_{1}$ in reference frame $K$ and to the vector $\vb{r}_{21}' = \vb{r}_{2}' - \vb{r}_{1}'$( in reference frame $K'$. It follows from the Galilean transformations that $\vb{r}_{21} = \vb{r}_{21}' - \vb{r}_{21}$. Therefore, the distance between particles 1 and 2 is the same in reference frames $K$ and $K'$:
\begin{align*}%
\vb{r}_{21}' & = \vb{r}_{21} = \,\,\textrm{or}\\
(x_{2}' - x_{1}')^{2} & + (y_{2}' - y_{1}')^{2} + (z_{2}' - z_{1}')^{2}\\
= (x_{2} - x_{1})^{2} & + (y_{2} - y_{1})^{2} + (z_{2} - z_{1})^{2}
\end{align*}
The velocity of particle 2 with respect to particle 1 is equal to the difference of the velocities of these two particles: $\vb{v}_{2}- \vb{v}_{1}$(in reference frame $K$) and $\vb{v}_{2}'- \vb{v}_{1}'$ (in reference frame $K'$). It follows from the Galilean transformations that $\vb{v}_{2}'- \vb{v}_{1}' = \vb{v}_{2}- \vb{v}_{1}$. 

Thus, the relative position and the velocity of relative motion of any two particles are independent of the choice of inertial reference frame; they are invariant under a Galilean transfor­mation. Accordingly, the forces acting on a particle are also invariant under a Galilean transformation: $\vb{F}' = \vb{F}$.

\subsection{}\label{2.8.4} Equations expressing Newton’s laws \ssect{2.4.3} and \ssect{2.5.1} are invariant with respect to Galilean transformations, i.e. their form is unchanged in transforming the coordinates and time from one inertial frame of reference $(K)$ to another $(K')$:
\begin{align*}%
m\vb{a} = \vb{F}\,\,\textrm{and}\,\, \vb{F}_{ki} = - \vb{F}_{ik}\,\, \intertext{(in reference frame $K$)}\\
m'\vb{a}' = \vb{F}'\,\,\textrm{and}\,\, \vb{F}_{ki}' = - \vb{F}_{ik}'\,\, \intertext{(in reference frame $K'$)}
\end{align*}
where $m' = m$ is the mass of the particle being considered and is the same in all frames of reference.

Thus, the \redem{mechanical principle of relativity (Galileo's principle of relativity)} is valid in classical mechanics. It contends that the laws of mechanics are the same in all inertial frames of ref­erence. This implies that under the same conditions all mechan­ical processes proceed in the same way in different frames of reference. Consequently, it is impossible to establish, by means of any mechanical experiments conducted within a closed sys­tem of bodies, whether the system is at rest or is travelling at uniform velocity in a straight line (with respect to any inertial frame of reference whatsoever).

The mechanical principle of relativity points to the fact that all inertial frames of reference are equally valid in mechanics. It is impossible to single out any special, or ``main'', inertial frame of reference with respect to which the motion of bodies could be regarded as ``absolute motion''.

\subsection{}\label{2.8.5} The principle of relativity was generalized to include all physical phenomena by Albert Einstein in his special theory of relativity \ssect{5.1.2}. It was found that coordinates and time in various inertial frames of reference are related by the Lorentz transformations \ssect{5.3.2} rather than the Galilean transforma­tions. But at low velocities of relative motion of the frames of reference (in comparison to the velocity of light in free space), the Lorentz transformations become the Galilean transforma­tions.
