% !TEX root = handbook-physics.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode

\chapter{Liquids}
\label{ch-13}

\section{Certain Properties of Liquids}
\label{sec-13.1}

\subsection{}\label{13.1.1} \emph{Liquids} are bodies that have a definite volume, but assume the shape of the vessel that holds them. On the thermal motion in liquids, see \ssect{8.1.5}. The nature of the thermal motion in liquids determines the similarity of their properties with those of both solids and gases. Like solids, liquids are almost incompressible. This property is associated with the strong intermolecular interaction of the particles in liquids. When liquids are compressed, the distances between the molecules are reduced and the repulsive forces that impede compression in­ crease drastically \ssect{12.1.4}. Liquids have relatively high densities and, like solids, resist, not only compression, but tension as well \ssect{40.3.0}. This is manifested in the fact that the Van der Waals isothermals enter the region of negative pressures (the isothermal at $T = T_{1}$ in \figr{fig-12-05}). The resemblance of the properties of liquids and real gases at high temperatures and low densities is observed, for instance, in that the surface tension of liquids \ssect{13.4.4} and the specific heat of vaporization \ssect{13.6.4} are reduced as the temperature is raised. Besides, the densities of dry saturated vapour \ssect{12.3.2} and of boiling liquid \ssect{12.3.2} approach each other as the temperature is increased. 

\subsection{}\label{13.1.2} The similarity between liquids and solids is confirmed by $X$-ray diffraction analysis \ssect{33.4.1}. At temperatures close to that of solidification, the arrangement of the particles in liquids is similar to the ordered arrangement of particles that is typical of solidified liquids. The mutual arrangement of neighbouring particles in liquids resembles the ordered arrangement of neighbouring particles in crystals. In liquids, however, this ordered arrangement is observed only within small volumes. At distances $r > (3 \, \text{to} \, 4) \,d$ from a certain selected ``central'' molecule, order is no longer evident (here $d$ is the effective diameter of the molecule, see \ssect{8.4.1}). Such order in the arrangement of the particles is called \emph{short-range order in liquids}.

\subsection{}\label{13.1.3} $X$-ray patterns of liquids do not differ from those of polycrystalline bodies \ssect{33.4.5} consisting of extremely fine crystals (having linear dimensions of the order of \SI{d-9}{\meter}) that are randomly oriented with respect to one another in so-called \emph{cybotaxic regions}. Within the boundaries of these regions the particle distribution is ordered, but the kind of order changes from one cybotaxic region to another. In the course of time, intensive thermal motion at temperatures that are not too low rapidly alters the arrangement and structure of the cybotaxic regions.

\section{Frenkel's Hole Theory of the Liquid State}
\label{sec-13.2}

\subsection{}\label{13.2.1} The most important parameter determining the structure and physical properties of liquids is the specific volume. In melting a crystalline body the specific volume increases only slightly, by approximately 10 per cent. Such an increase in the specific volume takes place in a solid subject to negative pres­ sure equal to the ultimate strength \ssect{40.3.7} of the solid. This enables a liquid to be dealt with as a body whose integrity has been violated at various places. In melting crystalline bodies, their particles acquire higher mobility. This is due to the vital property of \emph{fluidity of liquids}, as well as the violation of long-range order in crystals and the emergence of short-range order in liquids \ssect{13.1.2}. Moreover, owing to the high mobility of the particles, microscopic gaps or microvoids, which shall be called \emph{holes}, appear in liquids. The thermal motion in liquids causes the holes to disappear at random at certain places and to appear simultaneously at others. This is equivalent to chaotic movement of the holes.

\subsection{}\label{13.2.2} The hole theory of the structure of liquids is inapplicable to liquids subject to high external pressures of the order of thousands of atmospheres, at which the compressibility of liquids is comparable to that of solids. At high temperatures* close to the critical one\footnote{Substances with an exceptionally low critical temperature are not considered here.}, the structure and properties of a liquid are close to those of a gas for which the concept of holes is meaningless. Hence, the hole theory is inapplicable for liquids under these conditions.

\subsection{}\label{13.2.3} It follows from the nature of thermal motion in liquids \ssect{8.1.5} that the molecules vibrate about a certain equilibrium position for the length of time $\tau$, after which this equilibrium position is displaced with a jump over a distance of an order of magnitude equal to the average distance $\expval{d}$ between adjacent molecules:
\begin{equation*}%
\expval{d} \approx \sqrt[3]{\frac{1}{n_{0}}} = \sqrt[3]{\frac{\mu}{N_{A} \rho}}
\end{equation*}
where $n_{0}$ is the number of molecules in unit volume, $N_{A}$ is Avogadro’s number (Appendix II), $\rho$ is the density of the liquid, and $\mu$ is its molar mass. For water, for example, $\rho = \SI{d3}{\kilo\gram\per\meter\cubed}$, $\mu = \SI{0.018}{\kilo\gram\per\mole}$ and $\expval{d} \approx \SI{3d-10}{\meter}$.

\subsection{}\label{13.2.4} The \emph{relaxation time} is the average time $\expval{\tau}$ that a molecule of the liquid remains in a temporarily settled state close to a certain equilibrium position. As the temperature is raised $\expval{\tau}$ decreases rapidly \ssect{13.2.5}. This explains the high mobility of molecules in a liquid at high temperatures and the low viscosity of liquids under these conditions.

\subsection{}\label{13.2.5} The expenditure of a certain amount of \emph{activation energy} $E$ is required for a molecule to pass from one equilibrium posi­tion to another \ssect{13.2.4}. Such a passage is regarded as overcoming a potential barrier of height $E$ \ssect{38.7.1} because to accom­plish the passage the potential energy of the molecule should increase by the amount $E$. Only after this can it pass over to a new equilibrium position. The whole process turns out to be possible because high energy is concentrated on some of the molecules as a result of collisions in thermal motion. It is transferred to them by other molecules. The dependence of the relaxation time \ssect{13.2.4} on $E$ and the absolute temperature is of the form
\begin{equation*}%
\expval{\tau}  = \tau_{0} \exp \left( \frac{E}{kT} \right)
\end{equation*}
where $k$ is Boltzmann’s constant \ssect{8.4.5} and $\tau_{0}$ is the average period of vibrations of the molecule about its equilibrium position.

\subsection{}\label{13.2.6} When a liquid is subject to the action of an external force for the length of time $t \gg \expval{\tau}$ the particles of the liquid are displaced mainly in the direction of this force. This reveals the fluidity of the liquid. But if $t \ll \expval{\tau}$ a particle has no opportunity to change its equilibrium position during the time of
force action. At this the liquid manifests elastic properties, resisting changes in both volume and shape.

During the time $\expval{\tau}$ a particle of liquid is displaced, on an average, over the distance $\expval{d}$, the average velocity $\expval{v}$ of dis­ placement of the molecules is determined by the equation
\begin{equation*}%
\expval{v}  = \frac{\expval{d} }{\expval{\tau} }  \qor \expval{v}  = \frac{\expval{d} }{\expval{\tau_{0}} }   \exp \left(- \, \frac{E}{kT} \right)
\end{equation*}
The average velocity of motion of molecules in liquids is quite considerable, as a rule, but an order of magnitude less than the average velocities of molecules of the vapour of the same sub­ stance at the same temperatures.


\section{Diffusion and Viscosity Phenomena in Liquids}
\label{sec-13.3}

\subsection{}\label{13.3.1} If conditions required for transport phenomena \ssect{10.8.1} arise, diffusion, heat conduction and viscosity are observed in liquids. The difference between transport phenomena in liquids and similar phenomena in gases affects the transport coefficients and their dependence on the characteristics of properties of the liquids.

\subsection{}\label{13.3.2} For a chemically homogeneous liquid the diffusion coefficient $D$ \ssect{10.8.3} is calculated by the equation
\begin{equation*}%
D = \frac{1}{6} \, \frac{\expval{d}^{2}}{\tau_{0}} \, \exp \left(- \, \frac{E}{kT} \right)
\end{equation*}
The quantities are defined in Sects. \ssect{13.2.3}, \ssect{13.2.4} and \ssect{13.2.5}. The diffusion coefficient grows rapidly with temperature. This is due, for the most part, to the sharp drop in the relaxation time $\expval{d}$ \ssect{13.2.4}. Besides, the quantity $\expval{d}$ \ssect{13.2.3} increases somewhat with the temperature $T$.


\subsection{}\label{13.3.3} When the temperature approaches the critical value \ssect{12.3.1}, the average velocity $\expval{v}$ of the particles of the liquid approaches the average velocity of molecules of a real gas and the values of the diffusion coefficient $D$ for liquids become close to those for gases.

At temperatures much lower than the critical one, the diffusion coefficients for liquids are extremely small compared to those for the corresponding vapours or gases at ordinary pressures. For example, $D \approx \SI{1.5d-9}{\meter\squared\per\second}$ for water at $T = \SI{300}{\kelvin}$, while for the vapour of water (steam) in air at the same temperature and at standard atmospheric pressure $D \approx \SI{2d-5}{\meter\squared\per\second}$. 

\subsection{}\label{13.3.4} At temperatures close to the critical one \footnote{See the footnote on page \pageref{13.2.2}.}, the thermal motion in liquids acquires features that differ from those described in \ssect{8.3.5} and approach those of thermal motion in gases. Under these conditions the internal friction (viscosity) in liquids is of the same nature as in gases \ssect{10.8.4}.

At temperatures close to the melting point, the viscosity of liquids cannot be accounted for in the same way as for gases. The mechanism by means of which viscosity arises is a complex one. The coefficient of dynamic viscosity $\eta$ \ssect{10.8.4} for liquids may be related to the \emph{mobility $u_{0}$ of the molecule}. This is defined as the velocity $u$ gained by a molecule subject to an external force $F$ equal to unity: $u_{0} = u/F$. The relation between $\eta$ and $u_{0}$ is an inversely proportional one: $\eta \propto 1/u_{0}$. 

In its turn $u_{0} \propto D/kT$, where $D$ is the diffusion coefficient, $T$ is the absolute temperature, and $k$ is Boltzmann’s constant \ssect{8.4.5}. Consequently, $\eta \propto T/D$ or $\eta \propto T \, \exp (E/kT)$, where $E$ is the activation energy \ssect{13.2.5}. As the temperature is raised the viscosity of liquids drops rapidly, especially in the region of low temperatures. At high pressures the viscosity of liquids increases rapidly with pressure. This is due to the increase in the activation energy \ssect{13.2.5} and the corresponding increase in the relaxation lime \ssect{13.2.4}.

\section{Surface Tension of Liquids}
\label{sec-13.4}

\subsection{}\label{13.4.1} A molecule of a liquid in the surface layer is subject to uncompensated, inwardly directed forces of attraction exerted by the rest of the liquid. As a result, the surface layer exerts a high \emph{internal pressure} on the whole liquid. It is of the order of tens of thousands of atmospheres.

\subsection{}\label{13.4.2} Particles of the surface layer of a liquid have a higher potential energy than those inside the liquid. The reason for this is that in order to emerge from inside the liquid to its surface the molecules must do work to overcome the inwardly directed forces of internal pressure \ssect{13.4.1}. This work increases the potential energy of the molecules that reach the surface. 

The work $W$ that must be performed to increase the area of the surface layer of a liquid isothermally is equal to
\begin{equation*}%
W = \expval{(F_{S} - F_{V})} \, N,
\end{equation*}
where $\expval{(F_{S} - F_{V})}$ is the average difference between the free energy \ssect{11.4.5} per molecule at the surface $F_{S}$ and within the bulk $F_{V}$ of the liquid, and $N$ is the number of molecules in the surface layer of the liquid.

\subsection{}\label{13.4.3} The condition of minimum potential energy of a liquid, required to maintain its stable equilibrium, is complied with when the liquid has its minimum possible free surface area. The state of stable equilibrium of a liquid incompressible body corresponds to the minimum ratio of its surface area to its volume. Therefore, small drops of liquid suspended in air are of spherical shape. A liquid tends to reduce the area of its free surface. Consequently, the surface layer resembles a stretched elastic film in which tensile forces act (see also \ssect{13.4.5}).

\subsection{}\label{13.4.4} The work done in the isothermal formation of unit sur­ face area is called the \emph{surface tension} $\sigma$ of the given liquid at the boundary with another phase \ssect{12.3.3}:
\begin{equation*}%
\sigma = \frac{W}{A} = \expval{(F_{S} - F_{V})} \, \frac{N}{A} = \expval{(F_{S} - F_{V})} \,n_{1}
\end{equation*}
where $n_{1} = N/A$ is the number of molecules per unit area of the surface layer. The surface tension is also calculated by the equation 
\begin{equation*}%
\sigma = \frac{\Delta F }{\Delta A}
\end{equation*}
where $\Delta F$ is the change in the free energy of the surface layer, and $\Delta A$ is the change in the surface area.

The surface tension $\sigma$ depends upon the chemical composition of the liquid and its temperature. It decreases when the temperature is raised and vanishes at the critical temperature \ssect{12.3.1}. The surface tension can be reduced by adding \emph{surface-active substances} to the liquid. This is due to the fact that such substances are adsorbed on the surface layer of the liquid and reduce the free energy of this layer \ssect{11.4.5}.

\subsection{}\label{13.4.4}13.4.5 There is an essential difference between the surface layer of a liquid and an elastic film \ssect{13.4.3}. The surface tension of liquids does not depend upon the surface area and tends to reduce this area to zero. The tension of an elastic film is proportional to its deformation and equals zero at a definite finite area of the film's surface.

This peculiarity in the properties of liquid films is associated with the change in the number of molecules in the surface layer when these films are stretched (or compressed). But the average distances between the molecules and the forces of intermolecular interaction determined by these distances do not change. This is why the magnitude of the surface tension is independent of the area of the free, surface of the liquid.

\subsection{}\label{13.4.6} If the surface of a liquid is bounded by a wetted perimeter \ssect{13.5.1}, then the quantity a is equal to the force acting on unit length of the wetted perimeter, perpendicular to this perimeter. This force lies in a plane tangent to the free surface of the liquid.

\section{Wetting and Capillary Phenomena}
\label{sec-13.5}

\subsection{}\label{13.5.1} The free surface of a liquid, curved adjacent to the wall of the vessel holding the liquid, is called the \emph{meniscus}. The line along which the meniscus contacts the wall is called the \emph{wetted perimeter}. The meniscus is characterized by the \emph{contact angle} $\theta$ between the wetted surface of the wall and the meniscus at their point of contact. When $\theta < \pi/2$ (\figr{fig-13-01}~\inred{(a)}), the liquid is said to \emph{wet} the wall(to be a \emph{wetting} liquid); when $\theta > \pi/2$ (\figr{fig-13-01}~\inred{(b)}) the liquid \emph{does not wet} the wall and is a \emph{nonwetting} liquid. 

\begin{figure}[h]
\centering
\includegraphics[width=0.75\textwidth]{figs/ch-13/fig-13-01.pdf}
\caption{Angle of contact and wetting in liquids. \label{fig-13-01}}
\end{figure}

Wetting (nonwetting) is said to be ideal when $\theta = 0 \, (\theta = \pi)$. Here the meniscus is of spherical shape and is concave or convex. The absence of both wetting and nonwetting corresponds to the condition $\theta = \pi/2$, in which case the liquid has a plane free surface.

\subsection{}\label{13.5.2} The formation of a meniscus is due to the interaction of the molecules with one another and with the particles of the rigid body (wall). Molecule $A$ of the surface layer, located close to the wall of the vessel and having a sphere of molecular action \ssect{12.2.3} of radius $R_{m}$ (range of molecular action) (\figr{fig-13-02}), is subject to the resultant force of attraction exerted on it by all the other molecules of the liquid $(\vb{F}_{1})$ and by all the particles of the wall $(\vb{F}_{2})$. Force $\vb{F}_{2}$ is perpendicular to the wall. This follows from considerations of symmetry. 

\begin{marginfigure}%[h]
\centering
\includegraphics[width=\textwidth]{figs/ch-13/fig-13-02.pdf}
\caption{Range of molecular attraction. \label{fig-13-02}}
\end{marginfigure}

The direction of force $\vb{F}_{1}$ depends upon the shape of the meniscus and the position of molecule $A$ with respect to the wall. For example, if the meniscus is flat \ssect{13.5.1} and molecule $A$ is right at the wall, force $\vb{F}_{1}$ is directed at an angle of \ang{45} to the wall (\figr{fig-13-03}). Molecule A can be in equilibrium only when the resultant force $\vb{F} = \vb{F}_{1} + \vb{F}_{2}$ is perpendicular to the surface of the liquid.\footnote{Here we ignore the gravity force acting on molecule $A$ because it is negligibly small compared to forces $\vb{F}_{1}$ and $\vb{F}_{2}$.} Otherwise, molecule $A$ would move along this surface.


\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-13/fig-13-03.pdf}
\caption{Range of molecular attraction. \label{fig-13-03}}
\end{figure}

\subsection{}\label{13.5.3} The shape of the meniscus is determined by the three possible directions of force $\vb{F}$:
\begin{enumerate}[label=(\alph*)]
\item force $\vb{F}$ is parallel to the surface of the wall, the liquid has a plane surface and $\theta = \pi/2$ (\figr{fig-13-04}~\inred{(a)});
\item force $\vb{F}$ is directed towards the wall; the forces of attraction exerted by the wall on molecule $A$ exceed the forces of attraction exerted on the molecule by all the other molecules of the liquid. The liquid has a concave meniscus with $\theta < \pi/2$, i.e. the liquid wets the wall (\figr{fig-13-04}~\inred{(b)});
\item force $\vb{F}$ is directed towards the liquid; the forces of attraction exerted by the molecules of the liquid on molecule $A$
prevail over those exerted on it by the particles of the wall. The meniscus is convex with $\theta > \pi/2$, i.e. the liquid does not wet the wall (\figr{fig-13-04}~\inred{(c)}).
\end{enumerate}


\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-13/fig-13-04.pdf}
\caption{The shape of meniscus. \label{fig-13-04}}
\end{figure}

\subsection{}\label{13.5.4} The curved shape of the surface layer is the cause of the pressure $\Delta p$, additional to the external pressure, acting on the liquid. This additional pressure is due to the surface tension. This is similar to the way in which a stretched elastic envelope (like a rubber balloon) exerts pressure on the gas it contains.

The additional pressure exerted on a liquid with a surface layer of arbitrary shape is calculated by the equation 
\begin{equation*}%
\Delta p = \sigma \left(\frac{1}{R_{1}} + \frac{1}{R_{2}}  \right)
\end{equation*}
where $\sigma$ is the surface tension \ssect{13.4.4}, and $R_{1}$ and $R_{2}$ are the radii of curvature of any two mutually perpendicular normal sections\footnote{A normal section of the surface through point $A$ is the curve obtained as the intersection of the surface and a plane passed through the normal to the surface at this point. Radii $R_{1}$ and $R_{2}$ are called the principal radii of curvature of the surface layer.} of the surface of the liquid at the point where molecule $A$ is located. The radius of curvature $R_{1}$ (or $R_{2}$) is taken positive if its centre in the corresponding section is inside the liquid. Otherwise, the radius of curvature is taken to be negative. Thus, $\Delta p > 0$ when the meniscus is convex and $\Delta p < 0$ when it is concave. In the case of a plane surface $R_{1} = R_{2} = \infty$, and there is no additional pressure ($\Delta p = 0$). The preceding equation is called the Laplace theorem. For a spherical surface $R_{1} = R_{2 }= R$ and $\Delta p = 2\sigma/R$. Such a surplus pressure exists, for instance, within a bubble of gas of radius $R$, located inside a liquid near the surface.

The surplus pressure inside a soap bubble of radius $R$ is due to the action of both superficial layers of the thin spherical film of soap (inside and outside the bubble). Thus $\Delta p = 4\sigma/R$. 

\subsection{}\label{13.5.5} The level of a liquid in narrow cylindrical vessels (\emph{capillary tubes}) of radius $r$ differs from the level of the liquid in a larger vessel into which the capillary tube is placed vertically. The level in the capillary tube is higher (lower) than in the larger vessel by the amount $h$ if the liquid wets (does not wet) the walls of the vessel. Here
\begin{equation*}%
h = \frac{2 \sigma \cos \theta}{r g \rho}
\end{equation*}
where $\theta$ is the contact angle \ssect{13.5.1}, $\rho$ is the density of the liquid, and $g$ is the acceleration due to gravity \ssect{7.3.3}. This is called \emph{Jurin's rule}.

If the liquid is in a narrow slit of constant width $\delta$ between two parallel plates, the meniscus is of cylindrical shape having the radius $\delta/2$ and the level in the slit is above (for a wetting liquid) or below (for a nonwetting liquid) the level in the vessel in which the plates are immersed by the amount
\begin{equation*}%
h = \frac{2 \sigma \cos \theta}{\delta g \rho}
\end{equation*}

\subsection{}\label{13.5.6} The pressure of saturated vapour \ssect{12.3.2} above the curved surface of a liquid depends upon the shape of the meniscus. If the meniscus is concave (convex), the pressure of the saturated vapour is less (greater) than that above a plane surface by the amount $\Delta p_{\text{ms}}$:
\begin{equation*}%
\Delta p_{\text{ms}} = \frac{\rho}{\rho_{1} - \rho} \, \Delta p
\end{equation*}
where $\rho$ is the density of the saturated vapour, $\rho_{1}$ is the density of the liquid, and $\Delta p$ is the additional pressure due to the curvature of the surface \ssect{13.5.4}.

\section{Vaporization and Boiling of Liquids}
\label{sec-13.6}

\subsection{}\label{13.6.1} \emph{Vaporization}, or \emph{evaporation}, is the process of vapour formation at the free surface of a liquid. It takes place at any temperature and increases as the temperature is raised. There are molecules in the surface layer of a liquid that have high velocities and kinetic energies of thermal motion. The escape of such molecules from the surface into the space above the liquid is the explanation for evaporation with the consequent reduction of the store of internal energy and cooling of the liquid. A measure of the process of vaporization is the \emph{rate of evaporation} $u$, which is the amount of liquid converted into vapour in unit time. The rate $u$ depends upon the external pressure and the motion of the gaseous phase above the free surface of the liquid. Thus
\begin{equation*}%
u = \frac{cA}{p_{0}} \, (p_{v} - p)
\end{equation*}
where $c$ is a constant, $A$ is the area of the free surface of the liquid, $p_{v}$ is the pressure of saturated vapour, $p$ is the pressure of the vapour above the free surface of the liquid being vaporized, and $p_{0}$ is the external barometric pressure.


\subsection{}\label{13.6.2} \emph{Boiling} is the process of intense vaporization of a liquid, not only from its free surface, but throughout the whole volume of the liquid within the bubbles of vapour that are produced in boiling. The bubbles of vapour in a boiling liquid rapidly increase in size, rise to the surface and burst. This causes the turbulence in a boiling liquid. The pressure $p$ within a bubble of vapour located within the liquid is the sum of the external pressure $p_{0}$, the hydrostatic pressure pi of the overlying layers of liquid, and the additional pressure $\Delta p$ due to the surface tension \ssect{13.5.4}. Thus
\begin{equation*}%
p = p_{0} + p_{1} + \Delta p 
\end{equation*}
and
\begin{equation*}%
p_{1}= \rho gh, \qand \Delta p = \frac{2 \sigma}{r}
\end{equation*}
where $r$ is the radius of the vapour bubble, $h$ is the distance from its centre to the surface of the liquid, and $p$ and $\sigma$ are the density and surface tension of the liquid.

The boiling of a liquid begins at the temperature at which the pressure $p_{v}$ of the saturated vapour inside a bubble is at least as high as the pressure $p$ \ssect{13.6.2}:
\begin{equation*}%
p_{v} \geqslant p_{0} + \rho gh + \frac{2 \sigma}{r}
\end{equation*}
If this condition is not complied with, the bubble collapses and the vapour it contains is condensed.

\subsection{}\label{13.6.3} With small radii $r$ of the vapour bubbles, the pressure $p_{v}$ should be quite high and it is necessary to heat the liquid to a high temperature before it starts boiling. If the liquid contains \emph{centres of vaporization} (dust specks, bubbles of dissolved gases, etc.), boiling begins at a much lower temperature. This is due to the fact that the vapour bubbles that appear on the centres of vaporization are of such a size that the effect of the third member in the inequality of \ssect{13.6.2} can be neglected. More­ over, $\rho gh \ll p_{0}$ as a rule, so that the approximate condition for the beginning of boiling can be written in the form
\begin{equation*}%
p_{v} \approx  p_{0}
\end{equation*}
The temperature of a liquid at which the pressure of its saturated vapour is equal to the external pressure is called the \emph{boiling point} of the liquid.

\subsection{}\label{13.6.4} When a liquid boils at constant pressure, its temperature remains constant. The quantity of heat transferred to a boiling liquid is completely expended in converting the molecules of the liquid into vapour. The heat $r_{b}$ required to vapor­ize unit mass of a liquid heated to the boiling point is called the \emph{heat of vaporization}. The value of $r_{b}$ decreases as the boiling point is raised and vanishes at the critical temperature \ssect{12.3.1}.

The change in the internal energy of a liquid \ssect{9.1.1} as a unit of its mass is converted into vapour at the boiling point is called the \emph{internal heat of vaporization}.

\subsection{}\label{13.6.5} The boiling of liquids and the condensation of vapour are examples of first-order phase transitions (cf. \ssect{12.4.2}). Typ­ical of such phase transitions are the simultaneous constant values of pressure and temperature, but the changing ratio of the masses of the two phases \ssect{12.3.3}. For a first-order phase transition to occur, the heat $r_{b}$ (\emph{specific latent heat of vaporization}) must be supplied to or taken away from the system. The heat $r_{b}$ per unit mass is calculated by the \emph{Clausius-Clapeyron equation}:
\begin{equation*}%
r_{b} = (v_{2} - v_{1}) \, T \, \dv{p}{T}
\end{equation*}
where $v_{1}$ and $v_{2}$ are the specific volumes of the substance in the initial and final phases, $T$ and $p$ are the temperature and pressure of the phase transition.

\subsection{}\label{13.6.6} It follows from the Clausius-Clapeyron equation for the boiling of liquids that
\begin{equation*}%
\dv{T}{p} = \frac{v_{\text{v}} - v_{1}}{r_{b}}
\end{equation*}
where $v_{1}$ and $v_{\text{v}}$ are the specific volumes of the liquid and vapour at the boiling point $T$. Since $v_{\text{v}} > v_{1}$ and $r_{b} > 0$, $\dv*{T}{p} > 0$, i.e. the boiling point increases with pressure. For instance, at the pressure $p = \SI{1.25d7}{\pascal}$ water can be heated without boiling to a temperature at which lead can be melted in the water. The melting point of lead is \SI{900}{\kelvin}.



