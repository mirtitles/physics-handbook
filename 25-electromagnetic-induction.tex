% !TEX root = handbook-physics.tex
% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode






\chapter{Electromagnetic Induction}
\label{ch-25}

\section{Basic Law of Electromagnetic Induction}
\label{sec-25.1}

\subsection{}\label{25.1.1} The \redem{phenomenon of electromagnetic induction} is the setting up of an \redem{induced electric field} in a conducting loop, or circuit, located in a varying magnetic field. The energy measure of this induced field is the \redem{electromotive force $\emf_{i}$ of electromagnetic induction}. If the loop is closed, the induced electric field causes ordered motion of electrons in the loop, i.e. a current, called the \redem{induced current}, is produced.

\subsection{}\label{25.1.2} \redem{Faraday's law of electromagnetic induction} states that the emf of electromagnetic induction in a loop, or circuit, is proportional to the rate of change of the magnetic flux \ssect{23.5.5} through the surface area bounded by this loop. Thus
\begin{align*}%
\emf_{i} & = - \dv{\Phi_{m}}{t} &&  \text{(in SI units),} \\
\emf_{i} & = - \frac{1}{c}\,\dv{\Phi_{m}}{t} &&  \text{(in Gaussian units).\sidenote{In all the formulas written in Gaussian units, $c$ is the electrodynamic constant.}}
\end{align*}



\ssect{23.2.2}

The cause of the change in the magnetic flux is unessential. It may be due to distortion of the loop or its motion in the external magnetic field or to any other reasons for the change in the magnetic field with time. When a closed conducting circuit (or loop) moves in a magnetic field, emf’s are induced in all of the parts of the loop that cut the lines of magnetic induction. The algebraic sum of these emf’s \ssect{25.1.3} is equal to the total emf of the loop. The work that must be done to move a closed loop, or circuit, in a magnetic field is equal to the work of the current induced in the loop.

In calculating $\Phi_{m}$ and $\emf_{i}$ the directions around the loop \ssect{21.3.2} and of the outward normal $\vu{n}$ (\ssect{23.5.5} and \ssect{23.5.8}) are to be coordinated so that from the head of vector n the direction around the circuit (or loop) is seen to be counterclockwise. If a closed circuit consists of $N$ turns connected in series (for example, the solenoid in Section \ssect{23.3.7}), then the magnetic flux $\Phi_{m}$ in Faraday’s law is to be replaced by the flux, or magnetic, linkage $\Psi$ of the circuit (\ssect{23.6.5}:
\begin{align*}%
\emf_{i} & = - \dv{\Psi}{t} &&  \text{(in SI units),} \\
\emf_{i} & = - \frac{1}{c}\,\dv{\Psi}{t} &&  \text{(in Gaussian units)}.
\end{align*}

\subsection{}\label{25.1.3} The emf of electromagnetic induction in a loop (or circuit) is said to be positive if the magnetic moment vector $\vb{p}_{m}$ of the corresponding induction current \ssect{23.3.4} makes an acute angle with the lines of magnetic induction of the field that induces this current. Otherwise, $\emf_{i}$ is said to be negative. In \figr{fig-25-01}~\drkgry{(a)}, the emf $\emf_{i} < 0$, whereas in \figr{fig-25-01}~\drkgry{(b)}, $\emf_{i} > 0$. 

\begin{figure}%[-2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-25/fig-25-01.pdf}
\sidecaption{Determining the direction of emf generated.\label{fig-25-01}}
\end{figure}


\subsection{}\label{25.1.4} The minus sign in the formula for  complies with the \redem{Lenz law}, which states that upon any change in the magnetic flux through a surface bounded by a closed loop, the current induced in the loop is always of a direction in which the magnetic field set up by the induced current opposes the change in the magnetic flux that induced the current.

\subsection{}\label{25.1.5} An emf of electromagnetic induction is developed in a part of a conductor which, in its motion, cuts lines of induction of a magnetic field \ssect{23.1.3}. In the case illustrated in Fig. 25.2, the conduction electrons of the metal \ssect{20.3.1} are subject to the Lorentz force \ssect{24.1.1}. Thus $\vb{F}_{L} = — e [(\vb{v} + \vb{v}') \cross \vb{B}]$, where $\vb{v}$ is the velocity of the part $AC$ of the conductor in the magnetic field, whose induction vector $\vb{B}$ is perpendicular to the plane passing through the part of the conductor and the velocity vector of its motion. The electrons travel orderly along the conductor at the velocity $\vb{v}'$ by the action of the component of the Lorentz force tangent to the conductor in the direction from $A$ to $C$. The motion of the electrons ceases when the electrostatic field set up in the conductor and acting on the electrons with the force $e\vb{E}$ counterbalances the Lorentz force.
\begin{figure}%[-2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-25/fig-25-02.pdf}
\sidecaption{An emf of electromagnetic induction is developed in a part of a conductor which, in its motion, cuts lines of induction of a magnetic field.\label{fig-25-02}}
\end{figure}


According to Ohm’s law for an open circuit (at $I = 0$) \ssect{21.2.5}, the equilibrium potential difference $\Delta V = V_{A} - V_{C}$ between points $A$ and $C$ that is established at $\vb{v}' = 0$ is equal to
\begin{equation*}%
\Delta V = V_{A} - V_{C} = - \emf_{i},
\end{equation*}
where $\emf_{i}$ is the electromotive force of induction, because there are no sources of electrical energy in part $AC$ of the conductor. The emf of electromagnetic induction in a piece of conductor of length $l$, moving at the velocity $v$, is
\begin{align*}%
\emf_{i} & = - Bvl, \qor \\
\emf_{i} & = - \dv{\Phi_{m}}{t} \,\, \text{(in SI units)}, 
\end{align*}

where $\dv*{\Phi_{m}}{t}$ is the ratio of the magnetic flux through the surface generated by the conductor in its motion during an infinitesimal length of time to the value $\dd t$ of this length of time or, in other words, the velocity at which the conductor cuts the lines of induction of the magnetic field (cf. Section \ssect{25.1.2}). 

\subsection{}\label{25.1.6} The phenomenon of electromagnetic induction in stationary closed conducting circuits, located in an external varying magnetic field, cannot be explained by means of the Lorentz force, which has no effect on fixed charges \ssect{24.1.1}.

The phenomenon of electromagnetic induction in stationary conductors is due to the fact that the varying magnetic field excites a rotational electric field. The circulation of the strength vector of this field along the closed path $L$ of the conductor \ssect{16.1.4} is the emf of electromagnetic induction. Thus
\begin{align*}%
\emf_{i} & = \oint \vb{E}\, \dd \vb{l} = \dv{\Phi_{m}}{t} &&  \text{(in SI units),} \\
\emf_{i} & = \oint \vb{E}\, \dd \vb{l} = \dv{\Phi_{m}}{t} \frac{1}{c}\,\dv{\Psi}{t} &&  \text{(in Gaussian units)}.
\end{align*}
where the partial derivative $\dv*{\Phi_{m}}{t}$ takes into account the dependence of the magnetic induction flux only on time. The choice of the direction of normal $\vu{n}$ in calculating the magnetic flux is discussed in Section \ssect{25.1.2}.

\subsection{}\label{25.1.7} The magnitude $q$ of the electric charge passing through the cross section of the wire of the turn in which the current is induced is
\begin{align*}%
q & = \frac{\Phi_{m}' - \Phi_{m}''}{R} && \text{(in SI units),} \\
q & = \frac{1}{c} \, \frac{\Phi_{m}' - \Phi_{m}''}{R}  &&  \text{(in Gaussian units)},
\end{align*}
where $\Phi_{m}'$ and $\Phi_{m}''$ are the values of the magnetic flux through the area of the turn in its initial and final positions, and $R$ is the electrical resistance of the turn.


\section{Phenomenon of Self-Induction}
\label{sec-25.2}

\subsection{}\label{25.2.1} \redem{Self-induction} is the production of an induced electric field in a circuit as a result of the variations of current in the same circuit. The energy characteristic of the induced field is the emf of self-induction $\emf_{S}$.

\subsection{}\label{25.2.2} The intrinsic magnetic field of the current in the circuit produces the magnetic flux Oms through the surface $S$ bounded by the current-carrying loop. Thus
\begin{equation*}%
\Phi_{ms} = \int_{S} B_{n} \, \dd S.
\end{equation*}
where $B_{n}$ is the projection of the induction vector $\vb{B}$ of the magnetic field \ssect{23.1.2} on the normal $\vu{n}$ to the element of surface $\dd S$. The quantity $\Phi_{ms}$ is called the \redem{magnetic flux of self-induction of the loop}. Making use of the Biot-Savart-Laplace law \ssect{23.2.2}, $\Phi_{ms}$ can be calculated for the case in which the loop is in a non-ferromagnetic medium \ssect{26.5.2}:
\begin{align*}%
\Phi_{ms} & = I \frac{\mu_{0}}{4 \pi} \int_{S} \dd S \, \oint_{L} \frac{\mu_{r}}{r^{3}} [\dd\vb{l} \cross 	\vb{r}]_{n}&& \text{(in SI units),} \\
\Phi_{ms} & = \frac{1}{c}I \int_{S} \dd S \, \oint_{L} \frac{\mu_{r}}{r^{3}} [\dd\vb{l} \cross 	\vb{r}]_{n} &&  \text{(in Gaussian units)}, \qor \\
\Phi_{ms} & = LI && \text{(in SI units),} \\
\Phi_{ms} & = \frac{1}{c}LI &&  \text{(in Gaussian units)},
\end{align*}
where $L$ is called the \redem{self-inductance} of the loop and is determined by the formulas
\begin{align*}%
L & = \frac{\mu_{0}}{4 \pi} \int_{S} \dd S \, \oint_{L} \frac{\mu_{r}}{r^{3}} [\dd\vb{l} \cross 	\vb{r}]_{n}&& \text{(in SI units),} \\
L & = \int_{S} \dd S \, \oint_{L} \frac{\mu_{r}}{r^{3}} [\dd\vb{l} \cross 	\vb{r}]_{n} &&  \text{(in Gaussian units).}
\end{align*}
Here $\mu_{0}$ is the magnetic constant in SI units \ssect{23.2.2}, $\mu_{r}$ is the relative magnetic permeability of the medium, $\vb{r}$ is the radius vector from element $\dd \vb{l}$ of the loop to element $\dd S$ of surface $S$, bounded by the loop, and subscript $n$ indicates the projection on the normal to the element $\dd S$.

\subsection{}\label{25.2.3} The self-inductance $L$ of the loop is numerically equal to the self-induction flux at a current of $I = \SI{1}{\ampere}$ (in SI units) or $I = c$ (in Gaussian units). Self-inductance $L$ depends only upon the geometric shape and dimensions of the loop, as well as on the magnetic properties of the medium it is located in. The self-inductance of a sufficiently long solenoid \ssect{23.4.8} is
\begin{align*}%
L & = \frac{\mu_{0}\mu_{r}N^{2}S}{l}= \mu_{0}\mu_{r}n^{2}V&& \text{(in SI units),} \\
L & = \frac{4 \pi \mu_{r}N^{2}S}{l}= 4 \pi\mu_{r}n^{2}V &&  \text{(in Gaussian units),}
\end{align*}
where $l$ and $S$ are the length and cross-sectional area of the solenoid, $N$ is the total number of turns, $n = N/l$ is the number of turns per unit length, and $V = Sl$ is the volume of the solenoid.

\subsection{}\label{25.2.4} The expression for the emf of self-induction follows from Faraday’s law \ssect{25.1.2}:
\begin{align*}%
\emf_{S} & = - \dv{\Phi_{ms}}{t} = - \dv{t} (LI)&& \text{(in SI units),} \\
\emf_{S} & = - \frac{1}{c} \,\dv{\Phi_{ms}}{t} = - \frac{1}{c^{2}}\dv{t} (LI)&&  \text{(in Gaussian units).}
\end{align*}
For a rigid (undeformable) loop in a non-ferromagnetic medium \ssect{26.5.2} $L = \text{const.}$ Hence
\begin{align*}%
\emf_{S} & = - L \dv{I}{t} && \text{(in SI units),} \\
\emf_{S} & = - \frac{L}{c^{2}} \dv{I}{t} &&  \text{(in Gaussian units).}
\end{align*}

\subsection{}\label{25.2.5} The electromotive force of self-induction causes a \redem{self-induction current} to flow in the loop. According to the Lenz law \ssect{25.1.4}, this current counteracts the change in the main current in the circuit, impeding its increase or decrease. The self-inductance of the loop \ssect{25.2.3} is a measure of its inertia to changes of the current.

\subsection{}\label{25.2.6} The law for the change of the current in closing and opening a circuit, i.e. with a non-steady state in the circuit, is
\begin{equation*}%
I = I_{0} \exp \left( -\frac{Rt}{L}\right) + \frac{\emf}{R} \, \left(1 - \exp\left(-\frac{Rt}{L}  \right) \right),
\end{equation*}
where $I_{0}$ is the current at the initial instant of time ($t = 0$), $R$ is the electrical resistance of the circuit, $L$ is its self-inductance, and  is the algebraic sum of the emf’s of the sources of electrical energy connected into the circuit \ssect{21.2.2}.
\begin{marginfigure}[-3cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-25/fig-25-03.pdf}
\sidecaption{Self induced current as a function of time when it is switched on.\label{fig-25-03}}
\end{marginfigure}



When the source of emf is switched in there is no initial current in the circuit $(I_{0}= 0)$:
\begin{equation*}%
I =  \frac{\emf}{R} \, \left(1 - \exp\left(-\frac{Rt}{L}  \right).\right)
\end{equation*}
The current in the circuit increases from zero to the value $\emf/R$, corresponding to that of the constant current. The higher the
ratio $R/L$ (\figr{fig-25-03}), the more rapidly the current increases. When the source of emf is switched off, $\emf = 0$:
\begin{equation*}%
I =  I_{0}\, \left(1 - \exp\left(-\frac{Rt}{L}  \right) \right).
\end{equation*}
The current decreases exponentially from the initial value $I_{0}$. The higher the ratio $R/L$, the more rapidly the current decreases (\figr{fig-25-04}).


\begin{marginfigure}[-4cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-25/fig-25-04.pdf}
\sidecaption{Self induced current as a function of time when it is switched off.\label{fig-25-04}}
\end{marginfigure}

\section{Mutual Induction}
\label{sec-25.3}

\subsection{}\label{25.3.1} An emf is induced in all conductors in the vicinity of a circuit in which the current varies. This phenomenon is called \redem{mutual induction}. Upon a change in the current $I_{1}$ in circuit $\mathit{1}$ (\figr{fig-25-05}), for example, an emf $\emf_{21}$ mutual induction is induced in circuit $\mathit{2}$. It equals
\begin{align*}%
\emf_{21} & = - \dv{\Phi_{m21}}{t} && \text{(in SI units),} \\
\emf_{21} & = - \frac{1}{c} \,\dv{\Phi_{m21}}{t} &&  \text{(in Gaussian units),}
\end{align*}
where $\Phi_{m21}$ is the magnetic flux through the surface bounded by the second circuit. This flux is due to the magnetic field set up by the current in the first circuit (magnetic flux of mutual induction).


\begin{figure}%[-4cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-25/fig-25-05.pdf}
\sidecaption{Mutual induction between two coils.\label{fig-25-05}}
\end{figure}



\subsection{}\label{25.3.2} By analogy with section \ssect{25.2.2}, the magnetic flux $\Phi_{m21}$ is proportional to the current $I_{1}$. Thus
\begin{align*}%
\Phi_{m21} & = M_{21} I_{1} && \text{(in SI units),} \\
\Phi_{m21} & = - \frac{1}{c} \,M_{21} I_{1}&&  \text{(in Gaussian units),}
\end{align*}
where $M_{21}$ is called the \redem{mutual inductance} of the second and first circuits. The quantity $M_{21}$ depends upon the shape, dimensions and relative arrangement of the circuits, as well as the relative magnetic permeability of the medium surrounding them. The magnetic flux $\Phi_{m12}$ through the surface bounded by the first circuit is due to the magnetic field set up by current $I_{2}$ in the second circuit. Thus
\begin{align*}%
\Phi_{m12} & = M_{12} I_{2} && \text{(in SI units),} \\
\Phi_{m12} & = - \frac{1}{c} \,M_{12} I_{2}&&  \text{(in Gaussian units),}
\end{align*}
where $M_{12}$ is the mutual inductance of the first and second circuits.

If the circuits are in a non-ferromagnetic medium \ssect{26.5.2}, then $M_{21}= M_{12}$. or ferromagnetic media $M_{21}$ and $M_{12}$ are not equal and depend, in addition to the above-mentioned factors, on the currents in the two circuits and the way in which they vary.

\subsection{}\label{25.3.3} The expressions for the emf of mutual induction, under the condition that $M_{21}$ and $M_{12}$ are both constant values, are
\begin{align*}%
\emf_{21} & = - M_{21}\dv{I_{1}}{t} && \text{(in SI units),} \\
\emf_{21} & = - \frac{1}{c^{2}} \,M_{21}\dv{I_{1}}{t} &&  \text{(in Gaussian units),}\\
\emf_{12} & = - M_{12}\dv{I_{2}}{t} && \text{(in SI units),} \\
\emf_{12} & = - \frac{1}{c^{2}} \,M_{12}\dv{I_{2}}{t} &&  \text{(in Gaussian units).}
\end{align*}
An example of the application of mutual induction is the principle of the transformer which serves to raise or lower the volt­ age of alternating current.


\section[Energy of a Magnetic Field]{Energy of a Magnetic Field Set up by an Electric Current}\sidenote{It is assumed in this section that the conductors carrying currents are in a non-ferromagnetic, homogeneous and isotropic medium.}
\label{sec-25.4}

\subsection{}\label{25.4.1} To produce an electric current in a closed circuit (or loop) and to increase this current from zero to $I$ it is necessary to do work $W$ to overcome the emf of self-induction that impedes the increase in current \ssect{25.2.6}. Thus
\begin{align*}%
W & = \frac{\Phi_{ms}I}{2} = \frac{LI^{2}}{2} && \text{(in SI units),} \\
W &  = \frac{1}{c} \frac{\Phi_{ms}I}{2} = \frac{1}{c^{2}}\frac{LI^{2}}{2} = &&  \text{(in Gaussian units),}
\end{align*}
where $\Phi_{ms}$ is the magnetic flux of self-induction of the loop \ssect{25.2.2} and $L$ is the self-inductance of the loop \ssect{25.2.2}. According to the energy conservation law, $W$ determines the \redem{intrinsic energy} $E_{m}$ of current $I$ in a circuit. Thus
\begin{align*}%
E_{m} & =  \frac{LI^{2}}{2} && \text{(in SI units),} \\
E_{m} &  =  \frac{1}{c^{2}}\frac{LI^{2}}{2} &&  \text{(in Gaussian units).}
\end{align*}

\subsection{}\label{25.4.2} Increasing together with the current in a circuit is the magnetic field set up by this current. Hence, the intrinsic energy of a current is dealt with as \redem{energy of} this \redem{magnetic field}. In a long solenoid \ssect{23.3.8}, for instance, having a uniform magnetic field \ssect{23.1.2},
\begin{align*}%
E_{m} & =  \frac{1}{2} \mu_{0}\mu_{r} n^{2}I^{2}V&& \text{(in SI units),} \\
E_{m} &  =  \frac{2 \pi \mu_{r}}{c^{2}}  n^{2}I^{2}V  &&  \text{(in Gaussian units),}
\end{align*}
where $V$ is the volume of the solenoid, $n$ is the number of turns per unit length, $\mu_{0}$ is the magnetic constant \ssect{23.2.2}, and $\mu_{r}$ is the relative permeability of the medium.

\subsection{}\label{25.4.3} The \redem{volume energy density} $E_{m}^{d}$ of a magnetic field is the energy per unit volume of the field. Thus
\begin{equation*}%
E_{m}^{d} = \dv{E_{m}}{V}.
\end{equation*}
For a uniform magnetic field \ssect{23.1.3}, $E_{m}^{d} = E_{m}/V$.
The volume energy density of any, including a nonuniform magnetic field, is expressed by the formula
\begin{align*}%
E_{m}^{d} & =  \frac{1}{2} BH = \frac{1}{2} \mu_{0}\mu_{r}  H^{2} = \frac{1}{2} \frac{B^{2}}{\mu_{0}\mu_{r} }&& \text{(in SI units),} \\
E_{m}^{d} & =  \frac{BH}{8\pi}  = \frac{\mu_{r}  H^{2}}{8 \pi} = \frac{1}{8 \pi} \frac{B^{2}}{\mu_{r} }&&  \text{(in Gaussian units).}
\end{align*}Here $B$ and $H$ are the magnitudes of the vectors of magnetic induction \ssect{23.1.2} and strength \ssect{23.2.3} at the point being considered in the magnetic field. The remaining notation is defined in Section \ssect{25.4.2}.

\subsection{}\label{25.4.4} The energy $E_{m}$ of a magnetic field, localized in the volume $V$ is
\begin{align*}%
E_{m} & =  \int_{V} \frac{BH}{2}\, \dd V&& \text{(in SI units),} \\
E_{m} &  = \int_{V} \frac{BH}{8\pi}\, \dd V &&  \text{(in Gaussian units).}
\end{align*}
\subsection{}\label{25.4.5} If the magnetic field is set up by an arbitrary system of n circuits, or loops, with the currents $I_{1}, \, I_{2}, \, I_{3}, \ldots{}, I_{n}$, the energy of the field can be expressed by the formula of Section \ssect{25.4.4}, in which $B$ and $H$ are understood to be the magnitudes of vectors $\vb{B}$ and $\vb{H}$ of the resultant magnetic field, in accordance with the principle of superposition of fields \ssect{15.2.2}. Moreover, the energy of the magnetic field in this case is
\begin{equation*}%
E_{m} = \sum_{k=1}^{n} \frac{\Phi_{mk}I_{k}}{2},
\end{equation*}
where $\Phi_{mk}$ is the total magnetic flux linked to the $k$-th loop \ssect{23.6.5}. In calculating this flux, the normal $\vu{n}_{k}$ to the surface bounded by the loop is such that from the head of vector $\vu{n}_{k}$ the current is seen to be counterclockwise. The magnetic flux $\Phi_{mk}$ equals
\begin{equation*}%
\Phi_{mk} = \Phi_{msk} + \Phi_{mmk},
\end{equation*}
where $\Phi_{msk}$ is the magnetic flux of self-induction of the $k$-th loop \ssect{25.2.2} and $\Phi_{mmk}$ is the magnetic flux of its mutual induction, produced by all the other loops carrying current \ssect{25.3.1}. Hence the energy $E_{m}$ of the magnetic field is equal to
\begin{align*}%
E_{m} & =  \sum_{k=1}^{n} \frac{L_{k}I_{k}}{2} + \frac{1}{2} \sum_{k=1}^{n}\sum_{l=1(l \neq k)}^{n} M_{kl} I_{k}I_{l}&& \text{(in SI units),} \\
E_{m} &  =\frac{1}{c^{2}} \, \sum_{k=1}^{n} \frac{L_{k}I_{k}}{2} + \frac{1}{2c^{2}} \sum_{k=1}^{n}\sum_{l=1(l \neq k)}^{n} M_{kl} I_{k}I_{l} &&  \text{(in Gaussian units).}
\end{align*}
The first term is the sum of the intrinsic energies of all the cur­ rents \ssect{25.4.1}. The second term is called the \redem{mutual energy of the currents}. Here $M_{kl}$ is the mutual inductance of the $k$-th and $l$-th loops, or circuits, \ssect{25.3.2} with the currents $I_{k}$ and $I_{l}$.

% for multiline subscript 
% \lim_{\substack{x\rightarrow 0\\y\rightarrow 0}} f(x,y)
% $\displaystyle \mathop{\sum_{i<j ; m\in\mathbf{Z}}}_{d\in\mathcal{N}}  $

