% !TEX root = handbook-physics.tex
% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode






\chapter{Polarization of Light}
\label{ch-35}

\section{Polarization of Light in Reflection and Refraction
at the Interface Between Two Dielectric Media}
\label{sec-35.1} 


\subsection{}\label{35.1.1} Light emitted by ordinary (nonlaser) sources is made up of a set of a great many plane polarized wave trains, or packets \ssect{32.1.3}, whose electric vectors $\vb{E}$ vary along all possible directions perpendicular to the ray \ssect{30.2.1}. Light is said to be
\redem{natural}, or \redem{unpolarized}, if not one of the above-indicated directions of vibration is predominant. In natural light, the resultant strength $\vb{E}$ oscillates at each point of the field in a direction that rapidly and randomly varies in a plane perpendicular to the ray.

Light is said to be \redem{partially polarized} if in it there is a predominant direction of oscillation of vector $\vb{E}$. Partially polarized light can be regarded as a collection (``mixture'') of natural and linearly polarized light, simultaneously propagating in a single direction.

\subsection{}\label{35.1.2} The \redem{polarization of light} is the separation of linearly polarized light out of natural or partially polarized light. Special devices, called \redem{polarizers}, are used for this purpose. Their principle
of operation is based on the polarization of light upon
being reflected or refracted by the interface between two dielectric media, as well as on the phenomenon of birefringence \ssect{35.2.1} and dichroism \ssect{35.2.10}. The same devices can be utilized as \redem{analyzers}, i.e. devices to determine the kind and degree of polarization of light.

\begin{marginfigure}%[-3cm] 
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-35/fig-35-01.pdf}
\sidecaption{Analysing linearly polarized light.\label{fig-35-01}}
\end{marginfigure}


Assume that linearly polarized light, whose electric vector $\vb{E}_{p}$ is along the line $p$-$p$ and oscillates with the amplitude $A_{p}$, falls on the analyzer perpendicularly to the plane of \figr{fig-35-01}. Assume further that the electric vector $\vb{E}_{a}$ of the light passed by the analyzer is along line $a$-$a$, which makes the angle $\alpha$ with line $p$-$p$. The incident light can be represented in the form of two waves, linearly polarized in mutually perpendicular planes \ssect{31.1.8}. The wave whose electric vector $\vb{E}_{1}$ oscillates along the direction perpendicular to $a$-$a$, with the amplitude $A_{1} = A_{p} \sin \alpha$, cannot pass through the analyzer. But the other wave, whose electric vector $\vb{E}_{2}$ oscillates along the direction of $a$-$a$, with the amplitude $A_{2} = A_{p} \cos \alpha$ passes completely through the analyzer. Consequently, the amplitude of the light emerging from the analyzer is
\begin{equation*}%
A_{a} = A_{2} = A_{p} \cos \alpha.
\end{equation*}
The intensities $I_{a}$ and $I_{p}$ of the linearly polarized light incident on and emerging from the analyzer, respectively, are related by the \redem{Malus law}:
\begin{equation*}%
I_{a}= I_{p} \cos^{2} \alpha.
\end{equation*}
The \redem{principal plane of a polarizer} (or \redem{analyzer}) is the plane of polarization [plane of oscillation, according to previous terminology
\ssect{31.1.7}] of light passed through the polarizer (or analyzer).

\subsection{}\label{35.1.3} In investigating the laws of light polarization as a result of reflection and refraction of natural light, the light can be conveniently dealt with as a combination of linearly polarized waves, of equal intensities, of two types: $s$- and $p$-waves \ssect{31.5.4}.

The reflection coefficient \ssect{31.5.6} of the $s$-waves ($R_{s}$) is always greater than that ($R_{p}$) of the $p$-waves. Therefore, in contrast to incident natural light, reflected and transmitted (refracted) light is partially polarized. Oscillations of the electric field vector
$\vb{E} $ of the $s$-type (perpendicular to the plane of incidence) predominate in reflected light, whereas oscillations of the $p$-type (in the plane of incidence) predominate in transmitted light.

\redem{Brewster's law} states that reflected light is completely linearly polarized at the angle of incidence $i = i_{Br}$, complying with the condition $\tan i_{Br} =  n_{21}$, where $n_{21}$ is the relative refractive index of a medium reflecting light.

The angle $i_{Br}$ is called \redem{Brewster's} angle. When $i = i_{Br}$, the reflected and refracted rays are perpendicular to each other and the the reflection coefficient of the $p$-wave is $R_{p} = 0$ \ssect{31.5.6}. Hence, only $s$-type waves are reflected. But their reflection coefficient is considerably less than unity (about 0.15 for glass). Thus, the transmitted light is only partially polarized.

\subsection{}\label{35.1.4} Polarization of transmitted light can be enhanced by subjecting the light to a number of consecutive reflections and refractions. This is done in a stack consisting of several identical and parallel plates of some transparent dielectric (for instance, glass), set at the Brewster angle to the incident light beam.

If the number of plates in the stack is sufficiently large, then the light passing through the stack is found to be practically linearly polarized ($p$-type). In the absence of light absorption in the stack, the intensities $I_{s}$ and $I_{p}$ of the reflected and transmitted linearly polarized light are the same and equal to one-half of the intensity $I_{0}$ of the incident natural light. Thus
\begin{equation*}%
I_{s} = I_{p} = \frac{1}{2} I_{0}.
\end{equation*}




\subsection{}\label{35.1.5} According to the conceptions of classical electron theory, the formation of the reflected wave is due to the secondary waves emitted by the molecules, the
oscillators of the light reflecting medium \ssect{34.1.3}. Oscillators (vibrating electric dipoles), whose axis is perpendicular to the plane of incidence, correspond to a wave of the $s$-type. These oscillators are shown in \figr{fig-35-02}
by black dots on the refracted and reflected rays. As is evident from the polar directional diagram (\figr{fig-35-04}), such oscillators should radiate intensively in all directions lying in the plane of incidence, i.e. they should participate in the formation of both the reflected and the refracted $s$-waves.

\begin{marginfigure}%[-3cm] 
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-35/fig-35-02.pdf}
\sidecaption{Polarization of light on refraction and reflection.\label{fig-35-02}}
\end{marginfigure}



Oscillators, whose axis lies in the plane of incidence and is
perpendicular to the refracted ray (shown in \figr{fig-35-02} by transverse dashes), correspond to a wave of the $p$-type. Oscillators do not radiate along their axis (\figr{fig-35-04}), and at $i = i_{Br}$ the reflected ray is perpendicular to the refracted one, thereby being parallel to the axes of these oscillators. Hence, at $i = i_{Br}$ these oscillators do not radiate in the direction of the reflected ray, making no contribution to the reflected wave. Consequently, the reflected light is completely linearly polarized ($s$-type
waves).

\section{Birefringence (Double Refraction)}
\label{sec-35.2} 


\begin{marginfigure}%[-3cm] 
\centering
\includegraphics[width=\textwidth]{figs/ch-35/fig-35-03.pdf}
\sidecaption{The birefringence of light in a crystal
of Iceland spar (\ce{CaCO3}).\label{fig-35-03}}
\end{marginfigure}



\subsection{}\label{35.2.1} Most crystals are optically anisotropic \ssect{30.1.6}. Their relative dielectric permittivity (dielectric constant) and refractive index depend on the direction of the electric vector $\vb{E}$ of the light wave. Observed in optically anisotropic crystals is the phenomenon of \redem{double refraction}, or \redem{birefringence}, which consists in the splitting of a light ray, falling on a surface of the crystal, into two refracted rays. The birefringence of light in a crystal
of Iceland spar (\ce{CaCO3}) is illustrated in \figr{fig-35-03}.

\subsection{}\label{35.2.2} The \redem{optic axis of a crystal} is the direction in an optically anisotropic crystal along which light propagates without undergoing birefringence. It is necessary to underline that the optic axis of a crystal is no single special straight line similar, for instance, to the axis of symmetry of a body. It only characterizes a certain \redem{direction} in the crystal and can be passed through any point of the crystal.

Depending upon their type of symmetry, optically anisotropic
crystals can be either \redem{uniaxial} or \redem{biaxial}, i.e. they can have either one or two optic axes. An example of a uniaxial crystal is Iceland spar, whose optic axis coincides in direction with the diagonal $M_{0}N_{0}$ of the crystal (\figr{fig-35-03}).

The \redem{principal plane} (or \redem{principal section}) \redem{of a uniaxial crystal} for a certain ray is the plane passing through this ray and intersecting the optic axis.


\begin{marginfigure}%[-3cm] 
\centering
\includegraphics[width=\textwidth]{figs/ch-35/fig-35-04.pdf}
\sidecaption{The ordinary and extraordinary rays.\label{fig-35-04}}
\end{marginfigure}


\subsection{}\label{35.2.3} In a uniaxial crystal, one of the rays formed in birefringence (double refraction) obeys the ordinary laws of light refraction \ssect{31.5.3}. It lies in the plane of incidence and complies with Snell’s law. It is therefore called the \redem{ordinary}, or \redem{$O$ ray}. The
second ray is called the \redem{extraordinary} or \redem{$E$ ray} because it generally does not lie in the plane of incidence and does not obey Snell’s law. 

For example, even in the case of normal incidence of light on a plate cut from a uniaxial crystal, the extraordinary ray is refracted (\figr{fig-35-04}). Its refractive angle $r_{e}$ depends on how the surface of the plate is oriented with respect to the optic axis of the crystal. It may be equal to zero only in two cases:
\begin{enumerate}[label=(\alph*)]
\item if the surface of the plate is perpendicular to the optic axis (light propagates in the plate along the optic axis and is thereby not subject to birefringence);
\item if the surface of the plate is parallel to the optic axis (light propagates in the plate perpendicular to the optic axis).
\end{enumerate}
In a biaxial crystal both refracted rays behave like extraordinary ones.

\subsection{}\label{35.2.4} Birefringence indicates that a light wave incident on an optically anisotropic crystal produces two waves, propagating through the crystal, generally in different directions. In a uniaxial crystal, they are called the \redem{ordinary} and \redem{extraordinary} waves. The ordinary and extraordinary rays indicate the directions of the Umov-Poynting vectors \ssect{31.2.3} of the corresponding
waves in the crystal, i.e. the direction of energy transfer
by these waves.

The ordinary and extraordinary waves are linearly polarized
\ssect{31.1.7}.\sidenote{Frequently, mention is made of linear polarization of the ordinary and extraordinary rays, actually meaning the polarization of the waves corresponding to them.} In the ordinary wave, vector $\vb{E}$ is perpendicular to the principal plane of the crystal for the ordinary ray. The
electric vector $\vb{E}$ of the extraordinary wave lies in the principal plane of the crystal for the extraordinary ray. The directions of vectors $\vb{E}$ in the ordinary and extraordinary waves are conditionally shown in \figr{fig-35-04} by black dots on the ordinary ray and transverse dashes on the extraordinary ray (it is assumed that both rays, and optic axis $MN$ intersecting them, lie
in the plane of the figure).



\begin{marginfigure}%[-3cm] 
\centering
\includegraphics[width=\textwidth]{figs/ch-35/fig-35-05.pdf}
\sidecaption{The velocity of ordinary and extraordinary rays in an anisotropic crystal.\label{fig-35-05}}
\end{marginfigure}

\subsection{}\label{35.2.5} The \redem{ray velocity of the wave}, or simply \redem{ray velocity}, in an optically anisotropic crystal is the velocity $\vb{v}$ of energy transfer by the wave. In a uniaxial crystal, the \redem{velocity $\vb{v}_{o}$ of the ordinary ray} is numerically equal in all directions: $v_{o} = c/n_{o}$, where $c$ is the velocity of light in free space (or in vacuum), and $n_{o} = \text{const}.$ is the \redem{refractive index of the crystal for the ordinary ray}. Accordingly, the \redem{velocity $\vb{v}_{e}$ of the extraordinary ray} is numerically equal to $v_{e} = c/n_{e}$, where $n_{e}$ is the refractive index of the crystal for the extraordinary ray. The values of $n_{e}$ and $v_{e}$ depend on the direction of the extraordinary ray with respect to the optic axis of the crystal. For a ray propagating along the optic axis,
$n_{e} = n_{o}$ and $v_{e} = v_{o}$. The value of $n_{e}$ differs to the greatest extent from that of $n_{o}$ for a direction perpendicular to the optic axis: $n_{e} = n_{e0}$.

\subsection{}\label{35.2.6} The \redem{ray surface of a wave in a crystal} is the locus of the heads of vectors $\vb{v}$ of the ray velocity of the wave that extend from a certain point $O$ in the crystal in all possible directions. 

In a uniaxial crystal, the ray surface of the ordinary wave has the shape of a sphere, whereas that of the extraordinary wave is an ellipsoid of revolution about the optic axis $MN$ passing through point $O$. The ellipsoid and sphere contact each other at the points of intersection with optic axis $MN$. When $n_{e} \geqslant  n_{o}$, the ellipsoid is inscribed in the sphere (\figr{fig-35-0}~(a)), whereas if $n_{e} \leqslant  n_{o}$ the ellipsoid is circumscribed about the sphere (\figr{fig-35-0}~(b)). In the former case, the uniaxial crystal is said to be \redem{optically positive}, in the latter, \redem{optically negative}.

\begin{marginfigure}%[-3cm] 
\centering
\includegraphics[width=\textwidth]{figs/ch-35/fig-35-06.pdf}
\sidecaption{Analysing birefringence in a uniaxial crystal using the Huygens graphical method.\label{fig-35-06}}
\end{marginfigure}

\subsection{}\label{35.2.7} Birefringence in a uniaxial crystal can be explained and the directions of the ordinary and extraordinary rays can be found by using the Huygens graphical method. Assume that a plane unpolarized light wave is incident on the plane surface $ab$ of a uniaxial optically negative crystal (or a plate cut out of this crystal) at the angle $i$ (\figr{fig-35-06}). The optic axis $MN$ of the crystal, passing through point $A$ of surface $ab$, lies in
 the plane of the figure and makes the angle $\gamma$ with plane $ab$.

At the instant $t$ of time being considered, the front $AD$ of the incident wave has reached pointed on the surface of the crystal, and this point becomes the source of two linearly polarized elementary secondary waves, ordinary and extraordinary, in the crystal. At the instant of time $t + \Delta t$, where $\Delta t$ is the time required for the incident light to travel the distance $\seg{DK}$, the
disturbance, propagating from point $A$ in the form of the ordinary elementary wave, reaches the points of a sphere of radius $v_{0}\Delta t$ having its centre at point $A$. 

The disturbance, propagating from point $A$ in the form of the extraordinary elementary wave, reaches points at the same time on the surface of the ellipsoid of revolution that contacts the sphere of radius v0At at point $L$ of its intersection with optic axis $MN$. This ellipsoid is geometrically
similar to the ray surface of the extraordinary wave in
the crystal \ssect{35.2.5}. 

Planes $KC_{o}$ and $KC_{e}$, perpendicular to the plane of the figure and tangent, respectively, to the sphere and the ellipsoid, indicate, according to the Huygens principle \ssect{33.1.1}, the positions, at the instant of time $t + \Delta t$, of the fronts of the ordinary and extraordinary waves that actually propagate in a uniaxial crystal. Straight lines from point $A$ to the points of tangency $B$ and $F$ indicate the directions of the ordinary and extraordinary rays. Both rays lie in the plane of incidence, but the extraordinary ray is not orthogonal to the wave surface $KC_{e}$. 

The ordinary and extraordinary waves are linearly polarized in mutually perpendicular planes. The directions of the electric vectors $\vb{E}_{o}$ and $\vb{E}_{e}$ in the ordinary and extraordinary waves are shown in \figr{fig-35-06} by black dots and transverse dashes located on the corresponding rays.

\hlblue{Note.} If the optic axis $MN$ of the crystal does not lie in the plane of incidence of the light, then, in general, the extraordinary ray also does not lie in the plane of incidence. Consequently, the angle between the planes of polarization of the ordinary and extraordinary waves differs slightly from a right angle.


\begin{figure}%[-3cm] 
\centering
\includegraphics[width=\textwidth]{figs/ch-35/fig-35-07.pdf}
\sidecaption{The ordinary and extraordinary rays in the case of normal incidence of light on the surface of
an optically negative uniaxial crystal.\label{fig-35-07}}
\end{figure}

\subsection{}\label{35.2.8}. The construction for the ordinary and extraordinary rays in the case of normal incidence of light on the surface of an optically negative uniaxial crystal is shown in \figr{fig-35-07}. Here $ab$ is the position of the incident wavefront at the instant $t$ of time, and $C_{o}C_{o}'$ and $C_{e}C_{e}'$ are the positions of the fronts of the ordinary and extraordinary waves in the crystal at the instant of time $t + \Delta t$.
\begin{marginfigure}%[-3cm] 
\centering
\includegraphics[width=\textwidth]{figs/ch-35/fig-35-08.pdf}
\sidecaption{Analysing the case when light is normally
incident on the plane surface of an optically negative uniaxial crystal.\label{fig-35-08}}
\end{marginfigure}


It is assumed that the optic axis $M$N lies in the plane of incidence and makes the angle $\gamma$, differing from 0 and $\pi/2$, with the refracting surface $ab$. It is evident from \figr{fig-35-07} that the ordinary ray is an extension of the incident one and that the extraordinary ray is refracted
by the angle $r_{e} \neq 0$.



Considered in \figr{fig-35-08} is a case in which light is normally incident on the plane surface $ab$ of an optically negative uniaxial crystal, whose optic axis $MN$ is parallel to $ab$. Here, as is evident from the construction, the extraordinary ray is not refracted at the surface ab and
coincides in direction with the ordinary and incident
rays. But the velocities of the ordinary and extraordinary
rays in the crystal in this direction differ and are respectively equal to \ssect{35.2.5}: $v_{o} = c/n_{o}$ and $v_{e} = c/n_{e0}$. Therefore, the two rays  (waves) travel the same distance $d$ in the crystal with an optical-path difference \ssect{32.3.2}: 
\begin{equation*}%
\Delta s = d (n_{o} — n_{e0}).
\end{equation*}


\subsection{}\label{35.2.9} The path of the rays in a \redem{polarizing prism} are shown in \figr{fig-35-09}. This prism is cut out of a crystal of Iceland spar so that its sides $AB$ and $CD$ are parallel to its optic axis $MN$. Then the prism is cut apart along its diagonal plane $AC$ and
glued together along these diagonal surfaces by a thin layer of an optically isotropic transparent substance called Canada balsam.
\begin{marginfigure}%[-3cm] 
\centering
\includegraphics[width=\textwidth]{figs/ch-35/fig-35-08.pdf}
\sidecaption{Analysing the case when light is normally
incident on the plane surface of an optically negative uniaxial crystal.\label{fig-35-08}}
\end{marginfigure}

The crystal of Iceland spar is uniaxial and optically nega-tive; its refractive indices \ssect{35.2.5} are: $n_{o} = 1.658 $ and $n_{e0} = 1.486$. The refractive index of Canada balsam is $n_{Cb} = 1.550$, i.e. Canada balsam is a medium less optically dense than the material of the prism for the ordinary ray and more optically dense than this material for the extraordinary ray. Light falls on the prism normal to its side $AB$ (ray $S$ in \figr{fig-35-09}). 

The ordinary and extraordinary rays propagate in the prism without being refracted until they reach the layer $AC$ of Canada balsam. The dimensions of the prism are selected so that the angle of incidence $i$ of the ordinary ray on surface $AC$ is greater than the critical angle for total internal reflection \ssect{31.5.8}. Therefore, the ordinary wave is completely reflected by the layer of Canada balsam (ray $o$ in \figr{fig-35-09}). The extraordinary wave passes freely through the layer of Canada balsam and the second half of the polarizing prism. Thus, the polarizing prism can be used as a
polarizer \ssect{35.1.2}.

\subsection{}\label{35.2.10} All birefringent crystals absorb light to one or another degree. This absorption is anisotropic: the absorption factor \ssect{34.2.1} depends upon the orientation of the electric vector of the light wave and on the direction of light propagation in the crystal, as well as on the wavelength. This phenomenon is called \redem{dichroism}, or \redem{pleochroism}, because it is manifested in the different colouring of the crystals in different directions. An example of a strongly dichroic crystal is tourmaline, a uniaxial crystal in which the ordinary ray is absorbed much more intensely than the extraordinary ray. Even more pronounced dichroism is displayed by crystals of herapathite, which is used to make thin films, converting natural light into linearly polarized light and called \redem{polaraids}.

\section{Interference of Polarized Light}
\label{sec-35.3} 


\subsection{}\label{35.3.1} Trains of waves with all possible orientations with respect
to the ray of their planes of polarization, included in the
composition of natural light, are incoherent because they correspond
to the radiation of various independent atoms of the
light source. All these trains participate in the formation of the
ordinary and extraordinary waves that propagate in a uniaxial
crystal when natural light falls on it. But the contribution of
each separate train to these two waves is, in general, not the
same. It is greater to the wave whose plane of polarization makes the smaller angle a with the plane of polarization of the wave
train. In other words, the ordinary and extraordinary waves
are generated mainly by different wave trains included in the
composition of natural light. Consequently, the ordinary and
extraordinary waves, propagated in a uniaxial crystal on which
natural light falls, are not coherent.

\subsection{}\label{35.3.2} The ordinary and extraordinary waves, propagated in
a uniaxial crystal upon the incidence of linearly polarized light [obtained from natural light by means, for example, of a polarizing
prism \ssect{35.2.9} or some other polarizer] are coherent with
each other. This is due to the fact that the planes of polarization
of all the wave trains included in the composition of the incident
light have the same orientation.
Assume that a parallel beam of light, after passing through polarizer
P (Fig. 35.10), is normally incident on the plane surface ab
Fig. 35.10
of plane-parallel plate B, cut out of a uniaxial crystal parallel
to its optic axis MN (axis MN is parallel to plane ab). Shown
in Fig. 35.11 is vector At of the amplitude of the ith wave train.
This vector is oriented along line p~p, corresponding to the direction
of oscillation of the electric vector in the light emerging
from the polarizer. The contributions of the ith wave train to
the ordinary and extraordinary waves are characterized by the
amplitudes Aio = sin a and A  — Aj cos a, whose ratio
 AiolAif?)=tdJia is the same for all trains. In particular,
if a = jt/4, then Ai0 = Aie because pairwise coherent trains,
polarized in mutually perpendicular planes, have the same intensity.
\subsection{}\label{35.3.3} At the entrance to crystal plate B \ssect{35.3.2}, the electric
vectors E0 and Ee of the ordinary and extraordinary waves oscillate
in phase and their vector sum is equal to electric vector
of linearly polarized monochromatic incident light: Ep =
= E0 + Ee. In the plate the ordinary and extraordinary waves
propagate at different velocities \ssect{35.2.8}. Therefore, at the exit
from the plate (of thickness d), the mutually perpendicular electric
vectors E' and E' of the ordinary and extraordinary waves
oscillate with a phase difference of
A 2jxAs 2nd . .
A<p— « ---— * (n0 neo)i
A/0 /v0
where As is the optical-path difference of these waves (35.2.8),
and is the wavelength of light in vacuum. Consequently, as
a result of its passing through the plate, the light, in the general
case, becomes elliptically polarized \ssect{31.1.7}: the head of vector
E' = E' + Eg describes an ellipse lying in a plane perpendicular
to the beam. If a is the angle between the direction of oscillation
of vector Ep and the optic axis MN of the plate, then amplitudes
Ao and Ae of vectors E0 and Ee equal: A0 = Ap sin a
and Ae = Ap cos a, where Ap is the amplitude of vector Ep.
In the absence of light absorption in the plate, the amplitudes of
vectors E' and E'e are also equal to A0 and Ae.

\subsection{}\label{35.3.4} In accordance with the thickness d of the plate, several
special cases are possible.
(a) A quarter-wave plate is one whose thickness satisfies the relation:
d(n0 — rae0) = =b (m -f- 1/4) A,0, where m = 0, 1, 2, ...;
the plus sign corresponding to an optically negative crystal,
and the minus sign to an optically positive one \ssect{35.2.6}. At the
exit from such a plate the oscillations of vectors E' and E'e have
a phase difference of Jt/2. If, in addition, a = jt/4, the light
emerging from the plate is circularly polarized \ssect{31.1.7}.
(b) For a half-wave plate: d (n0 — ne0) = ± (m -|- 1/2) 7,0.
At the exit from such a plate, the oscillations of vectors E0
and E' have a phase difference of Jt. Light emerging from the
plate remains linearly polarized. But the directions of oscilla-lion of vectors Ep and E' of the incident and transmitted light
are symmetrical with respect to the principal plane of the plate
(Fig. 35.12).
(c) For a full-wave plate: d (n0 — ne0) = As a result of passing
through the plate, the light remains linearly polarized
in the same plane as the incident
light.

\subsection{}\label{35.3.5} Coherent waves emerging from
crystal plate B (Fig. 35.10) cannot
display interference because they are
polarized in mutually perpendicular
planes. Therefore, one more polarizing
prism, the analyzer A (Fig. 35.13)
is mounted beyond plate B. The analyzer
separates out of the incident
coherent waves, the components polarized
in one plane. It thus provides
the conditions necessary for obtaining
the interference of these waves.
The results of this interference depend upon the phase difference
Atp acquired by the ordinary and extraordinary waves in the
plate, on the ratio'of the amplitudes of these waves and on the
angle P between the principal planes of the analyzer and polarizer
\ssect{35.1.2}.
For example, if the angle between the principal plane of the
polarizer and optic axis MN of the plate a = jt/4, the amplitudes
and intensities of the ordinary and extraordinary waves
are the same. Assume in this case that monochromatic light of
the wavelength X0 in vacuum is incident on the plate. The two
following limiting cases may occur:
2jid . f +
A(p=~h~ ("°~"eo) =   ± (2m-I-1) n
(where m = 0, 1, 2, ...).
In the first case, corresponding to a full-wave plate, the light
falling on the analyzer is linearly polarized in the principal
plane of the polarizer. Therefore, at p = 0 (the analyzer is
mounted parallel to the polarizer), the intensity 7a of the light
passing through the analyzer is maximal, whereas at P = n!2
(tlie analyzer and polarizer are crossed), /a — 0, i.e. at P = 0
an interference maximum is observed, whereas at P = ji/2 a
minimum is observed.
In the second case, corresponding to a half-wave plate, the light
falling on the analyzer is linearly polarized in a plane making
the angle 2a = ji/2 with the principal plane of the polarizer.
Therefore, at P = 0 an interference minimum is observed,
whereas at P = jt/2 a maximum is observed.
If linearly polarized white light is incident on plate B (Fig. 35.13),
then in observing the light through the analyzer, the plate is
Fig. 35.13
seen to be coloured. Upon rotating the analyzer about the beam,
i.e. in varying angle P, the colouring changes. This is due to
the fact that the value of the phase difference Acp, determining
the result of interference, depends upon the wavelength of the
light. A plate, whose thickness d is not the same at various
places, is seen in white light to be oddly coloured, with each coloured
interference line (isochromate) passing through points of
equal thickness d. A similar pattern is observed in a plate, which
is of equal thickness at all points, but the value of the difference
(n0 — ne0) varies. In this case, each isochromatc passes through
points of the plate corresponding to the same value of
(n0 — ne0).

\section{ Artificial Optical Anisotropy}
\label{sec-35.4}

\subsection{}\label{35.4.1} An optically isotropic transparent body becomes anisotropic
if it is subjected to mechanical deformation. This
phenomenon is sometimes called photoelasticity. Upon linear
(uniaxial) tension or compression of an isotropic body alongthe OX axis, it acquires the optical properties of a uniaxial
crystal \ssect{35.2.2}, whose optic axis is parallel to OX. The difference
between the refractive indices for the ordinary (n0) and
extraordinary (ne0) rays, in the direction perpendicular to axis
0X, is proportional to the normal stress a \ssect{40.3.3}. Thus
n0 — ne0 = 7ca,
where k is the proportionality factor depending upon the properties
of the body.

\subsection{}\label{35.4.2} The electrooptical Kerr effect is the appearance of optical
anisotropy in a transparent isotropic solid, liquid or gaseous
dielectric upon being placed in an external electric field. By
the action of the uniform electric field, the dielectric is polarized
and acquires the optical properties of a uniaxial crystal
whose optic axis coincides- in direction with the field strength
vector E. The difference between the refractive indices of a
polarized dielectric for extraordinary and ordinary rays of monochromatic light, propagating perpendicular to the direction
of vector E, satisfies the Kerr law:
ne0 — n0 — BXqE2,
where A,0 is the wavelength of the light in vacuum, and B is
the Kerr constant. The value of B depends upon the nature of the
substance, wavelength X0 and the temperature, decreasing, as
a rule, as the temperature is raised. The sign of the difference
(neo — n0) is independent of the direction of the field. For the
majority of substances B > 0, so that they are similar with
respect to their optical properties in a uniform electric field
to optically positive uniaxial crystals \ssect{35.2.6}.

\subsection{}\label{35.4.3} The Cotton-Mouton effect is the appearance of optical
anisotropy in certain isotropic substances (liquids, glasses and
colloids) when they are placed in a strong external magnetic
field. In a uniform magnetic field, the substance acquires the
optical properties of a uniaxial crystal, whose optic axis coincides
in direction with the magnetic field strength vector H. The
difference between the refractive indices of the substance for
extraordinary and ordinary rays of monochromatic light, propagating
in the direction perpendicular to vector H, is proportional
to Id2. Thus
neo no = CX,II 510
where C is the Cotton-Mouton constant, and is the wavelength
of light in vacuum. The value of constant C depends upon the
nature of the substance, wavelength X0 and the temperature.

\section{Rotation of the Plane of Polarization}
\label{sec-35.5}

\subsection{}\label{35.5.1} As linearly polarized light passes through certain substances
that are said to be optically active, the plane of polarization
of the light \ssect{31.1.7} rotates about the direction’of the ray. Optical activity is displayed by certain crystals (quartz, cinnabar,
etc.), pure liquids and solutions (turpentine, a solution
of sugar in water, etc.). All substances that are optically active
in the liquid state, possess the same property in the crystalline
state as well. But some substances that are optically active in the crystalline state, lose this activity in the liquid state. Consequently,
optical activity can be due either to the structure of the
molecules of the substance themselves or to the arrangement
of the particles in the crystal lattice.

\subsection{}\label{35.5.2} In optically active crystals and pure liquids, the angle (p
of rotation of the plane of polarization of light is proportional
to the thickness Z of the layer of the substance through which
light passes. Thus cp = al. The proportionality factor a is
called the rotatory power, or specific rotation. The rotatory power
depends upon the nature of the substance, the temperature and
the wavelength h0 of the light in vacuum. The dependence of
cl on %Q is called the rotary, or rotatory, dispersion. Far from the
band of light absorption by the substance, rotary dispersion
obeys Biot's law: a CC

\subsection{}\label{35.5.3} The majority of optically active crystals exist in two
versions. As light passes through a crystal of one version, said
to be dextrorotatory or right-handed, the plane of polarization
is rotated to the right, i.e. clockwise (for an observer looking
toward the oncoming beam). In the passing of light through a
crystal of the other version, said to be levorotatory or left-handed,
the plane of polarization is rotated to the left (counterclockwise).
The values of the rotatory power for the two versions of the same
optically active crystal differ only in sign.

\subsection{}\label{35.5.4} The angle of rotation of the plane of polarization of light
in travelling a path of length Z in an optically active solution is
<p = [a] cl = [a] DKl.Here c is the volume-mass concentration of the optically active
substance in the solution (in kg/m3), D is the density of the
solution, and K — clD is the weight-part concentration, i.e.
the ratio of the mass of the optically active substance to the
mass of the whole solution. The proportionality factor [a] is
called the rotatory power, or specific rotation, of the solution.
The value of [a] depends on the nature of the optically active
substance and of the solvent, the wavelength of the light and
the temperatufe.

\subsection{}\label{35.5.5} When subjected to the action of an external magnetic
field, an optically inactive medium acquires the capacity to
rotate the plane of polarization of light propagating along the
direction of the field. This phenomenon is known as the Faraday
effect, or magnetic rotation of the plane of polarization of light. The angle (p of rotation of the plane of polarization is proportional
to the path length of the light in the substance and to the
strength II of the magnetic field. Thus cp = VIII. The proportionality
factor V is called the Verdet constant. It depends upon
the nature of the substance and the wavelength of the light.
The direction of magnetic rotation of the plane of polarization
(for an observer looking along the magnetic field) is the same
whether the light propagates along the direction of vector H
or in the opposite direction. In this feature the Faraday effect
differs from the rotation of the plane of polarization of light
in natural optically active media.