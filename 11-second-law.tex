% !TEX root = handbook-physics.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode

\chapter{Second Law Of Thermodynamics}
\label{ch-11}


\section{Cycles. The Carnot Cycle}
\label{sec-11.1}

\subsection{}\label{11.1.1}A \emph{cycle} is a succession of thermodynamic processes \ssect{8.3.7} as a result of which the system returns to its initial state. In $pV$, $pT$ and other thermodynamic diagrams, equilibrium cycles \ssect{8.3.7} are represented in the form of closed curves, because two identical states, the beginning and end of the cycle, correspond to the same point of the diagram. A thermodynamic system that accomplishes a cycle and exchanges energy with other bodies is called the \emph{working medium}, or \emph{substance}. This medium is usually a gas.

\subsection{}\label{11.1.2} The arbitrary equilibrium cycle $C_{1}aC_{2}bC_{1}$ \figr{fig-11-01}, using an ideal gas, can be divided into the process of expansion of the gas from state $C_{1}$, to state $C_{2}$ (curve $C_{1}aC_{2}$) and the process of compressing the gas from state $C_{2}$ to state $C_{1}$ (curve $C_{2}bC_{1}$). In expansion the gas does positive work $W_{1}$ measured by the area under the curve $C_{1}aC_{2}$ (the area of the figure $V_{1}C_{1}C_{2}V_{2}$ see Sect. \ssect{9.4.2}. The gas is compressed by external forces that do positive work $W_{2}' = -W_{2}$ measured by the area under the curve $C_{1}bC_{2}$ (the area of the figure $V_{1}C_{1}bC_{2}V_{2}$). Since $W_{1}> W_{2}'$ (\figr{fig-11-01}), the gas does the \emph{positive} work $W = W_{1} + W_{2} = W_{1} — W_{2}'$ in the cycle. This work is measured by the hatched area, bounded by the curve $C_{1}aC_{2}bC_{1}$ of the process.


\begin{figure}[!ht]
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-11/fig-11-01.pdf}
\caption{An arbitrary equilibrium of a cycle.}
\label{fig-11-01}
\end{figure}

\subsection{}\label{11.1.3} A \emph{direct cycle} is one in which the system does positive work $W =\oint p \,\dd V > 0$.In a $pV$ diagram a direct cycle is represented by a closed curve along which the working medium passes in the clockwise direction \figr{fig-11-01}. An example of a direct cycle is that accomplished by the working medium in a heat engine. In such an engine the working medium \ssect{11.1.1} receives energy in the form of heat \ssect{9.2.1} from external sources and gives a part of it up in the form of work \ssect{9.2.1} (see Sect. \ssect{11.1.5}).

\subsection{}\label{11.1.4} A \emph{reverse cycle} is one in which the system does negative work $W =\oint p \, \dd V < 0$. In a $pV$ diagram a reverse cycle is represented by a closed curve along which the working medium passes in the counterclockwise direction. An example of a re­ verse cycle is that accomplished by the working medium in a refrigerator. In such a device the working medium receives energy in the form of work and transmits energy in the form of heat from a cold body to a hotter one (see Sect. \ssect{11.1.5}). 

\subsection{}\label{11.1.5} Since the total change in the internal energy of a gas in a cycle is equal to zero \ssect{9.1.3}, the first law of thermo­ dynamics \ssect{9.3.1} for such a process is of the form
\begin{equation*}%
Q = \Delta U + W = W
\end{equation*}
where $Q$ is the total quantity of heat delivered to the gas in the cycle, and $W$ is the work done by the gas in this cycle.

In a direct cycle \ssect{11.1.3} $Q > 0$ and $A > 0$,and the gas does work owing to the heat delivered to it. In a reverse cycle \ssect{11.1.4} the work done \emph{on the gas} is $W' = -W$ (where $W > 0$) and a quantity of heat equivalent to this work is removed from the gas. 


\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-11/fig-11-02.pdf}
\caption{The Carnot cycle.}
\label{fig-11-02}
\end{figure}


\subsection{}\label{11.1.6} The \emph{Carnot cycle} is the one shown in \figr{fig-11-02}. The \emph{direct Carnot cycle} consists of four consecutive processes: isother­mal expansion $1-1'$ at the temperature $T_{1}$ (where $T_{1}' = T_{2}$) adiabatic expansion $1'-2$, isothermal compression $2-2'$ at the temperature $T_{2}$ (where $T_{2}' = T_{2}$), and adiabatic compres­sion $2'-1$.

\subsection{}\label{11.1.7} In practice a direct Carnot cycle could be carried out by means of a gas enclosed in a vessel with a movable piston. In the process $1-1'$ the gas is in thermal contact with a \emph{heat source (heater)} at the constant temperature $T_{1}$\footnote{For instance, a large tank filled with water.}. From the heater the gas receives a certain quantity of heat $Q_{1}$ (where $Q_{1} > 0$). It is assumed that this does not change the temperature of the heater. This is possible, strictly speaking, only if the heater has an infinite heat capacity. In the process $1'-2$ the gas is thermally isolated and its expansion is adiabatic\footnote{ For instance, the cylinder with the gas is covered with a thick layer of felt.} \ssect{9.5.10}. In the process $2-2'$ the gas is brought into thermal contact with a \emph{cooler (heat sink)} having the constant temperature $T_{2}$ (where $T_{2} < T_{1}$). Here the gas is isothermically compressed and gives off the quantity of heat $—Q_{2}$ to the cooler (assuming that $Q$ is the quantity of heat received by the gas from the cooler). The heat capacity of the cooler is assumed to be infinitely high. In the state $2'$ the gas is again thermally insulated and contracts adiabatically to its initial state $1$.

\subsection{}\label{11.1.8} The work done by the gas in an equilibrium (8.3.7) direct Carnot cycle is
\begin{equation*}%
W = Q = Q_{1} + Q_{2} = Q_{1} - \abs{Q_{2}}
\end{equation*}
It is evident from the equation that $W< Q_{1}$ i, i.e. the work done by the working medium \ssect{11.1.1} in the Carnot cycle is less than the energy received from the heater by the amount of energy given up to the cooler in the form of heat. This is valid for an arbitrary process: the work $W$ done in the cycle is always less than the sum $Q_{\textrm{del}}$ of all the quantities of heat delivered to the working medium from heaters. 

The operation of heat engines is based on the direct Carnot cycle.

\subsection{}\label{11.1.9} The \emph{thermal efficiency} $\eta$ is the ratio of the work $W$ done by the working medium in a direct cycle to the sum $Q_{\textrm{del}}$ of all quantities of heat delivered in this cycle to the working medium by heaters:
\begin{equation*}%
\eta = \frac{W}{Q_{\textrm{del}}}
\end{equation*}
The quantity $\eta$ characterizes the economy of operation of a heat engine.

\subsection{}\label{11.1.10} The thermal efficiency of a direct equilibrium Carnot cycle carried out with an ideal gas is
\begin{equation*}%
\eta_{c} = \frac{Q_{1}+Q_{2}}{Q_{1}} = \frac{T_{1}-T_{2}}{T_{1}} = 1 - \frac{T_{2}}{T_{1}}
\end{equation*}
The value of $\eta_{c}$ depends only on the ratio of the temperatures of the cooler $(T_{2})$ and the heater $(T_{1})$.

\begin{figure}[!ht]
%\begin{wrapfigure}{r}{0.5\linewidth}
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-11/fig-11-03.pdf}
%\input{figs/ch-03/fig-03-01}
%  \vspace{-13pt}
\caption{The reverse Carnot cycle.}
\label{fig-11-03}
\end{figure}


\subsection{}\label{11.1.11} The \emph{reverse Carnot cycle} is shown in \figr{fig-11-03}. In isothermal compression, carried out in the process $1'-1$, the quantity of heat $Q_{1}$ is removed from the gas at the temperature $T_{1}$, which remains constant. In the process of isothermal expansion $2'-2$ at the temperature $T_{2} < T_{1}$,the quantity of heat $Q_{2}$ is delivered to the gas. In the reverse Carnot cycle $Q_{1} < 0$, $Q_{2} > 0$ and the work $W$ done by the gas in one cycle is negative: $W= (Q_{1} + Q_{2}) < 0$. This conclusion is valid for any reverse cycle. If the working medium carries out a reverse cycle, energy can be transferred in the form of heat \ssect{9.2.1} from a cold body to a hotter one at the expense of the corresponding work done by external forces. This principle is used in all refrigerating devices. The less the work $W'= —W$ done by external forces to extract the quantity of heat $Q_{2}$ from a cold body, the higher the economical efficiency (coefficient of performance) of a refrigerator:
\begin{equation*}%
Q_{2} = W_{1} - Q_{1} = W - \frac{W}{\eta} = - \frac{1 - \eta}{\eta} W
\end{equation*}
where $\eta$ is the thermal efficiency of the direct cycle between the same temperatures $T_{1}$ and $T_{2}$ \ssect{11.1.3} and \ssect{11.1.4}.


\section{Reversible and Irreversible Processes}
\label{sec-11.2}

\subsection{}\label{11.2.1} A thermodynamic process is said to be reversible (\emph{rever­sible process}) if the thermodynamic system \ssect{8.3.1}, in carrying out the process first in the direct and then in the exactly re­ versed order, returns, together with all the external bodies with which it interacts, to its initial state. In other words, in a rever­sible process the thermodynamic system returns to its initial state without leaving any changes in the surrounding medium. A necessary and sufficient condition for the reversibility of a thermodynamic process is the equilibrium (8.3.3) of all the consecutive states in the process. Here all corresponding states of the thermodynamic system in the direct and reverse pro­cesses are absolutely identical.

\textbf{Example 1.} The mechanical motion of a body in a vacuum in the complete absence of friction is a reversible process. Assume, for instance, that a body under these conditions is thrown with a certain initial velocity into a gravitational field \ssect{6.2.1} at a definite angle to the horizon. After describing a parabolic path the body drops to the earth at a certain place. Now, if the body is thrown from this place at the same angle and the same initial velocity, but in the opposite direction, it describes the same path in the opposite direction and drops to the earth at the initial point. Any intermediate states of the moving body in the forward and reverse motions are entirely identical. The \emph{reversibility of mechanical motions} indicates their symmetry with respect to the interchanging of the future by the past, i.e. with respect to the reversal of the sign of the time. The re­versibility of mechanical motions follows from the differential equations of motion \ssect{2.4.4}. When the sign of the time is changed, this changes the sign of the velocity as well, but the acceleration, in the motion equations, retains its sign. 

\textbf{Example 2.} Another reversible process is the undamped vibrations of a body suspended in a vacuum on a perfectly elastic spring \ssect{28.2.3}. The body-spring system is a conservative one \ssect{3.1.7}. Its mechanical vibrations do not change the energy of thermal random motion of its particles. Only changes in the configuration and velocity of the system lead to a change of its state. But these changes are completely repeated after each period $T$ \ssect{28.1.2} of vibration. Hence, the conditions for the
reversibility \ssect{11.2.1} of the process are complied with.

\subsection{}\label{11.2.2} Any process that does not comply with the conditions for reversibility is said to be irreversible (\emph{irreversible process}). 

\textbf{Example~1.} The direct process of braking a moving body by means of friction forces is an irreversible process. If these forces are the only ones acting on the body, its velocity is reduced and it finally stops. The energy of mechanical motion of the body as a whole is reduced and is expended in increasing the energy of random motion of particles of the body and the surrounding medium. The internal energy \ssect{9.1.2} of the body and the medium is increased; they are heated by the action of the friction forces. The direct process dealt with here proceeds spontaneously; it is carried out without any processes taking place in surrounding bodies. For the reverse process to occur, return­ ing the system to its initial state, it would be necessary for the braked and stopped body to start moving again by cooling it and the surrounding medium. As is evident from experiments, the thermal random motion of particles of a body cannot spon­taneously lead to the initiation of ordered motion of all the particles of the body as a whole. To accomplish such motion it is necessary to provide for an \emph{additional compensating process} in which the body and surrounding medium are cooled to the initial temperature. In this process, the quantity of heat $Q$ is given up to the cooler and the work $A' = Q$ is performed on the body.

Thus, the consecutive performance of such direct and reverse processes returns the body-medium system to its initial state, but the states of external bodies are changed. Hence, all pro­cesses accompanied by friction are irreversible ones.

\textbf{Example~2.} The direct process of heat exchange \ssect{9.2.4} between two bodies having different temperatures is a spontaneous one. The reverse process, heating one body by cooling another that initially has the same temperature as the first body, cannot proceed spontaneously. To accomplish this process, a refrigerat­ing device \ssect{11.1.11} is required. The process of heat exchange at a finite temperature difference is an irreversible one.


\section{Second Law of Thermodynamics}
\label{sec-11.3}

\subsection{}\label{11.3.1} The reference in the preceding section to experiments that prove the irreversibility of the processes of heat exchange and motion with friction \ssect{11.2.2} was no chance one. The first law of thermodynamics \ssect{9.3.1} cannot describe thermodynamic processes comprehensively. An essential limitation of the first law is that it cannot be used to predict in which direction a thermodynamic process will proceed. Any process that does not violate the law of conservation of energy is feasible from the point of view of the first law of thermodynamics. Among others, (he spontaneous transfer of energy in the form of heat from a cooler body to a hotter body is also possible. Also feasible is a process whose only result would be the obtaining of a certain quantity of heat from a body and the conversion of this heat into an equivalent amount of work. A periodic-action device, based on the first law of thermodynamics, which performs work by cooling a single source of heat (for instance, the internal energy of large bodies of water), is called a \emph{perpetual motion machine of the second kind}.

\subsection{}\label{11.3.2} The \emph{second law of thermodynamics} is the experimentally established statement of the impossibility of building a perpe­tual motion machine of the second kind \ssect{11.3.1}. The two most common statements of the second law, equivalent to each other, are:
\begin{enumerate}[label=(\alph*),noitemsep,nolistsep]
\item no process is possible whose sole result is the conversion of all the heat received from a certain body into an equivalent amount of work;
\item no process is possible whose sole result is the transfer of energy in the form of heat from a cold body to a hotter one. 
\end{enumerate}

\subsection{}\label{11.3.3} It follows from the second law of thermodynamics that work and heat are not equivalent forms of transferring energy. The conversion of orderly motion of a body as a whole into the random motion of its particles is an irreversible process, pro­ceeding without compensating processes \ssect{11.2.2}. The conver­sion of random motion of the particles of a body into ordered motion of the body as a whole requires the provision of some simultaneous compensating process.\footnote{Such a process is sometimes, not quite correctly, called the ``conversion of heat into work''.}

\textbf{Example~1.} In the isothermal expansion of an ideal gas work is done that is fully equivalent to the quantity of heat delivered (o the gas \ssect{9.5.9}. The heat received by the gas is completely converted into an equivalent amount of work. But the gas does not return to its initial state. It expands and its specific volume is increased. The conversion of heat into work$^{\textrm{\textcolor{IndianRed}{3}}}$ is not the sole result of the isothermal expansion of an ideal gas.


\textbf{Example~2.} In a heat engine based on the direct Carnot cycle \ssect{11.1.8}, work is done owing to the heat delivered from the heater. But a part of the quantity of heat obtained is given up to the cooler \ssect{11.1.7}. Hence, the work done in a cycle is not equi­valent to the whole quantity of heat delivered to the working medium.
Example 3. In refrigerating devices operating on the reverse Carnot cycle \ssect{11.1.11}, a certain quantity of heat is transferred from a cold body to a hotter one. But external forces do work to accomplish this and, consequently, a compensating process is being carried out.
\subsection{}\label{11.3.4} The \emph{Carnot theorem} states that the thermal efficiency \ssect{11.1.9} of the reversible Carnot cycle does not depend upon the composition of the working medium and is always ex­ pressed by the formula \ssect{11.1.10}:
\begin{equation*}%
\eta_{C} = \frac{Q_{1}+Q_{2}}{Q_{1}} = \frac{T_{1} - T_{2}}{T_{1}}
\end{equation*}
The thermal efficiency $\eta_{C\,\textrm{ir}}$ of the irreversible Carnot cycle is always less than the thermal efficiency $\eta_{C\,\textrm{rev}}$ of the reversible Carnot cycle, carried out at the same temperatures $T_{1}$ and $T_{2}$: $\eta_{C\,\textrm{ir}} < \eta_{C\,\textrm{rev}}$.

The thermal efficiency of any reversible cycle cannot be higher than that $\eta_{C}$, of a reversible Carnot cycle being accomplished by means of a heater and cooler having the temperatures $T_{1}$ and $T_{2}$:
\begin{equation*}%
\eta_{\textrm{rev}} \leq \eta_{C} = \frac{T_{1} - T_{2}}{T_{1}}
\end{equation*}

\subsection{}\label{11.3.5} The Carnot theorem \ssect{11.3.4} is used to establish the \emph{thermodynamic temperature scale}. It follows from the first equation in the preceding subsection that $T_{2}/T_{1} = -Q_{2}/Q_{1}$ or, since $Q_{2} < 0$,
\begin{equation*}%
\frac{T_{2}}{T_{1}} = \frac{\abs{Q_{2}}}{Q_{1}}
\end{equation*}
In order to compare the temperatures $T_{1}$ and $T_{2}$ of two bodies it is necessary to carry out a reversible Carnot cycle in which these bodies are the heater and cooler. Then, according to the ratio of the numerical values of the quantities of heat delivered to and received from the bodies, the ratio of their temperatures is determined. The result of such a comparison of temperatures does not depend upon the chemical composition of the working medium \ssect{11.3.4}. Therefore, the thermodynamic temperature scale is independent of the properties of the temperature-indicating substance \ssect{8.3.4} and, in this respect, has great generali­ty. However, since all real thermodynamic processes are irre­versible, the comparison of temperatures by the above-men­tioned procedure is unfeasible; it has only a theoretical signi­ficance.

\section{Entropy and Free Energy}
\label{sec-11.4}

\subsection{}\label{11.4.1} The ratio of the quantity of heat $Q$, received by a body in an isothermal process, to the temperature $T$ of the body giving up the heat is called the \emph{reduced quantity of heat} $Q*$, i.e. $Q* = Q/T$. In heating a body $(Q > 0)$, $Q*$ is positive; in cooling $(Q < 0)$, $Q*$ is negative.

The reduced quantity of heat delivered to a body in an infinitely small portion of an arbitrary process, is equal to $\delta Q/T$ where $T$ is the temperature of the corresponding heater.\footnote{In the case of a reversible process, $T$ coincides with the tem­perature of the body that is carrying out the process.} The reduced quantity of heat ($Q*_{1-2}$for the arbitrary portion $1 \to 2$ of the process $C_{1}C_{2}$ is
\begin{equation*}%
Q*_{1-2} = \int_{C_{1}}^{C_{2}} \frac{\delta Q}{T} 
\end{equation*}
\subsection{}\label{11.4.2} The reduced quantity of heat $Q*_{\textrm{rev}}$ delivered to a body in any reversible cycle equals zero:
\begin{equation*}%
Q*_{\textrm{rev}} = \oint_{\textrm{rev}} \frac{\delta Q}{T} = 0
\end{equation*}
Here $T$ is the temperature at which an element of the quantity of heat $\delta Q$ is delivered to the body. It follows from the equation in the preceding subsection that the function $\delta Q/T$ is a complete differential of a certain function $S$ [in contrast to $\delta Q$, which is not a complete differential \ssect{9.4.3}]:
\begin{equation*}%
\dd S = \left( \frac{\delta Q}{T} \right)_{\textrm{rev}}
\end{equation*}
The single-valued state function \ssect{9.1.3} $S$, whose complete differential is determined by the preceding equation, is called the \emph{entropy} of a body. It is evident from this equation that $\dd S$ and 
$\delta Q$ have the same sign. Consequently, from the way in which the entropy varies it is possible to determine in which direction heat exchange \ssect{9.2.4} takes place. In heating a body $(\delta Q > 0)$ its entropy increases $(\dd S >0)$. If a body is cooled $(\delta Q < 0)$, its entropy decreases $(\dd S < 0)$.

\textbf{Example.} The complete differential of the entropy of an ideal gas is expressed by the equation 
\begin{equation*}%
\dd S = \left( \frac{\delta Q}{T} \right)_{\textrm{rev}}= \frac{M}{\mu} C_{V \mu} \frac{\dd T}{T} + \frac{M}{\mu} R \frac{\dd V}{V}
\end{equation*}
where $M$ is the mass of the gas, $\mu$, is its molar mass \ssect{8.4.3}, $C_{V \mu}$ is its molar heat capacity at constant volume \ssect{9.5.4}, $R$ is the universal gas constant \ssect{8.4.4}, $T$ is its temperature, and $V$ is its volume. This result is obtained on the basis of the first law of thermodynamics \ssect{9.3.1} for $\delta Q$, with the Mendeleev-Clapeyron equation \ssect{8.4.4} taken into account.

The change $\Delta S_{1 \to 2}$ in the entropy of an ideal gas going over from state 1 to state 2 does not depend upon the kind of pro­ cess $1 \to 2$ used in this transition:
\begin{equation*}%
\Delta S_{1 \to 2} = S_{2} - S_{1} = \frac{M}{\mu}  \left( C_{V \mu} \ln \frac{T_{2}}{T_{1}} +  R \ln \frac{V_{2}}{V_{1}} \right)
\end{equation*}
\subsection{}\label{11.4.3} Certain of the most important properties of the entropy of closed systems \ssect{2.2.4} are:
\begin{enumerate}[label=(\alph*),noitemsep,nolistsep]
\item The entropy of a closed system in a reversible Carnot cycle \ssect{11.1.6} does not change:
\begin{equation*}%
\Delta S_{\textrm{rev}} = 0 \qand S = \textrm{const.}
\end{equation*}
\item The entropy of a closed system increases in an irreversible Carnot cycle:
\begin{equation*}%
\Delta S_{\textrm{ir}} > 0
\end{equation*}
\item In any processes that occur in a closed system its entropy never decreases:
\begin{equation*}%
\Delta S_{\textrm{rev}} \geq 0 
\end{equation*}
\end{enumerate}
In an element of change in the state of a closed system, the entropy does not decrease: $\Delta S_{\textrm{rev}} \geq 0$.

The equality sign pertains to reversible processes and the greater-than sign to irreversible ones. Item (c) is one of the statements of the second law of thermodynamics.

\subsection{}\label{11.4.4} The following relation is valid for an arbitrary process occurring in a thermodynamic system \ssect{8.3.1}:
\begin{equation*}%
\delta Q \leq  T \dd S
\end{equation*}
where $T$ is the temperature of the body that transfers the energy $\delta Q$ to the thermodynamic system in the process of an infinitely small change in the state of the system. Making use of an equa­tion of the first law of thermodynamics \ssect{9.3.1} for $\delta Q$, the preceding inequality can be rewritten in the form
\begin{equation*}%
T \dd \, S \geq \dd U + \delta W
\end{equation*}
combining the first and second laws.

\subsection{}\label{11.4.5} For a reversible process 
\begin{equation*}%
\delta W= -(\dd U - T \dd  \, S)
\end{equation*}
or
\begin{equation*}%
\delta W= -\dd (U - T S) - S  \, \dd T = - \dd F - S  \, \dd T
\end{equation*}
where $F = U - TS$
is called the \emph{free energy}. The free, or available, energy is the difference of two state functions \ssect{8.3.8} and is therefore also a state function of the thermodynamic system.

If the system carries out a reversible isothermal process, $\dd T = 0$ and $\delta W_{\textrm{isot}} = - \dd F$. When the system passes from state 1 to state 2 in a reversible isothermal process,
\begin{equation*}%
W_{\textrm{isot}}  = F_{1} - F_{2}
\end{equation*}
The decrease in free energy is the measure of the work done by a system (or body) in a reversible isothermal process.


\subsection{}\label{11.4.6} It follows from the equation $U = F + TS \, $ that the internal energy of a body (or system) is equal to the sum of the free energy $F$ and the \emph{bound energy} $TS$. The bound, or unavailable, energy is the part of the internal energy of a body (or system) that cannot be transferred in the form of work in an isothermal process. In this sense, this part of the internal energy is ``depreciated''. The more the entropy of the body (or system), the more the bound energy. Therefore, the entropy of a body (or system) serves as a measure of the ``depreciation'' of its energy.


\section{Statistical Interpretation of the Second Law of Thermodynamics}
\label{sec-11.5}


\subsection{}\label{11.5.1} The statement of the second law of thermodynamics on the impossibility of a decrease in the entropy in an isolated system \ssect{11.4.3} can be interpreted statistically, on the basis of the molecular-kinetic theory of the structure of matter, by means of Boltzmann’s relation:
\begin{equation*}%
S = k \ln P + \textrm{const.}
\end{equation*}
where $S$ is the entropy of the system, $k$ is Boltzmann’s constant \ssect{8.4.5}, and $P$ is the thermodynamic probability of the state. 

\subsection{}\label{11.5.2} The \emph{thermodynamic probability} $P$ of a state of a body (or system) is equal to the number of all possible kinds of coordinate and velocity distributions of its particles corresponding to the given thermodynamic state \ssect{8.3.3}. By definition, $P$ is a whole number not less than unity $(P \geq 1)$.The Boltzmann relation \ssect{11.5.1} leads to the following statistical interpretation of the second law of thermodynamics: the thermodynamic prob­ ability of a state of a closed system cannot decrease, regardless of the processes that occur in it. In any process that proceeds in a closed system and brings it from state 1 to state 2, the change $\Delta P$ in the thermodynamic probability is either posi­tive or zero: $\Delta P = P_{2} - P_{1} \geq 0$.

In a reversible process  $\Delta P = 0$, i.e. the thermodynamic probability $P$ is constant. If the process is irreversible,  $\Delta P > 0$ and $P$ increases. This means that an irreversible process transfers the system from a less probable state to a more probable one and, in the limit, to an equilibrium state \ssect{8.3.3}.

\subsection{}\label{11.5.3} Being a statistical law, the second law of thermodynamics describes the laws of random motion of a great number of particles making up a closed system. In systems that consist of a relatively small number of particles, fluctuations \ssect{11.6.1} are observed. They are deviations from the second law of thermodynamics.

\subsection{}\label{11.5.4} The second law of thermodynamics, established for closed systems on the earth, cannot be applied to the whole infinite universe. Such an application leads to the conclusion that the temperatures of all bodies in the universe equalize. This is incorrect from both the philosophical and physical points of view. According to this erroneous conclusion, all forms of motion, except random thermal motion, will cease and the universe will then reach a state of so-called ``heat death''. Actually, owing to the infinity of the universe, fluctuations \ssect{11.6.1} are inevitable in certain of its parts. These violate thermal equilibrium. The duration and magnitude of the fluctuations can be extremely great. It has been proven that there can be no equilibrium state, corresponding to ``heat death'', for the infinite universe.

\section{Fluctuations}
\label{sec-11.6}

\subsection{}\label{11.6.1} Substantial deviations of certain physical quantities, characterizing a system, from their average values may occur in systems consisting of a comparatively small number of particles. Such deviations are called \emph{fluctuations} of physical quantities. For instance, in highly rarified gases the density at var­ious places in the volume of the gas may differ from the aver­ age density corresponding to the equilibrium state for definite $p$ and $T$ values. Chance deviations of the temperature $T$, pressure $p$ and other physical quantities may occur in exactly the same way.

\subsection{}\label{11.6.2} If $M$ is the true value of a physical quantity, and $\expval{\,M\,}$ is its average value, then the quantity $\Delta M$, where $\Delta M = M - \expval{\,M\,}$, and its average value $\expval{\Delta M} = \expval{\,M - \expval{\,M\,}}$ cannot be measures of the fluctuation of the quantity $M$. The quantity $\Delta M$ is not constant with time, and the quantity
\begin{equation*}%
\expval{\Delta M\,} = \expval{\,M\,} — \expval{\,M\,} = 0\footnote{Used here is the statement that the mean value $\expval{\,M\,}$ of a constant quantity coincides with the quantity.}
\end{equation*}

This last equation follows from the fact that the deviation of the quantity $M$ from $\expval{\,M\,}$ occurs in both directions, towards values larger than the average and towards values smaller than the average, with equal frequency.

\subsection{}\label{11.6.3} A measure of the fluctuations of the physical quantity $M$ is the mean square of the difference $\Delta M$. It is called the \emph{mean square fluctuation}:
\begin{equation*}%
\expval{\,\Delta (M^{2})\,} = \expval{\,(M - \Delta M)^{2}\,} = \expval{\,M^{2}\,} - (\expval{\,M\,})^{2}\footnote{This last equation, following from the rules of algebraic oper­ations with average values, underlines the fact that the average value $\expval{\,M^{2}\,}$ of the square of a quantity should not be confused with the square of the average value $(\expval{\,M\,})^{2}$.}
\end{equation*}
The mean square fluctuation is essentially positive or zero: $(\expval{\,(\Delta M)^{2}\,} \geq 0$. 

The \emph{absolute fluctuation} is the quantity $\sqrt{\expval{\,(\Delta M)^{2}\,}}$, which also characterizes the deviation of $M$ from $\expval{\,M\,}$. A small absolute fluctuation signifies that large deviations of $M$ from $\expval{\,M\,}$ occur very rarely.

The \emph{relative fluctuation} $\delta_{M}$ is the ratio of the absolute fluctuation to the average value $\expval{\,M \,}$ of a physical quantity:
\begin{equation*}%
\delta_{M} = \frac{\sqrt{\expval{\,(\Delta M)^{2}\,}}}{\expval{\,M \,}}
\end{equation*}
The relative fluctuations in the concentration of particles (or density) of a gas, and in its pressure and temperature, decrease with an increase in the number N of gas molecules contained in the vessel:
\begin{align*}%
\delta_{\rho} & = \frac{\sqrt{\expval{\,(\Delta \rho)^{2}\,}}}{\expval{\,\rho \,}} \propto \frac{1}{\sqrt{N}} \\
\delta_{p} & = \frac{\sqrt{\expval{\,(\Delta p)^{2}\,}}}{\expval{\,p \,}} \propto \frac{1}{\sqrt{N}} \\
\delta_{T} & = \frac{\sqrt{\expval{\,(\Delta T)^{2}\,}}}{\expval{\,T \,}} \propto \frac{1}{\sqrt{N}} 
\end{align*}
When $N = N_{A}$ (Avogadro’s number, see Appendix~II), $\delta_{\rho}$, $\delta_{p}$ and $\delta_{T}$ have values of the order of \num{d-14}.

If a system consists of $N$ independent particles, the relative fluctuation of an additive state function \ssect{9.1.3} of the system is inversely proportional to the square root of $N$:
\begin{equation*}%
\delta_{M} \propto \frac{1}{\sqrt{N}}
\end{equation*}

\subsection{}\label{11.6.4} The following are examples of fluctuations of physical quantities.

\textbf{Example~1.} In measuring the temperature by means of a gas thermometer filled with an ideal gas \ssect{8.4.1}, the thermometer readings are not constant owing to fluctuations of temperature. The variations in temperature measured by the thermometer cannot be less than the absolute fluctuation of the thermometer indication, equal to $\sqrt{\expval{\,(\Delta T)^{2}\,}}$, i.e. $\Delta t \geq \sqrt{\expval{\,(\Delta T)^{2}\,}}$.
The absolute fluctuation can be determined by the formula in \ssect{11.6.3}:
\begin{equation*}%
\sqrt{\expval{\,(\Delta T)^{2}\,}} \propto \frac{\expval{\,T \,}}{\sqrt{N}}
\end{equation*}
Thus
\begin{equation*}%
\Delta t \geq \sqrt{\expval{\,(\Delta T)^{2}\,}} \propto \frac{\expval{\,T \,}}{\sqrt{N}}
\end{equation*}
If the gas thermometer contains \num{d-8} mole of gas, i.e. $N = \num{6.02d15}$, then the minimum changes in temperature $\Delta t$ that can he detected by the instrument, are of the order of magnitude $\Delta t \approx \num{d-10}\expval{\,T \,}$. This value indicates the limit of sensitivity of the gas thermometer.

Real variations in temperature, commonly encountered in experiments, are incommensurably larger than $\Delta t$.

\textbf{Example~2.} Electrical fluctuations in circuits restrict the lim­its of sensitivity of radio receiving equipment. Among others, fluctuations in the number of electrons emitted by the incandescent cathode lead to fluctuations in the current supplied to the vacuum tube. This is called \emph{shot}, or \emph{Schottky, noise}. A measure of the shot noise is the mean square fluctuation of the current:
\begin{equation*}%
\expval{\,(\Delta I)^{2}\,} \approx \frac{e I_{0}}{t}
\end{equation*}
where $e$ is the charge of the electron, and $I_{0}$ is the average current over the time $t$ during which it is measured. Here $t \gg \tau$, where $\tau$ is the time of flight of the electron in the tube.

\section{Brownian Movement}
\label{sec-11.7}

\subsection{}\label{11.7.1} The \emph{Brownian movement} is the continuous haphazard zig­zag motion of small particles suspended in a gas or liquid and can be observed in a microscope. The Brownian movement is caused by fluctuations \ssect{11.6.1} in the pressure exerted by the molecules of gas or liquid on the surfaces of the minute suspend­ ed particles. As a result of pressure fluctuations the Brownian particles are subject on all sides to the action of unbalanced forces which cause their complex motions that are observed. 

\subsection{}\label{11.7.2} Under constant external conditions, no changes are observed in the motions of the Brownian particles and they continue indefinitely. This is testimony to the continuous na­ture of random thermal motion of the molecules to which the displacements of the Brownian particles are due. The velocity $v$ and energy $E$ of motion of these particles depend upon their size, but not on their chemical composition. The values of $v$ and $E$ increase with the temperature and with a reduction in the viscosity of the gas or liquid.

\subsection{}\label{11.7.3} It has been experimentally established that Brownian particles can move upwards, rising in the fluid. This occurs when the molecules of the gas (or liquid) transmit an uncompensated momentum directed upwards to the particle. This increases the potential energy of the particle at the expense of the kinetic energy of the molecules surrounding it, thereby leading to localized cooling of the gas or liquid. This means that the mechanical energy of the Brownian particle has increased by cooling a single source of heat, the liquid or gas, thereby contradicting the second law of thermodynamics \ssect{11.3.2}. In this manner, Brownian movement is proof of the limited appli­cation of the second law of thermodynamics and of its statisti­cal nature \ssect{11.5.1}.

\subsection{}\label{11.7.4} The motions of Brownian particles are completely chao­tic. Therefore, the average displacement $\expval{\, x \,}$ of a particle along an arbitrary direction equals zero. Then mean square displacement $\expval{\, x^{2} \,}$ is proportional to the time $t$ of observation of the particle and is expressed by \emph{Einstein's equation}:
\begin{equation*}
\expval{\, x^{2} \,} = 2 \,D t
\end{equation*}
where $D$ is the diffusion coefficient of the Brownian particles. For spherical particles of radius $r$
\begin{equation*}
D = \frac{RT}{6 \pi \eta r N_{A}}
\end{equation*}
Here $T$ is the absolute temperature, $R$ is the universal gas con­stant \ssect{8.4.4}, $\eta$ is the coefficient of viscosity of the liquid or gas \ssect{10.8.4}, and $N_{A}$ is Avogadro’s number (Appendix~II).

\section{Third Law of Thermodynamics}
\label{sec-11.8}
 
\subsection{}\label{11.8.1} As a single-valued state function of a system entropy is introduced by means of a differential relation \ssect{11.4.2}. Hence, the entropy can be determined up to an arbitrary constant, which cannot be found from the first and second laws of thermo­ dynamics. It is consequently impossible to determine the absolute value of the entropy.

\subsection{}\label{11.8.2} An experimental investigation of the properties of substances at ultra-low temperatures led to the establishment of the \emph{third law of thermodynamics}. This law, also called the \emph{Nernst heat theorem}, states that the change in entropy equals zero in any isothermal process conducted at absolute zero:
\begin{equation*}%
\Delta S_{T \to 0} = 0 \qand S = S_{0} = \textrm{const}
\end{equation*}
regardless of the changes in any other state variables (for example, volume, pressure, and intensity of the external force field). The third law does not enable the absolute value of the entropy to be determined. But the fact that the entropy is constant as $T \to 0$ permits this constant value to be employed as the reference point for entropy values. This enables the change in en­tropy to be determined in processes being investigated.

\subsection{}\label{11.8.3} Planck extended Nernst’s heat theorem by stating that the entropy of a system equals zero at absolute zero tempera­ture. If $E_{0}$, $E_{1}, \ldots , E_{n}$ is a sequence of energy levels \ssect{38.2.5} of a system, then at absolute zero temperature the system in equilibrium is in a quite definite lowest state with energy exactly equal to $E_{0}$. Here the thermodynamic probability $P$ of the state \ssect{11.5.2} is equal to unity $(P = 1)$ and, according to Boltzmann’s relation \ssect{11.5.1},
\begin{equation*}%
S_{0}= k \ln P = 0
\end{equation*}

\subsection{}\label{11.8.4} The heat capacity $C_{V \mu}$ at constant volume \ssect{9.5.4} for all bodies vanishes at $T = \SI{0}{\kelvin}$. As a matter of fact, if the temperature of a system is sufficiently low, so that the average kinetic energy $kT$ of a particle \ssect{10.6.4} is substantially less than the difference $\Delta E$ between the lowest and first energy lev­els $(\Delta E > kT)$, then the thermal excitation of the system is insufficient to transfer it from the state with the energy $E_{0}$ to one with the energy $E_{1}$ Therefore, at ultra-low temperatures the system should be in a state having the least energy $E_{0}$. The internal energy $U_{0}$ of the system \ssect{9.1.2} is equal to $E_{0}$, i.e. $U_{0}= E_{0}$. Hence the heat capacity of the system at constant volume 
\begin{equation*}%
C_{V \mu} = \left( \dv{U}{T}\right)_{V} = \left( \dv{E_{0}}{T}\right)_{V} = 0 \qas T \to 0
\end{equation*}
The coefficient of cubic expansion \ssect{40.2.3} also vanishes at absolute zero temperature.

It follows from the third law that a process in which a body could be cooled to absolute zero temperature is impossible (\emph{principle of the unattainability of absolute zero temperature}, sometimes called the \emph{Nernst principle}).
