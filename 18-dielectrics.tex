% !TEX root = handbook-physics.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode






\chapter{Dielectrics In An Electric Field}
\label{ch-18}

\section{Dipole Moments of Molecules of a Dielectric}
\label{sec-18.1}

\subsection{}\label{18.1.1} \redem{Dielectrics} are substances that do not conduct electric current. At not especially high temperatures and under conditions when dielectrics are not subject to very strong electric fields, these substances, in contrast to conductors, have no carriers of electric current.

\subsection{}\label{18.1.2} The molecules of a dielectric are electrically neutral and contain an equal number of positive and negative charges. Nevertheless, the molecules do have electrical properties. As a first approximation, a molecule of a dielectric can be regarded as a dipole having the electric dipole moment $\vb{p}_{e} =q \vb{l}$ \ssect{15.2.3}, where $q$ is the magnitude of the total positive (and also total negative) charges, located, respectively, at the centres of gravity of the charges, $\vb{l}$ is the distance between the centres of gravity of the positive and negative charges. As any dipole, a molecule of matter sets up an electric held in the surrounding space \ssect{15.2.3}.

\subsection{}\label{18.1.3} A dielectric is said to be \redem{nonpolar} (a \redem{nonpolar dielectric}) if the electrons of the atoms in its molecules are arranged symmetrically with respect to their nuclei (\ce{H2}, \ce{O2}, \ce{CCl4}, etc.). In such molecules the centres of gravity of the positive and negative charges coincide when there is no external electric held [$l = 0$ \ssect{18.1.2}] and the dipole moment $\vb{p}_{e}$ of the molecule is equal to zero. When a nonpolar dielectric is placed into an external electric held, the electron shells \ssect{39.3.6} in the atoms (or molecules) are deformed and the centres of gravity of the positive and negative charges are displaced with respect to each other ($l \neq  0$). At this an \redem{induced electric dipole moment} appears in atoms (or molecules) of the dielectric. This moment is proportional to the strength $\vb{E}$ of the external electric held:
\begin{align*}
\vb{p}_{e} & = \varepsilon_{0} \alpha \vb{E} && \text{(in SI units.)} \\
\vb{p}_{e} & =  \alpha \vb{E}&& \text{(in cgse units.)}
\end{align*}
where $\alpha$ is the \redem{polarizability} of the molecule (or atom) and $\varepsilon_{0}$ is the electric constant in SI units \ssect{14.2.7}.


The polarizability of a molecule depends only on its volume. It is important that $\alpha$ does not depend upon temperature. The thermal motion of the molecules in non-polar dielectrics has no effect on the induction of dipole moments in them. Molecules with such dipole moments are like \redem{quasi-elastic (induced)} dipoles.

\subsection{}\label{18.1.4} A \redem{polar dielectric} is one in which the molecules (or atoms) have electrons arranged asymmetrically with respect to their nuclei (\ce{H2O}, \ce{HCl}, \ce{NH3}, \ce{CH3Cl}, etc.). In such molecules the centres of gravity of the positive and negative charges do not coincide, but are at a practically constant distance I from each other. With respect to electrical properties, molecules of polar dielectrics are like \redem{rigid dipoles} that have a \redem{permanent dipole moment} $\vb{p}_{e} = \text{const}$.

\subsection{}\label{18.1.5} A \redem{permanent dipole} placed in a homogeneous external electrostatic field is subject to the action of a force couple \ssect{4.1.6} with the moment of force (torque) $\vb{M}$ equal to $\vb{M} = \vb{p}_{e} \cross \vb{E}$.

\begin{marginfigure}
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-18/fig-18-01.pdf}
\caption{A permanent dipole.\label{fig-18-01}}
\end{marginfigure}

The moment $\vb{M}$ of the force couple is perpendicular to the plane passing through vectors $\vb{p}_{e}$ and $\vb{E}$. From the tip of vector $\vb{M}$ rotation through the smaller angle from vectors $\vb{p}_{e}$ to $\vb{E}$ is seen to he counterclockwise. In \figr{fig-18-01} the moment of force $\vb{M}$ is directed normal to the diagram into the page and tends to turn the dipole so that $\vb{p}_{e}$ and $\vb{E}$ are parallel to each other. In real molecules of polar dielectrics, the molecules are deformed in addition to the turning of the dipole axes into alignment with the field. This leads to the induction of a certain dipole moment \ssect{18.1.3}.



\subsection{}\label{18.1.6} When a permanent dipole is placed into a non-homogeneous electric field, in which the field strength $\vb{E}$ varies along the length $l$ of the dipole, then, in addition to the torque $\vb{M}$ \ssect{18.1.5}, the dipole is subject to force $\vb{F}$, which is equal to
\begin{equation*}%
\vb{F} = p_{e} \pdv{E}{l},
\end{equation*}
or, in the more general form:
\begin{equation*}%
\vb{F} = p_{ex} \pdv{\vb{E}}{x} + p_{ey} \pdv{\vb{E}}{y} + p_{ez} \pdv{\vb{E}}{z},
\end{equation*}
where $p_{ex}$, $p_{ey}$ and $p_{ez}$ are the projections of vector $\vb{p}_{e}$ on the axes of a Cartesian coordinate system. In particular, if vector $\vb{E}$ is directed along the $OX$ axis and depends only on the coordinate $x$: $[E_{x} = E (x), \,\, E_{y} = E_{z} = 0$ and $\vb{E} = E_{x}\vu{i}]$, then
By the action of force $\vb{F}$ a free permanent dipole tends to move to a region of highest field strength.

\subsection{}\label{18.1.7} The potential energy $E_{p}$ of a permanent dipole in an external electric field is
\begin{equation*}%
E_{p} = -\vb{p}_{e} \, \vb{E} = - p_{e} E \cos \theta,
\end{equation*}
where $\vb{p}_{e}$ is the electric dipole moment, $\vb{E}$ is the field strength at the point where the dipole is located and $\theta$ is the angle be­ tween the dipole axis and vector $\vb{E}$. The minus sign indicates that the stable portion of the dipole corresponds to the angle $\theta = 0$, at which the potential energy of the dipole has its minimum value and vector $\vb{p}_{e}$ is directed along vector $\vb{E}$.

\section{Polarization of Dielectrics}
\label{sec-18.2}

\subsection{}\label{18.2.1} When a polar dielectric \ssect{17.1.4} is not in an external electric field, the vectors of its dipole moments are chaotically oriented as a result of the random thermal motion of the molecules. Consequently, the sum of the dipole moments of all the molecules is equal to zero in any physically infinitesimal volume $\Delta V$.\sidenote{Here $\Delta V /gg v_{0}$, where $v_{0}$ is the volume of a single molecule, and an immense number of molecules are contained in volume $\Delta V$.}

No dipole moments of the molecules whatsoever can be induced \ssect{18.1.3} in a non-polar dielectric that is not in an external electric field.

\subsection{}\label{18.2.2} When a \redem{dielectric} is placed in an external electric field, it \redem{is polarized}. This means that in any element of volume $\Delta V$, the total dipole moment of its molecules is no longer equal to zero. A dielectric in such a state is said to be polarized (\redem{polarized dielectric}). In accordance with the structure of the molecules (or atoms), distinction is made between the following three types of polarization:
\begin{enumerate}[label=(\alph*)]
\item \redem{Orientation polarization} of polar dielectrics \ssect{18.1.4}. The external electric field tends to align the dipole moments of the permanent dipoles along the direction of the field \ssect{18.1.5}. This is hindered by the random thermal motion of the molecules which tends to disarrange the dipoles arbitrarily. The combined action of the field and thermal motion results in preferred orientation of the electric dipole moments along the field. This polarization increases with the strength of the electric field and with a drop in temperature.
\item \redem{Electronic polarization} in non-polar dielectrics \ssect{18.1.3}. In the molecules of dielectrics of this type dipole moments directed along the field are induced \ssect{18.1.3} by the action of the external electric field. The thermal motion of the molecules has no effect on electronic polarization. In gaseous and liquid dielectrics, electronic polarization takes place practically simultaneously with orientational polarization.
\item \redem{Ionic polarization} in solid dielectrics having ionic crystal lattices \ssect{40.1.3}, such as \ce{NaCl} and \ce{CsCl}. The external electric field causes displacement of all the positive ions in the direction of the field strength $\vb{E}$ and all the negative ions in the opposite direction.
\end{enumerate}

\subsection{}\label{18.2.3} A quantitative measure of the polarization of a dielectric is the polarization vector $\vb{P}_{e}$. The \redem{polarization vector} (or simply the \redem{polarization}) is the ratio of the electric dipole moment of a small volume $\Delta V$ of the dielectric to this volume:
\begin{equation*}%
\vb{P}_{e} = \frac{1}{\Delta V} \sum_{i = 1}^{n} \vb{p}_{ei},
\end{equation*}
where $\vb{p}_{ei}$ is the electric dipole moment of the $i$-th molecule, and $n$ is the total number of molecules in the volume $\Delta V$. This volume should be so small that the electric field \ssect{15.1.2} within it can be assumed uniform. At the same time, the number $n$ of molecules in it should be sufficiently large to allow statistical methods of investigation \ssect{8.2.2} to be applied.

\subsection{}\label{18.2.4} For a homogeneous non-polar dielectric \ssect{18.1.3} in a uniform electric field
\begin{equation*}%
\vb{P}_{e} = n_{0} \, \vb{p}_{e},
\end{equation*}
where $n_{0}$ is the number of molecules in unit volume, and $\vb{p}_{e}$ is the dipole moment of a single molecule. Making use of the
formula for $\vb{p}_{e},$ \ssect{18.1.3} we obtain
\begin{align*}
\vb{P}_{e} & = n_{0} \varepsilon_{0} \alpha \vb{E} =  \varepsilon_{0} \chi_{e} \vb{E} && \text{(in SI units),} \\
\vb{P}_{e} & = n_{0} \alpha \vb{E} = \chi_{e} \vb{E} && \text{(in cgse units),}
\end{align*}
where $\chi_{e} = n_{0} \alpha$ is the \redem{dielectric} (or \redem{electric}) \redem{susceptibility of the substance}.


\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-18/fig-18-02.pdf}
\caption{Dependence of $\chi_{e}$ the susceptibility of the substance on absolute temperature on (a) non-polar and (b) polar molecules.\label{fig-18-02}}
\end{marginfigure}

\subsection{}\label{18.2.5} For a homogeneous polar dielectric \ssect{18.1.4} in a uniform electric field, $\vb{P}_{e} = n_{0}\,\expval{\vb{p}_{e}}$, where $\expval{\vb{p}_{e}}$ is the average value of the component along the field of the permanent dipole moment of the molecule. When a polar dielectric is in a weak external electric field, the dielectric susceptibility is calculated by the \redem{Debye-Langevin formula}:
\begin{align*}
\chi_{e} & = \frac{n_{0}p_{e}^{2}}{3 \varepsilon_{0} kT} && \text{(in SI units),} \\
\chi_{e} & = \frac{n_{0}p_{e}^{2}}{3 kT} && \text{(in cgse units).}
\end{align*}
Here $k$ is Boltzmann’s constant \ssect{8.4.5}, and $T$ is the absolute temperature. The remaining notation is given in \ssect{18.1.3}. Shown in \figr{fig-18-02} is the dependence $\chi_{e} = \chi_{e} (1/T)$ for non­ polar (a) and for polar (b) molecules. The straight line (b) does not pass through the origin of coordinates because orientational and electronic polarization \ssect{18.2.2} usually occur in polar molecules and the dielectric susceptibility consists of two parts: $\chi_{e} = \chi_{e}' + \chi_{e}''$ where $\chi_{e}'$ and $\chi_{e}''$ are expressed by the equations in Sections \ssect{18.2.4} and \ssect{18.2.5}.



\begin{marginfigure}%[-2cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-18/fig-18-03.pdf}
\caption{The mutual compensation of the dipole charges of opposite sign alongside one another.\label{fig-18-03}}
\end{marginfigure}

\subsection{}\label{18.2.6} If a dielectric is in a uniform electric field, the electrical neutrality of any volume $\Delta V$ of the dielectric, containing a sufficiently large number of molecules, is provided for by the mutual compensation of the dipole charges of opposite sign alongside one another (\figr{fig-18-03}). In the thin layers at surfaces $S_{1}$ and $S_{2}$ of the dielectric, bounding its volume, \redem{surface-bound polarization} charges appear as a result of the polarization of the dielectric. At the surface where the lines of force \ssect{15.1.6} of the external electric field enter the dielectric, negative charges of the ends of the dipole molecules appear. Positive charges appear at the opposite surface. The surface density \ssect{15.2.3} $\sigma_{p}$ of the polarization charges is calculated by the formula
\begin{equation*}%
\sigma_{p} = P_{en},
\end{equation*}
where $P_{en}$ is the projection of vector $\vb{P}_{e}$ on the outward normal to the surface of the dielectric.

In a nonuniform electric field, the polarization of a dielectric is also nonuniform and the polarization vector $\vb{P}_{e}$ depends upon the coordinates. Hence, in addition to surface-bound charges, space polarization charges also appear. These are distributed with the volume charge density \ssect{15.2.3} $\rho_{p}$:
\begin{equation*}%
\rho_{p} = - \div{\vb{P}_{en}},
\end{equation*}
where $\div{\vb{P}_{en}} = \pdv*{P_{ex}}{x} + \pdv*{P_{ey}}{y}  + \pdv*{P_{ez}}{z}$ is the divergence of the polarization vector.

\section{Relation Between Displacement, Field Strength and Polarization Vectors}
\label{sec-18.3}

\subsection{}\label{18.3.1} Distinction is made in substances between two kinds of charges: free and bound.

\redem{Bound charges} are those included in the composition of atoms and molecules, as well as the charges of ions in crystalline solids with an ionic lattice \ssect{40.1.3}. Charges not bound to the aforementioned particles of matter are said to be free. \redem{Free charges} include: 
\begin{enumerate}[label=(\alph*)]
\item current-conducting charges in conducting media [conduction electrons in metals and semiconductors \ssect{16.4.1}, holes in semiconductors \ssect{41.10.3}, ions in electrolytes and gases],
\item surplus charges that are transmitted to a body by various means and violate its electrical neutrality. An example are charges imparted from outside onto the surface of a dielectric. 
\end{enumerate}

\subsection{}\label{18.3.2} Generally, an electric held is set up in a dielectric by both free and bound charges. The held strength vector $\vb{E}$ characterizes the resultant held in the dielectric, set up by the two kinds of charges, and depends upon its electrical properties: the relative permittivity (dielectric constant) $\varepsilon_{r}$ \ssect{14.2.4}. But the primary source of an electric held in a dielectric is the free charges. As a matter of fact, a field of bound charges is set up in a dielectric as a result of its polarization when it is placed in an external electric held set up by free electric charges. 

\subsection{}\label{18.3.3} The information contained in Sections \ssect{18.3.1} and \ssect{18.3.2} call for a certain revision of the Ostrogradsky-Gauss theorem \ssect{15.3.3}. The algebraic sum of the charges $\sum_{i=1}^{k} q_{i}$, on the right­ hand side of the theorem is to be regarded as the algebraic
sum of the free electric charges enclosed by surface $S$, i.e. 
\begin{equation*}%
\sum_{i=1}^{k} q_{i} = q_{\text{free}},
\end{equation*}
and the theorem is then of the form 
\begin{align*}
\oint_{S} \vb{D}\, \dd \vb{S} & = \oint_{S} D_{n}\, \dd S =  q_{\text{free}}&& \text{(in SI units),} \\
\oint_{S} \vb{D}\, \dd \vb{S} & = \oint_{S} D_{n}\, \dd S =  4\pi q_{\text{free}}&& \text{(in cgse units).}
\end{align*}

In this form the Ostrogradsky-Gauss theorem is valid for an electric field either in homogeneous and isotropic or in non-homogeneous and anisotropic media (cf. \ssect{15.3.3}).

\subsection{}\label{18.3.4} For an electric field in vacuum
\begin{align*}%
\vb{D} & = \varepsilon_{0} \vb{E} && \text{(in SI units),} \\
\vb{D} & =  \vb{E}&& \text{(in cgse units).}
\end{align*}
The flux of the field strength vector $\vb{E}$ through an arbitrary closed surface $S$ in vacuum is
\begin{align*}%
\oint_{S} \varepsilon_{0}  E_{n}\, \dd S =  q_{\text{free}} && \text{(in SI units),} \\
\oint_{S}  E_{n}\, \dd S =  4 \pi q_{\text{free}}&& \text{(in cgse units).}
\end{align*}
In accordance with Section \ssect{18.3.2}, the flux of vector $\vb{E}$ for a field in a substance is
\begin{align*}%
\oint_{S} \varepsilon_{0}  E_{n}\, \dd S & =  q_{\text{free}} + q_{\text{bound}} && \text{(in SI units),} \\
\oint_{S}  E_{n}\, \dd S & =  4 \pi (q_{\text{free}} + q_{\text{bound}})&& \text{(in cgse units).}
\end{align*}
The sum of bound charges $q_{\text{bound}}$ within closed surface $S$ is calculated by the equation
\begin{equation*}%
q_{\text{bound}} = - \oint_{S} P_{en} \, \dd S,
\end{equation*}
where $P_{en}$ is the projection of the polarization vector on the outward normal to surface $\dd S$. Hence
\begin{align*}%
\oint_{S} (\varepsilon_{0}  E_{n} + P_{en})\, \dd S & =  q_{\text{free}}  && \text{(in SI units),} \\
\oint_{S} ( E_{n} + 4 \pi P_{en})\, \dd S & =  4 \pi q_{\text{free}} && \text{(in cgse units).}
\end{align*}
A correlation with the general formulation of the Ostrogradsky-Gauss theorem \ssect{18.3.3} enables a relation to be established between vectors $\vb{D}$, $\vb{E}$ and $\vb{P}_{e}$:
\begin{align*}%
D_{n} & =  \varepsilon_{0}E_{n} + P_{en} && \text{(in SI units),} \\
D_{n} & = E_{n} + 4 \pi P_{en} && \text{(in cgse units).}
\end{align*}
or, by virtue of the arbitrary nature of outward normal 
\begin{align*}%
\vb{D} & =  \varepsilon_{0} \vb{E} + \vb{P}_{en} && \text{(in SI units),} \\
\vb{D} & =  \vb{E} + 4 \pi \vb{P}_{en} && \text{(in cgse units).}
\end{align*}
These formulas are generalizations of the formulas in Section \ssect{15.3.1}. Making use of the results obtained in Section \ssect{18.2.3}, we have for an isotropic homogeneous medium
\begin{align*}%
\vb{D} & =  \varepsilon_{0} \vb{E} + \varepsilon_{0} \chi_{e} \vb{E} = \varepsilon_{0} (1 + \chi_{e}) \vb{E} && \text{(in SI units),} \\
\vb{D} & =  \vb{E} + 4 \pi \chi_{e} \vb{E} = (1 + 4 \pi \chi_{e}) \vb{E} && \text{(in cgse units).}
\end{align*}
Consequently,
\begin{align*}%
\vb{D} & =  \varepsilon_{0} \varepsilon_{r} \vb{E}, \,\,\text{where} \,\, \varepsilon_{r} = 1 + \chi_{e} && \text{(in SI units),} \\
\vb{D} & =  \varepsilon_{r} \vb{E}, \,\,\text{where} \,\, \varepsilon_{r} = 1 + 4 \pi \chi_{e} && \text{(in cgse units).}
\end{align*}
The quantity $ \varepsilon_{r}$ is the relative permittivity (dielectric constant) (cf. \ssect{14.2.4}).

For vacuum $ \varepsilon_{r} = 1$ and $\chi_{e} = 0$.

The concept of $ \varepsilon_{r}$ introduced in Section \ssect{14.2.4} has a meaning only for isotropic homogeneous media.



\section{Ferroelectric Materials}
\label{sec-18.4}


\subsection{}\label{18.4.1} \redem{Ferroelectrics} belong to a group of crystalline dielectrics. The first substance of this type to be investigated was Seignette’s salt (potassium sodium tartrate) \ce{NaKC4H4O6-4H2O} and, consequently, they are sometimes called seignette-electrics. An­ other example of a ferroelectric is barium titanate \ce{BaTiO3}. Typical of ferroelectrics is a drastic increase in the relative permittivity (dielectric constant) \ssect{14.2.4} in a definite temperature range (\figr{fig-18-04}).

\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-18/fig-18-04.pdf}
\caption{Dependence of relative permittivity on temperature in ferroelectrics.\label{fig-18-04}}
\end{marginfigure}

\subsection{}\label{18.4.2} The relative permittivity $\varepsilon_{r}$ and the dielectric susceptibility $\chi_{e}$ \ssect{18.2.4} of ferroelectrics are functions of the field strength $\vb{E}$ in the substance (\figr{fig-18-05}). As a result, no linear relation between vectors $\vb{P}_{e}$ and $\vb{E}$ is observed in those substances. The dependence of the electric displacement $\vb{D}$ on the field strength is of a complex nature and a linear relation between $\vb{D}$ and $\vb{E}$ exists only at very high $\vb{E}$ values (\figr{fig-18-06}).


\begin{marginfigure}[-1cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-18/fig-18-05.pdf}
\caption{Dependence of relative permittivity and the dielectric susceptibility on the field strength in ferroelectrics.\label{fig-18-05}}
\end{marginfigure}




\subsection{}\label{18.4.3} A monocrystal of a ferroelectric is divided into spontaneously polarized regions, called \redem{domains} (cf. \ssect{26.5.4}). This \redem{spontaneous polarization} of the domains is a result of the orientation of the dipole moments of the molecules in the domain in a definite direction. In the absence of an external electric field, the polarization vectors of the various domains are directed at random and for a large monocrystal or a polycrystal the total polarization is equal, on an average, to zero. When a ferroelectric is placed in an electric field, the electric moments of the domains are reoriented and the crystal acquires a total polarization not equal to zero.



\subsection{}\label{18.4.4} Domains are formed in ferroelectrics within a definite temperature range, between the upper and lower \redem{Curie points}
$\theta^{\mathrm{up}}_{C}$ and $\theta^{\mathrm{low}}_{C}$ (cf. \ssect{26.5.2}). For Seignette’s salt (also called Rochelle salt) (18.4.1), $\theta^{\mathrm{up}}_{C} = \SI{298}{\kelvin}$ and $\theta^{\mathrm{up}}_{C} = \SI{258}{\kelvin}$. At $T \geqslant \theta^{\mathrm{up}}_{C}$ the forces of interaction between the dipoles cannot resist the thermal motion, the spontaneous polarization of the domains is violated and the ferroelectric is converted into an ordinary polar dielectric. The drastic increase in heat capacity of the substance indicates that a second-order phase transition \ssect{12.4.2}) occurs at the Curie point. A disordered phase exists above the Curie point $\theta^{\mathrm{up}}_{C}$ and the dielectric is not polarized in the absence of an external field. At $T < \theta^{\mathrm{up}}_{C}$ there is an ordered phase indicated by the spontaneous po­larization in the domains.\sidenote{A discussion of the phenomena that occur in ferroelectrics at $T = \theta^{\mathrm{low}}_{C}$ and lead to the destruction of the domains is beyond the scope of the present handbook.}
\begin{marginfigure}[-5cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-18/fig-18-06.pdf}
\caption{The dependence of the electric displacement $\vb{D}$ on the field strength  in ferroelectrics.\label{fig-18-06}}
\end{marginfigure}

\subsection{}\label{18.4.5} The phenomenon of \redem{dielectric}, or \redem{ferromagnetic, hysteresis} (lag) is observed in ferroelectrics. As is evident from \figr{fig-18-07}, with an increase in the strength of the external field, the magnitude of vector $\vb{P}_{e}$ increases and reaches saturation at point $a$. Then, as $E$ is reduced to zero, a ferroelectric has \redem{remanent polarization}, characterized by the value of the polarization vector $\vb{P}_{e0}$. Polarization completely disappears only after applying a depolarization electric field of the strength $-\vb{E}_{c}$ in the opposite direction. This is called the \redem{coercive force}. Periodic reversal of polarization of a ferromagnetic material is accompanied by the consumption of electric energy, spent in heating the sub­ stance. The area of the hysteresis loop is proportional to the electric energy that is converted into internal energy per unit volume of the ferroelectric per cycle.


\begin{marginfigure}%[2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-18/fig-18-07.pdf}
\caption{The phenomenon of dielectric, or ferromagnetic, hysteresis in ferroelectrics.\label{fig-18-07}}
\end{marginfigure}