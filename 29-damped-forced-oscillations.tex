% !TEX root = handbook-physics.tex
% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode






\chapter{Damped And Forced Oscillations}
\label{ch-29}

\section{Damped Oscillations}
\label{sec-29.1} 

\subsection{}\label{29.1.1} The \redem{damping of an oscillation or vibration} is its gradual weakening in the course of time due to the loss of energy of the oscillatory (vibratory) system. Free oscillations (vibrations) of real systems are always damped. The damping of free mechanical vibrations is due chiefly to friction and the excitation of elastic waves \ssect{30.1.3} in the surrounding medium. Damping in electrical oscillatory systems is due to thermal losses in the conductors making up the system or located in its variable electric field, energy losses on the radiation of electromagnetic waves \ssect{31.1.1}, and heat losses in dielectrics and ferromagnetic materials as a result of electrical and magnetic hysteresis \ssect{18.4.5} and \ssect{26.5.2}.

The damping of oscillations depends upon the properties of the oscillatory system. A system is said to be \redem{linear} if the para­ meters characterizing physical properties of the system, essential in the process being considered, do not change in the course of the process. Linear systems are described by linear differential equations. A spring pendulum \ssect{28.2.3}, for instance, moving in a viscous medium, is a linear system if the resistance coefficient of the medium and the elasticity of the spring are independent of the velocity and displacement of the pendulum. An electrical oscillatory circuit \ssect{28.3.1} can be considered linear if its electrical resistance $R$, capacitance $C$ and inductance $L$ are independent of the current in the circuit, and the voltage. In the majority of cases real oscillatory or vibratory systems arc sufficiently close, with respect to their properties, to linear ones.

\subsection{}\label{29.1.2} \redem{The differential equation for the free damped oscillation of a linear system} is of the form:
\begin{equation}%
\dv[2]{s}{t} + 2 \beta \dv{s}{t} + \omega_{0}^{2}s = 0.
\end{equation}
Here $s$ is the physical characteristic of the system that varies in the oscillation, $\beta = \text{const.} > 0$ is the \redem{damping factor}, and $\omega_{0}$ is the angular frequency of free \redem{undamped} oscillation of the same system, i.e. in the absence of energy losses (at $\beta = 0$).

\textbf{\large Example~1.} \redem{Free damped oscillation of a spring pendulum} \ssect{28.2.3}. A pendulum of mass $m$, oscillating in a straight line along the $OX$ axis due to the elastic force of its spring, is also subject to a resisting force $\vb{F}_{\textrm{res}} = -b \vb{v}$, where $\vb{v}$ is the velocity of the pendulum and $b = \text{const.} > 0$ is the \redem{resistance coefficient}. The differential equation of free damped oscillation of the pendulum is
\begin{align*}%
m \dv[2]{x}{t} & = -b \dv{x}{t}  - kx, \qor \\
\dv[2]{x}{t} & + 2\beta \dv{x}{t} + \omega_{0}^{2} x = 0,
\end{align*}
where $\beta = b/2m$, $\omega_{0} = \sqrt{k/m}$, and $k$ is a factor specifying the elastic properties of the spring.

\textbf{\large Example~2.} \redem{Free damped oscillation in an electric oscillatory circuit}. The electrical resistance of a real circuit $R \neq 0$, and the differential equation of the oscillation in the circuit \ssect{28.3.1} is of the form
\begin{equation*}%
\dv[2]{q}{t} + 2\beta \dv{q}{t} + \omega_{0}^{2} q = 0,
\end{equation*}
where $\beta = R/2L$ and $\omega_{0}= 1/\sqrt{LC}$.

\subsection{}\label{29.1.3} If damping is small $(\beta < \omega_{0})$ the dependence of $s$ on $t$ that satisfies the equation of damped oscillation \ssect{29.1.2} is of the form
\begin{equation*}%
s = A_{0} e^{-\beta t} \sin (\omega t + \psi_{0}).
\end{equation*}
Here $\omega = \sqrt{\omega_{0}^{2} - \beta^{2}}$ and the constant quantities $A_{0}$ and $\psi_{0}$ depend upon the initial conditions, i.e. upon the values of $s$ and $\dv*{s}{t}$ at the initial instant of time $(t = 0)$. The curve of the dependence of s on $t$ at $\psi_{0} = 0$ is shown in \figr{fig-29-01}. 

\begin{figure}[h]%[-2cm]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-29/fig-29-01.pdf}
\sidecaption{The curve of the dependence of s on $t$ at $\psi_{0} = 0$ for damped oscillations.\label{fig-29-01}}
\end{figure}


Damped oscillations are not periodical ones \ssect{28.1.2}. For example, the maximum value of the varying quantity $s$, reached at a certain instant $t_{1}$ of time, is never repeated subsequently
(at $t > t_{1}$). But the quantity $s$ becomes zero in damped oscillation as it varies in a single direction (decreases, for instance) and also reaches its maximum and minimum values after \redem{equal} time intervals. Thus
\begin{equation*}%
T = \frac{2\pi}{\omega} = \frac{2 \pi}{\sqrt{\omega_{0}^{2} - \beta^{2}}}.
\end{equation*}
For this reason, the quantities $T$ and $\omega$ are conditionally called the \redem{period (conditional period)} and \redem{angular frequency (conditional angular frequency) of damped oscillation}.

The quantity
\begin{equation*}%
A = A_{0} \exp(-\beta t)
\end{equation*}
Is called the \redem{amplitude of damped oscillation}, where $A_{0}$ is the initial amplitude. The amplitude of damped oscillation decreases in the course of time; the greater the damping factor $\beta$, the more rapidly the amplitude decreases.

The time interval $\tau = 1/\beta$ during which there is an $e$-fold decrease in the amplitude of damped oscillation, is called the \redem{relaxation time}.

\subsection{}\label{29.1.4} \redem{The logarithmic decrement of damping} is the dimensionless quantity $\delta$, equal to the natural logarithm of the ratio of the amplitudes of damped oscillation at the instants of time $t$ and $t + T$ (where $T$ is the conditional period of oscillation). Thus
\begin{equation*}%
\delta =\ln \frac{A(t)}{A(t+ T)} = \beta T = \frac{T}{\tau} = \frac{1}{N},
\end{equation*}
where $N$ is the number of oscillations in which the amplitude is reduced $e$-fold.

The relation between the angular frequency $\omega$ of damped oscillation of a system and the logarithmic decrement of damping $\delta$ is
\begin{equation*}%
\omega = \omega_{0} \sqrt{1 - \left( \frac{\omega}{\omega_{0}}\right)^{2} \,  \left( \frac{\delta}{2\pi}\right)^{2}}.
\end{equation*}


\subsection{}\label{29.1.5} \redem{The $Q$ factor (or quality factor or storage factor)} of an oscillatory system is the dimensionless quantity $Q$, equal to the product of $2\pi$ by the ratio of the energy of oscillation $E (t)$ of the system at the arbitrary time instant $t$ to the decrease of this energy during the length of time from $t$ to $t + T$, i.e. during one conditional period of damped oscillation. Thus
\begin{equation*}%
Q = 2 \pi \frac{E(t)}{E(t) - E(t+T)}.
\end{equation*}
Since the energy $E (t)$ is proportional to the square of the amplitude $A (t)$ of the oscillation,
\begin{align*}%
Q & = 2 \pi \frac{A^{2}(t)}{A^{2}(t) - A^{2}(t+T)} = \frac{2\pi}{1 - \exp(-2 \beta T)}\\ 
& = \frac{2\pi}{1 - \exp(-2\delta)}.
\end{align*}
At low values of the logarithmic decrement of damping $\delta$, the $Q$ factor of an oscillatory system is $Q = \pi/\delta$. Here the conditional period of damped oscillation $T$ is practically equal to the period $T_{0}$ of free undamped oscillation and, consequently, $Q = \pi/\beta T_{0} = \omega_{0}/2\beta$. For example, the $Q$ factor of an electrical
oscillatory circuit \ssect{28.3.1} is $Q = \sqrt{L/C}/R$, and that of a spring pendulum \ssect{29.1.2} is $Q = \sqrt{km}/b$.

\subsection{}\label{29.1.6} The conditional period of damped oscillation increases with the damping factor $\beta$ and becomes infinite at $\beta = \omega_{0}$. If $\beta > \omega_{0}$ the differential equation of motion of the system
\begin{equation*}%
\dv[2]{s}{t} + 2\beta \dv{s}{t} + \omega_{0}^{2} s = 0,
\end{equation*}
has the following general solution: 
\begin{equation*}%
s = C_{1} \exp(-\alpha_{1}t) + C_{2} \exp(-\alpha_{2}t),
\end{equation*}
where $\alpha_{1} = \beta + \sqrt{\beta^{2} - \omega_{0}^{2}}$,  $\alpha_{2} = \beta - \sqrt{\beta^{2} - \omega_{0}^{2}}$ and $C_{1}$ and $C_{2}$ are constant factors depending upon the initial conditions. If the initial values (at the instant of time $t = 0$) equal: $s = s_{0}$ and $\dv*{s}{t} = v_{0}$, then
\begin{equation*}%
C_{1} = -\frac{\alpha_{2}s_{0} + v_{0}}{\alpha_{1} - \alpha_{2}} \qand C_{2} = \frac{\alpha_{1}s_{0} + v_{0}}{\alpha_{1} - \alpha_{2}} 
\end{equation*}
Such motion of the system is not of an oscillatory nature and is said to be \redem{aperiodic}. Depending upon the initial conditions, aperiodic motion of the system may be either of two kinds (\figr{fig-29-02}). Type $a$ motion occurs when $s_{0}$ and $v_{0}$ are opposite in sign and $\abs{v_{0}} > \alpha_{1} \abs{s_{0}}$. Otherwise, the aperiodic motion is of the $b$ type.

\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-29/fig-29-02.pdf}
\sidecaption{The different types of aperiodic motions.\label{fig-29-02}}
\end{marginfigure}

\section{Forced Mechanical Vibration}
\label{sec-29.2} 



\subsection{}\label{29.2.1} A variable external force, applied to a system and causing its forced mechanical vibration, is called the \redem{disturbing}, or \redem{driving}, force.

The \redem{differential equation of forced vibration} (or \redem{oscillation}) of the simplest linear system \ssect{29.1.1}, the spring pendulum \ssect{29.1.2}, executed along the $OX$ axis due to the action of the variable external force $\vb{F} (t)$ is:
\begin{equation*}%
\dv[2]{x}{t} + 2\beta \dv{x}{t} + \omega_{0}^{2} x = \frac{1}{m} \, F_{x}(t).
\end{equation*}

If $F_{x} (t)$ is a periodic function of time, then, after application of the force to the pendulum, the \redem{forced vibration} begins \redem{with a transient condition}. The pendulum simultaneously participates in two vibrations:
\begin{equation*}%
x = x_{1}(t) + x_{2}(t)
\end{equation*}
The first term corresponds to free damped oscillation\sidenote{It is assumed that $\beta < \omega_{0}$. Otherwise, the free motion of the pendulum would be aperiodic \ssect{29.1.6}, i.e.
\begin{align*}
x_{1}(t) &= C_{1} \exp \left(-(\beta + \sqrt{\beta^{2} - \omega_{0}^{2}}) t\right) \\
&\quad + C_{2} \exp \left(-(\beta - \sqrt{\beta^{2} - \omega_{0}^{2}}) t\right).
\end{align*}
} of the pendulum \ssect{29.1.3}:
\begin{equation*}%
x_{1}(t) = A_{0} \exp(- \beta t) \sin (\omega t + \psi_{0}),
\end{equation*}
where $\omega = \sqrt{\omega_{0}^{2} - \beta^{2}}$.

The second term corresponds to the undamped periodic oscillation of the pendulum at a frequency equal to that of the driving force $F_{x} (t)$.

The amplitude value of $x_{1} (t)$, equal to $A_{0} \exp (-\beta t)$, is reduced at a more or less rapid rate after the beginning of the forced vibration (or oscillation). During the time $\tau = 4.6/\beta$ the amplitude of $x_{1} (t)$ decreases by a factor of 100. Consequently, after a certain time $\tau$ following the beginning of vibration $(\tau \approx \tau_{0})$, free vibration of the pendulum practically ceases: $x (t) \approx x_{2}(t)$. The pendulum reaches a state of \redem{steady forced vibration} (or oscillation) at the frequency of the driving force.

\subsection{}\label{29.2.2} When the driving force varies according to a harmonic law, i.e. $F_{x} = F_{0}\cos \Omega t$, the steady-state forced vibration of the pendulum is also harmonic at the same frequency. Thus
\begin{equation*}%
x = A \cos (\Omega t + \varphi_{0}).
\end{equation*}
The amplitude $A$ of this vibration and the phase shift $\varphi_{0}$ between the displacement and the driving force depend upon the relation between the angular frequencies of the forced vibration $(\Omega)$ and the free undamped vibration $(\omega_{0})$ of the pendulum. Thus
\begin{align*}%
A &= \frac{F_{0}}{m \sqrt{(\omega_{0}^{2} - \Omega^{2})^{2}+ 4\beta^{2}\Omega^{2}}}, \qand \\ 
\tan \varphi_{0} & = - \frac{2 \beta \Omega}{\omega_{0}^{2} - \Omega^{2}}.
\end{align*}
\begin{marginfigure}[-3cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-29/fig-29-03.pdf}
\sidecaption{The curves of the functions $A (\Omega)$ for various values of the damping factor $\beta$.\label{fig-29-03}}
\end{marginfigure}
At $\Omega = 0$ we obtain $\varphi_{0}(0) = 0$ and $A (0) = A_{0}= F_{0}/m\omega_{0}^{2} = F_{0}/k$ which is the \redem{static displacement} of the pendulum from its equilibrium position by the constant force $F_{x} = F_{0}$. At $\Omega \to \infty$, the amplitude $A (\Omega) \to 0$ and $\tan \varphi_{0} \to 0$, whereas $\varphi_{0} \to -\pi$. The curves of the functions $A (\Omega)$ and $\varphi_{0} (\Omega)$ are shown in \figr{fig-29-03} and \figr{fig-29-04} for various values of the damping factor $\beta$.
\begin{marginfigure}[1cm]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-29/fig-29-04.pdf}
\sidecaption{The curves of the functions $\varphi_{0} (\Omega)$ for various values of the damping factor $\beta$.\label{fig-29-04}}
\end{marginfigure}

\subsection{}\label{29.2.3} The amplitude of the displacement in the case of steady-state forced harmonic vibration (oscillation) of the pendulum reaches its maximum value at the angular frequency
\begin{equation*}%
\Omega_{r} = \sqrt{\omega_{0}^{2} - 2 \beta^{2}} = \sqrt{\omega^{2} - \beta^{2}},
\end{equation*}
where $\omega$ is the angular frequency of free damped vibration of the pendulum \ssect{29.1.3}. The frequency $\Omega_{r}$ is called the \redem{resonance frequency}.

The maximum amplitude is
\begin{equation*}%{
A_{\text{max}} = A(\Omega_{r}) = \frac{F_{0}}{2m\beta \omega} = \frac{\pi F_{0}}{m\delta \omega^{2}},
\end{equation*}
where $\delta$ is the logarithmic decrement of damping \ssect{29.1.4}. When $\beta \ll \omega_{0}$, $\Omega_{r} \approx \omega_{0}$
$\varphi_{0}(\Omega_{r}) \approx  -\pi/2$, and $A_{\text{max}} \approx Q A_{0}$, where $Q  \approx \pi/\delta$ is the $Q$ factor of the pendulum \ssect{29.1.5} and $A_{0}$ is the static displacement \ssect{29.2.2}.

The drastic increase in the amplitude of forced mechanical vibration (or oscillation) as the angular frequency of the driving force approaches the value $\Omega_{r}$ is the phenomenon called \redem{mechanical resonance}. Accordingly, the curves of the dependence of $A$ on $\Omega$, shown in \figr{fig-29-03} are called \redem{resonance curves}.

As the damping factor $\beta$ is increased, the peaks on the resonance curves are rapidly reduced (at small $\beta$ values the amplitude $A_{\text{max}} \approx 1/\beta$) and the resonance frequency $\Omega_{r}$ is slowly decreased.

\subsection{}\label{29.2.4} The \redem{velocity of the pendulum} in steady-state forced harmonic vibration (oscillation) is
\begin{equation*}%
v_{x} = \dv{x}{t} = - A \Omega \sin ( \Omega t + \varphi_{0}) = A_{v}\cos(\Omega t + \alpha).
\end{equation*}
Here $A_{v} = A\Omega$ and $\alpha = \varphi_{0} + \pi/2$ are the amplitude of the velocity and the phase difference between the velocity and the driving force:
\begin{align*}%
A_{v} &= \frac{F_{0}\Omega}{m \sqrt{(\omega_{0}^{2} - \Omega^{2})^{2}+ 4\beta^{2}\Omega^{2}}} =  \frac{F_{0}}{m \sqrt{\dfrac{(\omega_{0}^{2} - \Omega^{2})^{2}}{\Omega^{2}} + 4 \beta^{2}}}, \qand \\ 
\tan \alpha & = -\cot \varphi_{0} = \frac{\omega_{0}^{2} - \Omega^{2}}{2 \beta \Omega}.
\end{align*}
The amplitude of the velocity is maximal at $\Omega = \omega_{0}$ and equals
\begin{equation*}%
(A_{v})_{\text{max}} = A_{v} (\omega_{0}) = \frac{F_{0}}{2m \beta}.
\end{equation*}
In this case, $\alpha = 0$, i.e. the velocity of the pendulum oscillates in phase with the driving force. As $\Omega \to \infty$, the amplitude $A_{v} \to 0$ and $\alpha \to -\pi/2$, whereas as $\Omega \to 0$, the amplitude $A_{v} \to 0$ and $\alpha \to \pi/2$.

\subsection{}\label{29.2.5} The \redem{acceleration of the pendulum} in steady-state forced harmonic vibration (oscillation) is
\begin{align*}%
a_{x} & = dv[2]{x}{t} = -A\Omega^{2} \cos (\Omega t + \varphi_{0}) \\
& = A_{a} \cos (\Omega t + \gamma).
\end{align*}
Here $A_{a} = A\Omega^{2}$ and $\gamma = \varphi_{0} + \pi$ are the amplitude of the acceleration and the phase difference between the acceleration and the driving force:
\begin{equation*}%
A_{a} = \frac{F_{0}\Omega^{2}}{m \sqrt{(\omega_{0}^{2} - \Omega^{2})^{2} + 4 \beta^{2}\Omega^{2}}} = \frac{F_{0}}{m \sqrt{[(\omega_{0}/\Omega)^{2})^{2} - 1]^{2} + (2\beta/\Omega)^{2}}}.
\end{equation*}
The amplitude of acceleration is maximal at 
\begin{equation*}%
\Omega = \frac{\omega_{0}}{\sqrt{1 + (\beta/\omega_{0})^{2}}}  \approx \sqrt{\omega_{0}^{2} - \beta^{2}}.
\end{equation*}
At $\Omega = 0$, the amplitude $A_{a} = 0$, whereas as $\Omega \to \infty$ the amplitude of the acceleration tends to the value $A_{a} (\infty) = F_{0}/m$.

\subsection{}\label{29.2.6} In steady-state forced vibration, the energy loss of the vibratory system, due to dissipative forces \ssect{1.3.7}, is completely compensated by the work performed on the system by the driving force. The work, for example, done in one complete oscillation by the force of resistance acting on the spring pendulum is
\begin{align*}%
W_{1} & = -b \int_{0}^{T} \left( \dv{x}{t} \right)^{2} \, \dd t = - 2m \beta A^{2}\Omega^{2} \int_{0}^{T} \sin^{2} (\Omega t + \varphi_{0}) \, \dd t \\
& = -m\beta A^{2} \Omega^{2},
\end{align*}
where $b = 2 m \beta$ is the resistance coefficient \ssect{29.1.2}.

The work done during the same time by the driving force $F_{x} = F_{0}\cos \Omega t$ is
\begin{align*}%
W_{2} & = F_{0} \int_{0}^{T} \dv{x}{t} \cos \Omega t \, \dd t = - A\Omega F_{0} \int_{0}^{T} \cos \Omega t \sin(\Omega t  \varphi_{0}) \, \dd t \\
& = -\frac{1}{2} A \Omega F_{0} \sin \varphi_{0} = - W_{1},
\end{align*}
since $\sin \varphi_{0} = —2m\beta A \Omega/F_{0}$.

\subsection{}\label{29.2.7} If the driving force acting on the spring pendulum varies periodically, but not according to a harmonic law, it can he represented as the sum of the harmonics of this force \ssect{28.4.6}. These have different amplitudes, initial phases and angular frequencies that are multiples of $\Omega = 2\pi/T$, where $T$ is the period of variation of the driving force. Since the pendulum is a linear vibratory (or oscillatory) system \ssect{29.1.1}, each harmonic of the driving force acts on the system as if there were no other harmonics. Therefore, the steady-state forced oscillation of the pendulum, caused by an arbitrary periodic driving force, can be dealt with as the result of superposing steady-state forced oscillations due to each harmonic component of the driving force acting separately. 

The ``contribution'' of the various harmonic components to the resultant oscillation depends upon their frequency and amplitude. Owing to resonance phenomena, an essential role, is played by only the harmonic components of the driving force that have an angular frequency close to the resonance frequency of the pendulum \ssect{29.2.3}. If the damping factor $\beta$ of the pendulum is small, the pendulum can execute steady-state forced oscillation close to the harmonic one, even if the driving force is far from harmonic. An example of such forced oscillation is that of a pendulum subject to short periodical external action in the form of impacts in a single direction and repeated over equal lengths of time, which are equal to the period of free oscillation of the pendulum.

\section{Forced Electrical Oscillation}
\label{sec-29.3}

\subsection{}\label{29.3.1} To obtain forced oscillation in an electrical oscillatory circuit \ssect{28.3.1} it is necessary to connect into it a source of electrical energy, whose emf $\emf$ varies with time (\figr{fig-29-05}).
\begin{marginfigure}[1cm]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-29/fig-29-05.pdf}
\sidecaption{Setup for obtaining forced electrical oscillaiors with $RLC$ circuit.\label{fig-29-05}}
\end{marginfigure}

In electrical engineering a source of electrical energy, characterized by its emf and internal electrical resistance, is called a \redem{seat}, or \redem{source, of emf (voltage source)}. According to Ohm’s law for the portion $\mathit{1}-R-L-\mathit{2}$ \ssect{21.2.3}, the quasi-stationary current \ssect{28.3.1}, produced in the circuit in forced oscillation, can be found from the formula
\begin{equation}%
IR = V_{1} - V_{2} - L \dv{I}{t} + \emf ( t ) .
\end{equation}
Here $V_{2}- V_{1}= q/C$ is the potential difference across the capacitor plates, $q$ is the capacitor charge, and the internal electrical resistance of the source of emf is assumed to be negligibly small compared to $R$ (such a source of emf is said
to be \redem{ideal}). From the law of conservation of electrical charge \ssect{14.1.3} it follows that $I = \dv*{q}{t}$. Hence, the \redem{differential equation of forced electrical oscillation in a circuit} can be presented in a form similar to the equation for forced mechanical vibration \ssect{29.2.1}:
\begin{equation*}%
\dv[2]{q}{t} + 2 \beta \dv{q}{t} + \omega_{0}^{2} q = \frac{1}{L} \emf (t).
\end{equation*}
Here $\beta = R/2L$ is the damping factor for free oscillation in a
circuit, and $\omega_{0} = 1/\sqrt{L C}$ is the angular frequency of free undamped oscillation (i.e. at $R = 0$).

\subsection{}\label{29.3.2} If the driving emf $\emf (t)$ varies according to the harmonic law $\emf (t) = \emf_{0}\cos \Omega t$, then at steady-state forced oscillation \ssect{29.2.1}, the capacitor charge varies harmonically at the same angular frequency $\Omega$:
\begin{equation*}%
q = q_{0} \cos(\Omega t+ \varphi_{0}).
\end{equation*}
The amplitude $q_{0}$ and the initial phase $\varphi_{0}$ are determined by the formulas
\begin{align*}%
q_{0} & = \frac{\emf_{0}}{\Omega \sqrt{R^{2} + (\Omega L - 1/\Omega C)^{2}}} = \frac{\emf_{0}}{L \sqrt{(\omega_{0}^{2} - \Omega^{2})^{2} + 4 \beta^{2}\Omega^{2}}} \qand \\
\tan \varphi_{0} & = \frac{R}{\Omega L - 1/\Omega C} = - \frac{2\beta \Omega}{\omega_{0}^{2} - \Omega^{2}}
\end{align*}
At $\Omega = 0$, the phase $\varphi_{0}(0) = 0$ and $q_{0}(0) = \emf_{0}C$ which is the charge of the capacitor at a constant potential difference between the plates equal to $\emf_{0}$. At $\Omega \to \infty$, the amplitude $q_{0}\to 0$, and $\varphi_{0} \to - \pi$. The curves showing the dependence of $\varphi_{0}$ on $\Omega$ are shown in \figr{fig-29-04}, and those of the dependence of $q_{0}$ on $\Omega$, in \figr{fig-29-03}, where $A= q_{0}$ and $A_{0}= q_{0}(0)= \emf_{0}C$.

\subsection{}\label{29.3.3} With steady-state forced oscillation in the circuit, the current is
\begin{equation*}%
I = \dv{q}{t} = - q_{0} \Omega \sin ( \Omega t + \varphi_{0}) = I_{0}\cos (\Omega t - \varphi).
\end{equation*}
The amplitude of the current $I_{0}= q_{0}\Omega$ and the initial phase $-\varphi = \varphi_{0}+ \pi/2$ are determined by the formulas:
\begin{align*}%
I_{0} & = \frac{\emf_{0}}{\sqrt{R^{2} + (\Omega L - 1/\Omega C)^{2}}}\qand \\
\tan \varphi & = \frac{\Omega L - 1/\Omega C}{R}.
\end{align*}
Curves showing the dependence of $I_{0}$ on $\Omega$ for various values of $R$ are called resonance curves of the oscillatory circuit and are shown in \figr{fig-29-06}; curves of the dependence of $\varphi$ on $\Omega$ are shown in \figr{fig-29-07}. The resonance angular frequency $\Omega_{r}$, corresponding to the maximum amplitude of the current in the circuit in forced oscillation, is independent of $R$. Thus
\begin{equation*}%
\Omega_{r} = \omega_{0} = \frac{1}{\sqrt{LC}}.
\end{equation*}
The current amplitude at resonance is $I_{0}(\Omega_{r}) = \emf_{0}/R$, and the phase difference between the current and emf is $\varphi (\Omega_{r}) = 0$. When $\Omega < \omega_{0}$, $\varphi < 0$, i.e. the current leads the emf in phase; the lower the frequency $\Omega$, the greater this phase lead ($\varphi = -\pi/2$ at  $\Omega = 0$). When $\Omega > \varphi_{0}$, ($ \varphi > 0$, i.e. the current lags in phase behind the emf; the higher the frequency $\Omega$, the greater this phase lag ($\varphi \to \pi/2$ as $\Omega \to \infty$).

\subsection{}\label{29.3.4} The potential difference across the terminals $a$ and $b$ of an ideal source of harmonic emf (\figr{fig-29-05}) is equal to its emf. Thus
\begin{equation*}%
u = V_{a} - V_{b} = \emf_{0} \cos \Omega t.
\end{equation*}
The potential drops across various portions of the circuit illustrated in \figr{fig-29-05} and having the variable sinusoidal (harmonic) current $I = I_{0}\cos (\Omega t - \varphi)$, a capacitor of capacitance $C$, resistor of resistance $R$ and a coil of inductance $L$ are as follows:
\begin{align*}%
u_{C} & = V_{2} - V_{1} = \frac{q}{C} = U_{C} \cos \left( \Omega t - \varphi - \frac{\pi}{2} \right), \\ 
u_{R} & = IR =  U_{R} \cos ( \Omega t - \varphi), \\ 
u_{L} & = L \dv{I}{t} = U_{L} \cos \left( \Omega t - \varphi + \frac{\pi}{2} \right).
\end{align*}
The variation of $u_{R}$ is in phase with that of current $I$ in the circuit, $u_{L}$ leads the current in phase by $\pi/2$, and $u_{C}$ lags behind the current in phase by $\pi/2$. Here
\begin{equation*}%
u_{C} + u_{R} + u_{L} = u =  \emf_{0} \cos \Omega t.
\end{equation*}

\subsection{}\label{29.3.5} The amplitude values of $u_{C}$ , $u_{L}$ and $u_{R}$ are equal, respectively, to
\begin{equation*}%
U_{C} = X_{C}I_{0}, \quad U_{L}= X_{L}I_{0} \qand U_{R}  =RI_{0}.
\end{equation*}
where $X_{C} = 1/\Omega C$ is the \redem{capacitive reactance} of the circuit, and $X_{L} = \Omega L$ is the \redem{inductive reactance} of the circuit. The quantity $X = X_{L} - X_{C} = \Omega L - 1/\Omega C$ is called the \redem{reactance of the circuit}, $R$ is called the \redem{resistance} and $Z = \sqrt{ R^{2} + (\Omega L - 1/\Omega C)^{2}}$ is called its \redem{impedance}.

The formulas in \ssect{29.3.3} for the amplitude of the sinusoidal current in the circuit and its initial phase can be rewritten in the form: 
\begin{equation*}%
I_{0} = \frac{\emf_{0}}{Z} \qand \tan \varphi = \frac{X}{R},
\end{equation*}
with $\cos \varphi = R/Z$ and $\sin \varphi = X/Z$. 

At resonance $\Omega = 1/\sqrt{LC}$ and $X_{L} = X_{C}$,so that the reactance of the circuit becomes zero and the impedance reaches its minimum value, which is equal to the resistance $R$. Thus
\begin{equation*}%
X (\Omega_{r}) = 0 \qand Z (\Omega_{r}) = Z_{\text{min}} = R.
\end{equation*}
In this case $U_{R} = 0$ and $U_{C} = U_{L} = \sqrt{L/C}\, \emf_{0}/R$.

\subsection{}\label{29.3.6} The \redem{effective value of a periodic current} (and, accordingly, the emf, voltage, etc.) is the root-mean-square value of the current during the time $T$ of its variation. Thus
\begin{equation*}%
I_{\text{eff}} = \sqrt{\frac{1}{T}\int_{0}^{T} I^{2} \, \dd t }
\end{equation*}
For a sinusoidal current and a sinusoidal emf
\begin{equation*}%
I_{\text{eff}} = \frac{I_{0}}{\sqrt{2}} \qand \emf_{\text{eff}}
 = \frac{\emf_{0}}{\sqrt{2}}. 
\end{equation*}
An element of work done by sinusoidal current during the short time $\dd t$ in the circuit illustrated in \figr{fig-29-05} is equal to
\begin{equation*}%
\delta W = I u \, \dd t = I_{0}\emf_{0} \cos (\Omega t - \varphi) \, \cos \Omega t \, \dd t.
\end{equation*}
The \redem{instantaneous power of the current in the circuit} is
\begin{equation*}%
P_{\text{ins}} = \frac{\delta W}{\dd t} - Iu = I_{0}\emf_{0} \cos (\Omega t - \varphi) \, \cos \Omega t.
\end{equation*}
The average value of the instantaneous power during a period is called the \redem{active}, or \redem{true, power} $P$ of the current in the electric circuit. Thus
\begin{align*}%
P & = \frac{1}{T} \int_{0}^{T} P_{\text{ins}} \, \dd t = \frac{1}{2} I_{0}\emf_{0} \cos \varphi \\
& = I_{\text{eff}}\emf_{\text{eff}} \cos \varphi.
\end{align*}
The factor $\cos \varphi$ is called the \redem{power factor}. Since $I_{\text{eff}} = \emf_{\text{eff}}/Z$ and $\cos \varphi = R/Z$,
\begin{equation*}%
P  = \frac{R}{Z^{2}}\emf_{\text{eff}}^{2}.
\end{equation*}
At resonance $Z = R$ and the active power reaches its maximum value
\begin{equation*}%
P  = \frac{\emf_{\text{eff}}^{2}}{R} = \frac{\emf_{0}^{2}}{2R}.
\end{equation*}

