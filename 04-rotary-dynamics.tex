% !TEX root = handbook-physics.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter{Dynamics of Rotary Motion}
\label{ch-04}

\section{Moment of Force and Angular Momentum}
\label{sec-4.1}
\subsection{}\label{4.1.1} The moment of force, or force moment is a concept introduced in mechanics to characterize the external mechanical action exerted on a body that changes its rotary motion. Dis­tinction is made between the moment of force about a fixed point and about a fixed axis.

The \emph{moment of the force $\vb{F}$ about a fixed point $O$} (the \emph{pole}) is the vector quantity $\vb{M}$, equal to the vector product of the radius vector $\vb{r}$, from point $O$ to point $A$ of force application (\figr{fig-04-01}), by
the force vector $\vb{F}$: 
\begin{equation*}%
\vb{M} = \qty[\vb{r\,F}]
\end{equation*}
The magnitude of the moment of force is $M = Fr \sin \alpha = F l$, where $\alpha$ is the angle between vectors $\vb{r}$ and $F$, and $l = r \sin \alpha$ is the length of the perpendic­ular $OB$ (\figr{fig-04-01}) dropped from point $O$ to the line of action of the force. The quantity $l$ is called the \emph{arm of the force} with respect to point $O$. If the point of application of force $\vb{F}$ is displaced along its line of action, the moment $\vb{M}$ of this force about the same fixed point $O$ is not changed. If the line of action of the force passes through point $O$, the moment of the force about point $O$ equals zero.
\begin{figure}[!ht]
%\begin{wrapfigure}{r}{0.5\linewidth}
\centering
\includegraphics[width=0.6\textwidth,angle=-1]{figs/ch-04/fig-04-01.pdf}
%\input{figs/ch-03/fig-03-01}
%  \vspace{-13pt}
\caption{The moment of force $\vb{M}$.}
\label{fig-04-01}
\end{figure}

\subsection{}\label{4.1.2} The \emph{principal}, or \emph{resultant, moment} of a system of forces \emph{about fixed point} $O$ (\emph{pole}) is the vector $\vb{M}$, equal to the geometric sum of the moments about point $O$ of all $n$ forces of the system:
\begin{equation*}%
\vb{M} = \sum_{i=1}^{n} \qty[\vb{r}_{i}\vb{F}_{i}]
\end{equation*}
where $\vb{r}_{i}$ is the radius vector from point $O$ to the point of appli­cation of force $\vb{F}_{i}$.

It follows from Newton’s third law \sect{2.5.1} that the moments of the internal forces of interaction between the particles of a system about pole $O$ are compensated for (cancelled out) pair­ wise: $\qty[\vb{r}_{i}\vb{F}_{ik}] = - \qty[\vb{r}_{k}\vb{F}_{ki}]$. Con­sequently, in calculating the resultant moment of forces, on­ly the \emph{external} forces exerted on the mechanical system being considered need be taken into account.

\subsection{}\label{4.1.3} The \emph{moment of force $\vb{F}$ about a fixed axis $a$} is the scalar quantity $M_{a}$, equal to the pro­jection on this axis of vector $\vb{M}$, which is the moment of force $\vb{F}$ about arbitrary point $O$ of axis $a$.

The value of moment $M_{a}$ is independent of the choice of the position of point $O$ along axis $a$.

\textbf{Note.} Sometimes the moment of a force about fixed axis $a$ is understood to mean the vector quantity $\vb{M}_{a} = M_{a}\vb{i}_{a}$, where $\vb{i}_{a}$ is the unit vector of axis $a$. The vector $\vb{M}_{a}$ is the compo­nent of vector $\vb{M}$ of the moment of force about pole $O$, this component being directed along axis $a$.

If the line of action of the force intersects or is parallel to axis $a$, the moment of force about this axis is equal to zero.

Let $A$ be the point of application of force $\vb{F}$, and $O_{1}$ be the foot
of the perpendicular dropped from point $A$ to the axis $OZ$ being considered (\figr{fig-04-02}). It proves convenient to resolve force $\vb{F}$ into three mutually perpendicular components: axial compo­nent $F_{z}$, parallel to axis $OZ$; radial component $\vb{F}_{n}$, directed along vector $\pmb{\rho} = \vec{O_{1}A}$, and tangential component $\vb{F}_{\tau}$ directed per­pendicular to the axis $a$ and perpendicular to vector $\pmb{\rho}$. 
\begin{figure}[!ht]
%\begin{wrapfigure}{r}{0.5\linewidth}
\centering
\includegraphics[width=0.5\textwidth,angle=-1]{figs/ch-04/fig-04-02.pdf}
%\input{figs/ch-03/fig-03-01}
%  \vspace{-13pt}
\caption{Resolving the components of the moment of force $\vb{M}$.}
\label{fig-04-02}
\end{figure}

The moment of force $\vb{F}$ about axis $OZ$ is
\begin{equation*}%
M_{z} = \qty[\pmb{\rho}\vb{F}_{\tau}]_{z}\,\, \textrm{and}\,\, \vb{M}_{z}=\qty[\pmb{\rho}\vb{F}_{\tau}]
\end{equation*}
Since vectors $\pmb{\rho}$ and $\vb{F}_{\tau}$ are perpendicular to each other
\begin{equation*}%
\abs{\vb{M}_{z}}= \abs{M_{z}} = \rho \abs{\vb{F}_{\tau}}
\end{equation*}
The \emph{principal}, or \emph{resultant, moment} of a system of forces \emph{about a fixed axis $a$} is equal to the algebraic sum of the moments of all the forces of the system about this axis.

\subsection{}\label{4.1.4} The \emph{angular momentum}, or \emph{moment of momentum, of a particle about fixed point $O$} (\emph{pole}) is the vector $\vb{L}$. It is equal to the vector product of radius vector $\vb{r}$, from point $O$ to the location of the particle, by vector $\vb{p}$ of its momentum:
\begin{equation*}%
\vb{L} = \qty[\vb{r\,p}] = \qty[\vb{r}m\vb{v}],
\end{equation*}
where $m$ and $\vb{v}$ are the mass and velocity of the particle.

The \emph{angular momentum of a system about fixed point $O$} is denoted by vector $\vb{L}$ and equals the vector sum of the angular momenta of all the particles of the system about the same point $O$:
\begin{equation*}%
\vb{L} = \sum_{i=1}^{n}\qty[\vb{r}_{i}\,\vb{p}_{i}] = \sum_{i=1}^{n}\qty[\vb{r}_{i}m_{i}\vb{v}_{i}],
\end{equation*}
where $m_{i}$, $\vb{r}_{i}$ and $\vb{v}_{i}$ are the mass, radius vector and velocity of the $i$-th particle, and $n$ is the total number of particles.

The \emph{angular momentum of a system about fixed axis $a$} is the scalar quantity $L_{a}$, equal to the projection on axis $a$ of vector $\vb{L}$, the angular momentum of the system about some point $O$ lying on this axis:
\begin{equation*}%
L_{a} = \sum_{i=1}^{n} \qty[\vb{r}_{i}m_{i}\vb{v}_{i}]_{a}
\end{equation*}
The numerical value $L_{a}$ of the angular momentum is indepen­dent of the choice of the position of point $O$ along axis $a$.

\textbf{Note.} Sometimes the angular momentum of a system about a fixed axis $a$ is understood to mean the vector quantity $\vb{L}_{a}  = L_{a}\vb{i}_{a}$ where $\vb{i}_{a}$ is the unit vector of axis $a$.

\subsection{}\label{4.1.5} The \emph{angular momentum of a body rotating about a fixed point O} at the angular velocity is equal to:
\begin{equation*}%
\vb{L} = \int_{(m)} \qty[\vb{r}\vb{v}] \dd m = \int_{(m)} \qty[\vb{r}[\pmb{\omega}\vb{r}]] \dd m
\end{equation*}
where $\vb{r}$ is the radius vector from point $O$ to an infinitely small (dement of the body of mass $\dd m$, and $\vb{v} = \qty[\pmb{\omega}\vb{r}]$ is the velocity of the element of the body. Since $\qty[\vb{r} \qty[\pmb{\omega}\vb{r}]] = r^{2}\pmb{\omega} - (\pmb{\omega}\vb{r}) \vb{r}$, vectors $\vb{L}$ and $\pmb{\omega}$ do not coincide in direction in the general case:
\begin{equation*}%
\vb{L} = \pmb{\omega} \int_{(m)} r^{2} \dd m - \int_{(m)} (\pmb{\omega}\vb{r}) \vb{r} \dd m
\end{equation*}
The angular momentum of a body hinged at point $O$ and its angular velocity coincide in direction if the body rotates about one of its principal axes of inertia \ssect{4.2.4} passing through point $O$
\begin{equation*}%
\vb{L} = \vb{I} \pmb{\omega}
\end{equation*}
where $\vb{I}$ is the moment of inertia \ssect{4.2.1} of the body about this principal axis.
\subsection{}\label{4.1.6} The resultant moments $\vb{M}$ and $\vb{M}^{*}$ of a system of forces about two different fixed points $O$ and $O^{*}$ are related by the equation
\begin{equation*}%
\vb{M} = \vb{M}^{*} + \qty[\vb{r}^{*}\vb{F}]
\end{equation*}
where $\vb{r}^{*}$ is the radius vector from origin $O$ to point $O^{*}$, and $\vb{F}$ is the principal vector of the system of forces being considered. If $\vb{F} = 0$ the resultant moment of the system of forces is the same about any fixed point: $\vb{M}^{*} = \vb{M}$. This, precisely, is the property possessed by a \emph{force couple}, or simply \emph{couple}, i.e. a system of two forces, equal in magnitude and directed along two parallel straight lines in opposite directions. The shortest (perpendicular) distance d between the lines of action of the forces is called the \emph{arm of the couple}. The moment, or torque, of a force couple is directed perpendicular to the plane of the forces, and its magnitude is equal to $M = Fd$, where $F$ is the magnitude of each of the forces of the couple.

The resultant moment $\vb{M}_{C}$ of all the forces acting on a mechan­ical system about the centre of mass \ssect{2.3.3} of this system is related to the resultant moment $\vb{M}$ of the same system of forces about fixed point $O$ by the equation
\begin{equation*}%
\vb{M} = \vb{M}_{C} + \qty[\vb{r}_{C}\vb{F}]
\end{equation*}
where $\vb{r}_{C}$ is a radius vector from origin $O$ to point $C$, and $\vb{F}$ is the principal vector of the system of forces.

\subsection{}\label{4.1.7} The value of the angular momentum of a mechanical system about its centre of mass $C$ is the same for the absolute motion of the particles at the velocities (i.e. with respect to a fixed inertial frame of reference) and for the relative motion at the velocities $\vb{v}_{i}' = \vb{v}_{i} - \vb{v}_{C}$ (i.e. with respect to a frame of reference in translational motion with the origin of coordinates at point $C$):
\begin{equation*}%
\sum_{i=1}^{n} \qty[\vb{r}_{i}m_{i} \vb{v}_{i}] = \sum_{i=1}^{n} \qty[\vb{r}_{i}'m_{i} \vb{v}_{i}'] = \vb{L}_{C}
\end{equation*}
where $\vb{r}_{i}' = - \vb{r}_{C}$ is the radius vector of the $i$-th particle in a frame of reference travelling together with the centre of mass.

The relation between the values of the angular momentum $\vb{L}$ of a mechanical system about fixed point $O$ and $\vb{L}_{C}$ about the system’s centre of mass is of the form
\begin{equation*}%
\vb{L} = \vb{L}_{C} + \qty[\vb{r}_{C}\vb{p}]
\end{equation*}
where $\vb{p} = \sum_{i=1}^{n} m_{i}\vb{v}_{i}$ is the momentum of the system in its 
absolute motion.


\section{Moment of Inertia}
\label{sec-4.2}

\subsection{}\label{4.2.1} The \emph{moment of inertia of a mechanical system} about fixed axis $a$ is the physical quantity denoted by $I_{a}$ and equal to the sum of the products of the masses of all $n$ particles of the system by the squares of their normal distances from the axis:
\begin{equation*}%
I_{a} =  \sum_{i=1}^{n} m_{i} \rho_{i}^{2}
\end{equation*}
where $m_{i}$ and $\rho_{i}$ are the mass of the $i$-th particle and its normal distance from the axis.

The moment of inertia of a body about fixed axis $a$ is
\begin{equation*}%
I_{a} =  \int_{(m)} \rho^{2} \dd m = \int_{(V)} \rho^{2} D \dd V
\end{equation*}
where $\dd m = D \dd V$ is the mass of an infinitely small element of volume $\dd V$ of the body, $D$ is the density and $\rho$ is the normal distance from the element $\dd V$ to axis $a$.

If the body is homogeneous, i.e. its density is the same through­ out, then
\begin{equation*}%
I_{a} =  D \int_{(V)} \rho^{2} \dd V
\end{equation*}
The moment of inertia $I_{a}$ is a measure of inertness of a body in its rotary motion about fixed axis $a$ \ssect{4.3.4}, in the same way that its mass is a measure of inertness of the body in its transla­tional motion.

\subsection{}\label{4.2.2} The moment of inertia of a given body about some axis depends, not only on the mass, shape and size of the body, but also on the position of the body with respect to this axis. Ac­ cording to \emph{Steiner's theorem of parallel axes}, the moment of inertia $I$ of a body about an arbitrary axis is equal to the sum of its moment of inertia $I_{C}$ about a parallel axis through the centre of mass (or of inertia) of the body plus the product of the mass $m$ of the body by the square of the normal distance d between the two axes:
\begin{equation*}%
I = I_{C} + md^{2}
\end{equation*}

\subsection{}\label{4.2.3} The moments of inertia of certain homogeneous regularly shaped bodies about certain axes are listed in \tabl{4.1-mi-of-body}.

\begin{table}[ht]
\begin{small}
\begin{tabular}{p{3.25cm}p{2.5cm}p{1.75cm}}
\toprule
\hlblue{Body} & \hlblue{Position of axis $a$} & \hlblue{Moment of inertia $I_{a}$}\\
\midrule
Hollow thin-walled cir­cular cylinder of radius $R$ and mass $m$& Axis of the cylinder & $mR^{2}$ \\
Solid circular cylinder (or disk) of radius $R$ and mass $m$& Axis of the cylinder
& $mR^{2}/2$ \\
Solid sphere of radius $R$ and mass $m$ & Axis through the centre of the sphere & $2mR^{2}/5$ \\
Hollow thin-walled sphere of radius $R$ and mass $m$ & Axis through the centre of the sphere& $2mR^{2}/3$\\
Thin straight rod of length $l$ and mass $m$ & Axis perpendicular to the rod and through its centre along its length& $ml^{2}/12$\\
The same rod & Axis perpendicular to the rod and through one end of it& $ml^{2}/3$ \\
\bottomrule
\end{tabular}
\end{small}
\caption{The moments of inertia of certain homogeneous regularly shaped bodies about certain axes.}
\label{4.1-mi-of-body}

\end{table}

\subsection{}\label{4.2.4} The \emph{products of inertia of a body} about the axes of a rec­tangular Cartesian coordinate system are the quantities:
\begin{align*}%
I_{xy} & =  \int_{(m)} xy \dd m = \int_{(V)} xy\, D \dd V \\
I_{xz} & =  \int_{(m)} xz \dd m = \int_{(V)} xz\, D \dd V \\
I_{yz} & =  \int_{(m)} yz \dd m = \int_{(V)} yz\, D \dd V
\end{align*}
where $x$, $y$ and $z$ are the coordinates of an infinitely small element of the body of volume $\dd V$, density $D$ and mass $m$.

Axis $OX$ is called the \emph{principal axis of inertia of the body} if the products of inertia $I_{xy}$ and $I_{xz}$ are simultaneously equal to zero. Three principal axes of inertia can be passed through each point of the body. The axes are mutually perpendicular.

The moments of inertia of a body about the three principal axes of inertia, passed through an arbitrary point $O$ of the body, are called the \emph{principal moments of inertia of the body}.

The principal axes of inertia, passing through the centre of mass of the body, are called the \emph{principal central axes of inertia of the body}, and the moments of inertia about these axes are called the \emph{principal central moments of inertia of the body}. An axis of symmetry of a homogeneous body is always one of its principal central axes of inertia.

\section{The Fundamental Law in the Dynamics of Rotary Motion}
\label{sec-4.3}

\subsection{}\label{4.3.1} It follows from Newton’s laws that the first derivative with respect to time $t$ of the angular momentum $\vb{L}$ of a mechan­ical system about any fixed point 0 is equal to the resultant moment $M^{\textrm{ext}}$ of all the external forces, applied to the system, about the same point $O$:
\begin{equation*}%
\dv{\vb{L}}{t} = M^{\textrm{ext}}
\end{equation*}
This equation expresses the \emph{law of the variation of angular mo­mentum of the system}. It is valid, in particular, for a rigid body, hinged at point $O$ and rotating about it. In this case, the equation expresses the \emph{fundamental law of dynamics of a rigid body rotating about a fixed point}.

In terms of the projections on the axes of a fixed rectangular Cartesian coordinate system with its origin at point $O$, the law for the variation of the angular momentum is written in the form:
\begin{equation*}%
\dv{L_{x}}{t} = M^{\textrm{ext}}_{x}, \,\,\dv{L_{y}}{t} = M^{\textrm{ext}}_{y}, \,\,\dv{L_{z}}{t} = M^{\textrm{ext}}_{z}
\end{equation*}

Here $L_{x}, \,L_{y}, \,L_{z}, \,M^{\textrm{ext}_{x}}, \,M^{\textrm{ext}}_{x}, \,M^{\textrm{ext}}_{y}\,M^{\textrm{ext}}_{z}$ are the angular mo­menta of the system and the resultant moments of the external forces about the corresponding coordinate axes.

\subsection{}\label{4.3.2} \hlblue{Example.} The \emph{regular precession of a gyroscope} due to its gravitational force. A \emph{gyroscope (symmetrical gyroscope)} is a rigid body spinning (rotating rapidly) about its axis of symmetry that can change its position in space. A gyroscope has three degrees of freedom \ssect{1.5.6} if it is mounted at fixed point $O$, which lies on its axis and is called the \emph{centre of suspension of the gyroscope}. 
\begin{figure}[!ht]
%\begin{wrapfigure}{r}{0.5\linewidth}
\centering
\includegraphics[width=0.4\textwidth,angle=-1]{figs/ch-04/fig-04-03.pdf}
%\input{figs/ch-03/fig-03-01}
%  \vspace{-13pt}
\caption{Analysing the rotary motion of a heavy gyroscope or a top.}
\label{fig-04-03}
\end{figure}
If the centre of suspension coincides with the centre of gravity $C$ \ssect{7.3.2} of the gyroscope, the gyroscope is said to be \emph{balanced}: the force of gravity does not affect its state of rotation. Otherwise, it is said to be a \emph{heavy gyroscope}, or top (\figr{fig-04-03}). Owing to the action of the moment of gravitational force about point $O$
\begin{equation*}%
M^{\textrm{ext}} = \qty[\vb{r}_{C}m \vb{g}]
\end{equation*}
a heavy gyroscope rotates about this point so that its axis $OZ'$ rotates uni­formly about vertical axis $OZ$ describ­ing a conical surface (shown by dashed lines in \figr{fig-04-03}). Such motion of the gyroscope is called \emph{regular precession}. If the angular velocity of precession $\Omega \ll \omega$ (where $\omega$ is the spin angular velocity or, simply, spin velocity of the gyroscope about its axis of symmetry $OZ'$), it can be assumed with some approximation that the angular momentum $\vb{L}$ of the gyroscope about point $O$ is directed along axis $OZ'$ of the gyroscope and equals
\begin{equation*}%
\vb{L} = I \pmb{\omega}
\end{equation*}
where $I$ is the moment of inertia of the gyroscope about axis $OZ'$. Hence
\begin{equation*}%
\dv{\vb{L}}{t} = \qty[\vb{r}_{C}m\vb{g}] = \qty[\frac{\vb{r}_{C}}{I \omega_{z'}} \vb{L} m\vb{g}] = \qty[\pmb{\Omega}\vb{L}]
\end{equation*}
where $\pmb{\Omega} = - m\vb{r}_{C}g/I\omega_{z'}$, is the angular velocity of precession, and $\omega_{z'} = \omega$ in the case shown in \figr{fig-04-03}. The greater the spin angular velocity of the gyroscope the slower its precession. 

\subsection{}\label{4.3.3} The kinetic energy of a rigid body rotating about a fixed point at the angular velocity $\pmb{\omega}$ is 
\begin{equation*}%
E_{k} = \frac{I \pmb{\omega}^{2}}{2}
\end{equation*}
where $I$ is the moment of inertia of the body about the instan­taneous axis of rotation \ssect{1.5.6}.

An element of work done in the infinitely short time interval $\dd t$ by force $\vb{F}$ acting on the body is
\begin{equation*}%
\delta W = \vb{M} \pmb{\omega} \dd t = \vb{M} \dd \pmb{\varphi}  = M_{\omega} \dd \varphi
\end{equation*}
where $\vb{M} = [\vb{rF}]$ is the moment of force $\vb{F}$ about point $O$ ($\vb{r}$ is the radius vector from point $O$ to the point of application of force $\vb{F}$), $\dd \varphi  = \omega \dd t$ and $\dd \pmb{\varphi} = \pmb{\omega} \dd t$ are the angle of rotation and the vector of an element of rotation of the body in the time interval $\dd t$, and $M_{\omega}$ is the moment of force $\vb{F}$ about an instantaneous axis of rotation ($\vb{M}_{\omega}$ equal to the projection of vector $\vb{M}$ on the direction of vector $\omega$).

The change in the kinetic energy of a rotating rigid body during the time interval $\dd t$ is equal to the work done by the external forces:
\begin{equation*}%
\dd E_{k} = M^{\textrm{ext}}_{\omega} \dd \varphi
\end{equation*}
where is the resultant moment \ssect{4.1.3} of external forces about an instantaneous axis of rotation of the body.

\subsection{}\label{4.3.4} If a rigid body rotates about fixed axis $OZ$ at the angular velocity $\omega$, its angular momentum about this axis is
\begin{equation*}%
L_{z} = I_{z} \omega_{z}\,\, \textrm{and}\,\, \vb{L}_{z} = I_{z}\pmb{\omega}
\end{equation*}
Here $I_{z}$ is the moment of inertia of the body about axis $OZ$ and does not change with time $(I_{z}\,= \,\textrm{const})$, and $\abs{\omega_{z}} = \omega > 0$ ($\omega_{z} = \omega$ if vector $\pmb{\omega}$ and the unit vector of axis $OZ$ coincide in direction, otherwise $\omega_{z} = - \omega$).

The \emph{fundamental law in the dynamics of a rigid body rotating about fixed axis $OZ$ is}
\begin{equation*}%
I_{z} = \dv{\pmb{\omega}}{t} =  \vb{M}^{\textrm{ext}}_{z} \,\, \textrm{or} \,\, \pmb{\varepsilon} = \frac{1}{I_{z}} \vb{M}^{\textrm{ext}}_{z}
\end{equation*}
where $\pmb{\varepsilon} = \dv*{\pmb{\omega}}{t}$ is the angular acceleration of the body.

It is evident from the preceding equation that the moment of inertia of a rigid body about some fixed axis is a measure of inertness of the body in its rotation about the given axis: the greater the moment of inertia of the body, the less the angular acceleration it acquires under the action of the same moment of
external forces.

\subsection{}\label{4.3.5} The kinetic energy of a rigid body rotating about fixed axis $OZ$ at the angular velocity $\omega$ is
\begin{equation*}%
E_{k} =  \frac{1}{2} I_{z} \omega^{2}
\end{equation*}
An element of work done during the infinitely short time $\dd t$ by force $\vb{F}$ applied to the body is
\begin{equation*}%
\delta W = \vb{M}_{z} \pmb{\omega} \dd t = M_{z} \dd \varphi
\end{equation*}
where $M_{z}$ is the moment of force $\vb{F}$ about the axis $OZ$ of rota­tion (the unit vector of axis $OZ$ coincides in direction with vector $\pmb{\omega}$).

The change in the kinetic energy of a rigid body during the time interval $\dd t$ is equal to the work done by the external forces during this interval:
\begin{equation*}%
\dd E_{k} = M^{\textrm{ext}}_{z} \dd \varphi
\end{equation*}
where $M^{\textrm{ext}}_{z}$ is the resultant moment of external forces about the axis of rotation of the body.

\subsection{}\label{4.3.6} The motion of a free rigid body satisfies the following two differential equations:
\begin{equation*}%
\dv{t}(m\vb{v}_{C}) = \vb{F}^{\textrm{ext}} \,\, \textrm{and}\,\,\dv{\vb{L}_{C}}{t} = \vb{M}^{\textrm{ext}}_{C}
\end{equation*}
Here $m$ is the mass of the body, $\vb{v}_{C}$ is the velocity of its centre of mass $C$, $ \vb{F}^{\textrm{ext}}$ is the principal vector of external forces applied
to the body \ssect{2.5.2}, $\vb{M}^{\textrm{ext}}_{C}$ is the resultant moment of external forces about point $C$ \ssect{4.1.6}, and $\vb{L}_{C}$ is the angular momentum about the same point $C$ \ssect{4.1.7}.

The first equation describes translational motion of the free body at the velocity of its centre of mass \ssect{2.5.3}. The second equation follows from the law of the variation of angular momentum \ssect{4.3.1} and describes the rotation of a rigid body about its centre of mass \ssect{1.5.9}.

\subsection{}\label{4.3.7} The kinetic energy of a free rigid body can be deter­ mined on the basis of Koenig’s theorem \ssect{3.2.4}:
\begin{equation*}%
E_{k} = \frac{mv_{C}^{2}}{2} + \frac{I_{C}\omega^{2}}{2}
\end{equation*}
where $I_{C}$ is the moment of inertia of the body about an instan­taneous axis of rotation passing through its centre of mass $C$, and $\omega$ is the angular velocity of the body. In the general case, the instantaneous axis is displaced in the body and the moment of inertia $I_{C}$ varies with time. The value of $I_{C}$ is constant if the body is in plane motion \ssect{1.5.9}.

\hlblue{Example.} The kinetic energy of a homogeneous circular cylinder rolling down an inclined plane without slipping. The cylinder has plane motion because all of its points travel in vertical planes that are parallel to one another. The cylinder travels with translational motion at the velocity $\vb{v}_{C}$ directed along the inclined plane, and rotates about its axis ($I_{C} = mR^{2}/2$, where $m$ and $R$ are the mass and radius of the cylinder) at the angular velocity $\omega$. From the condition that the cylinder rolls without slipping it follows that the instantaneous veloci­ties of the points of contact between the cylinder and inclined plane equal zero, i.e. $\omega = v_{C}/R$. Hence, the kinetic energy of the rolling cylinder is
\begin{equation*}%
E_{k} = \frac{mv_{C}^{2}}{2} + \frac{I_{C}\omega^{2}}{2} = \frac{3}{4}mv_{C}^{2}.
\end{equation*}


\section{Law of Conservation of Angular Momentum}
\label{sec-4.4}

\subsection{}\label{4.4.1} The \emph{law of the conservation of angular momentum} can be formulated as follows: the angular momentum of a closed system \ssect{2.2.4} about any fixed point remains constant with time, i.e.
\begin{equation*}%
\dv{\vb{L}}{t} \equiv 0 \,\, \textrm{and} \,\, \vb{L} = \textrm{const}
\end{equation*}
Accordingly, the angular momentum of a closed system about its centre of mass \ssect{4.1.7} remains constant with time:
\begin{equation*}%
\dv{\vb{L_{C}}}{t} \equiv 0 \,\, \textrm{and} \,\, \vb{L_{C}} = \textrm{const}
\end{equation*}
Like the laws of conservation of momentum and energy, the law of conservation of angular momentum is of significance far beyond the scope of classical mechanics. It belongs to the most Fundamental of physical laws because it is associated with a definite symmetry property of space: its isotropy. The \emph{isotropy of space} is manifested in the fact that the physical properties and laws of motion of a closed system are independent of the direction chosen for the coordinate axes in an inertial frame of reference. This means that these properties and laws do not change when the closed system is turned as a whole through any angle in space.

According to modern concepts, not only particles and bodies can have an angular momentum, hut fields as well. Elementary particles and systems built from them (for instance, atomic nuclei) can have an angular momentum that is not related to their motion in space and is called their spin (\tabl{43.1-ele-par-ang-mom}). 

\subsection{}\label{4.4.2} With respect to systems that can be described by classical (Newtonian) mechanics, the law of conservation of angular momentum can be regarded as a consequence of Newton’s laws. For a closed mechanical system, the resultant moment of exter­nal forces about any fixed point (as well as about the centre of mass of the system) is identically zero: $\vb{M}^{\textrm{ext}} \equiv 0$ (consequently, $\vb{M}^{\textrm{ext}}_{C} \equiv 0$, see \ssect{4.1.6}, where $\vb{F} = \vb{F}^{\textrm{ext}} \equiv 0$) and the law of conservation of angular momentum follows from \ssect{4.3.1}:
\begin{equation*}%
\vb{L} = \sum_{i=1}^{n} \qty[\vb{r}_{i} m_{i} \vb{v}_{i}] = \textrm{const}
\end{equation*}
where $m_{i},\vb{r}_{i}$ and $\vb{v}_{i}$ are the mass, radius vector and velocity of the $i$-th particle of the system, which consists of $n$ such particles. 

Accordingly (see \ssect{4.1.7} and \ssect{2.5.3}),
\begin{equation*}%
\vb{L}_{C} = \sum_{i=1}^{n} \qty[\vb{r}_{i}' m_{i} \vb{v}_{i}'] = \sum_{i=1}^{n} \qty[\vb{r}_{i} m_{i} \vb{v}_{i}] =\textrm{const}
\end{equation*}
where $\vb{r}_{i}' = \vb{r}_{i} - \vb{r}_{C}$, and $\vb{v}_{i}' = \vb{v}_{i} - \vb{v}_{C}$ and $\vb{r}_{C},\,\vb{v}_{C}$ are the radius vector and velocity of the system’s centre of mass.

\subsection{}\label{4.4.3} If a system is not closed, but the external forces acting on it are such that their resultant moment about fixed point $O$ is identically zero $ (\vb{M}^{\textrm{ext}} \equiv 0)$, then, according to Newton’s laws \ssect{4.3.1}, the angular momentum of the system about the same point $O$ remains constant with time: $\vb{L} = \textrm{const}$. This condition is practically complied with, for example, by a bal­anced gyroscope \ssect{4.3.2} with three degrees of freedom, if the moment of the friction force in its suspension is sufficiently small.

If the stand of the gyroscope, holding its centre of suspension at rest, is turned in any way, the axis of the gyroscope retains its orientation with respect to a fixed inertial frame of reference.\footnote{It is assumed that vector $\vb{L}$ is directed along the axis of the gyroscope. Otherwise, a free gyroscope has a precessional motion: its axis describes a circular conical surface whose vertex is at the centre of suspension and whose axis is directed along the vector $\vb{L} = \textrm{const}$.}

Usually $\vb{M}^{\textrm{ext}} \nequiv 0$ and $\vb{L} \neq \textrm{const}$. But if the resultant moment of external forces about any fixed axis passing through point $O$ is identically zero, the angular momentum of the system about
this axis remains constant with time. For instance, if $M^{\textrm{ext}}_{z}\equiv 0$, then $L_{z} = \textrm{const}$.

If the system rotates about fixed axis $OZ$ and the resultant moment of external forces about this axis $M^{\textrm{ext}}_{z} \equiv 0$, the angular momentum of the system about the axis of rotation remains constant with time:
\begin{equation*}%
I_{z} \omega = \textrm{const}
\end{equation*}
where $\omega$ and $I_{z}$ are the angular velocity and moment of inertia of the system.

If the action of internal forces, as well as external forces that comply with the condition $M^{\textrm{ext}}_{z}\equiv 0$, leads to deformation of the system, changing its moment of inertia $I_{z}$, the angular velocity $\omega$ correspondingly increases or decreases.

\subsection{}\label{4.4.4} \emph{Free axes of a body} are ones about which a free rigid body \ssect{2.2.3} can rotate at constant angular velocity $\pmb{\omega}$ in the absence of all external action. Such rotation of a body is said to be \emph{free}, or \emph{inertial rotation}. The free axes of a body coincide with its
principal central axes of inertia \ssect{4.2.4}. In the general case, the values $I_{1}$, $I_{2}$ and $I_{3}$ of the principal central moments of inertia \ssect{4.2.4} differ. The free rotation of such a body (for ex­ ample, a homogeneous rectangular parallelepiped with edges of different length) is accomplished practically only about two free axes, corresponding to the extreme values of the principal central moments of inertia -- the maximum and minimum val­ues. The rotation of the body about its third principal central axis, corresponding to the intermediate value of the moment of inertia of the body, proves to be unstable: even very small external action is capable of leading to substantial deviation of the instantaneous axis of rotation from its initial direction in the body.

If the values of two principal central moments of inertia of a body are equal: $I_{1} = I_{2} \neq I_{3} $, stable free rotation of such a body (for instance, a homogeneous circular cylinder) is possi­ble only about the free axis corresponding to the third, differing value $I_{3}$ of the moment of inertia. For a homogeneous circular cylinder such a free axis is its axis of symmetry. But if a long thin cylinder is rotated by means of a string secured to one end, stable rotation of the cylinder can be accomplished about the free axis corresponding to the maximum value of its moment of inertia. This free axis is perpendicular to the axis of symmetry
of the cylinder.


%\begin{equation*}%
%\vb{F}= \sum_{i=1}^{n} \vb{F_{i}}
%\end{equation*}

%\end{wrapfigure}

%\begin{align*}%
%m\vb{a} = \vb{F}\,\,\textrm{and}\,\, \vb{F}_{ki} = - \vb{F}_{ik}\,\, \intertext{(in reference frame $K$)}\\
%m'\vb{a}' = \vb{F}'\,\,\textrm{and}\,\, \vb{F}_{ki}' = - \vb{F}_{ik}'\,\, \intertext{(in reference frame $K'$)}
%\end{align*}
