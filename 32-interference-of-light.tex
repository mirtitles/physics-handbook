% !TEX root = handbook-physics.tex
% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode






\chapter{Interference of Light}
\label{ch-32}

\section{Monochromaticity and Time Coherence of Light}
\label{sec-32.1} 

\subsection{}\label{32.1.1} \redem{Optics} is the branch of physics concerned with the study of the nature of light \ssect{31.4.3}, the laws of its production, propagation
and interaction with matter. Wave optics deals with optical
phenomena in which the wave nature of light is manifested
(for instance, interference, diffraction, polarization and dispersion of light). Since light comprises electromagnetic waves, wave optics is based on Maxwell’s equations \ssect{27.4.3} and the relations that follow from them for electromagnetic waves (Section \ref{sec-31.1}).

Classical wave optics deals with media that are linear with
respect to their optical properties \ssect{30.1.7}, i.e. media in which the dielectric constant er and the magnetic permeability $\mu_{r}$ are independent of the intensity of the light \ssect{31.2.4}. Consequently, the principle of superposition \ssect{30.4.1} is valid in wave optics.

Phenomena that are observed in the propagation of light in
nonlinear media are investigated in \redem{nonlinear optics}. Nonlinear optical effects become appreciable at the very high intensities of light radiated by powerful lasers \ssect{39.6.8}.

\subsection{}\label{32.1.2} It has been experimentally established that the effect of light on a photocell, photographic film, fluorescent screen or other devices that record light is determined by the \redem{electric vector} $\vb{E}$ of the electromagnetic field set up by the light wave.
The same conclusion is reached by classical electronic theory,
according to which the processes caused in matter by light are
associated with the effect of the field of the light wave on charged particles of matter, i.e. on the electrons and ions. The frequency of visible and more short-waved light is so high $(\nu \gtrsim \SI{d15}{\hertz})$ that only electrons can execute forced vibration anywhere near appreciable in amplitude. 

The force exerted by the electromagnetic
field \ssect{24.1.5} on an electron is
\begin{equation*}%
\vb{F} = - e (\vb{E} + [\vb{v}_{1}\cross \vb{B}])  = -e (\vb{E} + \mu_{r}\mu_{0} [\vb{v}_{1}\cross \vb{H}]) .
\end{equation*}
Where $-e$ and $\vb{v}_{1}$ are the charge and velocity of the electron, and $\vb{B} = \mu_{r}\mu_{0}\vb{H}$ is the vector of magnetic induction. It follows from \ssect{31.1.5} that the absolute value of the magnetic component
of force $\vb{F}$ is substantially less than its electrical component.

Thus
\begin{equation*}%
\mu_{r}\mu_{0} [\vb{v}_{1}\cross \vb{H}] \leqslant \mu_{r}\mu_{0} {v}_{1}H = \frac{v_{1}}{v} E \ll E \qand \vb{F} \approx -e \vb{E},
\end{equation*}
because the velocity of electromagnetic waves is $v \approx  \SI{d8}{\meter\per\second}$, and the velocity of the electron in the atom upon forced vibrations set up by light is $v_{1} \approx \SI{d6}{\meter\per\second}$.

\subsection{}\label{32.1.3} In superposing light from two nonlaser sources\sidenote{This refers to ordinary light sources (incandescent lamps, discharge lamps, electric arcs, etc.), based on the principle of spontaneous radiation \ssect{39.6.6}} (for instance, two identical gas-discharge lamps), or even from different portions of the same source, no interference \ssect{30.5.2} is observed. Consequently, independent sources of light are incoherent \ssect{30.5.1} and their radiation is non-monochromatic
\ssect{31.1.6}. 

The reason for this resides in the very mechanism by which light is radiated by the atoms (or molecules or ions) of the source. The excited atom radiates during an extremely
short de-excitation time, of the order of $\tau \approx \SI{d-8}{\second}$ \ssect{31.3.4}, after which it returns to its normal (unexcited) state, having expended its surplus energy on radiation. Following a certain period of time the atom may again be excited, obtaining energy externally, and begin to emit radiation again. Such intermittent radiation of light by atoms in the form of separate short impulses -- \redem{wave trains}, or \redem{packets} -- is typical of any light source, regardless of the specific processes that occur in the source and excite its atoms. In spontaneous radiation \ssect{39.6.6}, the atoms emit radiation independently of one another with random initial phases that vary without any order from one event of emission to another. Therefore, spontaneously radiating atoms are incoherent sources of light.


Induced, or stimulated, emission of radiation is quite
another matter. It is produced in a nonequilibrium (active)
medium by the action of a variable electromagnetic field \ssect{39.6.1}. Stimulated emission of radiation by all the particles of a system is coherent with the monochromatic radiation that it is produced by, having the same frequency, polarization and direction of propagation. These features of stimulated emission of radiation are made use of in lasers and masers \ssect{39.8.8}.

\subsection{}\label{32.1.4} A real wave, radiated during a limited period of time and encompassing a limited region of space, is not monochromatic. The spectrum of its angular frequencies \ssect{28.4.7} is of finite width $\Delta \omega$, i.e. it includes angular frequencies from $ \omega - \Delta  \omega/2$ to $ \omega + \Delta  \omega/2$. During the time interval $\Delta t \ll = \tau_{coh} = \pi/ \Delta \omega$
such a wave can be considered as an approximately monochromatic wave with the angular frequency $\omega$. 

The quantity $\tau_{coh}$ is called the \redem{time coherence of a nonmonochromatic wave}. During the time interval $\tau_{coh}$ the phase difference of the vibrations corresponding to waves of the frequencies $ \omega + \Delta  \omega/2$ and $ \omega +-\Delta  \omega/2$ changes by $\pi$. A wave with the angular frequency co and phase velocity $v$ propagates during this time over the distance $l_{coh} =  \tau_{coh} = \pi v/\Delta \omega$.

The quantity $l_{coh}$ is called the \redem{coherence length}, or the \redem{harmonic wave train length}, of the corresponding nonmonochromatic wave being considered. The closer a given wave is to a monochromatic one, the less the width $\Delta \omega$ of its frequency spectrum and the greater its coherence time and coherence length. For example, for visible sunlight, having a continuous frequency spectrum from \num{4d14} to \SI{8d14}{\hertz}, $\tau_{coh} \approx \SI{d-14}{\second}$ and $l_{coh} \approx \SI{d-8}{\meter}$. The coherence time of stimulated radiation is substantially longer than the de-excitation time of the atom \ssect{31.3.4}. For example, $\tau_{coh}$ reaches \SI{d-5}{\second} for a continuous-wave laser and the coherence length $l_{coh} \approx \SI{d3}{\meter}$.

\section{Interference of Light. Spatial Coherence of Light}
\label{sec-32.2} 


\subsection{}\label{32.2.1} To obtain coherent light waves by means of conventional
(nonlaser) light sources, light from a single source is split into two or more systems of waves. Each system represents the
radiation of the same atoms of the source. Hence, by virtue of
their common origin, these systems of waves are coherent with
one another and interfere when superposed. Light can be split
into coherent systems of waves by its reflection or refraction.
\begin{marginfigure}[-3cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-32/fig-32-01.pdf}
\sidecaption{Fresnel mirrors and interference of light.\label{fig-32-01}}
\end{marginfigure}
Illustrated as an example in \figr{fig-32-01} is the principle of the \redem{Fresnel mirrors}. Light from point source S is incident on two plane mirrors $A_{1}O$ and $A_{1}O$ arranged perpendicular to the plane of the drawing and joined along line $O$. The angle $\alpha$ be tween the planes of the mirrors is very small. Light from source $S$ propagates after being reflected as two beams having their centres at points $S_{1}$ and $S_{2}$, which are the virtual images of source S in the mirrors. These beams are coherent and upon being superposed produce an interference pattern on screen $E$ (the region $BC$ is sometimes called the \redem{field of interference}). The result of the interference at some point $M$ on the screen depends upon the wavelength $\lambda$ of the light and the path difference of the waves \ssect{30.5.2} from
the coherent virtual sources and $S_{2}$ to point $M$. 

Thus,
\begin{equation*}%
\Delta = r_{2} - r_{1} = \overline{MS_{2}} - \overline{MS_{1}}.
\end{equation*}
The initial phases of vibration of sources $S_{1}$ and $S_{2}$ are the same. Hence, the \redem{conditions for interference maxima and minima} \ssect{30.5.2} are of the form
\begin{equation*}%
 r_{2} - r_{1} = \pm m\,\lambda,
\end{equation*}
for a maximum of the $m$-th order $(m = 0, \,1,\, 2, \dots)$,
\begin{equation*}%
 r_{2} - r_{1} = \pm \, (2m - 1) \frac{\lambda}{2},
\end{equation*}
for a minimum of the $m$-th order $(m = 0, \,1,\, 2, \dots)$.

The angle $2\omega$ at apex $S$ between the two rays of light that converge at point $M$ of the interference pattern after being reflected from mirrors $A_{1}O$ and $A_{2}O$ is called the \redem{aperture angle of the interference}. This angle usually varies very little upon changing the position of point $M$ within the limits of the field of interference.
\begin{marginfigure}[-3cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-32/fig-32-02.pdf}
\sidecaption{the Fresnel biprism.\label{fig-32-02}}
\end{marginfigure}
\subsection{}\label{32.2.2} The principles of the techniques used for observing interference by means of the \redem{Fresnel biprism} (\figr{fig-32-02}) and the \redem{Billet split lens} ( \figr{fig-32-03}) are similar to those of the Fresnel mirrors. The biprism consists of two identical triangular prisms joined together at their bases and prepared as a whole. The refracting angles a at the upper and lower edges of the biprism are very small (of the order of a fraction of one degree). 
\begin{marginfigure}%[-3cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-32/fig-32-03.pdf}
\sidecaption{Billet split lens.\label{fig-32-03}}
\end{marginfigure}
Light from source $S$ is refracted in the biprism and propagates in it in the form of two systems of waves, corresponding to the coherent virtual sources of light $S_{1}$ and $S_{2}$. The interference of these waves is observed in the region of their overlapping on screen $E$.

The Billet split lens consists of two halves $L_{1}$ and $L_{2}$ of a collecting lens, cut along a diameter. The two halves are slightly displaced laterally (spread apart). In consequence, they produce two noncoinciding real images $S_{1}$ and $S_{2}$ of the point source of light $S$. The interference of light from these coherent secondary sources is observed on screen $E$. The space between parts $L_{1}$ and $L_{2}$ of the split lens is covered by opaque screen $A$.

The value of the aperture angle $2\omega$ of interference is shown in \figr{fig-32-02} and \figr{fig-32-03} for the central point $M_{0}$ of the interference pattern obtained by means of a biprism and a split lens.

\subsection{}\label{32.2.3} The \redem{width of an interference fringe} (as the bands are called) is the distance between two adjacent interference maxima (or minima). When the Fresnel mirrors or similar devices (biprisms or split lenses) are employed to obtain interference, the width of an interference fringe \ssect{30.5.2} is $\lambda L/l$. Here $l$ is the distance between sources $S_{1}$ and $S_{2}$, and $L$ is the distance from them to screen $E$. The wavelength of visible light is extremely short $(\lambda \approx \SI{5d-7}{\meter})$. Therefore, to obtain an interference fringe of a width that can be resolved by the eye, it is necessary
to comply with the condition: $l \ll L$. Consequently, the angle $\alpha$ in the Fresnel mirrors and the refracting angles $\alpha$ of the biprism must be very small.

The possibility of observing interference fringes also depends on the \redem{degree of contrast}, i.e. on the degree of different illumination of the screen at the maxima and minima. The illuminance is proportional to the intensity $I$ of the incident light. A quantitative characteristic of the degree of contrast of the interference pattern is the dimensionless quantity called the \redem{visibility of fringes}:
\begin{equation*}%
V = \frac{I_{\text{max}} - I_{\text{min}}}{I_{\text{max}} + I_{\text{min}}},
\end{equation*}
where $I_{\text{max}}$ and $I_{\text{min}}$ are the values of the light intensities in the interference maxima and minima on the screen. The human eye reliably distinguishes fringes if their visibility $V > 0.1$, i.e. when $I_{\text{min}} < 0.82 I_{\text{max}}$.

In superposing two likewise polarized coherent monochromatic
waves, with the amplitudes and intensities $A_{1}$, $I_{1}$, $A_{2}$ and $I_{2}$, the visibility of the fringes is
\begin{equation*}%
V = \frac{2A_{1}A_{2}}{A_{1}^{2}+A_{2}^{2}} = \frac{2 \sqrt{I_{1}I_{2}}}{I_{1} + I_{2}}.
\end{equation*}
The visibility of theiringes is a maximum $(V = 1)$ when $A_{1} = A_{2}$.

\subsection{}\label{32.2.4} In an interference arrangement of the type of the Fresnel mirrors, illuminated by a point source $S$ (\figr{fig-32-01}), the superposed waves are never ideally monochromatic \ssect{32.1.3}. Hence, these waves are only \redem{partly coherent}. They are capable of interference
only under the condition that the vibration, excited by
them at point $M$ being considered on the screen, corresponds to one and the same harmonic train, or packet, of radiation of
source $S$ \ssect{32.1.4} i.e. if
\begin{equation*}%
\abs{r_{2} - r_{1} } < v \tau_{\text{coh}} \qor \abs{r_{2} - r_{1} } < l_{\text{coh}}.
\end{equation*}
Here $r_{2} - r_{1}$ is the path difference of the superposed waves, $v$ is their velocity, $\tau_{\text{coh}}$ and xc0h and $l_{\text{coh}}$ are coherence time and coherence length of light from source $S$ \ssect{32.1.4}. \redem{Partly coherent
vibrations are combined} at point $M$. These vibrations are excited by the same source $S$ at different instants of time, $t$ and $t + \tau$, where $\tau = \abs{ r_{2} - r_{1} }/v$. Therefore, the visibility of the interference pattern in such arrangements essentially depends on the \redem{time-coherence of the vibrations}, which is limited by the mono-chromaticity of the light from source $S$, i.e. its coherence time
Tcoh \ssect{32.1.4}. At $\tau \ll \tau_{\text{coh}}$ the superposed vibrations are practically fully coherent and the visibility of the interference fringes (at equal intensity of the superposed waves) $V \approx 1$. If, however,
$\tau \geqslant \tau_{\text{coh}}$ the superposed vibrations are incoherent and there is no interference $(V = 0)$.

Thus, to observe the interference of light at large path differences $r_{2} - r_{1}$ (and corresponding large values of $\tau$) it is necessary for the light to have a sufficiently long coherence time, i.e. a sufficiently high degree of monochromaticity.

\subsection{}\label{32.2.5} The positions on the screen of all the interference maxima, with the exception of the zero-order maximum, depend upon the wavelength of the light. For two wavelengths $\lambda_{1}$ and $\lambda_{2}$, the higher the order $m$ to be observed, the more their maxima of the $m$-th order are displaced from each other. Therefore, as $m$ is
increased, the visibility of interference fringes, obtained by the illumination of Fresnel mirrors with nonmonochromatic light, is impaired. Fringes, corresponding to different values of $\lambda$, are superposed on one another, and the interference pattern is blurred.


Assume that the wavelength of light is within the limits from $\lambda$ to $\lambda + \Delta \lambda$, and the angular frequency, from $\omega$ to $\omega - \Delta \omega$,
where $\Delta \omega = 2 \pi v \Delta \lambda/ \lambda^{2}$. Then, according to the \redem{Rayleigh criterion}, the interference pattern is still distinguishable up to a maximum of the $m_{0}$ order for light with the wavelength $\lambda + \Delta \lambda$ (where $\Delta \lambda > 0$), which is superposed on the screen on the nearest interference minimum for light of wavelength $\lambda$:
\begin{align*}%
m_{0} (\lambda + \Delta \lambda) & = (2m_{0} +1) \frac{\lambda}{2}, \text{ from which}\\
m_{0} & = \frac{\lambda}{2 \Delta \lambda}.
\end{align*}
Thus, interference can be observed at path differences of the
waves that comply with the condition
\begin{equation*}%
\abs{r_{2} - r_{1}} \leqslant \frac{\lambda^{2}}{2\Delta  \lambda} =\frac{\pi v}{\Delta \omega} = v \tau_{\text{coh}}.
\end{equation*}
This result agrees with the assessment made in \ssect{32.2.4} on the basis of the concept of time coherence of vibrations.

\subsection{}\label{32.2.6} Partly coherent light, of a total intensity $I$, can be dealt with as the combination of two components: the coherent component of intensity $\gamma I$, where $\gamma$ is the \redem{degree of coherence of the
light}, and the incoherent component of the intensity $(1-\gamma)I$.

When partly coherent waves are superposed, only their coherent
components interfere. The incoherent components produce the
uniformly illuminated background of the interference pattern.
Therefore, as the degree of coherence of the light is reduced, the visibility $V$ of the interference fringes decreases. Thus
\begin{equation*}%
V = \frac{2 \sqrt{I_{1}I_{2}}}{I_{1}+ I_{2}} \, \gamma.
\end{equation*}
When the intensities of partly coherent waves are the same
$V = \gamma$.

\subsection{}\label{32.2.7} As a rule, in an interference device with mirrors (or a biprism), a brightly illuminated narrow slit, parallel to the joint $O$ between the mirrors, is used instead of a point source of light. In this case, the interference patterns, obtained on the screen from the various portions along the length of the slit, are displaced with respect to one another inthe direction of the slit $S$. Accordingly, a system of interference fringes, parallel
to joint $O$ of the mirrors, is observed on the screen.

The visibility of the interference fringes is reduced as the width of the slit $S$ is increased. This is due to the fact that the interference fringes that could be obtained on the screen from various narrow slits into which slit $S$ could be conceivably divided would be displaced with respect to one another. An interference pattern in monochromatic light of wavelength $\lambda$ is sharp and distinct if the following approximate condition is complied with:
\begin{equation*}%
b \sin \omega \leqslant  \frac{\lambda}{4},
\end{equation*}
where $b$ is the slit width and $2 \omega$ is the aperture angle of interference \ssect{32.2.1}.
\begin{marginfigure}%[-3cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-32/fig-32-04.pdf}
\sidecaption{Young's double-slit method for interference of light.\label{fig-32-04}}
\end{marginfigure}

\subsection{}\label{32.2.8} The principle of \redem{Young's two-slit interference method} is illustrated in \figr{fig-32-04}. The source of light is a brightly illuminated
narrow slit $S$ in screen $A_{1}$. Light from slit $S$ falls on a second opaque screen $A_{2}$ having two identical narrow slits $S_{1}$ and $S_{2}$ parallel to slit $S$. Two systems of cylindrical waves are propagated in the space beyond screen $A_{2}$ their interference is observed on screen $E$. The visibility of the interference fringes for small path differences is determined mainly by the degree of agreement in the way the vibrations are executed at points of 
slits $S_{1}$ and $S_{2}$. These slits can be considered as the ``sources'' of the waves interfering on the screen.

\subsection{}\label{32.2.9} The coherence of vibrations executed at the same instant of time at various points of a plane $Q$, perpendicular to the direction of wave propagation, is called spatial coherence (as distinguished from time coherence of vibrations, executed at one and the same point, but at different instants of time).

Spatial coherence depends upon the conditions for the radiation and formation of light waves. For instance, a light wave emitted by a point source has complete spatial coherence. In the case of an ideal plane wave, the amplitude and phase of the vibration are the same at all points, of plane $Q$, i.e. there is also complete spatial coherence. Spatial coherence is also maintained over the whole cross section of a light beam emitted by a laser.

In a real wave, emitted by a host of independent atoms of an
extended nonlaser light source, the phase difference of the
vibrations at two points $K_{1}$ and $K_{2}$ of plane $Q$ is a random function of time. The random variations of the phase difference increase with the distance between the points. The \redem{length of spatial coherence} is the distance $l_{sc}$ between points $K_{1}$ and $K_{2}$ of plane $Q$ at which the random changes of phase difference reach a value equal to it. 

If in Young’s two-slit interference arrangement the distance $l$ between slits $S_{1}$ and $S_{2}$ is greater than or equal to $l_{sc}$ the visibility of the interference fringes equals zero. To ensure spatial coherence in the illumination of slits $S_{1}$ and $S_{2}$, the width $b$ of inlet slit $S$ should be sufficiently narrow. Thus
\begin{equation*}%
b < \frac{\lambda d}{l} \qand \theta < \frac{\lambda}{l},
\end{equation*}
where $d$ is the distance between screens $A_{1}$ and $A_{2}$ and $\theta = b/d$ is the angular dimension of the light source, i.e. slit $S$.

The length of spatial coherence $l_{sc} = \lambda/\theta$ increases with the distance from the light source. For example, for a star of diameter $D$, located at the distance $r$, $\theta = D/r$ and $l_{sc} = \lambda r/D$. The area of a circle of radius $l_{sc}$ is called the \redem{dimension of spatial coherence}, and the volume of a right cylinder having such a circle as its base and a height equal to length of the harmonic wave train \ssect{32.1.4} $l_{\text{coh}} = v \tau_{\text{coh}}$, is called the \redem{volume of coherence}.


\section{Interference of Light in Thin Films}
\label{sec-32.3}

\subsection{}\label{32.3.1} Examples of light interference observed under everyday conditions are the iridescent colouring of thin films (soap bubbles, films of petroleum or other oils on the surface of water, transparent
films of oxides on the surfaces of heat-treated metal components, called temper colours, etc.). The formation of partly coherent waves, displaying interference upon superposition, occurs in these cases due to the reflection of light, incident on the film, from its upper and lower surfaces. This interference depends upon the phase difference acquired by the superposed waves in the film and depending upon their optical path difference.

\subsection{}\label{32.3.2} The \redem{optical path}, or \redem{optical distance}, $s$ of light is the product of the geometric path length $l$ travelled by light in the medium
by the refractive index of the medium \ssect{31.5.1}: $s = nl$. The quantity $s$ is equal to the path travelled by light in vacuum during the time it covers the path $l$ in the given medium.

The \redem{optical-path difference} of two waves is the difference in the optical path of these waves: $\Delta s = s_{2} - s_{1}$ (the optical-path difference is frequently denoted by $\Delta$ or $\delta$). An optical-path difference $\Delta s$ corresponds to a change in the phase by $\Delta \Phi = 2 \pi \Delta s/\lambda_{0}$, where $\lambda_{0} = n \lambda$,is the \redem{wavelength of the light in vacuum},
$\lambda$ is the light wavelength in a medium with the refractive index $n$.

Paths of propagation of waves, having the same optical length,
arc said to be \redem{tautochronous}. Light requires equal amounts of time to travel them. In an optical system (microscope, telescope, etc.), for example, all possible paths of the rays of light from some point on the object to the corresponding point on its image are tautochronous.

\subsection{}\label{32.3.3} Assume that a plane monochromatic light wave $\mathit{1}$ is incident at the angle $i$ on a plane-parallel homogeneous plate of thickness $d$ and transparent to light (\figr{fig-32-05}). Owing to the reflection of light from the upper and lower surfaces of the plate,
two plane waves propagate in the directions of the reflected rays $\mathit{1}'$ and $\mathit{1}''$. Their optical-path difference is
\begin{equation*}%
\Delta s = n (\overline{AB} + \overline{BC}) - n_{1} \left(\overline{AD} + \frac{\lambda_{1}}{2} \right) = 2 dn \cos r - \frac{\lambda_{0}}{2}.
\end{equation*}
\begin{marginfigure}[-3cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-32/fig-32-05.pdf}
\sidecaption{Relfection and refraction of light on a plate.\label{fig-32-05}}
\end{marginfigure}
where $n_{1}$ and $n_{2}$ are the refractive indices of the surrounding medium and of the plate, $D$ is the foot of a perpendicular erected on ray $\mathit{1}'$ from point $C$, member $\lambda_{1}/2$ takes into account the phase difference of $\pi$ as light is reflected from point $A$ \ssect{31.5.5},\sidenote[][1cm]{It is assumed that $i < i_{\text{Br}}$ and that $n > n_{1}$. When $n < n_{1}$, a phase difference of $\pi$ occurs upon reflection of light from point $B$ and $\Delta s = 2dn \cos r + \lambda_{0}/2$, i.e. it differs from the value of $\Delta s$ for the case when $n > n_{1}$ by $\lambda_{0}$, has no effect on the result of the interference.}
$r$ is the refractive angle, and $\lambda_{0}$ is the wavelength of the light in vacuum.

The conditions for interference maxima of reflection are
\begin{align*}%
2dn \cos r & = (2m + 1) \frac{\lambda_{0}}{2}, \qor \\
2d \cos r & = (2m + 1) \frac{\lambda}{2},
\end{align*}
where $m = 0,\, 1,\, 2, \dots$ is the order of the interference maximum.

The conditions for interference minima of reflection are
\begin{align*}%
2dn \cos r & = m \lambda_{0}, \qor \\
2d \cos r & = m \lambda,
\end{align*}
where $m = 1,2, \dots{}$ is the order of the interference minimum.

The optical-path difference for the waves passing through the
plate (rays $\mathit{2}'$ and $\mathit{2}''$) is $\Delta s = 2dn \cos r$, i.e. it differs from $\Delta s$ for the reflected light by $\lambda_{0}/2$. Therefore, the maxima of reflection correspond to the minima of transmission of light and vice vcra. If the plate is illuminated with white light, it has additional colouring in the reflected and transmitted light.

The maximum thickness $d$ of the plate at which it is still possible to observe interference fringes is limited by the coherence time $\tau_{\text{coh}}$ of the light \ssect{32.1.4}. Thus
\begin{equation*}%
2dn \cos r < c \tau_{\text{coh}} \qor 2d cos r < l_{\text{coh}},
\end{equation*}
where $l_{\text{coh}} = v \tau_{\text{coh}} = c \tau_{\text{coh}}/n$ is the coherence length.

\subsection{}\label{32.3.4} In the calculation of the optical-path difference of interfering waves in a plate \ssect{32.3.3} only two waves were taken into account. They correspond to the first reflection from the upper and from the lower surfaces of the plate; the possibility of multiple reflection of light was neglected. Such simplification is justified only if the intensity $I_{2}$ of the wave corresponding to the second reflection from the lower surface of the plate is substantianlly less than the intensity $I_{1}$ of the wave corresponding to the first reflection. 


If $R$ is the coefficient of reflection \ssect{31.5.6} from
the upper and lower surfaces of the plate, then $I_{2} = R^{2}I_{1}$. As a rule $R^{2} \ll 1$. At the interface, for instance between air and glass $(n_{21} = 1.5)$, at angles
of incidence $i < \ang{50}$, the coefficient of reflection $R 
< 0.05$. In certain special cases, when $I_{2}$ is comparable
to $I_{1}$ it is necessary to deal with multiwave interference (\ref{sec-32.4}).

\subsection{}\label{32.3.5} In considering the interference of light in thin films, distinction is made between interference fringes of equal inclination (isoclinic fringes) and those of equal thickness (isopachic fringes). \redem{Equal-inclination fringes} are observed when a diverging (or converging) beam of light is incident at various angles $i$ on a thin plane-parallel film. These, for instance, are the conditions that prevail when the film is illuminated by an extended source
or scattered sunlight. Since $d$ and $n$ are the same all over the film, the optical-path difference of the interfering waves varies along the surface of the film only owing to the variation in the angle of incidence $i$. 
\begin{marginfigure}%[-3cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-32/fig-32-06.pdf}
\sidecaption{The conditions for intereference for equal inclination fringes.\label{fig-32-06}}
\end{marginfigure}

The conditions of interference are the same for all rays incident on the surface of the film and reflected from it at the same angle. Consequently, the interference pattern in this case is said to be made up of equal-inclination fringes.
Equal-inclination fringes are observed on screen $E$ arranged in the focal plane of a convergent lens $L$ (\figr{fig-32-06}). With no lens available the interference pattern could only be observed at infinity, at the point of intersection of the parallel rays $\mathit{1}'$ and $\mathit{1}''$, $\mathit{2}'$,  and $\mathit{2}''$ etc. Therefore, equal-inclination fringes are said to be localized at infinity. For their visual observation it would be necessary for the eyes to accommodate to infinity.

\subsection{}\label{32.3.6} \redem{Equal-thickness fringes} are observed in reflecting a parallel or almost parallel beam of light rays ($i$ = const.) from a thin transparent film whose thickness $d$ is not the same at various places. The optical-path difference of the interfering waves varies in going from certain points on the surface of the film to other points with a corresponding change in the film thickness $d$. Hence the conditions of interference are the same for points having
the same values of $d$. This is why the interference pattern being considered is said to he made up of equal-thickness fringes. Equal-thickness fringes are localized close to the surface of the film. Hence, they can he observed
by accommodation of the eyes practically
to the film’s surface.

When light interferes in a thin transparent wedge with a small wedge angle $\alpha$, equal-thickness fringes have the form of straight hands parallel to the edge of the wedge. If the wedge
is illuminated by monochromatic light with the wavelength $\lambda_{0}$ in vacuum, normally incident on the wedge surface
$(i = 0)$, the width of the interference fringes \ssect{32.2.3} is equal to $\lambda_{0}/2n\alpha$, where $n$ is the absolute refractive index of the wedge material.


\subsection{}\label{32.3.7} Equal-thickness fringes having the form of concentric rings, and called \redem{Newton's rings}, are observed in the interference of light in the thin
air clearance between a flat glass plate $A$ and a flat-convex lens $L$ pressed tightly against the plate (\figr{fig-32-07}). The flat surface of the lens is parallel to the plate
surface and light is normally incident to the lens surface. The centre of Newton’s rings coincides with point $O$ of contact between the lens and plate. At a small distance $r$ from point $O$ the optical-path difference of the waves reflected from the upper and lower surfaces of the nir clearance is 
\begin{equation*}%
\Delta s \approx n \frac{r^{2}}{R} + \frac{\lambda_{0}}{2},
\end{equation*}
where $n \approx 1$ (for air), and $R$ is the radius of the convex surface on the lens.
\begin{marginfigure}%[-3cm]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-32/fig-32-07.pdf}
\sidecaption{Newton's rings are fringes of equal thickness.\label{fig-32-07}}
\end{marginfigure}

\textbf{Note.} In calculations concerning Newton’s rings, the wave reflected from the upper (flat) surface of the lens need not be taken into account because the optical-path difference between this wave and the waves reflected from the boundaries of the air clearance is greater than the coherence length for nonlaser light \ssect{32.3.3}.

In reflected monochromatic light with the wavelength in air
$\lambda = \lambda_{0} /n  \approx \lambda_{0}$, the radii of the dark and bright Newton’s rings are
\begin{align*}%
r_{\text{dark}} & = \sqrt{m R \lambda } \qand \\
r_{\text{bright}} & = \sqrt{(2m + 1) \frac{R \lambda }{2}},
\end{align*}
where $m = 0, 1, 2, \dots{}$. The dark spot in the centre of the pattern corresponds to the change in the phase of the wave by $\pi$ when it is reflected from the lower surface of the air clearance.

If white light is incident on the lens, a central dark spot
is observed in the reflected light, surrounded by a system of coloured rings. These rings correspond to the interference maxima of the reflection of light with various values
of wavelength $\lambda$.

\section{Multiwave Interference}
\label{sec-32.4}


\subsection{}\label{32.4.1} Special interference instruments,
such as the diffraction grating \ssect{33.3.4}, the Fabry-Perot etalon and others are used to obtain interference of many light waves with close or equal amplitudes. The amplitude $A$ of the resultant vibration and its intensity $I = A^{2}$ at arbitrary point $M$ can be found by applying the method of vector diagrams for adding oscillations or vibrations
having the same direction \ssect{28.4.2}.

\begin{marginfigure}%[-3cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-32/fig-32-08.pdf}
\sidecaption{Analysing interferene of multiple waves.\label{fig-32-08}}
\end{marginfigure}

\subsection{}\label{32.4.2} Shown in \figr{fig-32-08} is a vector diagram of the addition of vibrations in interference of $N$ waves. At the point $M$ being considered, these vibrations set up coherent vibrations in the same direction with equal amplitudes $A_{i} = A_{1}$ and a phase difference, independent of $i$, between $(i + 1)$-th and $i$-th vibrations. Thus $\Phi_{i+1} (t) - \Phi_{i} (t) = \Delta \varphi_{0}$. The amplitude of the resultant vibration is
\begin{equation*}
A = 2 \times \seg{OO}_{1} \abs{\sin \frac{\alpha}{2}},
\end{equation*}
where 
\[
\alpha = 2\pi - N \Delta \varphi_{0}, \qand \seg{OO}_{1} = \frac{A_{1}}{2 \abs{\sin \dfrac{\Delta \varphi_{0}}{2}}}.
\]
Hence,
\begin{equation*}
A = A_{1} \abs{\frac{\sin \dfrac{N \Delta \varphi_{0}}{2}}{\sin \dfrac{\Delta \varphi_{0}}{2}}}, \qand  I = I_{1} \frac{\sin^{2} \dfrac{N \Delta \varphi_{0}}{2}}{\sin^{2} \dfrac{\Delta \varphi_{0}}{2}}.
\end{equation*}
where $I_{1} = A_{1}^{2}$ is the intensity of the vibration set up at point $M$ by each of the $N$ interfering waves separately.

\begin{figure}%[-3cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-32/fig-32-09.pdf}
\sidecaption{The nature of the dependence of $I/I_{1}$ on $\Delta \varphi_{0}$. \label{fig-32-09}}
\end{figure}

\subsection{}\label{32.4.3} The \redem{principal maxima in the interference} of $N$ waves \ssect{32.4.2} are at points $M$ that comply with the condition: $\Delta (\varphi_{0} = \pm 2n \pi$ where $n = 0, \,1,\, 2, \dots{}$ is the \redem{order of the principal maximum}. The amplitude and intensity of vibration at the principal maxima are:
\begin{equation*}%
A_{\text{max}} =  NA_{1} \qand I_{\text{max}} = N^{2}I_{1}.
\end{equation*}
\redem{Interference minima} $(A = 0)$ comply with the condition:
\begin{equation*}%
\Delta \varphi_{0} = \pm 2\pi \frac{p}{N}, 
\end{equation*}
where $p$ may be any positive whole number except a multiple of $N$.

The nature of the dependence of $I/I_{1}$ on $\Delta \varphi_{0}$ \ssect{32.4.2} is shown in \figr{fig-32-09}. Between each pair of adjacent interference minima there is a maximum, either a principal or a subordinate maximum. At high $N$ values the intensities of the subordinate maxima
are negligibly small compared to that of the principal maxima.
The values $\Delta \varphi_{0} = \pm (2\pi n \pm 2\pi/N)$ correspond to two minima that bound a principal maximum of the $n$-th. order. Therefore, the ``width'' of the principal maximum, equal to $4\pi/N$, is inversely proportional to the number N of interfering waves, and its intensity is proportional to $A^{2}$. 

The  fact that the interference pattern changes in such a manner as $N$ is varied fully complies with the law of 
conservation of energy. The total energy of vibration at all points of the screen on which the interference pattern is observed is proportional to $N$.

\subsection{}\label{32.4.4} If the number $N$ of interfering /
waves \ssect{32.4.2} is increased indefinitely, and their amplitudes $A_{1}$ and phase differences $\Delta \varphi_{0}$ are correspondingly reduced so that $N A_{1}$ and $N \Delta \varphi_{0}$ remain finite quantities equal to $A_{0}$ and $\Delta \varphi_{0}$, then, in the limit, the vector diagram (\figr{fig-32-08}) assumes the form illustrated in \figr{fig- 32-10}. Vector $A$, the amplitude of the resultant vibration,
closes the arc $BC$ of a circle. The length of this arc is equal to $A_{0}$ and its central angle $\angle BOC = \Delta \varphi$. Therefore, the radius of the circle $\overline{OB} = A_{0}/\Delta  \varphi$, and the amplitude $A$ and intensity $I$ of the resultant vibration equal
\begin{equation*}%
A = A_{0} \abs{\frac{\sin \dfrac{\Delta \varphi}{2}}{ \dfrac{\Delta \varphi}{2}}}, \qand  I = I_{0} \frac{\sin^{2} \dfrac{ \Delta \varphi}{2}}{ \left( \dfrac{\Delta \varphi}{2} \right)^{2}}.
\end{equation*}
where $I_{0} = A_{0}^{2}$.

\redem{Interference minima} are at points of the interference pattern for which
\begin{equation*}%
\Delta \varphi = \pm 2m \pi,
\end{equation*}
where $m = 1,\, 2, \, \dots{}$.
\redem{Interference maxima} are at points for which \begin{equation*}%
\Delta \varphi = \pm 2 k_{m} \pi,
\end{equation*}
where $m = 1,\, 2, \, \dots{}$ is the \redem{order of the maximum}. The values of factor $k_{m}$ are determined from the transcendental equation $\tan k_{m} \pi = k_{m} \pi$. For the central, zero-order maximum $k_{0} = 0$ and $\Delta \varphi_{0}$. The amplitude and intensity of vibration at a zero-order maximum are equal to $A_{0}$ and $I_{0}$. For all other maxima $(m \geqslant 1)$ it can be assumed approximately that 
\begin{equation*}%
k_{m} = \frac{(2m + l)}{2} \qand \Delta \varphi = \pm (2m + 1) \pi.
\end{equation*}
Accordingly, the ratio of the intensities for the $m$-th and zero-order maxima equals
\begin{equation*}%
\frac{I_{m}}{I_{0}} = \frac{4}{(2m + 1)^{2}\pi^{2}}.
\end{equation*}

\begin{table}
\centering
\begin{tabular}{p{2cm}ccccc}
\toprule
\textbf{Order of the maximum} & \textbf{0} & \textbf{1} & \textbf{2} & \textbf{3} & \textbf{4}\\
\midrule
$I_{m}/ I_{0}$ & 1 0.045 & 0.016 & 0.008 &   0.005 \\
\bottomrule
\end{tabular}
\sidecaption{Intensity of $m$-th and 0-th order maximum as a function of order. \label{tab-32-01}}
\end{table}
This ratio decreases rapidly with an increase of $m$ (\tabl{tab-32-01}). The shape of the curve showing the dependence of $I$ on $\Delta \varphi_{0}$ is
shown in \figr{fig-32-11}.


\begin{figure}%[-3cm]
%\centering
\includegraphics[width=0.8\textwidth]{figs/ch-32/fig-32-11.pdf}
\sidecaption{The dependence of $I$ on $\Delta \varphi_{0}$. \label{fig-32-11}}
\end{figure}


